package com.ibuildapp.FacebookPlugin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import com.appbuilder.sdk.android.*;
import com.appbuilder.sdk.android.authorization.Authorization;
import com.appbuilder.sdk.android.authorization.FacebookAuthorizationActivity;

public class ActivitySharing extends AppBuilderModuleMain {

    public static final int REQUEST_CODE = 10813;
    public static final String EXTRA_URL = "EXTRA_URL";

    private ProgressDialog progressDialog;

    @Override
    public void create() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        hideTopBar();
        setContentView(R.layout.activity_sharing);

        final String url = getIntent().getStringExtra(EXTRA_URL);
        final EditText message = (EditText)findViewById(R.id.message);

        ((TextView)findViewById(R.id.label)).setText("Facebook");

        findViewById(R.id.home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        findViewById(R.id.post).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if(!com.appbuilder.sdk.android.Utils.networkAvailable(ActivitySharing.this)) {
                            return;
                        }


                        try {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showProgressDialog();
                                }
                            });

                            final boolean result = FacebookAuthorizationActivity.sharePost(
                                    Authorization.getAuthorizedUser(Authorization.AUTHORIZATION_TYPE_FACEBOOK).getAccessToken(),
                                    message.getText().toString(), url);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    setResult(result ? RESULT_OK : RESULT_CANCELED);
                                    hideProgressDialog();
                                    finish();
                                }
                            });
                        } catch(Exception exception) {
                            exception.printStackTrace();
                        }
                    }
                }).start();
            }
        });
    }

    private void showProgressDialog() {
        if (progressDialog == null || !progressDialog.isShowing()) {
            progressDialog = ProgressDialog.show(this, null, getString(R.string.progress));
            progressDialog.setCancelable(true);
        }
    }

    private void hideProgressDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

}
