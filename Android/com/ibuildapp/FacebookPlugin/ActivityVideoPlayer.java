package com.ibuildapp.FacebookPlugin;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;
import com.appbuilder.sdk.android.authorization.Authorization;
import com.appbuilder.sdk.android.authorization.FacebookAuthorizationActivity;
import com.ibuildapp.FacebookPlugin.core.Plugin;
import com.ibuildapp.FacebookPlugin.core.StaticData;
import com.ibuildapp.FacebookPlugin.core.Utils;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class ActivityVideoPlayer extends ActivityParent {

    public static final String EXTRA_VIDEO_URL = "EXTRA_VIDEO_URL";
    public static final String EXTRA_VIDEO_ID = "EXTRA_VIDEO_ID";

    private static final int FACEBOOK_LIKE_AUTH = 10137;
    private static final int FACEBOOK_LIKE_SHARE = 10138;

    private Handler handler;
    private Resources resources;

    private VideoView videoView;
    private View actions;
    private View like;
    private Dialog dialog;

    private String url;
    private String id;
    private boolean loadComplete;
    private boolean isLiked;

    @Override
    public void destroy() {
        super.destroy();

        if(videoView != null)
            videoView.stopPlayback();
    }

    @Override
    protected void backend() {
        url = getIntent().getStringExtra(EXTRA_VIDEO_URL);
        id = getIntent().getStringExtra(EXTRA_VIDEO_ID);
        resources = getResources();
        handler = new Handler();
    }

    @Override
    protected void UI() {
        setContentView(R.layout.activity_video_player);

        View three_dots = LayoutInflater.from(this).inflate(R.layout.activity_video_player_three_dots, (ViewGroup)getTopBar(), false);

        actions = findViewById(R.id.actions);
        actions.setVisibility(View.GONE);

        like = actions.findViewById(R.id.like);
        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Authorization.isAuthorized(Authorization.AUTHORIZATION_TYPE_FACEBOOK)) {
                    if(!isLiked)
                        like();
                } else
                    Authorization.authorize(ActivityVideoPlayer.this, FACEBOOK_LIKE_AUTH, Authorization.AUTHORIZATION_TYPE_FACEBOOK);
            }
        });

        actions.findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Authorization.isAuthorized(Authorization.AUTHORIZATION_TYPE_FACEBOOK))
                    share();
                else
                    Authorization.authorize(ActivityVideoPlayer.this, FACEBOOK_LIKE_SHARE, Authorization.AUTHORIZATION_TYPE_FACEBOOK);
            }
        });
        setTopBarTitle(resources.getString(R.string.video));
        setTopBarBackgroundColor(StaticData.getXmlParsedData().getColorSkin().getColor1());
        setTopBarLeftButtonTextAndColor(getResources().getString(R.string.back), getResources().getColor(android.R.color.black), true, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        setTopBarRightVeiw(three_dots, false, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actions.setVisibility(actions.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            }
        });
        setTopBarTitleColor(getResources().getColor(android.R.color.black));

        dialog = new AlertDialog.Builder(this)
                .setTitle(R.string.video_player_dialog_title)
                .setMessage(R.string.video_player_dialog_message)
                .setPositiveButton(R.string.video_player_dialog_positive_button_text, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        finish();
                    }
                })
                .setCancelable(false)
                .create();

        videoView = (VideoView)findViewById(R.id.video);
        videoView.setZOrderOnTop(true);
        videoView.setMediaController(new MediaController(this));
        videoView.setVideoURI(Uri.parse(url.replace("https://", "http://")));
        videoView.getHolder().setFormat(PixelFormat.TRANSPARENT);
        videoView.setZOrderMediaOverlay(true);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                if (loadComplete)
                    progressHide();
                else
                    loadComplete = true;

                renderPreview();
            }
        });
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                if(i == 1 && (i1 == -2147483648 || i1 == 9100|| i1 == -22)) {
                    progressHide();
                    dialog.show();

                    return loadComplete = true;
                }

                return false;
            }
        });
        videoView.requestFocus();

    }

    @Override
    protected void content(boolean cancelable) {
        progressShow(cancelable);

        Plugin.INSTANCE.addTask(new Runnable() {
            @Override
            public void run() {
                final ArrayList<String> likes = new ArrayList<String>();

                try {
                    if (Authorization.isAuthorized(Authorization.AUTHORIZATION_TYPE_FACEBOOK))
                        likes.addAll(Plugin.INSTANCE.getInFuture(new Callable<Plugin.Result<ArrayList<String>>>() {
                            @Override
                            public Plugin.Result<ArrayList<String>> call() throws Exception {
                                return new Plugin.Result<ArrayList<String>>() {
                                    @Override
                                    public ArrayList<String> getResult() throws Exception {
                                        return FacebookAuthorizationActivity.getUserOgLikes();
                                    }
                                };
                            }
                        }).getResult());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            isLiked = likes.contains(url);
                            setAlpha();

                            if (loadComplete)
                                progressHide();
                            else
                                loadComplete = true;
                        }
                    });
                } catch (Exception exception) {
                    exception.printStackTrace();

                    isLiked = likes.contains(url);
                    setAlpha();

                    if (loadComplete)
                        progressHide();
                    else
                        loadComplete = true;
                }
            }
        });
    }

    @Override
    public void resume() {
        renderPreview();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            hideTopBar();
            actions.setVisibility(View.GONE);
        } else {
            visibleTopBar();
        }

        renderPreview();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            if(requestCode == FACEBOOK_LIKE_AUTH)
                like();
            else if(requestCode == FACEBOOK_LIKE_SHARE)
                share();
        }

        if(requestCode == ActivitySharing.REQUEST_CODE)
            Toast.makeText(this, resources.getString(resultCode == RESULT_OK ? R.string.shared_on_facebook : R.string.not_shared_on_facebook), Toast.LENGTH_LONG).show();

        renderPreview();
    }

    private void renderPreview() {
        if(!videoView.isPlaying())
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    videoView.start();
                    videoView.seekTo(videoView.getCurrentPosition() - 200 < 0 ? 0 : videoView.getCurrentPosition() - 200);
                    videoView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            videoView.pause();
                        }
                    }, 200);
                }
            }, 200);
    }

    private void setAlpha() {
        float alpha = isLiked ? 0.4f : 1f;

        if (Build.VERSION.SDK_INT < 11)
            like.startAnimation(new AlphaAnimation(alpha, alpha) {{
                setDuration(0);
                setFillAfter(true);
            }});
        else
            like.setAlpha(alpha);
    }

    private void like() {
        Plugin.INSTANCE.addTask(new Runnable() {
            @Override
            public void run() {
                try {
                    final Boolean[] result = Plugin.INSTANCE.getInFuture(new Callable<Plugin.Result<Boolean[]>>() {
                        @Override
                        public Plugin.Result<Boolean[]> call() throws Exception {
                            return new Plugin.Result<Boolean[]>() {
                                @Override
                                public Boolean[] getResult() throws Exception {
                                    boolean isLiked = false;
                                    boolean wasLiked = false;

                                    try {
                                        isLiked = Utils.complexSourceLike(id, url);
                                    } catch (FacebookAuthorizationActivity.FacebookAlreadyLiked exception) {
                                        wasLiked = isLiked = true;
                                    }

                                    return new Boolean[]{isLiked, wasLiked};
                                }
                            };
                        }
                    }).getResult();

                    isLiked = result[0];

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setAlpha();

                            if (isLiked && !result[1])
                                Toast.makeText(ActivityVideoPlayer.this, R.string.facebook_alert_liked, Toast.LENGTH_LONG).show();
                        }
                    });
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });
    }

    private void share() {
        Intent intent = new Intent(this, ActivitySharing.class);
        intent.putExtra(ActivitySharing.EXTRA_URL, url);
        startActivityForResult(intent, ActivitySharing.REQUEST_CODE);
    }

}
