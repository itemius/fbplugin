package com.ibuildapp.FacebookPlugin;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import com.appbuilder.sdk.android.AppBuilderModuleMain;
import com.ibuildapp.FacebookPlugin.view.TransparentProgressDialog;

public abstract class ActivityParent extends AppBuilderModuleMain {

    private Dialog progress;

    @Override
    public void create() {
        backend();
        UI();
        content(true);
    }

    @Override
    public void destroy() {
        progressHide();
        progress = null;
    }

    protected void progressShow(boolean cancelable) {
        if(progress == null || progress instanceof ProgressDialog && !cancelable) {
            if(cancelable) {
                progress = ProgressDialog.show(this, null, getString(R.string.progress), true);
                progress.setCancelable(true);
                progress.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        onBackPressed();
                    }
                });
            } else {
                progress = new TransparentProgressDialog(this);
                progress.setCancelable(false);
                progress.show();
            }

            return;
        }

        if(progress.isShowing())
            return;

        progress.show();
    }

    protected void progressHide() {
        if(progress != null)
            progress.dismiss();
    }

    abstract protected void backend();
    abstract protected void UI();
    abstract protected void content(boolean cancelable);

}
