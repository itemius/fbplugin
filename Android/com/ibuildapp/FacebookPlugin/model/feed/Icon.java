package com.ibuildapp.FacebookPlugin.model.feed;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ibuildapp.FacebookPlugin.model.Paging;

public class Icon {

    @Expose
    private Data data;
    @Expose
    private Paging paging;

    /**
     *
     * @return
     * The data
     */
    public Data getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(Data data) {
        this.data = data;
    }

    /**
     *
     * @return
     * The paging
     */
    public Paging getPaging() {
        return paging;
    }

    /**
     *
     * @param paging
     * The paging
     */
    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    public class Data {

        @SerializedName("is_silhouette")
        @Expose
        private Boolean isSilhouette;
        @Expose
        private String url;

        /**
         *
         * @return
         * The isSilhouette
         */
        public Boolean getIsSilhouette() {
            return isSilhouette;
        }

        /**
         *
         * @param isSilhouette
         * The is_silhouette
         */
        public void setIsSilhouette(Boolean isSilhouette) {
            this.isSilhouette = isSilhouette;
        }

        /**
         *
         * @return
         * The url
         */
        public String getUrl() {
            return url;
        }

        /**
         *
         * @param url
         * The url
         */
        public void setUrl(String url) {
            this.url = url;
        }

    }

}
