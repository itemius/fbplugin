package com.ibuildapp.FacebookPlugin.model.feed;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ibuildapp.FacebookPlugin.model.Paging;

import java.util.ArrayList;
import java.util.List;

public class Replies {

    @Expose
    private List<Item> data = new ArrayList<Item>();
    @Expose
    private Paging paging;

    public List<Item> getData() {
        return data;
    }

    public void setData(List<Item> data) {
        this.data = data;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    public static class Item {

        @Expose
        private String id;
        @Expose
        private From from;
        @Expose
        private String message;
        @SerializedName("can_remove")
        @Expose
        private Boolean canRemove;

        @SerializedName("comment_count")
        @Expose
        private Integer commentsCount;

        @SerializedName("created_time")
        @Expose
        private String createdTime;

        @SerializedName("like_count")
        @Expose
        private Integer likeCount;

        @SerializedName("user_likes")
        @Expose
        private Boolean userLikes;

        /**
         *
         * @return
         *     The id
         */
        public String getId() {
            return id;
        }

        /**
         *
         * @param id
         *     The id
         */
        public void setId(String id) {
            this.id = id;
        }

        /**
         *
         * @return
         *     The from
         */
        public From getFrom() {
            return from;
        }

        /**
         *
         * @param from
         *     The from
         */
        public void setFrom(From from) {
            this.from = from;
        }

        /**
         *
         * @return
         *     The message
         */
        public String getMessage() {
            return message;
        }

        /**
         *
         * @param message
         *     The message
         */
        public void setMessage(String message) {
            this.message = message;
        }

        /**
         *
         * @return
         *     The canRemove
         */
        public Boolean getCanRemove() {
            return canRemove;
        }

        /**
         *
         * @param canRemove
         *     The can_remove
         */
        public void setCanRemove(Boolean canRemove) {
            this.canRemove = canRemove;
        }

        /**
         *
         * @return
         *     The createdTime
         */
        public String getCreatedTime() {
            return createdTime;
        }

        /**
         *
         * @param createdTime
         *     The created_time
         */
        public void setCreatedTime(String createdTime) {
            this.createdTime = createdTime;
        }

        /**
         *
         * @return
         *     The likeCount
         */
        public Integer getLikeCount() {
            return likeCount;
        }

        /**
         *
         * @param likeCount
         *     The like_count
         */
        public void setLikeCount(Integer likeCount) {
            this.likeCount = likeCount;
        }

        /**
         *
         * @return
         *     The userLikes
         */
        public Boolean getUserLikes() {
            return userLikes;
        }

        /**
         *
         * @param userLikes
         *     The user_likes
         */
        public void setUserLikes(Boolean userLikes) {
            this.userLikes = userLikes;
        }

        public Integer getCommentsCount() {
            return commentsCount;
        }

        public void setCommentsCount(Integer commentsCount) {
            this.commentsCount = commentsCount;
        }

        public static class From {

            @Expose
            private String id;
            @Expose
            private String name;

            /**
             *
             * @return
             *     The id
             */
            public String getId() {
                return id;
            }

            /**
             *
             * @param id
             *     The id
             */
            public void setId(String id) {
                this.id = id;
            }

            /**
             *
             * @return
             *     The name
             */
            public String getName() {
                return name;
            }

            /**
             *
             * @param name
             *     The name
             */
            public void setName(String name) {
                this.name = name;
            }

        }
    }
}
