package com.ibuildapp.FacebookPlugin.model.feed;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class Picture {

    @Expose
    private List<Image> images = new ArrayList<Image>();

    /**
     *
     * @return
     * The images
     */
    public List<Image> getImages() {
        return images;
    }

    /**
     *
     * @param images
     * The images
     */
    public void setImages(List<Image> images) {
        this.images = images;
    }

    public class Image {

        @Expose
        private Integer height;
        @Expose
        private String source;
        @Expose
        private Integer width;

        /**
         *
         * @return
         * The height
         */
        public Integer getHeight() {
            return height;
        }

        /**
         *
         * @param height
         * The height
         */
        public void setHeight(Integer height) {
            this.height = height;
        }

        /**
         *
         * @return
         * The source
         */
        public String getSource() {
            return source;
        }

        /**
         *
         * @param source
         * The source
         */
        public void setSource(String source) {
            this.source = source;
        }

        /**
         *
         * @return
         * The width
         */
        public Integer getWidth() {
            return width;
        }

        /**
         *
         * @param width
         * The width
         */
        public void setWidth(Integer width) {
            this.width = width;
        }

    }

}
