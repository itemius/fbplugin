package com.ibuildapp.FacebookPlugin.model.feed;

import com.google.gson.annotations.Expose;
import com.ibuildapp.FacebookPlugin.model.Paging;

import java.util.ArrayList;
import java.util.List;

public class Feed {

    @Expose
    private List<FeedItem> data = new ArrayList<FeedItem>();
    @Expose
    private Paging paging;

    public String getIds() {
        StringBuilder builder = new StringBuilder();
        for (FeedItem item:data)
            builder.append(item.getId()+",");
        builder.deleteCharAt(builder.length()-1);
        return builder.toString();
    }
    /**
     *
     * @return
     *     The data
     */
    public List<FeedItem> getData() {
        return data;
    }

    /**
     *
     * @param data
     *     The data
     */
    public void setData(List<FeedItem> data) {
        this.data = data;
    }

    /**
     *
     * @return
     *     The paging
     */
    public Paging getPaging() {
        return paging;
    }

    /**
     *
     * @param paging
     *     The paging
     */
    public void setPaging(Paging paging) {
        this.paging = paging;
    }

}
