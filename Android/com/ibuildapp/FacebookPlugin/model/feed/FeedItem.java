package com.ibuildapp.FacebookPlugin.model.feed;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class FeedItem {

    @Expose
    private String id;
    @Expose
    private From from;
    @Expose
    private String message;
    @Expose
    private String picture;
    @Expose
    private String link;
    @Expose
    private String source;
    @Expose
    private String name;
    @Expose
    private String caption;
    @Expose
    private String description;
    @Expose
    private String icon;
    @Expose
    private List<Action> actions = new ArrayList<Action>();
    @Expose
    private Privacy privacy;
    @Expose
    private String type;
    @SerializedName("status_type")
    @Expose
    private String statusType;
    @SerializedName("created_time")
    @Expose
    private String createdTime;
    @SerializedName("updated_time")
    @Expose
    private String updatedTime;
    @SerializedName("is_hidden")
    @Expose
    private Boolean isHidden;
    @SerializedName("is_expired")
    @Expose
    private Boolean isExpired;
    @Expose
    private To to;
    @SerializedName("object_id")
    @Expose
    private String objectId;
    @Expose
    private Shares shares;
    @Expose
    private Likes likes;
    @Expose
    private Comments comments;
    private long time;
    private String handledTime;
    private String bigPictureUrl;

    private List<String> attachedImages = new ArrayList<String>();
    private boolean liked;
    private int likesCount;
    private int commentsCount;

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The from
     */
    public From getFrom() {
        return from;
    }

    /**
     * 
     * @param from
     *     The from
     */
    public void setFrom(From from) {
        this.from = from;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The picture
     */
    public String getPicture() {
        return picture;
    }

    /**
     * 
     * @param picture
     *     The picture
     */
    public void setPicture(String picture) {
        this.picture = picture;
    }

    /**
     * 
     * @return
     *     The link
     */
    public String getLink() {
        return link;
    }

    /**
     * 
     * @param link
     *     The link
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     *
     * @return
     *     The source
     */
    public String getSource() {
        return source;
    }

    /**
     *
     * @param source
     *     The source
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The caption
     */
    public String getCaption() {
        return caption;
    }

    /**
     * 
     * @param caption
     *     The caption
     */
    public void setCaption(String caption) {
        this.caption = caption;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * 
     * @param icon
     *     The icon
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * 
     * @return
     *     The actions
     */
    public List<Action> getActions() {
        return actions;
    }

    /**
     * 
     * @param actions
     *     The actions
     */
    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    /**
     * 
     * @return
     *     The privacy
     */
    public Privacy getPrivacy() {
        return privacy;
    }

    /**
     * 
     * @param privacy
     *     The privacy
     */
    public void setPrivacy(Privacy privacy) {
        this.privacy = privacy;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The statusType
     */
    public String getStatusType() {
        return statusType;
    }

    /**
     * 
     * @param statusType
     *     The status_type
     */
    public void setStatusType(String statusType) {
        this.statusType = statusType;
    }

    /**
     * 
     * @return
     *     The createdTime
     */
    public String getCreatedTime() {
        return createdTime;
    }

    /**
     * 
     * @param createdTime
     *     The created_time
     */
    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 
     * @return
     *     The updatedTime
     */
    public String getUpdatedTime() {
        return updatedTime;
    }

    /**
     * 
     * @param updatedTime
     *     The updated_time
     */
    public void setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
    }

    /**
     * 
     * @return
     *     The isHidden
     */
    public Boolean getIsHidden() {
        return isHidden;
    }

    /**
     * 
     * @param isHidden
     *     The is_hidden
     */
    public void setIsHidden(Boolean isHidden) {
        this.isHidden = isHidden;
    }

    /**
     * 
     * @return
     *     The isExpired
     */
    public Boolean getIsExpired() {
        return isExpired;
    }

    /**
     * 
     * @param isExpired
     *     The is_expired
     */
    public void setIsExpired(Boolean isExpired) {
        this.isExpired = isExpired;
    }

    /**
     * 
     * @return
     *     The to
     */
    public To getTo() {
        return to;
    }

    /**
     * 
     * @param to
     *     The to
     */
    public void setTo(To to) {
        this.to = to;
    }

    /**
     * 
     * @return
     *     The objectId
     */
    public String getObjectId() {
        return objectId;
    }

    /**
     * 
     * @param objectId
     *     The object_id
     */
    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    /**
     * 
     * @return
     *     The shares
     */
    public Shares getShares() {
        return shares;
    }

    /**
     * 
     * @param shares
     *     The shares
     */
    public void setShares(Shares shares) {
        this.shares = shares;
    }

    /**
     * 
     * @return
     *     The likes
     */
    public Likes getLikes() {
        return likes;
    }

    /**
     * 
     * @param likes
     *     The likes
     */
    public void setLikes(Likes likes) {
        this.likes = likes;
    }

    /**
     * 
     * @return
     *     The comments
     */
    public Comments getComments() {
        return comments;
    }

    /**
     * 
     * @param comments
     *     The comments
     */
    public void setComments(Comments comments) {
        this.comments = comments;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        return object instanceof FeedItem && hashCode() == object.hashCode();
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getHandledTime() {
        return handledTime;
    }

    public void setHandledTime(String handledTime) {
        this.handledTime = handledTime;
    }

    public String getBigPictureUrl() {
        return bigPictureUrl;
    }

    public void setBigPictureUrl(String bigPictureUrl) {
        this.bigPictureUrl = bigPictureUrl;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public List<String> getAttachedImages() {
        return attachedImages;
    }

    public void setAttachedImages(List<String> attachedImages) {
        this.attachedImages = attachedImages;
    }

    public static class Action {

        @Expose
        private String name;
        @Expose
        private String link;

        /**
         *
         * @return
         *     The name
         */
        public String getName() {
            return name;
        }

        /**
         *
         * @param name
         *     The name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         *
         * @return
         *     The link
         */
        public String getLink() {
            return link;
        }

        /**
         *
         * @param link
         *     The link
         */
        public void setLink(String link) {
            this.link = link;
        }

    }

    public static class From {

        @Expose
        private String category;
        @Expose
        private String name;
        @Expose
        private String id;

        /**
         *
         * @return
         *     The category
         */
        public String getCategory() {
            return category;
        }

        /**
         *
         * @param category
         *     The category
         */
        public void setCategory(String category) {
            this.category = category;
        }

        /**
         *
         * @return
         *     The name
         */
        public String getName() {
            return name;
        }

        /**
         *
         * @param name
         *     The name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         *
         * @return
         *     The id
         */
        public String getId() {
            return id;
        }

        /**
         *
         * @param id
         *     The id
         */
        public void setId(String id) {
            this.id = id;
        }

    }

    public static class Privacy {

        @Expose
        private String value;
        @Expose
        private String description;
        @Expose
        private String friends;
        @Expose
        private String allow;
        @Expose
        private String deny;

        /**
         *
         * @return
         *     The value
         */
        public String getValue() {
            return value;
        }

        /**
         *
         * @param value
         *     The value
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         *
         * @return
         *     The description
         */
        public String getDescription() {
            return description;
        }

        /**
         *
         * @param description
         *     The description
         */
        public void setDescription(String description) {
            this.description = description;
        }

        /**
         *
         * @return
         *     The friends
         */
        public String getFriends() {
            return friends;
        }

        /**
         *
         * @param friends
         *     The friends
         */
        public void setFriends(String friends) {
            this.friends = friends;
        }

        /**
         *
         * @return
         *     The allow
         */
        public String getAllow() {
            return allow;
        }

        /**
         *
         * @param allow
         *     The allow
         */
        public void setAllow(String allow) {
            this.allow = allow;
        }

        /**
         *
         * @return
         *     The deny
         */
        public String getDeny() {
            return deny;
        }

        /**
         *
         * @param deny
         *     The deny
         */
        public void setDeny(String deny) {
            this.deny = deny;
        }

    }

    public static class Shares {

        @Expose
        private Integer count;

        /**
         *
         * @return
         *     The count
         */
        public Integer getCount() {
            return count;
        }

        /**
         *
         * @param count
         *     The count
         */
        public void setCount(Integer count) {
            this.count = count;
        }

    }
}
