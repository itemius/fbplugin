package com.ibuildapp.FacebookPlugin.model.feed;

import com.google.gson.annotations.Expose;
import com.ibuildapp.FacebookPlugin.model.Paging;

import java.util.ArrayList;
import java.util.List;

public class Likes {

    @Expose
    private List<Item> data = new ArrayList<Item>();
    @Expose
    private Paging paging;

    /**
     * 
     * @return
     *     The data
     */
    public List<Item> getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The data
     */
    public void setData(List<Item> data) {
        this.data = data;
    }

    /**
     * 
     * @return
     *     The paging
     */
    public Paging getPaging() {
        return paging;
    }

    /**
     * 
     * @param paging
     *     The paging
     */
    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    public static class Item {

        @Expose
        private String id;
        @Expose
        private String name;

        /**
         *
         * @return
         *     The id
         */
        public String getId() {
            return id;
        }

        /**
         *
         * @param id
         *     The id
         */
        public void setId(String id) {
            this.id = id;
        }

        /**
         *
         * @return
         *     The name
         */
        public String getName() {
            return name;
        }

        /**
         *
         * @param name
         *     The name
         */
        public void setName(String name) {
            this.name = name;
        }

    }

}
