package com.ibuildapp.FacebookPlugin.model.feed;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PageInfo {

    @Expose
    private String id;
    @Expose
    private String about;
    @Expose
    private String awards;
    @SerializedName("can_post")
    @Expose
    private Boolean canPost;
    @Expose
    private String category;
    @Expose
    private Integer checkins;
    @SerializedName("company_overview")
    @Expose
    private String companyOverview;
    @Expose
    private Cover cover;
    @Expose
    private String description;
    @Expose
    private String founded;
    @SerializedName("general_info")
    @Expose
    private String generalInfo;
    @SerializedName("has_added_app")
    @Expose
    private Boolean hasAddedApp;
    @SerializedName("is_community_page")
    @Expose
    private Boolean isCommunityPage;
    @SerializedName("is_published")
    @Expose
    private Boolean isPublished;
    @Expose
    private Integer likes;
    @Expose
    private String link;
    @Expose
    private String mission;
    @Expose
    private String name;
    @Expose
    private Parking parking;
    @SerializedName("release_date")
    @Expose
    private String releaseDate;
    @SerializedName("talking_about_count")
    @Expose
    private Integer talkingAboutCount;
    @Expose
    private String username;
    @Expose
    private String website;
    @SerializedName("were_here_count")
    @Expose
    private Integer wereHereCount;

    private int videosCount;
    private int photosCount;

    public int getVideosCount() {
        return videosCount;
    }

    public void setVideosCount(int videosCount) {
        this.videosCount = videosCount;
    }

    public int getPhotosCount() {
        return photosCount;
    }

    public void setPhotosCount(int photosCount) {
        this.photosCount = photosCount;
    }

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The about
     */
    public String getAbout() {
        return about;
    }

    /**
     *
     * @param about
     * The about
     */
    public void setAbout(String about) {
        this.about = about;
    }

    /**
     *
     * @return
     * The awards
     */
    public String getAwards() {
        return awards;
    }

    /**
     *
     * @param awards
     * The awards
     */
    public void setAwards(String awards) {
        this.awards = awards;
    }

    /**
     *
     * @return
     * The canPost
     */
    public Boolean getCanPost() {
        return canPost;
    }

    /**
     *
     * @param canPost
     * The can_post
     */
    public void setCanPost(Boolean canPost) {
        this.canPost = canPost;
    }

    /**
     *
     * @return
     * The category
     */
    public String getCategory() {
        return category;
    }

    /**
     *
     * @param category
     * The category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     *
     * @return
     * The checkins
     */
    public Integer getCheckins() {
        return checkins;
    }

    /**
     *
     * @param checkins
     * The checkins
     */
    public void setCheckins(Integer checkins) {
        this.checkins = checkins;
    }

    /**
     *
     * @return
     * The companyOverview
     */
    public String getCompanyOverview() {
        return companyOverview;
    }

    /**
     *
     * @param companyOverview
     * The company_overview
     */
    public void setCompanyOverview(String companyOverview) {
        this.companyOverview = companyOverview;
    }

    /**
     *
     * @return
     * The cover
     */
    public Cover getCover() {
        return cover;
    }

    /**
     *
     * @param cover
     * The cover
     */
    public void setCover(Cover cover) {
        this.cover = cover;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The founded
     */
    public String getFounded() {
        return founded;
    }

    /**
     *
     * @param founded
     * The founded
     */
    public void setFounded(String founded) {
        this.founded = founded;
    }

    /**
     *
     * @return
     * The generalInfo
     */
    public String getGeneralInfo() {
        return generalInfo;
    }

    /**
     *
     * @param generalInfo
     * The general_info
     */
    public void setGeneralInfo(String generalInfo) {
        this.generalInfo = generalInfo;
    }

    /**
     *
     * @return
     * The hasAddedApp
     */
    public Boolean getHasAddedApp() {
        return hasAddedApp;
    }

    /**
     *
     * @param hasAddedApp
     * The has_added_app
     */
    public void setHasAddedApp(Boolean hasAddedApp) {
        this.hasAddedApp = hasAddedApp;
    }

    /**
     *
     * @return
     * The isCommunityPage
     */
    public Boolean getIsCommunityPage() {
        return isCommunityPage;
    }

    /**
     *
     * @param isCommunityPage
     * The is_community_page
     */
    public void setIsCommunityPage(Boolean isCommunityPage) {
        this.isCommunityPage = isCommunityPage;
    }

    /**
     *
     * @return
     * The isPublished
     */
    public Boolean getIsPublished() {
        return isPublished;
    }

    /**
     *
     * @param isPublished
     * The is_published
     */
    public void setIsPublished(Boolean isPublished) {
        this.isPublished = isPublished;
    }

    /**
     *
     * @return
     * The likes
     */
    public Integer getLikes() {
        return likes;
    }

    /**
     *
     * @param likes
     * The likes
     */
    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    /**
     *
     * @return
     * The link
     */
    public String getLink() {
        return link;
    }

    /**
     *
     * @param link
     * The link
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     *
     * @return
     * The mission
     */
    public String getMission() {
        return mission;
    }

    /**
     *
     * @param mission
     * The mission
     */
    public void setMission(String mission) {
        this.mission = mission;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The parking
     */
    public Parking getParking() {
        return parking;
    }

    /**
     *
     * @param parking
     * The parking
     */
    public void setParking(Parking parking) {
        this.parking = parking;
    }

    /**
     *
     * @return
     * The releaseDate
     */
    public String getReleaseDate() {
        return releaseDate;
    }

    /**
     *
     * @param releaseDate
     * The release_date
     */
    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    /**
     *
     * @return
     * The talkingAboutCount
     */
    public Integer getTalkingAboutCount() {
        return talkingAboutCount;
    }

    /**
     *
     * @param talkingAboutCount
     * The talking_about_count
     */
    public void setTalkingAboutCount(Integer talkingAboutCount) {
        this.talkingAboutCount = talkingAboutCount;
    }

    /**
     *
     * @return
     * The username
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     * The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     * The website
     */
    public String getWebsite() {
        return website;
    }

    /**
     *
     * @param website
     * The website
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     *
     * @return
     * The wereHereCount
     */
    public Integer getWereHereCount() {
        return wereHereCount;
    }

    /**
     *
     * @param wereHereCount
     * The were_here_count
     */
    public void setWereHereCount(Integer wereHereCount) {
        this.wereHereCount = wereHereCount;
    }

    public class Cover {

        @SerializedName("cover_id")
        @Expose
        private String coverId;
        @SerializedName("offset_x")
        @Expose
        private Integer offsetX;
        @SerializedName("offset_y")
        @Expose
        private Integer offsetY;
        @Expose
        private String source;
        @Expose
        private String id;

        /**
         *
         * @return
         * The coverId
         */
        public String getCoverId() {
            return coverId;
        }

        /**
         *
         * @param coverId
         * The cover_id
         */
        public void setCoverId(String coverId) {
            this.coverId = coverId;
        }

        /**
         *
         * @return
         * The offsetX
         */
        public Integer getOffsetX() {
            return offsetX;
        }

        /**
         *
         * @param offsetX
         * The offset_x
         */
        public void setOffsetX(Integer offsetX) {
            this.offsetX = offsetX;
        }

        /**
         *
         * @return
         * The offsetY
         */
        public Integer getOffsetY() {
            return offsetY;
        }

        /**
         *
         * @param offsetY
         * The offset_y
         */
        public void setOffsetY(Integer offsetY) {
            this.offsetY = offsetY;
        }

        /**
         *
         * @return
         * The source
         */
        public String getSource() {
            return source;
        }

        /**
         *
         * @param source
         * The source
         */
        public void setSource(String source) {
            this.source = source;
        }

        /**
         *
         * @return
         * The id
         */
        public String getId() {
            return id;
        }

        /**
         *
         * @param id
         * The id
         */
        public void setId(String id) {
            this.id = id;
        }

    }

    public class Parking {

        @Expose
        private Integer lot;
        @Expose
        private Integer street;
        @Expose
        private Integer valet;

        /**
         *
         * @return
         * The lot
         */
        public Integer getLot() {
            return lot;
        }

        /**
         *
         * @param lot
         * The lot
         */
        public void setLot(Integer lot) {
            this.lot = lot;
        }

        /**
         *
         * @return
         * The street
         */
        public Integer getStreet() {
            return street;
        }

        /**
         *
         * @param street
         * The street
         */
        public void setStreet(Integer street) {
            this.street = street;
        }

        /**
         *
         * @return
         * The valet
         */
        public Integer getValet() {
            return valet;
        }

        /**
         *
         * @param valet
         * The valet
         */
        public void setValet(Integer valet) {
            this.valet = valet;
        }

    }

}