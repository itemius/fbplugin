package com.ibuildapp.FacebookPlugin.model.adapters;

import com.ibuildapp.FacebookPlugin.model.uploads.Upload;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for converting data from List<String> url to List<Upload>
 */
public class ImageUploadAdapter {
    public List<Upload> toUpload(List<String> urls)
    {
        List<Upload> uploads = new ArrayList<Upload>();
        for (String s :urls ) {
            Upload upload = new Upload();
            upload.setSource(s);
            uploads.add(upload);
        }
        return uploads;
    }
}
