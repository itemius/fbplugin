package com.ibuildapp.FacebookPlugin.model.tabmanager;

import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Class wrapper for the organization tab UI for two layouts
 */
public class TabManager {
    RelativeLayoutWrapper leftLayout;
    RelativeLayoutWrapper rightLayout;
    private Context context;

    private PhotoTabEventsListener listener;

    public TabManager(Context context, RelativeLayout left,TextView leftTextView, RelativeLayout right, TextView rightTextView) {
        this.context = context;
        this.leftLayout = new RelativeLayoutWrapper(context, left, leftTextView, true);
        this.rightLayout = new RelativeLayoutWrapper(context, right, rightTextView);

        this.leftLayout.setOnTabClickListener(new OnTabClickListener() {
            @Override
            public void onClick() {
                rightLayout.unselected();
                if(listener!=null)
                    listener.onLeftTabSelected();
            }
        });

        this.rightLayout.setOnTabClickListener(new OnTabClickListener() {
            @Override
            public void onClick() {
                leftLayout.unselected();
                if(listener!=null)
                    listener.onRightTabSelected();
            }
        });
    }

    /**
     * Method add EventListener
     */
    public void setEventsListener(PhotoTabEventsListener listener) {
        this.listener = listener;
    }

    /**
     * Method returns true if left tab selected
     */
    public boolean isLeftSelected() {
        return leftLayout.isSelected();
    }
}
