package com.ibuildapp.FacebookPlugin.model.tabmanager;

/**
 * Interface for Tab Manager
 */
public interface PhotoTabEventsListener {
    public void onLeftTabSelected();

    public void onRightTabSelected();
}
