package com.ibuildapp.FacebookPlugin.model.tabmanager;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ibuildapp.FacebookPlugin.R;

/**
 * Wrapper for relative layout, contains TextView
 */
public class RelativeLayoutWrapper {
    private boolean isSelected = false;
    private TextView textView;
    private RelativeLayout layout;
    private Context context;
    private OnTabClickListener listener;

    public RelativeLayoutWrapper(Context context, RelativeLayout layout, TextView textView) {
     this(context, layout, textView, false);
    }

    public RelativeLayoutWrapper(Context context, RelativeLayout layout, TextView textView, boolean b) {
        this.context = context;
        this.layout = layout;
        this.textView= textView;
        this.isSelected = b;

        if (isSelected)
            setSelected();
        else unselected();

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelected();
                listener.onClick();
            }
        });
    }

    /**
     * Method unselected current layout
     */
    public boolean unselected() {
        if (!isSelected)
            return false;

        isSelected = false;
        textView.setTextColor(context.getResources().getColor(R.color.header_layout_text_color));
        return true;
    }

    /**
     * Method select current layout
     */
    public boolean setSelected() {
        if (isSelected)
            return false;

        isSelected = true;
        textView.setTextColor(context.getResources().getColor(R.color.blue));
        listener.onClick();
        return true;
    }

    /**
     * Method select current layout
     */
    public boolean isSelected() {
        return isSelected;
    }

    /**
     * Method add listener on tab select. Inner interface
     */
    public void setOnTabClickListener(OnTabClickListener listener) {
        this.listener = listener;
    }
}
