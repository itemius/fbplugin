package com.ibuildapp.FacebookPlugin.model.comments;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Entity to represent facebook comment count item
 */
public class CommentCountItem {
    @Expose
    private List<Object> data = new ArrayList<Object>();
    @Expose
    private Summary summary;

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }
}
