package com.ibuildapp.FacebookPlugin.model.comments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Entity to represent facebook Summary item
 */
public class Summary {
    @Expose
    private String order;
    @SerializedName("total_count")
    @Expose
    private Integer totalCount;

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }
}
