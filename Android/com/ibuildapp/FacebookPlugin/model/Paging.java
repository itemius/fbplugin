package com.ibuildapp.FacebookPlugin.model;

import com.google.gson.annotations.Expose;

public class Paging {

    @Expose
    private Cursors cursors;

    @Expose
    private String previous;
    @Expose
    private String next;

    /**
     *
     * @return
     *     The cursors
     */
    public Cursors getCursors() {
        return cursors;
    }

    /**
     *
     * @param cursors
     *     The cursors
     */
    public void setCursors(Cursors cursors) {
        this.cursors = cursors;
    }

    /**
     *
     * @return
     *     The previous
     */
    public String getPrevious() {
        return previous;
    }

    /**
     *
     * @param previous
     *     The previous
     */
    public void setPrevious(String previous) {
        this.previous = previous;
    }

    /**
     *
     * @return
     *     The next
     */
    public String getNext() {
        return next;
    }

    /**
     *
     * @param next
     *     The next
     */
    public void setNext(String next) {
        this.next = next;
    }

    public static class Cursors {

        @Expose
        private String after;
        @Expose
        private String before;

        /**
         *
         * @return
         *     The after
         */
        public String getAfter() {
            return after;
        }

        /**
         *
         * @param after
         *     The after
         */
        public void setAfter(String after) {
            this.after = after;
        }

        /**
         *
         * @return
         *     The before
         */
        public String getBefore() {
            return before;
        }

        /**
         *
         * @param before
         *     The before
         */
        public void setBefore(String before) {
            this.before = before;
        }

    }

}
