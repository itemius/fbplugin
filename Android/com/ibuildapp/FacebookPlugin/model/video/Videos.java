package com.ibuildapp.FacebookPlugin.model.video;

import com.google.gson.annotations.Expose;
import com.ibuildapp.FacebookPlugin.model.Paging;

import java.util.ArrayList;
import java.util.List;

public class Videos {

    @Expose
    private List<Video> data = new ArrayList<Video>();
    @Expose
    private Paging paging;

    /**
     *
     * @return
     * The data
     */
    public List<Video> getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(List<Video> data) {
        this.data = data;
    }

    /**
     *
     * @return
     * The paging
     */
    public Paging getPaging() {
        return paging;
    }

    /**
     *
     * @param paging
     * The paging
     */
    public void setPaging(Paging paging) {
        this.paging = paging;
    }

}
