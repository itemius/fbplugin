package com.ibuildapp.FacebookPlugin.model.video;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ibuildapp.FacebookPlugin.model.feed.Comments;
import com.ibuildapp.FacebookPlugin.model.feed.FeedItem;

import java.util.ArrayList;
import java.util.List;

public class Video {

    @Expose
    private String id;
    @Expose
    private Comments comments;
    @SerializedName("created_time")
    @Expose
    private String createdTime;
    @Expose
    private String description;
    @SerializedName("embed_html")
    @Expose
    private String embedHtml;
    @Expose
    private List<Format> format = new ArrayList<Format>();
    @Expose
    private FeedItem.From from;
    @Expose
    private String icon;
    @Expose
    private String picture;
    @Expose
    private String source;
    @SerializedName("updated_time")
    @Expose
    private String updatedTime;
    @Expose
    private Boolean published;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The comments
     */
    public Comments getComments() {
        return comments;
    }

    /**
     *
     * @param comments
     * The comments
     */
    public void setComments(Comments comments) {
        this.comments = comments;
    }

    /**
     *
     * @return
     * The createdTime
     */
    public String getCreatedTime() {
        return createdTime;
    }

    /**
     *
     * @param createdTime
     * The created_time
     */
    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The embedHtml
     */
    public String getEmbedHtml() {
        return embedHtml;
    }

    /**
     *
     * @param embedHtml
     * The embed_html
     */
    public void setEmbedHtml(String embedHtml) {
        this.embedHtml = embedHtml;
    }

    /**
     *
     * @return
     * The format
     */
    public List<Format> getFormat() {
        return format;
    }

    /**
     *
     * @param format
     * The format
     */
    public void setFormat(List<Format> format) {
        this.format = format;
    }

    /**
     *
     * @return
     * The from
     */
    public FeedItem.From getFrom() {
        return from;
    }

    /**
     *
     * @param from
     * The from
     */
    public void setFrom(FeedItem.From from) {
        this.from = from;
    }

    /**
     *
     * @return
     * The icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     *
     * @param icon
     * The icon
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     *
     * @return
     * The picture
     */
    public String getPicture() {
        return picture;
    }

    /**
     *
     * @param picture
     * The picture
     */
    public void setPicture(String picture) {
        this.picture = picture;
    }

    /**
     *
     * @return
     * The source
     */
    public String getSource() {
        return source;
    }

    /**
     *
     * @param source
     * The source
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     *
     * @return
     * The updatedTime
     */
    public String getUpdatedTime() {
        return updatedTime;
    }

    /**
     *
     * @param updatedTime
     * The updated_time
     */
    public void setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
    }

    /**
     *
     * @return
     * The published
     */
    public Boolean getPublished() {
        return published;
    }

    /**
     *
     * @param published
     * The published
     */
    public void setPublished(Boolean published) {
        this.published = published;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        return object instanceof Video && hashCode() == object.hashCode();
    }

    public class Format {

        @SerializedName("embed_html")
        @Expose
        private String embedHtml;
        @Expose
        private String filter;
        @Expose
        private Integer height;
        @Expose
        private String picture;
        @Expose
        private Integer width;

        /**
         *
         * @return
         * The embedHtml
         */
        public String getEmbedHtml() {
            return embedHtml;
        }

        /**
         *
         * @param embedHtml
         * The embed_html
         */
        public void setEmbedHtml(String embedHtml) {
            this.embedHtml = embedHtml;
        }

        /**
         *
         * @return
         * The filter
         */
        public String getFilter() {
            return filter;
        }

        /**
         *
         * @param filter
         * The filter
         */
        public void setFilter(String filter) {
            this.filter = filter;
        }

        /**
         *
         * @return
         * The height
         */
        public Integer getHeight() {
            return height;
        }

        /**
         *
         * @param height
         * The height
         */
        public void setHeight(Integer height) {
            this.height = height;
        }

        /**
         *
         * @return
         * The picture
         */
        public String getPicture() {
            return picture;
        }

        /**
         *
         * @param picture
         * The picture
         */
        public void setPicture(String picture) {
            this.picture = picture;
        }

        /**
         *
         * @return
         * The width
         */
        public Integer getWidth() {
            return width;
        }

        /**
         *
         * @param width
         * The width
         */
        public void setWidth(Integer width) {
            this.width = width;
        }

    }

}
