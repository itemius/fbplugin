package com.ibuildapp.FacebookPlugin.model.photos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Entity to represent facebook Album item structure
 */
public class Album {
    @Expose
    String name;

    @SerializedName("cover_photo")
    @Expose
    private String coverPhoto;
    @Expose
    private Integer count;
    @Expose
    private String id;
    @SerializedName("created_time")
    @Expose
    private String createdTime;

    @Override
    public boolean equals(Object o) {
        return (o instanceof Album)&& ((Album) o).getId().equals(id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
    /**
     *
     * @return
     * The coverPhoto
     */
    public String getCoverPhoto() {
        return coverPhoto;
    }

    /**
     *
     * @param coverPhoto
     * The cover_photo
     */
    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    /**
     *
     * @return
     * The count
     */
    public Integer getCount() {
        return count==null?0:count;
    }

    /**
     *
     * @param count
     * The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The createdTime
     */
    public String getCreatedTime() {
        return createdTime;
    }

    /**
     *
     * @param createdTime
     * The created_time
     */
    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}