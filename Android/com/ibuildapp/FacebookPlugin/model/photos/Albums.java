package com.ibuildapp.FacebookPlugin.model.photos;

import com.google.gson.annotations.Expose;
import com.ibuildapp.FacebookPlugin.model.Paging;
import com.ibuildapp.FacebookPlugin.model.uploads.DataCollection;

import java.util.ArrayList;
import java.util.List;

/**
 * Entity to represent facebook Albums item structure
 */
public class Albums implements DataCollection<Album>{
    @Expose
    private List<Album> data = new ArrayList<Album>();

    @Expose
    private Paging paging;
    /**
     *
     * @return
     * The data
     */
    public List<Album> getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(List<Album> data) {
        this.data = data;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }
}
