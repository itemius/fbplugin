package com.ibuildapp.FacebookPlugin.model.replies;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ibuildapp.FacebookPlugin.model.feed.Likes;
import com.ibuildapp.FacebookPlugin.model.feed.To;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ReplyItem {

    @Expose
    private String id;

    @SerializedName("can_remove")
    @Expose
    private Boolean canRemove;
    @SerializedName("created_time")
    @Expose
    private String createdTime;

    @Expose
    private From from;

    @SerializedName("like_count")
    @Expose
    private Integer likeCount;
    @Expose
    private String message;
    @SerializedName("user_likes")
    @Expose
    private Boolean userLikes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getCanRemove() {
        return canRemove;
    }

    public void setCanRemove(Boolean canRemove) {
        this.canRemove = canRemove;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public From getFrom() {
        return from;
    }

    public void setFrom(From from) {
        this.from = from;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getUserLikes() {
        return userLikes;
    }

    public void setUserLikes(Boolean userLikes) {
        this.userLikes = userLikes;
    }

    public static Comparator<ReplyItem> comparator = new Comparator<ReplyItem>() {

        private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
        private  SimpleDateFormat dateFormat  = new SimpleDateFormat(DATE_FORMAT);
        @Override
        public int compare(ReplyItem replyItem, ReplyItem t1) {
            try {
                Date first = dateFormat.parse(replyItem.getCreatedTime());
                Date second = dateFormat.parse(t1.getCreatedTime());
                return first.compareTo(second);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            return 0;
        }
    };

}
