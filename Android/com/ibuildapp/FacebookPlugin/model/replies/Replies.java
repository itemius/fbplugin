package com.ibuildapp.FacebookPlugin.model.replies;

import com.ibuildapp.FacebookPlugin.model.Paging;

import java.util.ArrayList;

public class Replies {
    ArrayList<ReplyItem> data = new ArrayList<ReplyItem>();

    public ArrayList<ReplyItem> getData() {
        return data;
    }

    public void setData(ArrayList<ReplyItem> data) {
        this.data = data;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    Paging paging;
}
