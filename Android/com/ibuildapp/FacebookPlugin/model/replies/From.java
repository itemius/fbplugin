package com.ibuildapp.FacebookPlugin.model.replies;

import com.google.gson.annotations.Expose;

public class From {
    @Expose
    private String id;
    @Expose
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
