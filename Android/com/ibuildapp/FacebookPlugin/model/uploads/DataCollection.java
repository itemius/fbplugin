package com.ibuildapp.FacebookPlugin.model.uploads;

import com.ibuildapp.FacebookPlugin.model.Paging;

import java.util.List;

/**
 * Interface to access entity data
 */
public interface DataCollection<T> {
    List<T> getData();

    Paging getPaging();
}
