package com.ibuildapp.FacebookPlugin.model.uploads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class represents container for facebook entities data.
 */
public class DataContainer<T> {
    private List<T> rootCollection;
    private List<T> extendedCollection;
    private String nextUrl;

    public DataContainer(DataCollection<T> data){
        rootCollection = data.getData();
        extendedCollection = new ArrayList<T>();
        setNextUrl(data);
    }

    private final void setNextUrl(DataCollection<T> data){
        nextUrl = data.getPaging()==null ? null: data.getPaging().getNext();
    }

    /**
     * Method add new data at start of list
     */
    public boolean addNews(DataCollection<T>  data){
        List<T> diff = data.getData();
        diff.removeAll(rootCollection);

        if(diff.size()==0 )
            return false;

        diff.addAll(rootCollection);
        rootCollection = diff;
        return true;
    }

    /**
     * Method add data to end of list
     */
    public boolean addToEnd(DataCollection<T>  data) {
        extendedCollection.addAll(data.getData());
        setNextUrl(data);
        return true;
    }

    /**
     * Method returns result list
     */
    public List<T> getList(){
        List<T> result = new ArrayList<T>();
        result.addAll(rootCollection);
        result.addAll(extendedCollection);
        return result;
    }

    /**
     * Method return url for next partition of data
     */
    public String getNextUrl(){
        return nextUrl==null?"":nextUrl;
    }
}
