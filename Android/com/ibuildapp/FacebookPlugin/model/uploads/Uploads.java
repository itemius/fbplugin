package com.ibuildapp.FacebookPlugin.model.uploads;

import com.ibuildapp.FacebookPlugin.model.Paging;
import com.ibuildapp.FacebookPlugin.model.photos.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Entity to represent facebook Uploads item structure
 */
public class Uploads  implements DataCollection<Upload> {

    private List<Upload> data = new ArrayList<Upload>();

    private Paging paging;

    public List<Upload> getData() {
        return data;
    }

    public void setData(List<Upload> data) {
        this.data = data;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

}
