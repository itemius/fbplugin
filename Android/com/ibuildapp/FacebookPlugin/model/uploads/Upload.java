package com.ibuildapp.FacebookPlugin.model.uploads;

import com.google.gson.annotations.Expose;

import java.util.Comparator;

/**
 * Entity to represent facebook Upload item structure
 */
public class Upload{
    @Expose
    private String id;
    @Expose
    private String picture;
    @Expose
    private String source;

    private Boolean isLike = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Boolean getIsLike() {
        return isLike;
    }

    public void setIsLike(Boolean isLike) {
        this.isLike = isLike;
    }

    public static  class Comparator implements java.util.Comparator<Upload>{

        @Override
        public int compare(Upload upload, Upload t1) {
            return upload.getId().compareTo(t1.getId());
        }
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Upload)&& ((Upload) o).getId().equals(id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
