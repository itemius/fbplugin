package com.ibuildapp.FacebookPlugin.model.feedimages;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Entity to represent facebook attachment items structure
 */
public class Subattachments {

    @Expose
    private List<Datum_> data = new ArrayList<Datum_>();

    /**
     *
     * @return
     * The data
     */
    public List<Datum_> getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(List<Datum_> data) {
        this.data = data;
    }

}
