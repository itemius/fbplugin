package com.ibuildapp.FacebookPlugin.model.feedimages;

import com.google.gson.annotations.Expose;

/**
 * Entity to represent facebook attachment items structure
 */
public class Target {

    @Expose
    private String id;
    @Expose
    private String url;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

}