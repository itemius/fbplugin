package com.ibuildapp.FacebookPlugin.model.feedimages;

import com.google.gson.annotations.Expose;

/**
 * Entity to represent facebook attachment items structure
 */
public class Media {

    @Expose
    private Image image;

    /**
     *
     * @return
     * The image
     */
    public Image getImage() {
        return image;
    }

    /**
     *
     * @param image
     * The image
     */
    public void setImage(Image image) {
        this.image = image;
    }

}