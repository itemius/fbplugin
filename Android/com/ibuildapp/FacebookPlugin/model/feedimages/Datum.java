package com.ibuildapp.FacebookPlugin.model.feedimages;

import com.google.gson.annotations.Expose;

/**
 * Entity to represent facebook attachment items structure
 */
public class Datum {

    @Expose
    private String description;

    @Expose
    private Media media;

    @Expose
    private Subattachments subattachments;
    @Expose
    private Target_ target;
    @Expose
    private String title;
    @Expose
    private String type;
    @Expose
    private String url;

    /**
     *
     * @return
     * The subattachments
     */
    public Subattachments getSubattachments() {
        return subattachments;
    }

    /**
     *
     * @param subattachments
     * The subattachments
     */
    public void setSubattachments(Subattachments subattachments) {
        this.subattachments = subattachments;
    }

    /**
     *
     * @return
     * The target
     */
    public Target_ getTarget() {
        return target;
    }

    /**
     *
     * @param target
     * The target
     */
    public void setTarget(Target_ target) {
        this.target = target;
    }

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}