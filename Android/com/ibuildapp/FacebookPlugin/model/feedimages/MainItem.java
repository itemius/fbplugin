package com.ibuildapp.FacebookPlugin.model.feedimages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Entity to represent facebook attachment items structure
 */
public class MainItem {

    @Expose
    private String id;
    @SerializedName("created_time")
    @Expose
    private String createdTime;
    @Expose
    private Attachments attachments;


    public List<String> getImageUrls() {
        List<String> result = new ArrayList<String>();

        if (attachments == null)
            return result;

          for( Datum dat :attachments.getData())
          {
              Media attMedia = dat.getMedia();
              if (attMedia!=null)
                  result.add(attMedia.getImage().getSrc());

             Subattachments subattachments= dat.getSubattachments();

              if(subattachments==null)
                  continue;

              for (Datum_ dat_:subattachments.getData())
              {
                  if (dat_ == null)
                      continue;

                  Media media = dat_.getMedia();
                  if (media == null)
                      continue;

                  Image image = media.getImage();
                  if (image == null)
                      continue;

                  result.add(image.getSrc());
              }

          }
        return result;
    }
    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The createdTime
     */
    public String getCreatedTime() {
        return createdTime;
    }

    /**
     *
     * @param createdTime
     * The created_time
     */
    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    /**
     *
     * @return
     * The attachments
     */
    public Attachments getAttachments() {
        return attachments;
    }

    /**
     *
     * @param attachments
     * The attachments
     */
    public void setAttachments(Attachments attachments) {
        this.attachments = attachments;
    }

}
