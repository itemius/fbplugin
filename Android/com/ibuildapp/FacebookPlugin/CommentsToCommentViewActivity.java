package com.ibuildapp.FacebookPlugin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.*;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import com.appbuilder.sdk.android.AppBuilderModuleMain;
import com.appbuilder.sdk.android.Widget;
import com.appbuilder.sdk.android.authorization.Authorization;
import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.ibuildapp.FacebookPlugin.adapter.CommentsToCommentAdapter;
import com.ibuildapp.FacebookPlugin.core.Facebook;
import com.ibuildapp.FacebookPlugin.core.StaticData;
import com.ibuildapp.FacebookPlugin.core.Utils;
import com.ibuildapp.FacebookPlugin.model.replies.Replies;
import com.ibuildapp.FacebookPlugin.model.replies.ReplyItem;
import com.ibuildapp.FacebookPlugin.model_.JsonFeed;
import com.seppius.i18n.plurals.PluralResources;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class CommentsToCommentViewActivity extends AppBuilderModuleMain implements
        OnClickListener, OnItemClickListener{
    public static final int COMMENT_TO_COMMENT_SEND_MESSAGE = 10050;
    private static final String COMMENT_ID = "COMMENTS_ID";
    private static final String FACEBOOK_GRAPH_URL = "https://graph.facebook.com/v2.3/";
    private static final String FACEBOOK_GRAPH_URL_ACCESS_TOKEN = "access_token=";
    private static final String FACEBOOK_GRAPH_REPLIES = FACEBOOK_GRAPH_URL + COMMENT_ID + "/comments?order=reverse_chronological&" + FACEBOOK_GRAPH_URL_ACCESS_TOKEN;

    private static final String USER_ID = "USER_ID";
    private static final String FACEBOOK_GRAPH_COMMENTS_COUNT = FACEBOOK_GRAPH_URL + USER_ID + "/picture?type=normal&" + FACEBOOK_GRAPH_URL_ACCESS_TOKEN;

    private List<JsonFeed.Posts.Post.Comments.Comment> comments;
    private int commentPosition = 0;
    private ArrayList<ReplyItem> replies;
    private CommentsToCommentAdapter adapter;
    private Replies decoded;

    private final int INITIALIZATION_FAILED = 0;
    private final int LOADING_ABORTED = 1;
    private final int SHOW_PROGRESS_DIALOG = 2;
    private final int HIDE_PROGRESS_DIALOG = 3;
    private final int SHOW_COMMENTS_LIST = 4;
    private final int SHOW_NO_COMMENTS = 5;
    private final int NEED_INTERNET_CONNECTION = 6;
    private final int REFRESH_LIST = 7;
    private final int AUTH = 8;
    private int position = 0;
    private TextView date = null;
    private DateFormat dateFormat;
    private Widget widget = null;
    private View headerView = null;
    private ImageView thumbImageView = null;
    private TextView titleTextView = null;
    private TextView descriptionTextView = null;
    private TextView homeBtn = null;
    private PullToRefreshListView listView = null;
    private ProgressDialog progressDialog = null;
    private LinearLayout hasCommentsLayout = null;
    private LinearLayout hasntCommentsLayout = null;
    private LinearLayout headerLayout = null;
    TextView textcount = null;
    private Button postCommentButton = null;
    private ImageView postCommentButtonTop = null;

    /**
     * Handler for asynch methods
     */
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message message) {
            switch (message.what) {
                case INITIALIZATION_FAILED: {
                    Toast.makeText(CommentsToCommentViewActivity.this,
                            getResources().getString(R.string.facebook_alert_cannot_init),
                            Toast.LENGTH_LONG).show();
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            finish();
                        }
                    }, 2000);
                }
                break;
                case LOADING_ABORTED: {
                    closeActivity();
                }
                break;
                case SHOW_PROGRESS_DIALOG: {
                    showProgressDialog();
                }
                break;
                case HIDE_PROGRESS_DIALOG: {
                    hideProgressDialog();
                }
                break;
                case SHOW_COMMENTS_LIST: {
                    showCommentsList();
                }
                break;
                case SHOW_NO_COMMENTS: {
                    showNoComments();
                }
                break;
                case NEED_INTERNET_CONNECTION: {
                    Toast.makeText(CommentsToCommentViewActivity.this,
                            getResources().getString(
                                    R.string.facebook_alert_no_internet),
                            Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    };

    /**
     * Initialization of view visibilities, background colors, add listeners, load replies with facebook graph api, etc.
     */
    @Override
    public void create() {
        setContentView(R.layout.facebook_comments_main);
         dateFormat= Utils.getDateFormat(this);

        setTopBarTitle(getResources().getString(R.string.facebook_replies_capture));
        setTopBarBackgroundColor(StaticData.getXmlParsedData().getColorSkin().getColor1());
        setTopBarTitleColor(getResources().getColor(android.R.color.black));

        swipeBlock();
        setTopBarLeftButtonTextAndColor(getResources().getString(R.string.back), getResources().getColor(android.R.color.black), true, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent();
                setResult(RESULT_OK, it);
                finish();
            }
        });

        LinearLayout l = (LinearLayout) findViewById(R.id.facebook_comments_main_layout2);

        l.setBackgroundColor(StaticData.getXmlParsedData().getColorSkin().getColor1());

        View voiceView = LayoutInflater.from(this).inflate(R.layout.facebook_share_btn, null);
        postCommentButtonTop = (ImageView) voiceView.findViewById(R.id.imageView);
        BitmapDrawable b_png = (BitmapDrawable) postCommentButtonTop.getDrawable();
        b_png.setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.MULTIPLY);
        postCommentButtonTop.setImageDrawable(b_png);
        setTopBarRightVeiw(voiceView, false, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo ni = cm.getActiveNetworkInfo();

                if (ni != null) {
                    if (ni.isConnectedOrConnecting()) {
                        if (ni.isConnectedOrConnecting()) {
                            if (!Authorization.isAuthorized(Authorization.AUTHORIZATION_TYPE_FACEBOOK))
                                Authorization.authorize(CommentsToCommentViewActivity.this, FacebookPlugin.FACEBOOK_COMMENT_TO_COMMENT_AUTH, Authorization.AUTHORIZATION_TYPE_FACEBOOK);
                            else {
                                Intent it = new Intent(CommentsToCommentViewActivity.this, SendMessageActivity.class);
                                it.putExtra("user", Authorization.getAuthorizedUser());
                                it.putExtra("id", StaticData.getJsonFeed().getPosts().getData().get(position).getComments().getData().get(commentPosition).getId());
                                CommentsToCommentViewActivity.this.startActivityForResult(it, COMMENT_TO_COMMENT_SEND_MESSAGE);
                            }
                        } else {
                            handler.sendEmptyMessage(NEED_INTERNET_CONNECTION);
                        }
                    }
                }
            }
        });


        Intent currentIntent = getIntent();
        position = currentIntent.getIntExtra("position", 0);
        commentPosition = currentIntent.getIntExtra("comment_position", 0);

        comments = StaticData.getJsonFeed().getPosts().getData().get(position).getComments()==null?null:StaticData.getJsonFeed().getPosts().getData().get(position).getComments().getData();
        if (comments == null) {
            handler.sendEmptyMessage(INITIALIZATION_FAILED);
            return;
        }

        if (comments.isEmpty()) {
            handler.sendEmptyMessage(INITIALIZATION_FAILED);
            return;
        }

        final JsonFeed.Posts.Post.Comments.Comment item = comments.get(commentPosition);
        if (item == null) {
            handler.sendEmptyMessage(INITIALIZATION_FAILED);
            return;
        }


        listView = (PullToRefreshListView) findViewById(R.id.facebook_comments_main_listview2);
        listView.setMode(PullToRefreshBase.Mode.BOTH);
        ListView lv = listView.getRefreshableView();
        lv.setCacheColorHint(StaticData.getXmlParsedData().getColorSkin().getColor1());
        lv.setHeaderDividersEnabled(false);
        listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                            listView.onRefreshComplete();
                        }
                    });
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                if (decoded.getPaging()!=null&& decoded.getPaging().getNext()!=null)
                    new Thread( new Runnable() {
                        @Override
                        public void run() {
                            try {
                                decoded = Utils.loadReplies(CommentsToCommentViewActivity.this, decoded.getPaging().getNext());
                                adapter.addToEnd(decoded);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        adapter.notifyDataSetChanged();
                                        listView.onRefreshComplete();
                                    }
                                });
                            } catch (Throwable e) {
                            }
                        }
                    }).start();
                else
                   runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            listView.onRefreshComplete();
                        }
                    } );
            }
        });

        headerView =  LayoutInflater.from(this).inflate(R.layout.facebook_commentstocomments_header, null);
        headerView.setBackgroundColor(StaticData.getXmlParsedData().getColorSkin().getColor1());

        date = (TextView) headerView.findViewById(R.id.facebook_commentstocomment_listview_header_date);

        textcount  = (TextView) headerView.findViewById(R.id.facebook_commentstocomment_listview_comment_count);
        textcount.setTextColor(StaticData.getXmlParsedData().getColorSkin().getColor4());

        int separatorColor = Color.parseColor(StaticData.getXmlParsedData().getColorSkin().isLight() ?"#33000000" : "#4DFFFFFF");

        headerView.findViewById(R.id.top_separator).setBackgroundColor(separatorColor);
        headerView.findViewById(R.id.bottom_separator).setBackgroundColor(separatorColor);

        try {
            PluralResources res = new PluralResources(getResources());
            String count = res.getQuantityString(R.plurals.numberOfComments, item.getCommentsCount(), item.getCommentsCount());
            textcount.setTextColor(StaticData.getXmlParsedData().getColorSkin().getColor4());
            textcount.setText(count);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        Date parsedDate;
        try {
            parsedDate = dateFormat.parse(item.getCreatedTime());
        } catch(Exception exception) {
            parsedDate = Calendar.getInstance().getTime();
        }

        String dateText = "";
        SimpleDateFormat sdf = new SimpleDateFormat(getResources().getString(R.string.date_fromat));
        dateText = sdf.format(parsedDate);
        date.setTextColor(Color.argb(Color.alpha(Color.parseColor("#80FFFFFF")), Color.red(StaticData.getXmlParsedData().getColorSkin().getColor4()), Color.red(StaticData.getXmlParsedData().getColorSkin().getColor4()), Color.red(StaticData.getXmlParsedData().getColorSkin().getColor4())));
        date.setText(dateText);

        thumbImageView = (ImageView) headerView.findViewById(R.id.facebook_commentstocomment_listview_header_thumb);

        titleTextView = (TextView) headerView.findViewById(R.id.facebook_commentstocomment_listview_header_name);
        titleTextView.setTextColor(StaticData.getXmlParsedData().getColorSkin().getColor3());
        titleTextView.setText(item.getFrom().getName());

        descriptionTextView = (TextView) headerView.findViewById(R.id.facebook_commentstocomment_listview_header_text);
        descriptionTextView.setTextColor(StaticData.getXmlParsedData().getColorSkin().getColor4());
        descriptionTextView.setText( item.getMessage());

        hasCommentsLayout = (LinearLayout) findViewById(R.id.facebook_comments_main_has_layout2);
        hasCommentsLayout.setVisibility(View.VISIBLE);
        hasntCommentsLayout = (LinearLayout) findViewById(R.id.facebook_comments_main_hasnt_layout2);
        hasntCommentsLayout.setVisibility(View.GONE);

       headerLayout = (LinearLayout) findViewById(R.id.facebook_comments_main_header2);
        new Thread(new Runnable() {
            public void run() {
                try {
                    handler.sendEmptyMessage(SHOW_PROGRESS_DIALOG);
                    String iconUrl = FACEBOOK_GRAPH_COMMENTS_COUNT.replace(USER_ID, item.getFrom().getId())+ Facebook.getAccessTokenOnThreadPool();

                    String jsonFeed2 = Utils.downloadFile(CommentsToCommentViewActivity.this, iconUrl);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap bitmap2 = BitmapFactory.decodeFile(jsonFeed2, options);

                    String feedUrl = FACEBOOK_GRAPH_REPLIES.replace(COMMENT_ID, item.getId());
                    String jsonFeed = Utils.downloadFile(CommentsToCommentViewActivity.this, feedUrl + Facebook.getAccessTokenOnThreadPool());
                    Gson gson = new Gson();
                    decoded = null;
                    replies = null;
                    try {
                        decoded = gson.fromJson(new FileReader(jsonFeed),Replies.class);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    replies = decoded.getData();
                    Collections.sort(replies, Collections.reverseOrder( ReplyItem.comparator));
                    thumbImageView.setImageBitmap(bitmap2);
                    if (replies == null) {
                        handler.sendEmptyMessage(INITIALIZATION_FAILED);
                        return;
                    }
                    if (replies.isEmpty()) {
                        handler.sendEmptyMessage(SHOW_NO_COMMENTS);
                    } else {
                        handler.sendEmptyMessage(SHOW_COMMENTS_LIST);
                    }
                } catch (NullPointerException nPEx) {
                    handler.sendEmptyMessage(SHOW_NO_COMMENTS);
                }

                handler.sendEmptyMessage(HIDE_PROGRESS_DIALOG);
            }
        }).start();

    }
    /**
     * Override action onActivityResult. Start sendMessageActivty if authorization successful. Add new reply to list
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == FacebookPlugin.FACEBOOK_COMMENT_TO_COMMENT_AUTH && resultCode == RESULT_OK) {
            Intent it = new Intent(this, SendMessageActivity.class);
            it.putExtra("user", Authorization.getAuthorizedUser());
            it.putExtra("id", StaticData.getJsonFeed().getPosts().getData().get(position).getComments().getData().get(commentPosition).getId());
            startActivityForResult(it, CommentsViewActivity.COMMENT_TO_COMMENT_SEND_MESSAGE);
        }
        else
        if ((requestCode == CommentsViewActivity.COMMENT_TO_COMMENT_SEND_MESSAGE || requestCode == COMMENT_TO_COMMENT_SEND_MESSAGE)&& resultCode == RESULT_OK) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    String id = data.getStringExtra("id");
                    String feedUrl = CommentsViewActivity.FACEBOOK_GRAPH_NEW_COMMENT.replace(CommentsViewActivity.COMMENT_IDS, id);
                    String jsonFeed = Utils.downloadFile(CommentsToCommentViewActivity.this, feedUrl + Facebook.getAccessTokenOnThreadPool());
                    Gson gson = new Gson();
                    ReplyItem decoded = null;
                    try {
                        decoded = gson.fromJson(new FileReader(jsonFeed),ReplyItem.class);
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                    replies.add(0, decoded);
                    CommentsToCommentViewActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                            try {
                                PluralResources res = new PluralResources(getResources());
                                String count = res.getQuantityString(R.plurals.numberOfComments, replies.size(), replies.size());
                                textcount.setText(count);
                                comments.get(commentPosition).setCommentsCount(comments.get(commentPosition).getCommentsCount()+1);
                            } catch (NoSuchMethodException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }).start();
        }
    }
    /**
     * Prepare view for show list of replies
     */
    private void showCommentsList() {
        if (replies == null) {
            return;
        }

        if (replies.isEmpty()) {
            return;
        }

        try {
            headerLayout.removeAllViews();
        } catch (Exception ex) {
            Log.d("", "");
        }
        ListView lv = listView.getRefreshableView();
        try {
            lv.removeHeaderView(headerView);
        } catch (Exception ex) {
            Log.d("", "");
        }
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                100, getResources().getDisplayMetrics());
        int height = (int) (px);
        headerView.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.FILL_PARENT, AbsListView.LayoutParams.FILL_PARENT));
        lv.addHeaderView(headerView, null, false);

        hasCommentsLayout.setVisibility(View.VISIBLE);
        hasntCommentsLayout.setVisibility(View.GONE);

        adapter = new CommentsToCommentAdapter(this, replies);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(this);

        handler.sendEmptyMessage(HIDE_PROGRESS_DIALOG);

    }

    /**
     * Shows the placeholder if comments list is empty.
     */
    private void showNoComments() {
        try {
            ListView lv = listView.getRefreshableView();
            headerLayout.removeAllViews();
            lv.removeHeaderView(headerView);
        } catch (Exception ex) {
            Log.d("", "");
        }

        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                100, getResources().getDisplayMetrics());
        int height = (int) (px);
        headerLayout.addView(headerView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, height));

        hasntCommentsLayout.setVisibility(View.VISIBLE);

        postCommentButtonTop.performClick();
    }


    /**
     * Method for show progress dialog
     */
    private void showProgressDialog() {
        try {
            if (progressDialog.isShowing()) {
                return;
            }
        } catch (NullPointerException nPEx) {
        }

        progressDialog = ProgressDialog.show(this, null, getString(R.string.facebook_loading), true);
        progressDialog.setCancelable(true);
        progressDialog.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                handler.sendEmptyMessage(LOADING_ABORTED);
            }
        });
    }


    /**
     * Method for hide progress dialog
     */
    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
    /**
     * Method for closing activity
     */
    private void closeActivity() {
        hideProgressDialog();
        finish();
    }

    /**
     * Override action on key BACK pressed event
     */
    @Override
    public void onBackPressed() {
        Intent it = new Intent();
        setResult(RESULT_OK, it);

        finish();
    }

    /**
     * Override method from OnClickListener to add action on buttons
     */
    public void onClick(View arg0) {
        if (arg0 == homeBtn) {
            finish();
        } else if (arg0 == postCommentButton || arg0 == postCommentButtonTop) {
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo ni = cm.getActiveNetworkInfo();
        }
    }

    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
    }
}

