package com.ibuildapp.FacebookPlugin.adapter;

import android.util.SparseArray;
import android.view.View;
import com.ibuildapp.FacebookPlugin.R;

public enum ViewHolder {

    ;

    @SuppressWarnings("unchecked")
    public static <T extends View> T get(View view, int id) {
        SparseArray<View> viewHolder = (SparseArray<View>) view.getTag(R.id.TAG_VIEW);

        if (viewHolder == null) {
            viewHolder = new SparseArray<View>();
            view.setTag(R.id.TAG_VIEW, viewHolder);
        }

        View childView = viewHolder.get(id);

        if (childView == null) {
            childView = view.findViewById(id);
            viewHolder.put(id, childView);
        }

        return (T)childView;
    }

}
