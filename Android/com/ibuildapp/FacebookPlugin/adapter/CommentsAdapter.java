package com.ibuildapp.FacebookPlugin.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.appbuilder.sdk.android.Widget;
import com.appbuilder.sdk.android.authorization.Authorization;
import com.ibuildapp.FacebookPlugin.*;
import com.ibuildapp.FacebookPlugin.core.StaticData;
import com.ibuildapp.FacebookPlugin.core.Utils;
import com.ibuildapp.FacebookPlugin.model.photos.Album;
import com.ibuildapp.FacebookPlugin.model.photos.Albums;
import com.ibuildapp.FacebookPlugin.model_.JsonFeed;
import com.seppius.i18n.plurals.PluralResources;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

public class CommentsAdapter extends AdapterParent {
    private static final String FACEBOOK_GRAPH_URL = "https://graph.facebook.com/v2.3/";
    private static final String FACEBOOK_GRAPH_URL_ACCESS_TOKEN = "access_token=";
    private static final String USER_ID = "USER_ID";
    private static final String FACEBOOK_GRAPH_COMMENTS_COUNT = FACEBOOK_GRAPH_URL + USER_ID + "/picture?type=normal&" + FACEBOOK_GRAPH_URL_ACCESS_TOKEN;
    private final LayoutInflater inflater;
    private Widget widget;
    private List<JsonFeed.Posts.Post.Comments.Comment> item;
    private int globalPosition = 0;
    private int currentPosition = 0;
    private Bitmap silhousette;
    private DateFormat dateFormat ;
    public CommentsAdapter(Activity activity, List<JsonFeed.Posts.Post.Comments.Comment> item, Widget widget, int globalPosition){
        super(activity);
        this.widget = widget;
        this.item = item;
        this.globalPosition = globalPosition;
        dateFormat = Utils.getDateFormat(activity);

        inflater = LayoutInflater.from(activity);
    }
    @Override
    public int getCount() {
        try {
            return item.size();
        } catch (NullPointerException nPEx) {
            return 0;
        }
    }

    @Override
    public Object getItem(int i) {
        try {
            return item.get(i);
        } catch (NullPointerException nPEx) {
            return null;
        } catch (IndexOutOfBoundsException iOOBEx) {
            return null;
        }
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, final ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.facebook_comments_list_item, null);

        final ImageView thumbImageView = (ImageView) view.findViewById(R.id.facebook_comments_list_item_thumb);
        String feedUrl = FACEBOOK_GRAPH_COMMENTS_COUNT.replace(USER_ID, item.get(i).getFrom().getId());

        if(TextUtils.isEmpty(feedUrl))
            thumbImageView.setImageResource(R.drawable.facebook_silhouette);
        else {
            Bitmap bitmap = getBitmap(feedUrl);

            if(bitmap == null)
                getBitmap(feedUrl, new OnBitmapPreparedListener() {
                    @Override
                    public void onBitmapPrepared(Bitmap bitmap) {
                        thumbImageView.setImageBitmap(bitmap);
                        notifyDataSetChanged();
                    }
                });
            else {
                thumbImageView.setImageBitmap(bitmap);
                notifyDataSetChanged();
            }
        }

        TextView authorTextView = (TextView) view.findViewById(R.id.facebook_comments_list_item_author);
        authorTextView.setTextColor(StaticData.getXmlParsedData().getColorSkin().getColor3());
        authorTextView.setText(item.get(i).getFrom().getName()); //Автор

        TextView textTextView = (TextView) view.findViewById(R.id.facebook_comments_list_item_text);
        textTextView.setTextColor(StaticData.getXmlParsedData().getColorSkin().getColor4());
        textTextView.setText(item.get(i).getMessage());

        TextView dateTextView = (TextView) view.findViewById(R.id.facebook_comments_list_item_date);
        dateTextView.setTextColor(StaticData.getXmlParsedData().getColorSkin().getColor5());

        java.util.Date parsedDate;

        try {
            parsedDate = dateFormat.parse(item.get(i).getCreatedTime());
        } catch(Exception exception) {
            parsedDate = Calendar.getInstance().getTime();
        }

        View bottom_separator = view.findViewById(R.id.bottom_separator);

        if(i == getCount() - 1) {
            bottom_separator.setVisibility(View.GONE);
        } else {
            bottom_separator.setBackgroundColor(Color.parseColor(StaticData.getXmlParsedData().getColorSkin().isLight() ? "#33000000" : "#4DFFFFFF"));
            bottom_separator.setVisibility(View.VISIBLE);
        }
        String dateText = "";
        SimpleDateFormat sdf = new SimpleDateFormat(getContext().getString(R.string.date_fromat));
        dateText = sdf.format(parsedDate);
        dateTextView.setText(dateText);

        dateTextView.setTextColor(Color.argb(Color.alpha(Color.parseColor("#80FFFFFF")), Color.red(StaticData.getXmlParsedData().getColorSkin().getColor4()), Color.red(StaticData.getXmlParsedData().getColorSkin().getColor4()), Color.red(StaticData.getXmlParsedData().getColorSkin().getColor4())));

        LinearLayout commentsCounterLayout = (LinearLayout) view.findViewById(R.id.facebook_comments_list_item_comments_count_layout2);
        TextView commentsCounter = (TextView) view.findViewById(R.id.facebook_comments_list_item_comments_count);
        commentsCounter.setTextColor(StaticData.getXmlParsedData().getColorSkin().getColor5());

        commentsCounterLayout.setOnClickListener(new btnCommentsCountListener(i));

        view.setBackgroundColor(StaticData.getXmlParsedData().getColorSkin().getColor1());
        TextView textcount = (TextView) view.findViewById(R.id.facebook_comments_count);
        textcount.setTextColor(StaticData.getXmlParsedData().getColorSkin().getColor5());
        try {
            PluralResources res = new PluralResources(getContext().getResources());
            if(item.get(i).getCommentsCount()==null)
                item.get(i).setCommentsCount(0);
            String count = res.getQuantityString(R.plurals.numberOfCommentsItem, item.get(i).getCommentsCount(), item.get(i).getCommentsCount());
            textcount.setText(count);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return view;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }

    public List<JsonFeed.Posts.Post.Comments.Comment> getItems() {
        return item;
    }

    public void setItems(List<JsonFeed.Posts.Post.Comments.Comment> items) {
        this.item = items;
    }

    public class btnCommentsCountListener implements View.OnClickListener {

        private int position = 0;

        public btnCommentsCountListener(int position) {
            this.position = position;
        }

        public void onClick(View arg0) {
            setCurrentPosition(position);
            if (item.get(position).getCommentsCount()== 0) {
                if (!Authorization.isAuthorized(Authorization.AUTHORIZATION_TYPE_FACEBOOK))
                    Authorization.authorize(getContext(), FacebookPlugin.FACEBOOK_COMMENT_TO_COMMENT_AUTH, Authorization.AUTHORIZATION_TYPE_FACEBOOK);
                else
                {
                    Intent it = new Intent(getContext(), SendMessageActivity.class);
                    it.putExtra("user", Authorization.getAuthorizedUser());
                    it.putExtra("id", item.get(position).getId() );
                    getContext().startActivityForResult(it, CommentsViewActivity.COMMENT_TO_COMMENT_SEND_MESSAGE);
                }

            } else {
                Intent it = new Intent(getContext(), CommentsToCommentViewActivity.class);
                it.putExtra("position", globalPosition);
                it.putExtra("comment_position", position);
                getContext().startActivityForResult(it, CommentsViewActivity.COMMENT_TO_COMMENT_VIEW);
            }
        }
    }

    public void addToEnd(JsonFeed.Posts.Post.Comments albums) {
        this.item.addAll(albums.getData());
    }
}

