package com.ibuildapp.FacebookPlugin.adapter;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.ibuildapp.FacebookPlugin.ActivityVideoPlayer;
import com.ibuildapp.FacebookPlugin.R;
import com.ibuildapp.FacebookPlugin.model.video.Video;
import com.ibuildapp.FacebookPlugin.model.video.Videos;

import java.util.List;

public class AdapterVideo extends AdapterParent {

    private Bitmap placeholder;

    private final int IMAGE_WIDTH;

    private float density;
    private Resources resources;
    private LayoutInflater layoutInflater;

    private Videos videos;

    public AdapterVideo(Activity context, Videos videos) {
        super(context);

        resources = getContext().getResources();
        density = resources.getDisplayMetrics().density;
        layoutInflater = LayoutInflater.from(getContext());
        placeholder = BitmapFactory.decodeResource(resources, R.drawable.activity_video_grid_item_placeholder);

        IMAGE_WIDTH = Float.valueOf(resources.getDisplayMetrics().widthPixels / 3f).intValue();

        this.videos = videos;
    }

    public List<Video> getData() {
        return videos.getData();
    }

    @Override
    public int getCount() {
        return videos.getData().size();
    }

    @Override
    public Video getItem(int i) {
        return videos.getData().get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).hashCode();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final Video video = getItem(i);

        if(view == null)
            view = layoutInflater.inflate(R.layout.activity_video_grid_item, viewGroup, false);

        final ImageView imageView = ViewHolder.get(view, R.id.preview);

        String previewUrl = video.getPicture();

        if(!TextUtils.isEmpty(previewUrl)) {
            Bitmap bitmap = getBitmap(previewUrl);

            if (bitmap == null) {
                imageView.setImageBitmap(placeholder);
                getBitmap(previewUrl, new OnBitmapPreparedListener() {
                    @Override
                    public void onBitmapPrepared(Bitmap bitmap) {
                        imageView.setImageBitmap(bitmap);
                    }
                }, IMAGE_WIDTH);
            } else
                imageView.setImageBitmap(bitmap);
        } else
            imageView.setImageBitmap(placeholder);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), ActivityVideoPlayer.class);
                intent.putExtra(ActivityVideoPlayer.EXTRA_VIDEO_URL, video.getSource());
                intent.putExtra(ActivityVideoPlayer.EXTRA_VIDEO_ID, video.getId());
                getContext().startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void kill() {
        placeholder = null;

        super.kill();
    }
}
