package com.ibuildapp.FacebookPlugin.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ibuildapp.FacebookPlugin.R;
import com.ibuildapp.FacebookPlugin.core.StaticData;
import com.ibuildapp.FacebookPlugin.core.Utils;
import com.ibuildapp.FacebookPlugin.model.feed.Comments;
import com.ibuildapp.FacebookPlugin.model.replies.Replies;
import com.ibuildapp.FacebookPlugin.model.replies.ReplyItem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class CommentsToCommentAdapter extends AdapterParent {
    private DateFormat dateFormat;
    private static final String USER_ID = "USER_ID";
    private static final String FACEBOOK_GRAPH_URL = "https://graph.facebook.com/v2.3/";
    private static final String FACEBOOK_GRAPH_URL_ACCESS_TOKEN = "access_token=";
    private static final String FACEBOOK_GRAPH_COMMENTS_COUNT = FACEBOOK_GRAPH_URL + USER_ID + "/picture?type=normal&" + FACEBOOK_GRAPH_URL_ACCESS_TOKEN;

    private final LayoutInflater inflater;
    private ArrayList<ReplyItem> item;
    private int globalPosition = 0;

    public CommentsToCommentAdapter(Activity activity, ArrayList<ReplyItem> item){
        super(activity);
        this.item = item;
        dateFormat = Utils.getDateFormat(activity);
        inflater = LayoutInflater.from(activity);
    }
    @Override
    public int getCount() {
        try {
            return item.size();
        } catch (NullPointerException nPEx) {
            return 0;
        }
    }

    @Override
    public Object getItem(int i) {
        try {
            return item.get(i);
        } catch (NullPointerException nPEx) {
            return null;
        } catch (IndexOutOfBoundsException iOOBEx) {
            return null;
        }
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
            view = inflater.inflate(R.layout.facebook_commentscomments_list_item, null);
        final ImageView thumbImageView = (ImageView) view.findViewById(R.id.facebook_comments_list_item_thumb);
        String feedUrl = FACEBOOK_GRAPH_COMMENTS_COUNT.replace(USER_ID, item.get(i).getFrom().getId());

        if(TextUtils.isEmpty(feedUrl))
            thumbImageView.setImageResource(R.drawable.facebook_silhouette);
        else {
            Bitmap bitmap = getBitmap(feedUrl);

            if(bitmap == null)
                getBitmap(feedUrl, new OnBitmapPreparedListener() {
                    @Override
                    public void onBitmapPrepared(Bitmap bitmap) {
                        thumbImageView.setImageBitmap(bitmap);
                        notifyDataSetChanged();
                    }
                });
            else {
                thumbImageView.setImageBitmap(bitmap);
                notifyDataSetChanged();
            }
        }
        TextView authorTextView = (TextView) view.findViewById(R.id.facebook_comments_list_item_author);
        authorTextView.setTextColor(StaticData.getXmlParsedData().getColorSkin().getColor3());
        authorTextView.setText(item.get(i).getFrom().getName()); //Автор

        TextView textTextView = (TextView) view.findViewById(R.id.facebook_comments_list_item_text);
        textTextView.setTextColor(StaticData.getXmlParsedData().getColorSkin().getColor4());
        textTextView.setText(item.get(i).getMessage());

        TextView dateTextView = (TextView) view.findViewById(R.id.facebook_comments_list_item_date);
        dateTextView.setTextColor(StaticData.getXmlParsedData().getColorSkin().getColor5());

        java.util.Date parsedDate;

        try {
            parsedDate = dateFormat.parse(item.get(i).getCreatedTime());
        } catch(Exception exception) {
            parsedDate = Calendar.getInstance().getTime();
        }

        View bottom_separator = view.findViewById(R.id.bottom_separator);

        if(i == getCount() - 1) {
            bottom_separator.setVisibility(View.GONE);
        } else {
            bottom_separator.setBackgroundColor(Color.parseColor(StaticData.getXmlParsedData().getColorSkin().isLight() ? "#33000000" : "#4DFFFFFF"));
            bottom_separator.setVisibility(View.VISIBLE);
        }
        String dateText = "";
        SimpleDateFormat sdf = new SimpleDateFormat(getContext().getString(R.string.date_fromat));
        dateText = sdf.format(parsedDate);
        dateTextView.setText(dateText);
        dateTextView.setTextColor(Color.argb(Color.alpha(Color.parseColor("#80FFFFFF")), Color.red(StaticData.getXmlParsedData().getColorSkin().getColor4()), Color.red(StaticData.getXmlParsedData().getColorSkin().getColor4()), Color.red(StaticData.getXmlParsedData().getColorSkin().getColor4())));

        LinearLayout commentsCounterLayout = (LinearLayout) view.findViewById(R.id.facebook_comments_list_item_comments_count_layout);
        LinearLayout commentsCounterLayout2 = (LinearLayout) view.findViewById(R.id.facebook_comments_list_item_comments_count_layout3);
        TextView commentsCounter = (TextView) view.findViewById(R.id.facebook_comments_list_item_comments_count);
        commentsCounter.setTextColor(StaticData.getXmlParsedData().getColorSkin().getColor5());
        commentsCounterLayout2.setOnClickListener(new btnCommentsCountListener(i));


        view.setBackgroundColor(StaticData.getXmlParsedData().getColorSkin().getColor1());
        return view;
    }

    public void addToEnd(Replies decoded) {
            this.item.addAll(decoded.getData());
    }

    public class btnCommentsCountListener implements View.OnClickListener {

        private int position = 0;

        public btnCommentsCountListener(int position) {
            this.position = position;
        }

        public void onClick(View arg0) {
        }
    }
}
