package com.ibuildapp.FacebookPlugin.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.ibuildapp.FacebookPlugin.AlbumViewActivity;
import com.ibuildapp.FacebookPlugin.PhotoViewActivity;
import com.ibuildapp.FacebookPlugin.R;
import com.ibuildapp.FacebookPlugin.core.StaticData;
import com.ibuildapp.FacebookPlugin.model.uploads.Upload;
import com.ibuildapp.FacebookPlugin.model.uploads.Uploads;
import com.ibuildapp.FacebookPlugin.model_.JsonFeed;

import java.util.Collections;
import java.util.List;

public class UploadsAdapter extends AdapterParent {

    private List<Upload> uploads;
    private LayoutInflater layoutInflater;
    private Bitmap picture_placeholder;

    public UploadsAdapter(Activity context, List<Upload> uploads) {
        super(context);
        this.uploads = uploads;
        layoutInflater = LayoutInflater.from(getContext());
        picture_placeholder = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.facebook_picture_placeholder);
    }

    @Override
    public int getCount() {
        return uploads.size();
    }

    @Override
    public Upload getItem(int i) {
        return uploads.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final Upload item = getItem(i);
        final int poisition = i;

        if(view == null)
            view = layoutInflater.inflate(R.layout.photo_view_item, viewGroup, false);

        final ImageView finalView = ViewHolder.get(view, R.id.album_image);
        String imageUrl = item.getPicture();

        if(!TextUtils.isEmpty(imageUrl)) {
            Bitmap bitmap = getBitmap(imageUrl);

            if(bitmap == null) {
                finalView.setImageBitmap(picture_placeholder);
                getBitmap(imageUrl, new OnBitmapPreparedListener() {
                    @Override
                    public void onBitmapPrepared(Bitmap bitmap) {
                        finalView.setImageBitmap(bitmap);
                    }
                });
            } else
                finalView.setImageBitmap(bitmap);
        } else
            finalView.setImageBitmap(picture_placeholder);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StaticData.setViewImages(uploads);
                Intent intent = new Intent(getContext(), PhotoViewActivity.class);
                intent.putExtra("position", poisition);
                intent.putExtra("data_source", PhotoViewActivity.DATA_SOURCE_STATIC);
                getContext().startActivity(intent);
            }
        });

        return view;
    }

    public void addToEnd(Uploads uploads) {
        this.uploads.addAll(uploads.getData());
    }

    public List<Upload> getItems() {
        return uploads;
    }

    public void setItems(List<Upload> items) {
        this.uploads = items;
    }
}
