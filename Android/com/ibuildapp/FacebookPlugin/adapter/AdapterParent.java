package com.ibuildapp.FacebookPlugin.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.webkit.URLUtil;
import android.widget.BaseAdapter;
import com.appbuilder.sdk.android.Utils;
import com.ibuildapp.FacebookPlugin.core.Plugin;
import com.ibuildapp.FacebookPlugin.core.StaticData;

import java.io.File;
import java.io.FileOutputStream;

public abstract class AdapterParent extends BaseAdapter {

    public interface OnBitmapPreparedListener {
        void onBitmapPrepared(Bitmap bitmap);
    }

    private Activity context;

    protected AdapterParent(Activity context) {
        this.context = context;
    }

    protected Activity getContext() {
        return context;
    }

    protected Bitmap getBitmap(String url) {
        return Plugin.INSTANCE.getCachedBitmap(Utils.md5(url));
    }

    protected Runnable getBitmap(String url, OnBitmapPreparedListener onBitmapPreparedListener) {
        Task task = new Task(url, onBitmapPreparedListener);
        Plugin.INSTANCE.addComplexTask(task);

        return task;
    }

    protected Runnable getBitmap(String url, OnBitmapPreparedListener onBitmapPreparedListener, int preferredWidth) {
        Task task = new Task(url, onBitmapPreparedListener, preferredWidth);
        Plugin.INSTANCE.addComplexTask(task);

        return task;
    }

    public void kill() {
        context = null;
    }

    private class Task extends Plugin.ComplexTask {

        private String url;
        private String md5;
        private String filePath;
        private int preferredWidth;
        private OnBitmapPreparedListener onBitmapPreparedListener;

        private Task(String url, OnBitmapPreparedListener onBitmapPreparedListener) {
            defaultInit(url, onBitmapPreparedListener);

            preferredWidth = getContext().getResources().getDisplayMetrics().widthPixels;
        }

        private Task(String url, OnBitmapPreparedListener onBitmapPreparedListener, int preferredWidth) {
            defaultInit(url, onBitmapPreparedListener);

            this.preferredWidth = preferredWidth;
        }

        private void defaultInit(String url, OnBitmapPreparedListener onBitmapPreparedListener) {
            if(TextUtils.isEmpty(url) || !URLUtil.isValidUrl(url))
                throw new IllegalArgumentException("Url must have a valid value");

            this.url = url;
            this.md5 = Utils.md5(url);
            this.filePath = StaticData.getCachePath(context) + md5;
            this.onBitmapPreparedListener = onBitmapPreparedListener;
        }

        private void callListener(final Bitmap bitmap, final OnBitmapPreparedListener onBitmapPreparedListener) {
            if (onBitmapPreparedListener != null && context != null)
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onBitmapPreparedListener.onBitmapPrepared(bitmap);
                    }
                });
        }

        @Override
        public int hashCode() {
            return url.hashCode();
        }

        @Override
        public boolean equals(Object object) {
            return object instanceof Task && url.equals(((Task)object).url);
        }

        @Override
        protected DownloadResult download() {
            File file = new File(filePath);

            if(file.exists())
                return DownloadResult.NEEDLESS;

            if (!file.exists()) {
                String downloadResult = com.ibuildapp.FacebookPlugin.core.Utils.downloadFile(context, url);

                if (TextUtils.isEmpty(downloadResult))
                    return DownloadResult.FAILED;
                else
                    return DownloadResult.COMPLETED;
            }

            return null;
        }

        @Override
        protected void work(DownloadResult downloadResult) {
            File file = new File(filePath);

            if(downloadResult == null || downloadResult == DownloadResult.FAILED) {
                file.delete();

                return;
            }

            Bitmap bitmap = com.ibuildapp.FacebookPlugin.core.Utils.processBitmap(
                    file.getAbsolutePath(),
                    Bitmap.Config.RGB_565,
                    preferredWidth
            );

            if(downloadResult == DownloadResult.COMPLETED) {
                FileOutputStream fileOutputStream = null;

                try {
                    fileOutputStream = new FileOutputStream(file, false);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
                } catch (Exception exception) {
                    exception.printStackTrace();
                    file.delete();
                } finally {
                    try {
                        if(fileOutputStream != null)
                            fileOutputStream.close();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        file.delete();
                    }
                }
            }

            if(bitmap != null && bitmap.getWidth() != 1 && bitmap.getHeight() != 1) {
                Plugin.INSTANCE.cacheBitmap(md5, bitmap);
                callListener(bitmap, onBitmapPreparedListener);
            } else
                file.delete();
        }
    }

}
