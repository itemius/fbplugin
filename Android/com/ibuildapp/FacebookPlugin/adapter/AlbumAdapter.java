package com.ibuildapp.FacebookPlugin.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ibuildapp.FacebookPlugin.ActivityVideoPlayer;
import com.ibuildapp.FacebookPlugin.AlbumViewActivity;
import com.ibuildapp.FacebookPlugin.FacebookPlugin;
import com.ibuildapp.FacebookPlugin.R;
import com.ibuildapp.FacebookPlugin.core.Facebook;
import com.ibuildapp.FacebookPlugin.core.StaticData;
import com.ibuildapp.FacebookPlugin.model.photos.Album;
import com.ibuildapp.FacebookPlugin.model.photos.Albums;
import com.ibuildapp.FacebookPlugin.model.uploads.Upload;
import com.ibuildapp.FacebookPlugin.model.uploads.Uploads;

import java.util.List;

/**
 * Adapter for PhotoActivity to represent the album list
 */
public class AlbumAdapter extends AdapterParent {
    private List<Album> albums;
    private LayoutInflater layoutInflater;
    private Bitmap picture_placeholder;

    public AlbumAdapter(Activity context, List<Album> albums) {
        super(context);
        this.albums = albums;
        layoutInflater = LayoutInflater.from(getContext());
        picture_placeholder = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.facebook_picture_placeholder);
    }

    @Override
    public int getCount() {
        return albums.size();
    }

    @Override
    public Album getItem(int i) {
        return albums.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        final Album album = getItem(i);
        if(view == null)
            view = layoutInflater.inflate(R.layout.facebook_photo_album_item, viewGroup, false);

        final ImageView finalView = ViewHolder.get(view, R.id.facebook_album_item);

        TextView textView =  ViewHolder.get(view, R.id.facebook_album_item_title);
        textView.setTextColor(StaticData.getXmlParsedData().getColorSkin().getColor3());
        textView.setText(album.getName());

        TextView countView =  ViewHolder.get(view, R.id.facebook_album_item_size);
        countView.setTextColor(Color.argb(179, Color.red(StaticData.getXmlParsedData().getColorSkin().getColor4()), Color.green(StaticData.getXmlParsedData().getColorSkin().getColor4()), Color.blue(StaticData.getXmlParsedData().getColorSkin().getColor4())));
        countView.setText(album.getCount().toString()+" " +  getContext().getResources().getString(R.string.photos));

     String imageUrl = FacebookPlugin.FACEBOOK_GRAPH_URL + albums.get(i).getCoverPhoto()+"/picture?type=album&access_token="+ Facebook.getAccessTokenOnThreadPool();
        if(!TextUtils.isEmpty(imageUrl)) {
            Bitmap bitmap = getBitmap(imageUrl);

            if(bitmap == null) {
                finalView.setImageBitmap(picture_placeholder);
                getBitmap(imageUrl, new OnBitmapPreparedListener() {
                    @Override
                    public void onBitmapPrepared(Bitmap bitmap) {
                        finalView.setImageBitmap(bitmap);
                    }
                });
            } else
                finalView.setImageBitmap(bitmap);
        } else
            finalView.setImageBitmap(picture_placeholder);

        view.setBackgroundColor(StaticData.getXmlParsedData().getColorSkin().getColor1());
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), AlbumViewActivity.class);
                intent.putExtra("album_id", album.getId());
                intent.putExtra("album_name", album.getName());
                getContext().startActivity(intent);
            }
        });
        return view;
    }

    public void addToEnd(Albums albums) {
        this.albums.addAll(albums.getData());
    }

    public List<Album> getItems() {
        return albums;
    }

    public void setItems(List<Album> items) {
        this.albums = items;
    }
}
