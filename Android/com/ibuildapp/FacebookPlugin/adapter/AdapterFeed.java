package com.ibuildapp.FacebookPlugin.adapter;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.text.*;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.appbuilder.sdk.android.authorization.Authorization;
import com.ibuildapp.FacebookPlugin.*;
import com.ibuildapp.FacebookPlugin.core.StaticData;
import com.ibuildapp.FacebookPlugin.core.Utils;
import com.ibuildapp.FacebookPlugin.model_.JsonFeed;
import com.ibuildapp.FacebookPlugin.view.ExpandableTextView;
import com.ibuildapp.FacebookPlugin.WebViewActivity;
import com.seppius.i18n.plurals.PluralResources;

import java.util.ArrayList;

public class AdapterFeed extends AdapterParent {

    private static final int PADDING = 10;

    private static final String SHARING_URL_BASE_REPLACEMENT = "{user-id}";
    private static final String SHARING_URL_BASE = "https://facebook.com/" + SHARING_URL_BASE_REPLACEMENT + "/posts/";

    private final int GRID_WIDTH_NO_GREED;
    private final int GRID_WIDTH_TWO;
    private final int GRID_WIDTH_THREE_TOP;
    private final int GRID_WIDTH_THREE_BOTTOM;
    private final int GRID_WIDTH_FOUR_TOP;
    private final int GRID_WIDTH_FOUR_BOTTOM;

    private JsonFeed jsonFeed;
    private float density;
    private int padding;
    private LayoutInflater layoutInflater;
    private Resources resources;
    private int currentPosition = 0;
    public AdapterFeed(Activity context, JsonFeed jsonFeed, final View header) {
        super(context);

        this.jsonFeed = jsonFeed;
        resources = getContext().getResources();
        density = resources.getDisplayMetrics().density;
        padding = Float.valueOf(PADDING * density).intValue();
        layoutInflater = LayoutInflater.from(getContext());

        GRID_WIDTH_NO_GREED = resources.getDisplayMetrics().widthPixels - 50;
        GRID_WIDTH_TWO = Float.valueOf(GRID_WIDTH_NO_GREED / 1.5f).intValue();
        GRID_WIDTH_THREE_TOP = GRID_WIDTH_NO_GREED;
        GRID_WIDTH_THREE_BOTTOM = Float.valueOf(GRID_WIDTH_NO_GREED / 2.5f).intValue();
        GRID_WIDTH_FOUR_TOP = GRID_WIDTH_NO_GREED;
        GRID_WIDTH_FOUR_BOTTOM = Float.valueOf(GRID_WIDTH_NO_GREED / 3f).intValue();

        if(!jsonFeed.getPicture().getData().getIsSilhouette())
            getBitmap(jsonFeed.getPicture().getData().getUrl(), new OnBitmapPreparedListener() {
                @Override
                public void onBitmapPrepared(Bitmap bitmap) {
                    ((ImageView)header.findViewById(R.id.icon)).setImageBitmap(bitmap);
                }
            });

        if (jsonFeed.getCover() != null && !TextUtils.isEmpty(jsonFeed.getCover().getSource()))
            getBitmap(jsonFeed.getCover().getSource(), new OnBitmapPreparedListener() {
                @Override
                public void onBitmapPrepared(final Bitmap bitmap) {
                    ((ImageView)header.findViewById(R.id.cover)).setImageDrawable(new LayerDrawable(new Drawable[]{
                            new BitmapDrawable(bitmap),
                            new ColorDrawable(Color.parseColor("#80000000"))
                    }));
                }
            });
    }
    @Override
    public int getCount() {
        return jsonFeed.getPosts().getData().size();
    }

    @Override
    public JsonFeed.Posts.Post getItem(int i) {
        return jsonFeed.getPosts().getData().get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).hashCode();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final JsonFeed.Posts.Post post = getItem(position);

        if(view == null)
            view = layoutInflater.inflate(R.layout.facebook_plugin_feed_item, parent, false);

        view.setEnabled(false);
        view.setOnClickListener(null);

        if(position == getCount() - 1)
            view.setPadding(padding, padding, padding, padding);
        else
            view.setPadding(padding, padding, padding, 0);

        final View layout = ViewHolder.get(view, R.id.layout);
        final ImageView icon = ViewHolder.get(view, R.id.icon);
        final TextView name = ViewHolder.get(view, R.id.name);
        final TextView date = ViewHolder.get(view, R.id.date);
        final ExpandableTextView text = ViewHolder.get(view, R.id.text);
        final View picture_holder = ViewHolder.get(view, R.id.picture_holder);
        final ImageView picture = ViewHolder.get(view, R.id.picture);
        final ImageView play_video = ViewHolder.get(view, R.id.play_video);
        final View grey_overlay = ViewHolder.get(view, R.id.grey_overlay);

        if(!TextUtils.isEmpty(post.getMessage())) {
            text.setVisibility(View.VISIBLE);
            text.setMovementMethod(EnchancedlinkMovementMethod.getInstance());
            text.setText(post.getSpannableMessage(), TextView.BufferType.SPANNABLE);
        } else
            text.setVisibility(View.GONE);

        if(post.getAttachedImages().size() == 1) {
            picture_holder.setVisibility(View.VISIBLE);
            picture.setVisibility(View.VISIBLE);

            if("video".equals(post.getType()) && !TextUtils.isEmpty(post.getSource())) {
                picture.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String url = post.getSource();

                        getContext().startActivity(Utils.detectYoutube(url) ?
                                (android.os.Build.VERSION.SDK_INT < 11 ?
                                        new Intent(Intent.ACTION_VIEW, Uri.parse(url)) :
                                        new Intent(getContext(), WebViewActivity.class).putExtra("url", url)) :
                                new Intent(getContext(), ActivityVideoPlayer.class).putExtra(ActivityVideoPlayer.EXTRA_VIDEO_URL, post.getSource()));
                    }
                });
                play_video.setVisibility(View.VISIBLE);
                grey_overlay.setVisibility(View.VISIBLE);
            } else {
                picture.setOnClickListener( new ImageClickListener(position, 0, new ArrayList<String>(post.getAttachedImages())));
                play_video.setVisibility(View.GONE);
                grey_overlay.setVisibility(View.GONE);
            }

            if(TextUtils.isEmpty(post.getAttachedImages().get(0))) {
                picture.setImageResource(android.R.color.transparent);
            } else {
                Bitmap bitmap = getBitmap(post.getAttachedImages().get(0));

                if(bitmap == null) {
                    picture.setImageResource(android.R.color.transparent);
                    addTaskToImageView(picture, post.getAttachedImages().get(0), GRID_WIDTH_NO_GREED);
                } else
                    picture.setImageBitmap(bitmap);
            }
        } else {
            picture_holder.setVisibility(View.GONE);
            picture.setVisibility(View.GONE);
            play_video.setVisibility(View.GONE);
            grey_overlay.setVisibility(View.GONE);
        }

        final LinearLayout pic2 = ViewHolder.get(view, R.id.pictures2);
        final ImageView picture21 = ViewHolder.get(view, R.id.picture21);
        final ImageView picture22 = ViewHolder.get(view, R.id.picture22);

        if(post.getAttachedImages().size() == 2) {
            pic2.setVisibility(View.VISIBLE);
            picture21.setVisibility(View.VISIBLE);
            picture21.setOnClickListener( new ImageClickListener(position, 0, new ArrayList<String>(post.getAttachedImages())));
            picture22.setVisibility(View.VISIBLE);
            picture22.setOnClickListener( new ImageClickListener(position,1, new ArrayList<String>(post.getAttachedImages())));

            if(TextUtils.isEmpty(post.getAttachedImages().get(0))) {
                picture21.setImageResource(android.R.color.transparent);
            } else {
                Bitmap bitmap = getBitmap(post.getAttachedImages().get(0));

                if(bitmap == null) {
                    picture21.setImageResource(android.R.color.transparent);
                    addTaskToImageView(picture21, post.getAttachedImages().get(0), GRID_WIDTH_TWO);
                } else
                    picture21.setImageBitmap(bitmap);
            }

            if(TextUtils.isEmpty(post.getAttachedImages().get(1))) {
                picture22.setImageResource(android.R.color.transparent);
            } else {
                Bitmap bitmap = getBitmap(post.getAttachedImages().get(1));

                if(bitmap == null) {
                    picture22.setImageResource(android.R.color.transparent);
                    addTaskToImageView(picture22, post.getAttachedImages().get(1), GRID_WIDTH_TWO);
                } else
                    picture22.setImageBitmap(bitmap);
            }
        } else {
            pic2.setVisibility(View.GONE);
            picture21.setVisibility(View.GONE);
            picture22.setVisibility(View.GONE);
        }

        final FrameLayout pic3 = ViewHolder.get(view, R.id.pictures3);
        final ImageView picture3 = ViewHolder.get(view, R.id.picture3);
        final ImageView picture31 = ViewHolder.get(view, R.id.picture31);
        final ImageView picture32 = ViewHolder.get(view, R.id.picture32);

        if(post.getAttachedImages().size() == 3) {
            pic3.setVisibility(View.VISIBLE);
            picture3.setVisibility(View.VISIBLE);
            picture3.setOnClickListener( new ImageClickListener(position, 0, new ArrayList<String>(post.getAttachedImages())));
            picture31.setVisibility(View.VISIBLE);
            picture31.setOnClickListener( new ImageClickListener(position,1, new ArrayList<String>(post.getAttachedImages())));
            picture32.setVisibility(View.VISIBLE);
            picture32.setOnClickListener( new ImageClickListener(position,2, new ArrayList<String>(post.getAttachedImages())));

            if(TextUtils.isEmpty(post.getAttachedImages().get(0))) {
                picture3.setImageResource(android.R.color.transparent);
            } else {
                Bitmap bitmap = getBitmap(post.getAttachedImages().get(0));

                if(bitmap == null) {
                    picture3.setImageResource(android.R.color.transparent);
                    addTaskToImageView(picture3, post.getAttachedImages().get(0), GRID_WIDTH_THREE_TOP);
                } else
                    picture3.setImageBitmap(bitmap);
            }

            if(TextUtils.isEmpty(post.getAttachedImages().get(1))) {
                picture31.setImageResource(android.R.color.transparent);
            } else {
                Bitmap bitmap = getBitmap(post.getAttachedImages().get(1));

                if(bitmap == null) {
                    picture31.setImageResource(android.R.color.transparent);
                    addTaskToImageView(picture31, post.getAttachedImages().get(1), GRID_WIDTH_THREE_BOTTOM);
                } else
                    picture31.setImageBitmap(bitmap);
            }

            if(TextUtils.isEmpty(post.getAttachedImages().get(2))) {
                picture32.setImageResource(android.R.color.transparent);
            } else {
                Bitmap bitmap = getBitmap(post.getAttachedImages().get(2));

                if(bitmap == null) {
                    picture32.setImageResource(android.R.color.transparent);
                    addTaskToImageView(picture32, post.getAttachedImages().get(2), GRID_WIDTH_THREE_BOTTOM);
                } else
                    picture32.setImageBitmap(bitmap);
            }
        } else {
            pic3.setVisibility(View.GONE);
            picture3.setVisibility(View.GONE);
            picture31.setVisibility(View.GONE);
            picture32.setVisibility(View.GONE);
        }

        final FrameLayout pic4 = ViewHolder.get(view, R.id.pictures4);
        final ImageView picture4 = ViewHolder.get(view, R.id.picture4);
        final ImageView picture41 = ViewHolder.get(view, R.id.picture41);
        final ImageView picture42 = ViewHolder.get(view, R.id.picture42);
        final ImageView picture43 = ViewHolder.get(view, R.id.picture43);
        final TextView imgCount = ViewHolder.get(view, R.id.imgCount);

        if(post.getAttachedImages().size() > 3){
            pic4.setVisibility(View.VISIBLE);
            picture4.setVisibility(View.VISIBLE);
            picture4.setOnClickListener( new ImageClickListener(position, 0, new ArrayList<String>(post.getAttachedImages())));
            picture41.setVisibility(View.VISIBLE);
            picture41.setOnClickListener( new ImageClickListener(position, 1, new ArrayList<String>(post.getAttachedImages())));
            picture42.setVisibility(View.VISIBLE);
            picture42.setOnClickListener( new ImageClickListener(position, 2, new ArrayList<String>(post.getAttachedImages())));
            picture43.setVisibility(View.VISIBLE);
            picture43.setOnClickListener( new ImageClickListener(position,3, new ArrayList<String>(post.getAttachedImages())));

            imgCount.setVisibility(View.VISIBLE);
            if (post.getAttachedImages().size() > 4) {
                int count = post.getAttachedImages().size() - 4;
                imgCount.setText("+" + count);
                picture43.setBackgroundColor(Color.parseColor("#66000000"));
            } else {
                imgCount.setText("");
            }


            if(TextUtils.isEmpty(post.getAttachedImages().get(0))) {
                picture4.setImageResource(android.R.color.transparent);
            } else {
                Bitmap bitmap = getBitmap(post.getAttachedImages().get(0));

                if(bitmap == null) {
                    picture4.setImageResource(android.R.color.transparent);
                    addTaskToImageView(picture4, post.getAttachedImages().get(0), GRID_WIDTH_FOUR_TOP);
                } else
                    picture4.setImageBitmap(bitmap);
            }

            if(TextUtils.isEmpty(post.getAttachedImages().get(1))) {
                picture41.setImageResource(android.R.color.transparent);
            } else {
                Bitmap bitmap = getBitmap(post.getAttachedImages().get(1));

                if(bitmap == null) {
                    picture41.setImageResource(android.R.color.transparent);
                    addTaskToImageView(picture41, post.getAttachedImages().get(1), GRID_WIDTH_FOUR_BOTTOM);
                } else
                    picture41.setImageBitmap(bitmap);
            }

            if(TextUtils.isEmpty(post.getAttachedImages().get(2))) {
                picture42.setImageResource(android.R.color.transparent);
            } else {
                Bitmap bitmap = getBitmap(post.getAttachedImages().get(2));

                if(bitmap == null) {
                    picture42.setImageResource(android.R.color.transparent);
                    addTaskToImageView(picture42, post.getAttachedImages().get(2), GRID_WIDTH_FOUR_BOTTOM);
                } else
                    picture42.setImageBitmap(bitmap);
            }

            if(TextUtils.isEmpty(post.getAttachedImages().get(3))) {
                picture43.setImageResource(android.R.color.transparent);
            } else {
                Bitmap bitmap = getBitmap(post.getAttachedImages().get(3));

                if(bitmap == null) {
                    picture43.setImageResource(android.R.color.transparent);
                    addTaskToImageView(picture43, post.getAttachedImages().get(3), GRID_WIDTH_FOUR_BOTTOM);
                } else
                    picture43.setImageBitmap(bitmap);
            }
        } else {
            pic4.setVisibility(View.GONE);
            picture4.setVisibility(View.GONE);
            picture41.setVisibility(View.GONE);
            picture42.setVisibility(View.GONE);
            picture43.setVisibility(View.GONE);
            imgCount.setVisibility(View.GONE);
        }

        final TextView likes_count = ViewHolder.get(view, R.id.likes_count);
        final TextView comments_count = ViewHolder.get(view, R.id.comments_count);
        final View like = ViewHolder.get(view, R.id.like);
        final View comment = ViewHolder.get(view, R.id.comment);
        final View share = ViewHolder.get(view, R.id.share);

        layout.setBackgroundColor(StaticData.getXmlParsedData().getColorSkin().getColor1());

        if(!jsonFeed.getPicture().getData().getIsSilhouette()) {
            Bitmap bitmap = getBitmap(jsonFeed.getPicture().getData().getUrl());

            if(bitmap == null) {
                icon.setImageResource(android.R.color.transparent);
                addTaskToImageView(icon, jsonFeed.getPicture().getData().getUrl(), GRID_WIDTH_TWO);
            } else
                icon.setImageBitmap(bitmap);
        }

        name.setText(jsonFeed.getName());
        date.setText(post.getFormattedCreatedTime());

        like.setEnabled(!post.isLiked());
        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                like.setEnabled(false);

                if(Authorization.isAuthorized(Authorization.AUTHORIZATION_TYPE_FACEBOOK)) {
                    if(!post.isLiked()) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    if(Utils.complexLike(post.getId())) {
                                        post.setLiked(true);
                                        post.setLikesCount(post.getLikesCount() + 1);

                                        getContext().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                notifyDataSetChanged();
                                            }
                                        });
                                    }
                                } catch (Exception exception) {
                                    exception.printStackTrace();
                                }
                            }
                        }).start();

                    }
                } else {
                    Activity context = getContext();

                    if(context instanceof FacebookPlugin) {
                        ((FacebookPlugin)context).setLikeObject(post.getId());
                        Authorization.authorize(context, FacebookPlugin.FACEBOOK_LIKE_AUTH, Authorization.AUTHORIZATION_TYPE_FACEBOOK);
                    }
                }
            }
        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String url = Utils.getPostDirectUrl(post.getId());

                        if(Authorization.isAuthorized(Authorization.AUTHORIZATION_TYPE_FACEBOOK)) {
                            Intent intent = new Intent(getContext(), ActivitySharing.class);
                            intent.putExtra(ActivitySharing.EXTRA_URL, url);
                            getContext().startActivityForResult(intent, ActivitySharing.REQUEST_CODE);
                        } else {
                            Activity context = getContext();

                            if(context instanceof FacebookPlugin) {
                                ((FacebookPlugin)context).setShareUrl(url);
                                Authorization.authorize(context, FacebookPlugin.FACEBOOK_LIKE_AUTH, Authorization.AUTHORIZATION_TYPE_FACEBOOK);
                            }
                        }
                    }
                }).start();
            }
        });
        comment.setOnClickListener(new CommentsListener(position));

        View count_holder = ViewHolder.get(view, R.id.count_holder);

        if(post.getLikesCount() == 0 && post.getCommentsCount() == 0)
            count_holder.setVisibility(View.GONE);
        else
            count_holder.setVisibility(View.VISIBLE);

        try {
            PluralResources res = new PluralResources(resources);
            String count = res.getQuantityString(R.plurals.numberOfLikes, post.getLikesCount(),post.getLikesCount());
            likes_count.setText(count);

            if (post.getLikesCount() == 0)
                likes_count.setVisibility(View.GONE);
            else
                likes_count.setVisibility(View.VISIBLE);

            String comment_count = res.getQuantityString(R.plurals.numberOfViewComments, post.getCommentsCount(),post.getCommentsCount());
            comments_count.setText(comment_count);

            if (post.getCommentsCount() == 0)
                comments_count.setVisibility(View.GONE);
            else
                comments_count.setVisibility(View.VISIBLE);

        } catch (Throwable e) {
            e.printStackTrace();
        }

        return view;
    }

    private void addTaskToImageView(final ImageView imageView, String url, int preferredWidth) {
        imageView.setTag(R.id.TAG_TASK, getBitmap(url, new OnBitmapPreparedListener() {
            @Override
            public void onBitmapPrepared(Bitmap bitmap) {
                imageView.setImageBitmap(bitmap);
                notifyDataSetChanged();
            }
        }, preferredWidth));
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }

    public class ImageClickListener implements View.OnClickListener {

        private int position = 0;
        private int subPosition = 0;
        private ArrayList<String> images = new ArrayList<String>();

        public ImageClickListener(int position, int subPosition, ArrayList<String> images) {
            this.position = position;
            this.subPosition = subPosition;
            images.addAll(images);
        }

        public void onClick(View arg0) {
            Intent it = new Intent(getContext(), PhotoViewActivity.class);

            it.putExtra("position", position);
            it.putExtra("subposition", subPosition);
            it.putStringArrayListExtra("images", images);
            it.putExtra("data_source", PhotoViewActivity.DATA_SOURCE_FEED);
            getContext().startActivity(it);
        }
    }

    public class CommentsListener implements View.OnClickListener {

        private int position = 0;

        public CommentsListener(int position) {
            this.position = position;
        }

        public void onClick(View arg0) {
            setCurrentPosition(position);
            if (getItem(position).getCommentsCount() == 0)
            {
                if (!Authorization.isAuthorized(Authorization.AUTHORIZATION_TYPE_FACEBOOK))
                    Authorization.authorize(getContext(), FacebookPlugin.FACEBOOK_MAIN_COMMENT_AUTH, Authorization.AUTHORIZATION_TYPE_FACEBOOK);
                else
                {
                    Intent it = new Intent(getContext(), SendMessageActivity.class);
                    it.putExtra("user", Authorization.getAuthorizedUser());
                    it.putExtra("id", getItem(position).getId());
                    getContext().startActivityForResult(it, CommentsViewActivity.COMMENT_SEND_MESSAGE);
                }
            }
            else {
                Intent it = new Intent(getContext(), CommentsViewActivity.class);
                it.putExtra("position", position);
                getContext().startActivityForResult(it, FacebookPlugin.COMMENTS_VIEW);
            }
        }
    }

    public JsonFeed getData() {
        return jsonFeed;
    }
}
