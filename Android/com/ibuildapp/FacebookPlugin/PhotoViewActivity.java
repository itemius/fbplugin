package com.ibuildapp.FacebookPlugin;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.os.*;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.*;
import com.appbuilder.sdk.android.authorization.Authorization;
import com.appbuilder.sdk.android.authorization.FacebookAuthorizationActivity;
import com.ibuildapp.FacebookPlugin.core.Plugin;
import com.ibuildapp.FacebookPlugin.core.StaticData;
import com.ibuildapp.FacebookPlugin.core.Utils;
import com.ibuildapp.FacebookPlugin.model.adapters.ImageUploadAdapter;
import com.ibuildapp.FacebookPlugin.model.uploads.Upload;
import com.ibuildapp.FacebookPlugin.view.ExtImageGallery;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;

public class PhotoViewActivity extends ActivityParent implements View.OnClickListener,ExtImageGallery.OnSlideEndListener{
    private static final int FACEBOOK_LIKE_AUTH = 10137;
    private static final int FACEBOOK_LIKE_SHARE = 10138;

    final public static int DATA_SOURCE_UNDEFINED = 10;
    final public static int DATA_SOURCE_FEED = 11;
    final public static int DATA_SOURCE_STATIC = 12;

    public static final int SHOW_PROGRESS_DIALOG = 0;
    public static final int HIDE_PROGRESS_DIALOG = 1;
    private final int LOADING_ABORTED = 2;
    private final int INITIALIZATION_FAILED = 3;
    private final int SLIDE_TO_LEFT_START = 4;
    private final int SLIDE_TO_RIGHT_START = 5;
    private final int SHOW_INFO = 7;
    private final int HIDE_INFO = 8;
    private final int SLIDING = 11;
    private final int SHOW_SAVE_IMAGE_DIALOG = 12;
    private final int SAVE_IMAGE = 13;
    private final int GET_OG_LIKES = 16;
    private final int UPDATE_LIKE_POSITON = 17;


    private LinearLayout mainLayout;
    private ImageView shareButton;
    private TextView titleTextView;
    private TextView descriptionTextView;
    private LinearLayout descriptionPanel;
    private ImageView likeButton;
    private DisplayMetrics metrix = new DisplayMetrics();
    private LinearLayout controlsLayout;
    private ExtImageGallery imageGallery;
    private ImageView postCommentButtonTop;


    private int displayHeight = 0;
    private int position;
    private List<Upload> images = null;
    private boolean sliding = false;
    private boolean slidingPaused = false;
    private ArrayList<String> likes = null;

    /**
     * Handler for async methods
     */
    public Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SHOW_PROGRESS_DIALOG: {
                    progressShow(true);
                }
                break;
                case HIDE_PROGRESS_DIALOG: {
                    progressHide();
                }
                break;
                case LOADING_ABORTED: {
                    closeActivity();
                }
                break;
                case INITIALIZATION_FAILED: {
                    Toast.makeText(PhotoViewActivity.this,
                            getResources().getString(R.string.facebook_alert_cannot_init),
                            Toast.LENGTH_LONG).show();
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            finish();
                        }
                    }, 2000);
                }
                break;
                case SLIDE_TO_LEFT_START: {
                    if (position < images.size() - 1) {
                        position++;
                        slideImage(-500);
                    }
                }
                break;
                case SLIDE_TO_RIGHT_START: {
                    if (position > 0) {
                        position--;
                        slideImage(500);
                    }
                }
                break;
                case HIDE_INFO: {
                    hideInfo();
                }
                break;
                case SHOW_INFO: {
                    showInfo();
                }
                break;
                case SLIDING: {
                    if (position < (images.size() - 1)) {
                        imageGallery.slideRight();
                    } else {
                        sliding = false;
                    }
                }
                break;
                case SAVE_IMAGE: {
                    saveImage();
                }
                break;
                case SHOW_SAVE_IMAGE_DIALOG: {
                    showSaveImageDialog();
                }
                break;

                case GET_OG_LIKES: {
                }
                break;

                case UPDATE_LIKE_POSITON: {
                    slideImage(500);
                }
                break;
            }
        }
    };


    /**
     * Load activity backend data. Run in create method. Get data from intent
     */
    @Override
    protected void backend() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.sendEmptyMessage(SHOW_PROGRESS_DIALOG);
                Intent currentIntent = getIntent();
                Bundle store = currentIntent.getExtras();

                position = store.getInt("position", 0);
                int dataSource = currentIntent.getIntExtra("data_source", DATA_SOURCE_UNDEFINED );
                if (dataSource == DATA_SOURCE_FEED) {
                    images = new ImageUploadAdapter().toUpload(StaticData.getJsonFeed().getPosts().getData().get(position).getAttachedImages());
                    position = store.getInt("subposition", 0);
                }
                else if (dataSource == DATA_SOURCE_STATIC)
                    images = StaticData.getViewImages();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imageGallery.setPosition(position);
                        imageGallery.setActivity(PhotoViewActivity.this);
                        imageGallery.setImageItems(images);

                        imageGallery.setOnClickListener(PhotoViewActivity.this);
                        imageGallery.setOnSlideEndListener(PhotoViewActivity.this);
                        if (hasAdView()) {
                            imageGallery.setIndent(getAdHeight());
                        }
                        Boolean isLike = images.get(imageGallery.getCurrentImagePosition()).getIsLike();
                        if (isLike!=null) {
                            if(isLike)
                                showLike();
                            else hideLike();
                        }
                        handler.sendEmptyMessage(HIDE_PROGRESS_DIALOG);
                    }
                });

            }
        }).start();
    }

    /**
     * Method for prepare and  load elements of user interface. set visibilities, add listeners, background colors ,etc .
     */
    protected void UI() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setTopBarTitle(getResources().getString(R.string.photo));
        setTopBarTitleColor(Color.parseColor("#000000"));

        View voiceView = LayoutInflater.from(this).inflate(R.layout.facebook_post_btn, null);
        postCommentButtonTop = (ImageView) voiceView.findViewById(R.id.imageView);

        BitmapDrawable b_png = (BitmapDrawable) postCommentButtonTop.getDrawable();
        b_png.setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.MULTIPLY);
        postCommentButtonTop.setImageDrawable(b_png);
        setTopBarRightVeiw(voiceView, false, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.sendEmptyMessage(SAVE_IMAGE);
            }
        });

        setContentView(R.layout.photo_view_details);

        disableSwipe();
        setTopBarLeftButtonTextAndColor(getResources().getString(R.string.back), Color.parseColor("#000000"), true, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeActivity();
            }
        });

        displayHeight = getWindowManager().getDefaultDisplay().getHeight();

        if (hasAdView()) {
            if (getAdvType().equalsIgnoreCase("html")) {
                displayHeight = displayHeight - 50;
            } else {
                displayHeight = displayHeight - (int) (50 * metrix.density);
            }
        }

        mainLayout = (LinearLayout) findViewById(R.id.facebook_details_main_layout);
        mainLayout.setBackgroundColor(Color.parseColor("#000000"));

        shareButton = (ImageView) findViewById(R.id.facebook_details_share);
        shareButton.setOnClickListener(this);
        setTopBarBackgroundColor(StaticData.getXmlParsedData().getColorSkin().getColor1() );

        FrameLayout frameLayout = (FrameLayout)findViewById(R.id.frame_layout);
        frameLayout.addView(popTopBar());

        titleTextView = (TextView) findViewById(R.id.facebook_details_title);
        titleTextView.setVisibility(View.GONE);

        descriptionTextView = (TextView) findViewById(R.id.facebook_details_description);
        descriptionTextView.setMovementMethod(new ScrollingMovementMethod());


        descriptionPanel = (LinearLayout) findViewById(R.id.facebook_details_description_panel);
        descriptionPanel.setVisibility(View.INVISIBLE);

        likeButton = (ImageView) findViewById(R.id.facebook_details_like);

        likeButton.setOnClickListener(this);

        controlsLayout = (LinearLayout) findViewById(R.id.facebook_details_controls_layout);
        imageGallery = (ExtImageGallery) findViewById(R.id.facebook_details_imagegallery);
    }
    /**
     * Method for loading user likes objects.
     */
    @Override
    protected void content(boolean cancelable) {

        Plugin.INSTANCE.addTask(new Runnable() {
            @Override
            public void run() {
                try {
                    if(Authorization.isAuthorized(Authorization.AUTHORIZATION_TYPE_FACEBOOK)) {
                        likes = new ArrayList<String>();
                        likes.addAll(Plugin.INSTANCE.getInFuture(new Callable<Plugin.Result<ArrayList<String>>>() {
                            @Override
                            public Plugin.Result<ArrayList<String>> call() throws Exception {
                                return new Plugin.Result<ArrayList<String>>() {
                                    @Override
                                    public ArrayList<String> getResult() throws Exception {
                                        return FacebookAuthorizationActivity.getUserOgLikes();
                                    }
                                };
                            }
                        }).getResult());
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressHide();
                        }
                    });
                } catch(Exception exception) {
                    exception.printStackTrace();
                }
            }
        });
    }

    /**
     * Method for closing activity
     */
    private void closeActivity() {
        progressHide();
        finish();
    }

    /**
     * Override action onActivityResult. Start sendMessageActivty if authorization successful. Add new comment to list
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            if(requestCode == FACEBOOK_LIKE_AUTH)
                like();
            else if(requestCode == FACEBOOK_LIKE_SHARE)
                share();
        }

        if(requestCode == ActivitySharing.REQUEST_CODE)
            Toast.makeText(this, getResources().getString(resultCode == RESULT_OK ? R.string.shared_on_facebook : R.string.not_shared_on_facebook), Toast.LENGTH_LONG).show();

    }

    /**
     * Override onClick from OnClickListener. Add action on buttons
     */
    public void onClick(View arg0) {
        if (arg0 == shareButton) {
            if(Authorization.isAuthorized(Authorization.AUTHORIZATION_TYPE_FACEBOOK))
                share();
            else
                Authorization.authorize(PhotoViewActivity.this, FACEBOOK_LIKE_SHARE, Authorization.AUTHORIZATION_TYPE_FACEBOOK);

        }  else if (arg0 == likeButton) {
            like();
        }
        else if (arg0 == imageGallery) {
            imageGalleryClick();
        }
    }

    /**
     * Method activates when image gallery clicks
     */
    private void imageGalleryClick(){
        if (controlsLayout.getVisibility() == View.GONE
                || controlsLayout.getVisibility() == View.INVISIBLE) {
            controlsLayout.setVisibility(View.VISIBLE);
            visibleTopBar();

            if (sliding) {
                handler.removeMessages(SLIDING);
                slidingPaused = false;
                sliding = false;
            }
        } else if (controlsLayout.getVisibility() == View.VISIBLE) {
            controlsLayout.setVisibility(View.INVISIBLE);
            descriptionPanel.setVisibility(View.INVISIBLE);
            invisibleTopBar();

            if (sliding) {
                handler.sendEmptyMessage(SLIDING);
                slidingPaused = false;
            }
        }
    }

    /**
     * Method activates when image in image gallery slides
     */
    private void slideImage( int pos ) {
        invisibleTopBar();
        controlsLayout.setVisibility(View.INVISIBLE);
        descriptionPanel.setVisibility(View.INVISIBLE);

        Boolean isLike = images.get(imageGallery.getCurrentImagePosition()).getIsLike();
            if (isLike!=null) {
               if(isLike)
                showLike();
                else hideLike();
            }
        else
            Plugin.INSTANCE.addTask(new Runnable() {
                Boolean result;
                final int pos = imageGallery.getCurrentImagePosition();
                @Override
                public void run() {
                    try {
                        if (Authorization.isAuthorized(Authorization.AUTHORIZATION_TYPE_FACEBOOK)) {
                            if(likes==null)
                                content(false);

                            result = Plugin.INSTANCE.getInFuture(new Callable<Plugin.Result<Boolean>>() {
                                @Override
                                public Plugin.Result<Boolean> call() throws Exception {
                                    return new Plugin.Result<Boolean>() {

                                        @Override
                                        public Boolean getResult() throws Exception {
                                            return likes.contains(images.get(pos).getSource());
                                        }
                                    };
                                }
                            }).getResult();
                            images.get(pos).setIsLike(result);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (pos == imageGallery.getCurrentImagePosition())
                                        if (result)
                                            showLike();
                                        else hideLike();
                                }
                            });
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
            });
    }

    /**
     * Method add show animation on like button
     */
    private void showLike() {
        float alpha = 0.4f;

        if (Build.VERSION.SDK_INT < 11)
            likeButton.startAnimation(new AlphaAnimation(alpha, alpha) {{
                setDuration(0);
                setFillAfter(true);
            }});
        else
            likeButton.setAlpha(alpha);
    }

    /**
     * Method add hide animation on like button
     */
    private void hideLike() {
        float alpha = 1.0f;

        if (Build.VERSION.SDK_INT < 11)
            likeButton.startAnimation(new AlphaAnimation(alpha, alpha) {{
                setDuration(0);
                setFillAfter(true);
            }});
        else
            likeButton.setAlpha(alpha);
    }


    /**
     * Method add show action on like button
     */
    private void showInfo() {
        try {
            controlsLayout.setVisibility(View.VISIBLE);
            visibleTopBar();
        } catch (Exception ex) {
        }
    }

    /**
     * Method add hide action on like button
     */
    private void hideInfo() {
        descriptionPanel.setVisibility(View.INVISIBLE);
        controlsLayout.setVisibility(View.INVISIBLE);
        invisibleTopBar();

        if (sliding && slidingPaused) {
            slidingPaused = false;
            handler.sendEmptyMessage(SLIDING);
        }
    }
    @Override
    public void onSlideEnd(int var1, int var2) {
        slideImage(0);

    }

    /**
     * Method save image in storage
     */
    private void saveImage() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String path = images.get(imageGallery.getCurrentImagePosition()).getSource();
                if (path.length() == 0) {
                    return;
                }

                String galleryPath = Environment.getExternalStorageDirectory() + "/images/" + "facebook_photos";
                File gallery = new File(galleryPath);
                if (!gallery.exists()) {
                    gallery.mkdirs();
                }

                if (!gallery.exists()) {
                    return;
                }

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss", Locale.ENGLISH);

                File dstFile = new File(galleryPath + "/image " + sdf.format(new Date()) + ".png");

                try {
                    Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(com.ibuildapp.FacebookPlugin.core.Utils.downloadFile(PhotoViewActivity.this, path, com.appbuilder.sdk.android.Utils.md5(path)+"full")), null,  null);
                    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(dstFile));
                    bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);

                    bos.flush();
                    bos.close();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(PhotoViewActivity.this, R.string.facebook_image_save, Toast.LENGTH_LONG).show();
                        }
                    });
                } catch (Exception e) {
                    Log.w("ImageDetails -->", "Error copying image " + e);
                } finally {
                }
            }
        }).start();

    }

    /**
     * Method show save image dialog
     */
    private void showSaveImageDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(getString(R.string.facebook_dialog_sure_save_image));
        dialog.setCancelable(false);

        dialog.setPositiveButton(getString(R.string.facebook_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                handler.sendEmptyMessage(SAVE_IMAGE);
            }
        }).setNegativeButton(getString(R.string.facebook_no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        AlertDialog alert = dialog.create();
        alert.show();
    }

    /**
     * Method execute like action in facebook graph api
     */
    private void like() {
        if(Authorization.isAuthorized(Authorization.AUTHORIZATION_TYPE_FACEBOOK)) {
            if(likes==null)
                content(false);

            Plugin.INSTANCE.addTask(new Runnable() {
                @Override
                public void run() {
                    try {
                        final Boolean[] result = Plugin.INSTANCE.getInFuture(new Callable<Plugin.Result<Boolean[]>>() {
                            @Override
                            public Plugin.Result<Boolean[]> call() throws Exception {
                                return new Plugin.Result<Boolean[]>() {
                                    @Override
                                    public Boolean[] getResult() throws Exception {
                                        boolean isLiked = false;
                                        boolean wasLiked = false;

                                        try {
                                            String url = images.get(imageGallery.getCurrentImagePosition()).getSource();
                                            String id = images.get(imageGallery.getCurrentImagePosition()).getId();
                                            isLiked = Utils.complexSourceLike(id, url);
                                            if (isLiked)
                                                likes.add(url);
                                        } catch (FacebookAuthorizationActivity.FacebookAlreadyLiked exception) {
                                            wasLiked = isLiked = true;
                                        }

                                        return new Boolean[]{isLiked, wasLiked};
                                    }
                                };
                            }
                        }).getResult();

                        final Boolean isLiked = result[0];
                        images.get(imageGallery.getCurrentImagePosition()).setIsLike(isLiked);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (isLiked)
                                    showLike();
                                else hideLike();

                                if (isLiked && !result[1])
                                    Toast.makeText(PhotoViewActivity.this, R.string.facebook_alert_liked, Toast.LENGTH_LONG).show();
                            }
                        });
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
            });
        }
        else
            Authorization.authorize(PhotoViewActivity.this, FACEBOOK_LIKE_AUTH, Authorization.AUTHORIZATION_TYPE_FACEBOOK);
    }
    /**
     * Method to start share activity
     */
    private void share() {
        if(likes==null)
            content(false);
        Intent intent = new Intent(this, ActivitySharing.class);
        intent.putExtra(ActivitySharing.EXTRA_URL, images.get(imageGallery.getCurrentImagePosition()).getSource());
        startActivityForResult(intent, ActivitySharing.REQUEST_CODE);
    }

}
