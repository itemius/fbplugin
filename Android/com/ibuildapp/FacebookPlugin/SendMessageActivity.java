package com.ibuildapp.FacebookPlugin;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.*;
import android.widget.TextView.OnEditorActionListener;
import com.appbuilder.sdk.android.AppBuilderModuleMain;
import com.appbuilder.sdk.android.authorization.Authorization;
import com.ibuildapp.FacebookPlugin.core.StaticData;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class SendMessageActivity extends AppBuilderModuleMain implements OnClickListener,
        OnEditorActionListener, TextWatcher, ViewTreeObserver.OnGlobalLayoutListener {

    private String TAG = "com.ibuildapp.SendMessageActivity";
    private final int TAKE_A_PICTURE_ACTIVITY = 10000;
    private final int PICK_IMAGE_ACTIVITY = 10001;
    private final int CLOSE_ACTIVITY_OK = 0;
    private final int CLOSE_ACTIVITY_BAD = 1;
    private final int PROGRESS_SIZE = 20;
    private LinearLayout root = null;
    private LinearLayout bottomPanel = null;
    private LinearLayout editLayout = null;
    private LinearLayout header = null;
    private View headerView = null;
    private EditText messageEditText = null;
    private TextView clearButton = null;
    private TextView postButton = null;
    private TextView symbolCounter = null;
    private TextView dateTextView = null;
    private String id = null;
    private ProgressBar progressBar = null;
    private int displayHeight = 0;
    private boolean uploading = false;

    ImageView view = null;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case CLOSE_ACTIVITY_OK: {
                    closeActivityOK();
                }
                break;
                case CLOSE_ACTIVITY_BAD: {
                    closeActivityBad();
                }
                break;
            }
        }
    };
    private String resultId;

    @Override
    public void create() {
        setContentView(R.layout.facebook_send_message);
        setTopBarTitleColor(getResources().getColor(android.R.color.black));
        // set topbar title
        swipeBlock();
        setTopBarLeftButtonTextAndColor(getResources().getString(R.string.back), getResources().getColor(android.R.color.black), true, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        setTopBarBackgroundColor(StaticData.getXmlParsedData().getColorSkin().getColor1());
        setTopBarTitle(getResources().getString(R.string.facebook_preview_capture));
        progressBar = new ProgressBar(this);
        progressBar.setVisibility(View.INVISIBLE);
        float density = getResources().getDisplayMetrics().density;
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams((int) (PROGRESS_SIZE * density), (int) (PROGRESS_SIZE * density));
        progressBar.setLayoutParams(params);
        drawTopBarRightButton(progressBar);

        Intent currentIntent = getIntent();
        id = currentIntent.getStringExtra("id");
        displayHeight = getResources().getDisplayMetrics().heightPixels;

        messageEditText = (EditText) findViewById(R.id.facebook_sendmessage_edittext);
        messageEditText.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        messageEditText.addTextChangedListener(this);

        clearButton = (TextView) findViewById(R.id.facebook_sendmessage_clear_btn);
        clearButton.setOnClickListener(this);
        designButton(clearButton, bottomBarDesign.leftButtonDesign);
        clearButton.setTextColor(Color.parseColor ("#FFFFFF"));//bottomBarDesign.leftButtonDesign.textColor);

        postButton = (TextView) findViewById(R.id.facebook_sendmessage_post_btn);
        postButton.setOnClickListener(this);
        designButton(postButton, bottomBarDesign.rightButtonDesign);
        postButton.setTextColor(Color.parseColor ("#FFFFFF"));//bottomBarDesign.leftButtonDesign.textColor);

        symbolCounter = (TextView) findViewById(R.id.facebook_sendmessage_symbols_counter);
        symbolCounter.setVisibility(View.GONE);
        bottomPanel = (LinearLayout) findViewById(R.id.facebook_sendmessage_bottom_panel);
        bottomPanel.setVisibility(View.GONE);

        root = (LinearLayout) findViewById(R.id.facebook_sendmessage_root);
        root.setBackgroundColor(StaticData.getXmlParsedData().getColorSkin().getColor1());
        root.getViewTreeObserver().addOnGlobalLayoutListener(this);

        editLayout = (LinearLayout) findViewById(R.id.facebook_sendmessage_edittext_layout);

        header = (LinearLayout) findViewById(R.id.facebook_sendmessage_header);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        Log.d("", "");
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Hides header view when keyboard is shown.
     */
    private void keyboardShown() {
        if (header == null)
            header = (LinearLayout) findViewById(R.id.facebook_sendmessage_header);
        header.setVisibility(View.VISIBLE);
        if (displayHeight < 481) {
            header.setVisibility(View.GONE);
        }
        bottomPanel.setVisibility(View.VISIBLE);

        int height = (int) getResources().getDisplayMetrics().density * 90;
        if (editLayout == null)
            editLayout = (LinearLayout) findViewById(R.id.facebook_sendmessage_edittext_layout);
        editLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, height));
    }

    /**
     * Shows header view when keyboard is hidden.
     */
    private void keyboardHidden() {
        header.setVisibility(View.VISIBLE);
        bottomPanel.setVisibility(View.GONE);

        int height = (int) getResources().getDisplayMetrics().density * 90;
        editLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, height));
    }

    /**
     * Closes this activity with "OK" result.
     */
    private void closeActivityOK() {
        Intent it = new Intent();
        it.putExtra("id", resultId);
        setResult(RESULT_OK, it);

        finish();
    }

    /**
     * Closes this activity with "Canceled" result.
     */
    private void closeActivityBad() {
        setResult(RESULT_CANCELED);

        finish();
    }

    public void onClick(View arg0) {
        if (!uploading) {
            if (arg0.getId() == R.id.facebook_sendmessage_clear_btn) {
                messageEditText.setText("");
            } else if (arg0.getId() == R.id.facebook_sendmessage_post_btn) {
                if (messageEditText.getText().length() < 1) {
                    Toast.makeText(this, getString(R.string.facebook_dialog_sure_save_image),
                            Toast.LENGTH_LONG).show();
                }

                progressBar.setVisibility(View.VISIBLE);

                uploading = true;

                if ((messageEditText.getText().length() == 0)) {
                    Toast.makeText(this, getString(R.string.facebook_alert_empty_message),
                            Toast.LENGTH_LONG).show();
                    uploading = false;

                    progressBar.setVisibility(View.INVISIBLE);

                    return;
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        resultId = comment(id, messageEditText.getText().toString());
                        handler.sendEmptyMessage(CLOSE_ACTIVITY_OK);
                    }
                }).start();

            }
        }
    }

    public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
        return false;
    }

    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
    }

    public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
    }

    public void onGlobalLayout() {
        int heightDiff = root.getRootView().getHeight() - root.getHeight();
        if (heightDiff > 100) { // if more than 100 pixels, its probably a keyboard...
            keyboardShown();
            Log.d("", "");
        } else {
            keyboardHidden();
            Log.d("", "");
        }
    }

    public static String comment(String object, String message) {
        JSONObject obj;
        String res = new String("");
        try {
            String url = "https://graph.facebook.com/v2.3/" + object + "/comments";
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setRequestProperty("Accept-Encoding", "identity");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("User-Agent",
                    "Mozilla/5.0 (iPhone; U; "
                            + "CPU iPhone OS 4_0 like Mac OS X; en-us) AppleWebKit/532.9 "
                            + "(KHTML, like Gecko) Version/4.0.5 Mobile/8A293 "
                            + "Safari/6531.22.7");

            conn.setRequestMethod("POST");
            conn.setDoOutput(true);

            StringBuilder sb = new StringBuilder();
            sb.append("method=");
            sb.append("POST");
            sb.append("&");
            sb.append("access_token=");
            sb.append(Authorization.getAuthorizedUser(Authorization.AUTHORIZATION_TYPE_FACEBOOK).getAccessToken());
            sb.append("&");
            sb.append("message=");
            sb.append(message);
            String params = sb.toString();

            conn.getOutputStream().write(params.getBytes("UTF-8"));

            String response = "";
            try {
                InputStream in = conn.getInputStream();

                StringBuilder sbr = new StringBuilder();
                BufferedReader r = new BufferedReader(new InputStreamReader(in), 1000);
                for (String line = r.readLine(); line != null; line = r.readLine()) {
                    sbr.append(line);
                }
                in.close();

                response = sbr.toString();
            } catch (FileNotFoundException e) {
                InputStream in = conn.getErrorStream();

                StringBuilder sbr = new StringBuilder();
                BufferedReader r = new BufferedReader(new InputStreamReader(in), 1000);
                for (String line = r.readLine(); line != null; line = r.readLine()) {
                    sbr.append(line);
                }
                in.close();

                response = sbr.toString();
            }

            try {
                obj = new JSONObject(response);
                System.out.println();
                res = (String) obj.get("id");
                return res;
            } catch (Throwable jSONEx) {
                jSONEx.printStackTrace();

            }
        } catch (MalformedURLException mURLEx) {
            return null;
        } catch (IOException iOEx) {
            return null;
        }
        return res;
    }
}