package com.ibuildapp.FacebookPlugin;

import android.content.res.Resources;
import android.view.View;
import android.widget.GridView;
import android.widget.Toast;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;
import com.ibuildapp.FacebookPlugin.adapter.AdapterVideo;
import com.ibuildapp.FacebookPlugin.core.Facebook;
import com.ibuildapp.FacebookPlugin.core.Plugin;
import com.ibuildapp.FacebookPlugin.core.StaticData;
import com.ibuildapp.FacebookPlugin.core.Utils;
import com.ibuildapp.FacebookPlugin.data.ParserJson;
import com.ibuildapp.FacebookPlugin.model.video.Videos;

import java.util.concurrent.Callable;

public class ActivityVideo extends ActivityParent {

    private static final String FACEBOOK_GRAPH_VIDEOS_REPLACEMENT = "{user-id}";
    private static final String FACEBOOK_GRAPH_VIDEOS = FacebookPlugin.FACEBOOK_GRAPH_URL + FACEBOOK_GRAPH_VIDEOS_REPLACEMENT + "?fields=videos";

    private Resources resources;

    private PullToRefreshGridView gridView;
    private AdapterVideo adapter;

    private String oldVideoUrl;

    @Override
    public void destroy() {
        super.destroy();

        if(adapter != null)
            adapter.kill();

        adapter = null;
    }

    @Override
    protected void backend() {
        resources = getResources();
    }

    @Override
    protected void UI() {
        setContentView(R.layout.activity_video);

        int backgroundColor = StaticData.getXmlParsedData().getColorSkin().getColor1();

        setTopBarTitle(resources.getString(R.string.video));
        setTopBarBackgroundColor(backgroundColor);
        setTopBarLeftButtonTextAndColor(getResources().getString(R.string.back), getResources().getColor(android.R.color.black), true, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        setTopBarTitleColor(getResources().getColor(android.R.color.black));

        gridView = (PullToRefreshGridView)findViewById(R.id.content);
        gridView.setBackgroundColor(backgroundColor);
        gridView.setNumColumns(3);
        gridView.setMode(PullToRefreshBase.Mode.BOTH);

        int padding = Float.valueOf(resources.getDisplayMetrics().density * 2).intValue();

        gridView.getRefreshableView().setPadding(padding, padding, padding, padding);
        gridView.getRefreshableView().setStretchMode(GridView.STRETCH_COLUMN_WIDTH);
        gridView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<GridView>() {
            @Override
            public void onRefresh(PullToRefreshBase<GridView> refreshView) {
                if (refreshView.getCurrentMode() == PullToRefreshBase.Mode.PULL_FROM_START)
                    content(false);
                else
                    oldContent();
            }
        });
    }

    @Override
    protected void content(final boolean cancelable) {
        progressShow(cancelable);

        Plugin.INSTANCE.addTask(new Runnable() {
            @Override
            public void run() {
                try {
                    String jsonVideo = Plugin.INSTANCE.getInFuture(new Callable<Plugin.Result<String>>() {
                        @Override
                        public Plugin.Result<String> call() throws Exception {
                            return new Plugin.Result<String>() {
                                @Override
                                public String getResult() {
                                    return Utils.downloadFile(
                                            ActivityVideo.this,
                                            Facebook.makeAPIRequestOwnerURL("fields=videos")
                                    );
                                }
                            };
                        }
                    }).getResult();

                    Videos videos = ParserJson.parseVideo(jsonVideo);

                    if (videos == null) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                handleErrorInit();
                            }
                        });

                        return;
                    }

                    if (videos.getData() == null || videos.getData().isEmpty()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                handleErrorNoVideo();
                            }
                        });

                        return;
                    }

                    adapter = new AdapterVideo(ActivityVideo.this, videos);

                    try {
                        oldVideoUrl = videos.getPaging().getNext();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            handleErrorInit();
                        }
                    });

                    return;
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        gridView.setAdapter(adapter);

                        progressHide();

                        if (!cancelable)
                            gridView.onRefreshComplete();
                    }
                });
            }
        });
    }

    private void oldContent() {
        progressShow(false);

        Plugin.INSTANCE.addTask(new Runnable() {
            @Override
            public void run() {
                try {
                    String jsonOldVideo = Plugin.INSTANCE.getInFuture(new Callable<Plugin.Result<String>>() {
                        @Override
                        public Plugin.Result<String> call() throws Exception {
                            return new Plugin.Result<String>() {
                                @Override
                                public String getResult() {
                                    return Utils.downloadFile(ActivityVideo.this, oldVideoUrl);
                                }
                            };
                        }
                    }).getResult();

                    Videos oldVideos = ParserJson.parseOldVideo(jsonOldVideo);

                    if (oldVideos == null) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                handleErrorRefresh();
                            }
                        });

                        return;
                    }

                    if (oldVideos.getData() == null || oldVideos.getData().isEmpty()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                handleErrorNoOldVideo();
                            }
                        });

                        return;
                    }

                    adapter.getData().addAll(oldVideos.getData());

                    try {
                        oldVideoUrl = oldVideos.getPaging().getNext();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            handleErrorRefresh();
                        }
                    });

                    return;
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressHide();

                        gridView.onRefreshComplete();
                        adapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    private void handleErrorInit() {
        Toast.makeText(this, getResources().getString(R.string.error_init), Toast.LENGTH_LONG).show();

        finish();
    }

    private void handleErrorNoVideo() {
        Toast.makeText(this, getResources().getString(R.string.error_no_video), Toast.LENGTH_LONG).show();

        finish();
    }

    private void handleErrorRefresh() {
        progressHide();
        gridView.onRefreshComplete();
        Toast.makeText(this, getResources().getString(R.string.error_refresh), Toast.LENGTH_LONG).show();
    }

    private void handleErrorNoOldVideo() {
        progressHide();
        gridView.onRefreshComplete();
        Toast.makeText(this, getResources().getString(R.string.error_no_old_video), Toast.LENGTH_LONG).show();
    }

}
