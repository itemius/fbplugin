package com.ibuildapp.FacebookPlugin;

import android.view.View;
import android.widget.*;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.ibuildapp.FacebookPlugin.adapter.AlbumAdapter;
import com.ibuildapp.FacebookPlugin.adapter.UploadsAdapter;
import com.ibuildapp.FacebookPlugin.model.photos.Album;
import com.ibuildapp.FacebookPlugin.model.photos.Albums;
import com.ibuildapp.FacebookPlugin.model.tabmanager.PhotoTabEventsListener;
import com.ibuildapp.FacebookPlugin.model.tabmanager.TabManager;
import com.ibuildapp.FacebookPlugin.model.uploads.DataContainer;
import com.ibuildapp.FacebookPlugin.model.uploads.Upload;
import com.ibuildapp.FacebookPlugin.model.uploads.Uploads;
import com.ibuildapp.FacebookPlugin.core.StaticData;
import com.ibuildapp.FacebookPlugin.core.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * This activity represents uploads and albums  page.
 */
public class PhotoActivity  extends ActivityParent {

    private RelativeLayout uploadButton;
    private RelativeLayout albumButtom;
    private PullToRefreshGridView gridView;
    private UploadsAdapter uploadsAdapter;
    private AlbumAdapter albumAdapter;
    private TabManager manager;
    private TextView rightTextView;
    private TextView leftTextView;
    private PullToRefreshListView listView;
    private DataContainer<Upload> uploadContainer;
    private DataContainer<Album> albumContainer;

    /**
     * Method for load albums
     */
    private void initAlbums() {
        new Thread( new Runnable() {
            @Override
            public void run() {
                try {
                    PhotoActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressShow(true);
                        }
                    });

                    Albums albums = Utils.loadAlbums(PhotoActivity.this);
                    albums = preareAlbums(albums);
                    albumContainer = new DataContainer<Album>(albums);
                    albumAdapter = new AlbumAdapter(PhotoActivity.this, albumContainer.getList());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            listView.setAdapter(albumAdapter);
                            gridView.setVisibility(View.GONE);
                            progressHide();
                        }
                    });
                } catch (Throwable e) {
                }
            }
        }).start();

    }
    /**
     * Method removes empty albums
     */
    private Albums preareAlbums(Albums albums) {
        List<Album> albumList = new ArrayList<Album>();
        for (Album albm: albums.getData())
            if (albm.getCount()== null || albm.getCount().equals(0))
                continue;
            else albumList.add(albm);

        albums.setData(albumList);
        return albums;

    }

    /**
     * Method for load uploads
     */
    private void initUploads() {
        new Thread( new Runnable() {
            @Override
            public void run() {
                try {
                    PhotoActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressShow(true);
                        }
                    });
                    uploadContainer = new DataContainer(Utils.loadAUploads(PhotoActivity.this));

                    uploadsAdapter = new UploadsAdapter(PhotoActivity.this, uploadContainer.getList());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            gridView.setAdapter(uploadsAdapter);
                            listView.setVisibility(View.GONE);
                            progressHide();
                        }
                    });
                } catch (Throwable e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            gridView.setAdapter(uploadsAdapter);
                            listView.setVisibility(View.GONE);
                            progressHide();
                        }
                    });
                }
            }
        }).start();
    }

    @Override
    protected void backend() {

    }

    /**
     * Method for prepare and load elements of user interface. set visibilities, add listeners, background colors ,etc .
     */
    @Override
    protected void UI() {
        setContentView(R.layout.photo_view_main);
        setTopBarTitle(getResources().getString(R.string.photo));
        swipeBlock();
        setTopBarLeftButtonTextAndColor(getResources().getString(R.string.back), getResources().getColor(android.R.color.black), true, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        setTopBarTitleColor(getResources().getColor(android.R.color.black));

        setTopBarBackgroundColor(StaticData.getXmlParsedData().getColorSkin().getColor1());
        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.photo_main_layout);
        mainLayout.setBackgroundColor(StaticData.getXmlParsedData().getColorSkin().getColor1());

        uploadButton = (RelativeLayout) findViewById(R.id.upload_layout);
        albumButtom = (RelativeLayout)findViewById(R.id.album_layout);
        leftTextView = (TextView) findViewById(R.id.left_text);
        rightTextView = (TextView) findViewById(R.id.right_text);

        manager = new TabManager(this, uploadButton, leftTextView, albumButtom, rightTextView);

        gridView = (PullToRefreshGridView) findViewById(R.id.photo_view_grid);
        Utils.prepareGridView(gridView.getRefreshableView(), getResources());
        gridView.setMode(PullToRefreshBase.Mode.BOTH);

        int padding = Float.valueOf(getResources().getDisplayMetrics().density * 2).intValue();

        gridView.getRefreshableView().setPadding(padding, padding, padding, padding);
        gridView.getRefreshableView().setStretchMode(GridView.STRETCH_COLUMN_WIDTH);

        gridView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<GridView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<GridView> refreshView) {
                new Thread( new Runnable() {
                    @Override
                    public void run() {
                        try {
                            PhotoActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressShow(false);
                                }
                            });

                            Uploads newUploads = Utils.loadAUploads(PhotoActivity.this);
                            if(uploadContainer.addNews(newUploads))
                                uploadsAdapter.setItems(uploadContainer.getList());
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    uploadsAdapter.notifyDataSetChanged();
                                    gridView.onRefreshComplete();
                                    progressHide();
                                }
                            });
                        } catch (Throwable e) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    uploadsAdapter.notifyDataSetChanged();
                                    gridView.onRefreshComplete();
                                    progressHide();
                                }
                            });
                        }
                    }
                }).start();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<GridView> refreshView) {
                if (!"".equals(uploadContainer.getNextUrl()))
                    new Thread( new Runnable() {
                        @Override
                        public void run() {
                            try {
                                PhotoActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressShow(false);
                                    }
                                });
                                uploadContainer.addToEnd(Utils.loadAUploads(PhotoActivity.this, uploadContainer.getNextUrl()));
                                uploadsAdapter.setItems(uploadContainer.getList());
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        uploadsAdapter.notifyDataSetChanged();
                                        gridView.onRefreshComplete();
                                        progressHide();
                                    }
                                });
                            } catch (Throwable e) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        uploadsAdapter.notifyDataSetChanged();
                                        gridView.onRefreshComplete();
                                        progressHide();
                                    }
                                });
                            }
                        }
                    }).start();
                else
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            uploadsAdapter.notifyDataSetChanged();
                            gridView.onRefreshComplete();

                        }
                    });
            }
        });

        listView = (PullToRefreshListView) findViewById(R.id.photo_view_list);
        listView.setMode(PullToRefreshBase.Mode.BOTH);
        listView.setBackgroundColor(StaticData.getXmlParsedData().getColorSkin().getColor1());
        listView.getRefreshableView().setBackgroundColor(StaticData.getXmlParsedData().getColorSkin().getColor1());
        listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        PhotoActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressShow(false);
                            }
                        });

                        Albums newAlbums = Utils.loadAlbums(PhotoActivity.this);
                        newAlbums = preareAlbums(newAlbums);
                        albumContainer.addNews(newAlbums);
                        albumAdapter.setItems(albumContainer.getList());

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                albumAdapter.notifyDataSetChanged();
                                listView.onRefreshComplete();
                                progressHide();
                            }
                        });
                    }
                }).start();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                if (!"".equals(albumContainer.getNextUrl()))
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                PhotoActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressShow(false);
                                    }
                                });
                                Albums albums = Utils.loadAlbums(PhotoActivity.this, albumContainer.getNextUrl());
                                albums = preareAlbums(albums);
                                albumContainer.addToEnd(albums);
                                albumAdapter.setItems(albumContainer.getList());

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        albumAdapter.notifyDataSetChanged();
                                        listView.onRefreshComplete();
                                        progressHide();
                                    }
                                });
                            } catch (Throwable e) {
                                albumAdapter.notifyDataSetChanged();
                                listView.onRefreshComplete();
                                progressHide();
                            }
                        }
                    }).start();
                else
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                albumAdapter.notifyDataSetChanged();
                                listView.onRefreshComplete();
                                progressHide();
                            }
                        });
                    }
                }).start();

            }
        });

        manager.setEventsListener(new PhotoTabEventsListener() {
            @Override
            public void onLeftTabSelected() {
                gridView.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
                if (uploadsAdapter==null)
                    initUploads();
            }

            @Override
            public void onRightTabSelected() {
                gridView.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
                if (albumAdapter==null)
                    initAlbums();
            }
        });
    }
    /**
     * Method for initializing content
     */
    @Override
    protected void content(boolean cancelable) {
        if (manager.isLeftSelected())
            initUploads();
        else initAlbums();
    }

}
