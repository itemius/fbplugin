package com.ibuildapp.FacebookPlugin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;
import com.appbuilder.sdk.android.Widget;
import com.appbuilder.sdk.android.authorization.Authorization;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.ibuildapp.FacebookPlugin.adapter.CommentsAdapter;
import com.ibuildapp.FacebookPlugin.core.Facebook;
import com.ibuildapp.FacebookPlugin.core.StaticData;
import com.ibuildapp.FacebookPlugin.core.Utils;
import com.ibuildapp.FacebookPlugin.model.comments.CommentCountItem;
import com.ibuildapp.FacebookPlugin.model_.JsonFeed;
import com.seppius.i18n.plurals.PluralResources;

import java.io.*;
import java.util.*;

public class CommentsViewActivity extends ActivityParent implements AdapterView.OnItemClickListener {
    public static final int COMMENT_TO_COMMENT_SEND_MESSAGE = 10041;
    public static final int COMMENT_SEND_MESSAGE = 10042;

    /**
     * Strings for facebook graph api query
     */
    public static final String COMMENT_IDS = "COMMENTS_IDS";
    public static final String ID = "ID";
    public static final String FACEBOOK_GRAPH_URL = "https://graph.facebook.com/v2.3/comments/?ids=";
    public static final String FACEBOOK_GRAPH_URL_NEW_COMMENT = "https://graph.facebook.com/v2.3/";
    public static final String FACEBOOK_GRAPH_URL_ACCESS_TOKEN = "access_token=";
    public static final String FACEBOOK_GRAPH_COMMENTS_COUNT = FACEBOOK_GRAPH_URL + COMMENT_IDS + "&summary=1&limit=0&" + FACEBOOK_GRAPH_URL_ACCESS_TOKEN;
    public static final String FACEBOOK_GRAPH_COMMENTS = "https://graph.facebook.com/v2.3/ID/comments/?summary=1&order=reverse_chronological&" + FACEBOOK_GRAPH_URL_ACCESS_TOKEN;

    public static final String FACEBOOK_GRAPH_NEW_COMMENT = FACEBOOK_GRAPH_URL_NEW_COMMENT + COMMENT_IDS + "?" + FACEBOOK_GRAPH_URL_ACCESS_TOKEN;
    public static final int COMMENT_TO_COMMENT_VIEW = 10043;


    private View headerView;
    private ImageView thumbImageView;
    private TextView descriptionTextView;
    private LinearLayout hasCommentsLayout;
    private LinearLayout hasntCommentsLayout;
    private LinearLayout headerLayout;
    private List<JsonFeed.Posts.Post.Comments.Comment> comments;
    private PullToRefreshListView listView;
    private CommentsAdapter adapter;
    private TextView titleTextView;
    private TextView dateTextView;
    private JsonFeed.Posts.Post.Comments comm;

    private final int INITIALIZATION_FAILED = 0;
    private final int LOADING_ABORTED = 1;
    private final int SHOW_PROGRESS_DIALOG = 2;
    private final int HIDE_PROGRESS_DIALOG = 3;
    private final int SHOW_COMMENTS_LIST = 4;
    private final int SHOW_NO_COMMENTS = 5;
    private final int NEED_INTERNET_CONNECTION = 6;
    private final int REFRESH_LIST = 7;
    private final int AUTH = 8;

    private ImageView postCommentButtonTop = null;
    private ProgressDialog progressDialog = null;
    private int position;
    private Widget widget;

    /**
     * Handler for asynch methods
     */
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message message) {
            switch (message.what) {
                case INITIALIZATION_FAILED: {
                    Toast.makeText(CommentsViewActivity.this,
                            getResources().getString(R.string.facebook_alert_cannot_init),
                            Toast.LENGTH_LONG).show();
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            finish();
                        }
                    }, 2000);
                }
                break;
                case LOADING_ABORTED: {
                    closeActivity();
                }
                break;
                case SHOW_PROGRESS_DIALOG: {
                    showProgressDialog();
                }
                break;
                case HIDE_PROGRESS_DIALOG: {
                    hideProgressDialog();
                }
                break;
                case SHOW_COMMENTS_LIST: {
                    showCommentsList();
                }
                break;
                case SHOW_NO_COMMENTS: {
                    showNoComments();
                }
                break;
                case NEED_INTERNET_CONNECTION: {
                    Toast.makeText(CommentsViewActivity.this,
                            getResources().getString(
                                    R.string.facebook_alert_no_internet),
                            Toast.LENGTH_LONG).show();
                }
                break;
                case REFRESH_LIST: {
                    refreshList();
                }
                break;
            }
        }
    };

    /**
     * Initialization backend. Get data from intent. Load facebook commens and comments count
     */
    @Override
    protected void backend() {
        handler.sendEmptyMessage(SHOW_PROGRESS_DIALOG);

        new Thread(new Runnable() {
            public void run() {
                Intent currentIntent = getIntent();
                position = currentIntent.getIntExtra("position", 0);
                String url = FACEBOOK_GRAPH_COMMENTS.replace("ID",StaticData.getJsonFeed().getPosts().getData().get(position).getId() )+ Facebook.getAccessTokenOnThreadPool();
                comm = Utils.loadComments(CommentsViewActivity.this, url);
                comments=comm.getData();
                comments = prepareComments(comments);

                if (comments == null) {
                    handler.sendEmptyMessage(SHOW_NO_COMMENTS);
                    return;
                }
                try {
                    if (comments.isEmpty()) {
                        handler.sendEmptyMessage(SHOW_NO_COMMENTS);
                    } else {
                        handler.sendEmptyMessage(SHOW_COMMENTS_LIST);
                    }
                } catch (NullPointerException nPEx) {
                    handler.sendEmptyMessage(SHOW_NO_COMMENTS);
                }

                handler.sendEmptyMessage(HIDE_PROGRESS_DIALOG);
            }
        }).start();
    }

    /**
     * Sort comments by date. Add to comment entity replies count
     */
    public List<JsonFeed.Posts.Post.Comments.Comment> prepareComments(List<JsonFeed.Posts.Post.Comments.Comment> comments){
        Collections.sort(comments, Collections.reverseOrder(new JsonFeed.Posts.Post.Comments.Comment.CommentComparator(CommentsViewActivity.this)));
        updateCommentCount();

        return comments;
    }

    /**
     * Method for prepare and  load elements of user interface. set visibilities, add listeners, background colors ,etc .
     */
    @Override
    protected void UI() {
        setContentView(R.layout.comments_view);

        setTopBarTitle(getResources().getString(R.string.facebook_replies_capture));
        swipeBlock();
        setTopBarLeftButtonTextAndColor(getResources().getString(R.string.back), getResources().getColor(android.R.color.black), true, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent();
                setResult(RESULT_OK, it);

                finish();
            }
        });
        setTopBarTitleColor(getResources().getColor(android.R.color.black));
        setTopBarBackgroundColor(StaticData.getXmlParsedData().getColorSkin().getColor1());
        View voiceView = LayoutInflater.from(this).inflate(R.layout.facebook_share_btn, null);
        postCommentButtonTop = (ImageView) voiceView.findViewById(R.id.imageView);
        BitmapDrawable b_png = (BitmapDrawable) postCommentButtonTop.getDrawable();
        postCommentButtonTop.setImageDrawable(b_png);
        b_png.setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.MULTIPLY);

        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.facebook_comments_main_layout);
        mainLayout.setBackgroundColor(StaticData.getXmlParsedData().getColorSkin().getColor1());

        listView = (PullToRefreshListView) findViewById(R.id.facebook_comments_main_listview);
        listView.setMode(PullToRefreshBase.Mode.BOTH);
        ListView lv = listView.getRefreshableView();

        lv.setCacheColorHint(StaticData.getXmlParsedData().getColorSkin().getColor1());
        lv.setHeaderDividersEnabled(false);

        headerView = LayoutInflater.from(this).inflate(R.layout.facebook_comments_list_header, null);

        hasCommentsLayout = (LinearLayout) findViewById(R.id.facebook_comments_main_has_layout);
        hasCommentsLayout.setVisibility(View.GONE);
        hasntCommentsLayout = (LinearLayout) findViewById(R.id.facebook_comments_main_hasnt_layout);

        headerLayout = (LinearLayout) findViewById(R.id.facebook_comments_main_header);

        setTopBarRightVeiw(voiceView, false, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo ni = cm.getActiveNetworkInfo();

                if (ni != null) {
                    if (ni.isConnectedOrConnecting()) {
                        if (!Authorization.isAuthorized(Authorization.AUTHORIZATION_TYPE_FACEBOOK))
                            Authorization.authorize(CommentsViewActivity.this, FacebookPlugin.FACEBOOK_COMMENT_AUTH, Authorization.AUTHORIZATION_TYPE_FACEBOOK);
                        else
                        {
                            Intent it = new Intent(CommentsViewActivity.this, SendMessageActivity.class);
                            it.putExtra("user", Authorization.getAuthorizedUser());
                            it.putExtra("id",StaticData.getJsonFeed().getPosts().getData().get(position).getId());
                            CommentsViewActivity.this.startActivityForResult(it, COMMENT_SEND_MESSAGE);
                        }
                    } else {
                        handler.sendEmptyMessage(NEED_INTERNET_CONNECTION);
                    }
                }
            }
        });

        listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String url = FACEBOOK_GRAPH_COMMENTS.replace("ID",StaticData.getJsonFeed().getPosts().getData().get(position).getId() )+ Facebook.getAccessTokenOnThreadPool();
                        JsonFeed.Posts.Post.Comments c = Utils.loadComments(CommentsViewActivity.this, url);
                        List<JsonFeed.Posts.Post.Comments.Comment> newComments = c.getData();
                        List<JsonFeed.Posts.Post.Comments.Comment> cmm = adapter.getItems();
                        cmm.addAll(newComments);

                        HashSet<JsonFeed.Posts.Post.Comments.Comment> hs = new HashSet<JsonFeed.Posts.Post.Comments.Comment>();
                        hs.addAll(cmm);
                        cmm.clear();
                        cmm.addAll(hs);
                        try {
                            prepareComments(cmm);
                        }catch(Throwable e){

                        }
                        final int countInt = c.getJsonSummary().getTotalCount();
                        c.setData(cmm);
                        JsonFeed.Posts.Post currentPost =   StaticData.getJsonFeed().getPosts().getData().get(position);
                        currentPost.setComments(c);

                        adapter.setItems(cmm);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                try {
                                    PluralResources res = new PluralResources(getResources());
                                    String count = res.getQuantityString(R.plurals.numberOfComments, countInt, countInt);//comments.size(), comments.size());
                                    titleTextView.setText(count);
                                } catch (NoSuchMethodException e) {
                                    e.printStackTrace();
                                }
                                adapter.notifyDataSetChanged();
                                listView.onRefreshComplete();
                            }
                        });
                    }
                }).start();

            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                if (comm.getPaging() != null && comm.getPaging().getNext()!=null)
                    new Thread( new Runnable() {
                        @Override
                        public void run() {
                            try {
                                comm = Utils.loadComments(CommentsViewActivity.this, comm.getPaging().getNext());
                                updateCommentCount(comm.getData());
                                adapter.addToEnd(comm);
                                 StaticData.getJsonFeed().getPosts().getData().get(position).getComments().getData().addAll(comm.getData());
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        adapter.notifyDataSetChanged();
                                        listView.onRefreshComplete();
                                    }
                                });
                            } catch (Throwable e) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        adapter.notifyDataSetChanged();
                                        listView.onRefreshComplete();
                                    }
                                } );
                            }
                        }
                    }).start();
                else
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            listView.onRefreshComplete();
                        }
                    } );
            }
        });

        thumbImageView = (ImageView) headerView.findViewById(R.id.facebook_comments_listview_header_thumb);
        thumbImageView.setVisibility(View.GONE);
        String desc = StaticData.getJsonFeed().getPosts().getData().get(position).getMessage();
        titleTextView = (TextView) headerView.findViewById(R.id.facebook_comments_listview_header_title);
        titleTextView.setGravity(Gravity.LEFT | Gravity.CENTER_HORIZONTAL);
        titleTextView.setTextColor(StaticData.getXmlParsedData().getColorSkin().getColor4());
        titleTextView.setText(desc);

        dateTextView = (TextView) headerView.findViewById(R.id.facebook_comments_listview_header_title);
        dateTextView.setGravity(Gravity.LEFT | Gravity.CENTER_HORIZONTAL);
        dateTextView.setTextColor(StaticData.getXmlParsedData().getColorSkin().getColor4());
        dateTextView.setText(desc);

        descriptionTextView = (TextView) headerView.findViewById(R.id.facebook_comments_listview_header_description);
        descriptionTextView.setVisibility(View.GONE);

        headerView.findViewById(R.id.bottom_separator).setBackgroundColor(Color.parseColor(StaticData.getXmlParsedData().getColorSkin().isLight() ? "#33000000" : "#4DFFFFFF"));

    }


    @Override
    protected void content(boolean cancelable) {

    }
    /**
     * Override action on key BACK pressed event
     */
    @Override
    public void onBackPressed() {
        Intent it = new Intent();
        setResult(RESULT_OK, it);

        finish();
    }

    /**
     * Method for hide progress dialog
     */
    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
    /**
     * Method for closing activity
     */
    private void closeActivity() {
        hideProgressDialog();
        finish();
    }

    /**
     * Method for show progress dialog
     */
    private void showProgressDialog() {
        try {
            if (progressDialog.isShowing()) {
                return;
            }
        } catch (NullPointerException nPEx) {
        }

        progressDialog = ProgressDialog.show(this, null, getString(R.string.facebook_loading), true);
        progressDialog.setCancelable(true);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                handler.sendEmptyMessage(LOADING_ABORTED);
            }
        });
    }

    /**
     * Prepare view for show list of comments
     */
    private void showCommentsList() {
        if (comments == null) {
            return;
        }

        if (comments.isEmpty()) {
            return;
        }

        try {
            headerLayout.removeAllViews();
        } catch (Exception ex) {
            Log.d("", "");
        }
        ListView lv = listView.getRefreshableView();
        try {
            lv.removeHeaderView(headerView);
        } catch (Exception ex) {
            Log.d("", "");
        }

        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                100, getResources().getDisplayMetrics());
        headerView.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.FILL_PARENT,
                AbsListView.LayoutParams.WRAP_CONTENT));
        lv.addHeaderView(headerView, null, false);

        hasCommentsLayout.setVisibility(View.VISIBLE);
        hasntCommentsLayout.setVisibility(View.GONE);
        int countInt = comm.getJsonSummary().getTotalCount();
        JsonFeed.Posts.Post currentPost =   StaticData.getJsonFeed().getPosts().getData().get(position);
        currentPost.setComments(comm);

        try {
            PluralResources res = new PluralResources(getResources());
            String count = res.getQuantityString(R.plurals.numberOfComments, countInt, countInt);//comments.size(), comments.size());
            titleTextView.setText(count);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        adapter = new CommentsAdapter(this,  comments, widget, position);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(this);

        handler.sendEmptyMessage(HIDE_PROGRESS_DIALOG);
    }

    /**
     * Override action onActivityResult. Start sendMessageActivty if authorization successful. Add new comment to list
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == FacebookPlugin.FACEBOOK_COMMENT_TO_COMMENT_AUTH && resultCode == RESULT_OK) {
            Intent it = new Intent(this, SendMessageActivity.class);
            it.putExtra("user", Authorization.getAuthorizedUser());
            it.putExtra("id", comments.get(adapter.getCurrentPosition()).getId() );
            startActivityForResult(it, COMMENT_TO_COMMENT_SEND_MESSAGE);
        }
        if (requestCode == FacebookPlugin.FACEBOOK_COMMENT_AUTH && resultCode == RESULT_OK) {
            Intent it = new Intent(this, SendMessageActivity.class);
            it.putExtra("user", Authorization.getAuthorizedUser());
            it.putExtra("id", StaticData.getJsonFeed().getPosts().getData().get(position).getId());
            startActivityForResult(it, COMMENT_SEND_MESSAGE);
        }
        else
        if (requestCode == COMMENT_SEND_MESSAGE && resultCode == RESULT_OK) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    String id = data.getStringExtra("id");
                    String feedUrl = FACEBOOK_GRAPH_NEW_COMMENT.replace(COMMENT_IDS, id);
                    String jsonFeed = Utils.downloadFile(CommentsViewActivity.this, feedUrl + Facebook.getAccessTokenOnThreadPool());
                    Gson gson = new Gson();
                    JsonFeed.Posts.Post.Comments.Comment decoded = null;
                    try {
                        decoded = gson.fromJson(new FileReader(jsonFeed),JsonFeed.Posts.Post.Comments.Comment.class);
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                    if (decoded!=null) {
                        decoded.setCommentsCount(0);
                        comments.add(0, decoded);
                        StaticData.getJsonFeed().getPosts().getData().get(position).setCommentsCount(StaticData.getJsonFeed().getPosts().getData().get(position).getCommentsCount()+1);
                        try {
                            PluralResources res = new PluralResources(getResources());
                            final String count = res.getQuantityString(R.plurals.numberOfComments, StaticData.getJsonFeed().getPosts().getData().get(position).getCommentsCount(),
                                    StaticData.getJsonFeed().getPosts().getData().get(position).getCommentsCount());
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    titleTextView.setText(count);
                                    adapter.notifyDataSetChanged();
                                }
                            });
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        finish();
                    }
                }
            }).start();

        }
        else
        if ((requestCode == COMMENT_TO_COMMENT_SEND_MESSAGE ||requestCode == CommentsToCommentViewActivity.COMMENT_TO_COMMENT_SEND_MESSAGE )&& resultCode == RESULT_OK) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    CommentsViewActivity.this.handler.sendEmptyMessage(SHOW_PROGRESS_DIALOG);
                    updateCommentCount();
                    CommentsViewActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                            CommentsViewActivity.this.handler.sendEmptyMessage(HIDE_PROGRESS_DIALOG);
                        }
                    });
                }
            }).start();

        }
        else
        if (requestCode == COMMENT_TO_COMMENT_VIEW && resultCode == RESULT_OK) {
            {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        CommentsViewActivity.this.handler.sendEmptyMessage(SHOW_PROGRESS_DIALOG);
                        updateCommentCount();
                        CommentsViewActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyDataSetChanged();
                                CommentsViewActivity.this.handler.sendEmptyMessage(HIDE_PROGRESS_DIALOG);
                            }
                        });
                    }
                }).start();
            }
        }
    }

    /**
     * Methods for update replies count for comment list
     */
    private void updateCommentCount() {
        updateCommentCount(this.comments);
        StaticData.getJsonFeed().getPosts().getData().get(position).getComments().setData(comments);
    }
    private void updateCommentCount(List<JsonFeed.Posts.Post.Comments.Comment> comments)
    {
        StringBuilder builder = new StringBuilder();
        for (JsonFeed.Posts.Post.Comments.Comment item :comments)
            builder.append(item.getId()+",");
        if (builder.length()>=1)
            builder.deleteCharAt(builder.length()-1);

        String feedUrl = FACEBOOK_GRAPH_COMMENTS_COUNT.replace(COMMENT_IDS, builder.toString());
        String jsonFeed = Utils.downloadFile(CommentsViewActivity.this, feedUrl + Facebook.getAccessTokenOnThreadPool());
        Gson gson = new Gson();
        Map<String, CommentCountItem> decoded = null;
        try {
            if(jsonFeed!=null)
                decoded = gson.fromJson(new FileReader(jsonFeed), new TypeToken<Map<String, CommentCountItem>>(){}.getType());
        } catch (Throwable e) {
            e.printStackTrace();
        }
        if (decoded!= null)
            for (JsonFeed.Posts.Post.Comments.Comment item :comments) {
                int commentsCount = decoded.containsKey(item.getId())?decoded.get(item.getId()).getSummary().getTotalCount():0;
                item.setCommentsCount(commentsCount);
            }
    }

    /**
     * Prepare view for show empty comment list
     */
    private void showNoComments() {
        try {
            ListView lv=listView.getRefreshableView();
            lv.removeHeaderView(headerView);
        } catch (Exception ex) {
            Log.d("", "");
        }
        titleTextView.setText(R.string.facebook_no_comments);
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                100, getResources().getDisplayMetrics());
        headerLayout.addView(headerView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        hasntCommentsLayout.setVisibility(View.VISIBLE);
        hasCommentsLayout.setVisibility(View.GONE);
    }
    private void refreshList() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }
}
