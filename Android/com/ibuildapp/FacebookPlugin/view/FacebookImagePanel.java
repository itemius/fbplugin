package com.ibuildapp.FacebookPlugin.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import com.ibuildapp.FacebookPlugin.R;

public class FacebookImagePanel extends RelativeLayout {

    private Paint innerPaint, borderPaint;

    public FacebookImagePanel(Context context) {
        super(context);
        init();
    }

    public FacebookImagePanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    /**
     * Creates paints to draw custom background.
     */
    private void init() {
        innerPaint = new Paint();
        innerPaint.setColor(getResources().getColor(R.color.transient_black));
        innerPaint.setAntiAlias(true);

        borderPaint = new Paint();
        borderPaint.setColor(getResources().getColor(R.color.white));
        borderPaint.setAntiAlias(true);
        borderPaint.setStyle(Style.STROKE);
        borderPaint.setStrokeWidth(2);
    }

    /**
     * Sets the custom background inner paint.
     *
     * @param innerPaint
     */
    public void setInnerPaint(Paint innerPaint) {
        this.innerPaint = innerPaint;
    }

    /**
     * Sets the custom background border paint.
     *
     * @param borderPaint
     */
    public void setBorderPaint(Paint borderPaint) {
        this.borderPaint = borderPaint;
    }

    /**
     * Draws LinearLayout with custom background.
     */
    @Override
    protected void dispatchDraw(Canvas canvas) {
        RectF drawRect = new RectF();
        drawRect.set(0, 0, getMeasuredWidth(), getMeasuredHeight());

        canvas.drawRoundRect(drawRect, 5, 5, innerPaint);
        canvas.drawRoundRect(drawRect, 5, 5, borderPaint);

        super.dispatchDraw(canvas);
    }
}
