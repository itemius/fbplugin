package com.ibuildapp.FacebookPlugin.view;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.WindowManager;
import com.ibuildapp.FacebookPlugin.R;

public class TransparentProgressDialog extends Dialog {

    public TransparentProgressDialog(Context context) {
        super(context, R.style.TransparentProgressDialog);
        WindowManager.LayoutParams windowAttributes = getWindow().getAttributes();
        windowAttributes.gravity = Gravity.CENTER_HORIZONTAL;
        getWindow().setAttributes(windowAttributes);
        setTitle(null);
        setCancelable(false);
        setOnCancelListener(null);
    }

}
