package com.ibuildapp.FacebookPlugin.view;

import android.content.Context;
import android.graphics.*;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class GridPhotos extends LinearLayout {

    private final float density = getResources().getDisplayMetrics().density;
    private final int separator = (int)(2 * density);
    private final int border = (int)(10 * density);
    private final ColorDrawable placeholder = new ColorDrawable(Color.parseColor("#80000000"));

    private final LayoutParams full = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
    private final LayoutParams width_weight = new LayoutParams(0, LayoutParams.MATCH_PARENT) {{
        weight = 1;
    }};
    private final LayoutParams height_weight = new LayoutParams(LayoutParams.MATCH_PARENT, 0) {{
        weight = 1;
    }};
    private final LayoutParams height_weight_third = new LayoutParams(LayoutParams.MATCH_PARENT, 0) {{
        weight = 2;
    }};
    private final LayoutParams width_weight_margin_left = new LayoutParams(0, LayoutParams.MATCH_PARENT) {{
        weight = 1;
        leftMargin = separator;
    }};
    private final LayoutParams width_weight_margin_left_right = new LayoutParams(0, LayoutParams.MATCH_PARENT) {{
        weight = 1;
        leftMargin = separator;
        rightMargin = separator;
    }};

    private SquaredLinearLayout top;
    private LinearLayout bottom;

    private SquaredImageView first;
    private SquaredImageView second;
    private SquaredImageView third;

    private SquaredRelativeLayout fourthLayout;
    private SquaredImageView fourth;
    private TextView fourthText;

    public GridPhotos(Context context) {
        super(context);

        init();
    }

    public GridPhotos(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public GridPhotos(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init();
    }

    private void init() {
        removeAllViews();
        setOrientation(VERTICAL);
        setPadding(border, 0, border, 0);

        top = new SquaredLinearLayout(getContext());
        top.setOrientation(HORIZONTAL);
        top.setLayoutParams(height_weight);

        bottom = new LinearLayout(getContext());
        bottom.setOrientation(HORIZONTAL);
        bottom.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 0) {{
            weight = 1;
            bottomMargin = Float.valueOf(20 * density).intValue();
        }});
        bottom.setPadding(0, separator, 0, 0);
        bottom.setVisibility(GONE);

        first = new SquaredImageView(getContext());
        first.setLayoutParams(full);
        first.setScaleType(ImageView.ScaleType.CENTER_CROP);
        first.setVisibility(GONE);

        second = new SquaredImageView(getContext());
        second.setLayoutParams(full);
        second.setScaleType(ImageView.ScaleType.CENTER_CROP);
        second.setVisibility(GONE);

        third = new SquaredImageView(getContext());
        third.setLayoutParams(full);
        third.setSquared(true);
        third.setScaleType(ImageView.ScaleType.CENTER_CROP);
        third.setVisibility(GONE);

        fourthLayout = new SquaredRelativeLayout(getContext());
        fourthLayout.setLayoutParams(full);
        fourthLayout.setVisibility(GONE);

        RelativeLayout.LayoutParams fourthLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        fourth = new SquaredImageView(getContext());
        fourth.setLayoutParams(fourthLayoutParams);
        fourth.setSquared(true);
        fourth.setScaleType(ImageView.ScaleType.CENTER_CROP);

        fourthText = new TextView(getContext());
        fourthText.setLayoutParams(fourthLayoutParams);
        fourthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
        fourthText.setTextColor(getResources().getColor(android.R.color.white));
        fourthText.setGravity(Gravity.CENTER);
        fourthText.setVisibility(GONE);

        fourthLayout.addView(fourth, 0);
        fourthLayout.addView(fourthText, 1);

        top.addView(first, 0);
        top.addView(second, 1);
        bottom.addView(third, 0);
        bottom.addView(fourthLayout, 1);

        addView(top);
        addView(bottom);
    }

    private void setSecondAtTop(boolean top) {
        if(top) {
            if(second != this.top.getChildAt(1)) {
                bottom.removeAllViews();
                bottom.addView(third, 0);
                bottom.addView(fourthLayout, 1);
                this.top.addView(second, 1);
            }
        } else {
            if(second != bottom.getChildAt(0)) {
                this.top.removeView(second);
                bottom.removeAllViews();
                bottom.addView(second, 0);
                bottom.addView(third, 1);
                bottom.addView(fourthLayout, 2);
            }
        }
    }

    public void setImagesCount(int value) {
        first.setImageDrawable(placeholder);
        second.setImageDrawable(placeholder);
        third.setImageDrawable(placeholder);
        fourth.setImageDrawable(placeholder);

        if(value == 0) {
            setVisibility(GONE);
            setSecondAtTop(true);
        } else if(value == 1) {
            setVisibility(VISIBLE);
            setSecondAtTop(true);

            top.setVisibility(VISIBLE);
            bottom.setVisibility(GONE);

            first.setVisibility(VISIBLE);
            second.setVisibility(GONE);
            third.setVisibility(GONE);
            fourthLayout.setVisibility(GONE);

            top.setSquared(true);
            top.setLayoutParams(full);
            first.setSquared(true);
            first.setLayoutParams(new LayoutParams(getWidth(), getWidth()));
        } else if(value == 2) {
            setVisibility(VISIBLE);
            setSecondAtTop(true);

            top.setVisibility(VISIBLE);
            bottom.setVisibility(GONE);

            first.setVisibility(VISIBLE);
            second.setVisibility(VISIBLE);
            third.setVisibility(GONE);
            fourthLayout.setVisibility(GONE);

            top.setSquared(true);
            top.setLayoutParams(full);
            first.setSquared(false);
            first.setLayoutParams(new LayoutParams(0, getWidth()) {{
                weight = 1;
                rightMargin = separator;
            }});
            second.setSquared(false);
            second.setLayoutParams(new LayoutParams(0, getWidth()) {{
                weight = 1;
            }});
        } else if(value == 3) {
            setVisibility(VISIBLE);
            setSecondAtTop(false);

            top.setVisibility(VISIBLE);
            bottom.setVisibility(VISIBLE);

            first.setVisibility(VISIBLE);
            second.setVisibility(VISIBLE);
            third.setVisibility(VISIBLE);
            fourthLayout.setVisibility(GONE);

            top.setSquared(false);
            top.setLayoutParams(height_weight);
            first.setSquared(false);
            first.setLayoutParams(new LayoutParams(getWidth(), getWidth()));
            second.setSquared(true);
            second.setLayoutParams(new LayoutParams(0, getWidth()) {{
                weight = 1;
            }});
            third.setLayoutParams(new LayoutParams(0, getWidth()) {{
                weight = 1;
                leftMargin = separator;
            }});
        } else {
            setVisibility(VISIBLE);
            setSecondAtTop(false);

            top.setVisibility(VISIBLE);
            bottom.setVisibility(VISIBLE);

            first.setVisibility(VISIBLE);
            second.setVisibility(VISIBLE);
            third.setVisibility(VISIBLE);
            fourthLayout.setVisibility(VISIBLE);

            top.setSquared(false);
            top.setLayoutParams(height_weight_third);
            first.setSquared(false);
            first.setLayoutParams(new LayoutParams(getWidth(), getWidth()));
            second.setSquared(true);
            second.setLayoutParams(new LayoutParams(0, getWidth()) {{
                weight = 1;
            }});
            third.setLayoutParams(new LayoutParams(0, getWidth()) {{
                weight = 1;
                leftMargin = separator;
                rightMargin = separator;
            }});
            fourthLayout.setLayoutParams(new LayoutParams(0, getWidth()) {{
                weight = 1;
            }});

            if(value > 4) {
                fourthText.setVisibility(VISIBLE);
                fourthText.setText("+" + (value - 4));
            } else {
                fourthText.setVisibility(GONE);
                fourthText.setText("");
            }
        }
    }

    public ImageView getFirst() {
        return first;
    }

    public ImageView getSecond() {
        return second;
    }

    public ImageView getThird() {
        return third;
    }

    public ImageView getFourth() {
        return fourth;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    private class SquaredImageView extends ImageView {

        private boolean squared;

        public SquaredImageView(Context context) {
            super(context);
        }

        public SquaredImageView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public SquaredImageView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
        }

        private void setSquared(boolean value) {
            squared = value;
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(widthMeasureSpec, squared ? widthMeasureSpec : heightMeasureSpec);
        }

    }

    private class SquaredRelativeLayout extends RelativeLayout {

        public SquaredRelativeLayout(Context context) {
            super(context);
        }

        public SquaredRelativeLayout(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public SquaredRelativeLayout(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
        }

        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(widthMeasureSpec, widthMeasureSpec);
        }

    }

    private class SquaredLinearLayout extends LinearLayout {

        private boolean squared;

        public SquaredLinearLayout(Context context) {
            super(context);
        }

        public SquaredLinearLayout(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public SquaredLinearLayout(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
        }

        private void setSquared(boolean value) {
            squared = value;
        }

        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(widthMeasureSpec, squared ? widthMeasureSpec : heightMeasureSpec);
        }

    }

}
