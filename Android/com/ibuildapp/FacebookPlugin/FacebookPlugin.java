package com.ibuildapp.FacebookPlugin;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.text.*;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.*;
import android.widget.*;
import com.appbuilder.sdk.android.Widget;
import com.appbuilder.sdk.android.authorization.Authorization;
import com.appbuilder.sdk.android.authorization.FacebookAuthorizationActivity;
import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.ibuildapp.FacebookPlugin.adapter.AdapterFeed;
import com.ibuildapp.FacebookPlugin.adapter.ViewHolder;
import com.ibuildapp.FacebookPlugin.core.Facebook;
import com.ibuildapp.FacebookPlugin.core.Plugin;
import com.ibuildapp.FacebookPlugin.core.StaticData;
import com.ibuildapp.FacebookPlugin.core.Utils;
import com.ibuildapp.FacebookPlugin.data.ParserJson;
import com.ibuildapp.FacebookPlugin.data.ParserXml;
import com.ibuildapp.FacebookPlugin.model.photos.Album;
import com.ibuildapp.FacebookPlugin.model.photos.Albums;
import com.ibuildapp.FacebookPlugin.model_.JsonFeed;
import com.seppius.i18n.plurals.PluralResources;

import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Callable;

public class FacebookPlugin extends ActivityParent {

    public static final int FACEBOOK_LIKE_AUTH = 10037;
    public static final int FACEBOOK_COMMENT_TO_COMMENT_AUTH = 10038;
    public static final int FACEBOOK_COMMENT_AUTH = 10039;

    public static final String FACEBOOK_GRAPH_URL = "https://graph.facebook.com/v2.3/";
    public static final String FACEBOOK_GRAPH_URL_ACCESS_TOKEN = "access_token=";

    public static final String FEED_ITEM_IDS = "fiids";
    public static final String FEED_ITEMS_ATTACHMENT = "?ids="+FEED_ITEM_IDS+"&fields=attachments&";

    private static final String EXTRA_WIDGET = "Widget";
    private static final String FACEBOOK_GRAPH_FEED_URL_REPLACE_ENTITY = "fb_id";
    private static final String FACEBOOK_GRAPH_FEED_URL = FACEBOOK_GRAPH_URL + FACEBOOK_GRAPH_FEED_URL_REPLACE_ENTITY + "?fields=id,name,picture,feed&order=reverse_chronological&" + FACEBOOK_GRAPH_URL_ACCESS_TOKEN;
    public static final int FACEBOOK_MAIN_COMMENT_AUTH = 10036 ;
    public static final int COMMENTS_VIEW = 10037 ;

    private PullToRefreshListView feed;
    private View feed_header;

    private String xml;
    private String title;
    private PluralResources pluralResources;
    private Resources resources;
    private AdapterFeed adapterFeed;
    private WebView workaroundLikeButton;

    private String likeObject;
    private String oldFeedUrl;
    private String shareUrl;

    private boolean isPageLiked;

    @Override
    public void finish() {
        destroy();

        super.finish();
    }

    @Override
    public void destroy() {
        super.destroy();

        if(adapterFeed != null)
            adapterFeed.kill();

        adapterFeed = null;
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapterFeed.notifyDataSetChanged();
            }
        });

        if(requestCode == FACEBOOK_LIKE_AUTH && resultCode == RESULT_OK) {
            if(!TextUtils.isEmpty(likeObject)) {
                Plugin.INSTANCE.addTask(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (Utils.complexLike(likeObject)) {
                                List<JsonFeed.Posts.Post> posts = adapterFeed.getData().getPosts().getData();
                                List<String> likes = new ArrayList<String>();

                                if (Authorization.isAuthorized(Authorization.AUTHORIZATION_TYPE_FACEBOOK))
                                    likes.addAll(FacebookAuthorizationActivity.getUserOgLikes());

                                for (final JsonFeed.Posts.Post post : posts) {
                                    if (likes.contains(Utils.getLikeUrl(post)))
                                        post.setLiked(true);

                                    if (likeObject.equals(post.getId()))
                                        post.setLikesCount(post.getLikesCount() + 1);
                                }

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        adapterFeed.notifyDataSetChanged();
                                    }
                                });
                            }

                            likeObject = "";
                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    }
                });
            } else if(!TextUtils.isEmpty(shareUrl)) {
                Intent intent = new Intent(this, ActivitySharing.class);
                intent.putExtra(ActivitySharing.EXTRA_URL, shareUrl);
                startActivityForResult(intent, ActivitySharing.REQUEST_CODE);

                shareUrl = "";
            } else {
                Plugin.INSTANCE.addTask(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            isPageLiked = Plugin.INSTANCE.getInFuture(new Callable<Plugin.Result<Boolean>>() {
                                @Override
                                public Plugin.Result<Boolean> call() throws Exception {
                                    return new Plugin.Result<Boolean>() {
                                        @Override
                                        public Boolean getResult() throws Exception {
                                            String request = Facebook.makeAPIRequestCurrentUserURL("fields=engagement");

                                            return !TextUtils.isEmpty(request) && Utils.downloadFileAsString(request).toLowerCase().contains("you");
                                        }
                                    };
                                }
                            }).getResult();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    feed_header.findViewById(R.id.like_image).setBackgroundResource(isPageLiked ? R.drawable.liked : R.drawable.facebook_plugin_like);
                                    ((TextView)feed_header.findViewById(R.id.like_text)).setText(resources.getString(isPageLiked ? R.string.like_pressed : R.string.like_unpressed));
                                    ((TextView)feed_header.findViewById(R.id.like_text)).setTextColor(resources.getColor(isPageLiked ? R.color.header_layout_like_pressed_text_color : R.color.header_layout_text_color));
                                }
                            });
                        } catch(Exception exception) {
                            exception.printStackTrace();
                        }
                    }
                });
            }
        }

        else if(requestCode == ActivitySharing.REQUEST_CODE) {
            Toast.makeText(this, resources.getString(resultCode == RESULT_OK ? R.string.shared_on_facebook : R.string.not_shared_on_facebook), Toast.LENGTH_LONG).show();
        }
        if (requestCode == FACEBOOK_MAIN_COMMENT_AUTH && resultCode == RESULT_OK) {
            Intent it = new Intent(this, SendMessageActivity.class);
            it.putExtra("user", Authorization.getAuthorizedUser());
            it.putExtra("id", adapterFeed.getData().getPosts().getData().get(adapterFeed.getCurrentPosition()).getId());
            startActivityForResult(it, CommentsViewActivity.COMMENT_SEND_MESSAGE);
        }
        if (requestCode == CommentsViewActivity.COMMENT_SEND_MESSAGE && resultCode == RESULT_OK) {
            Plugin.INSTANCE.addTask(new Runnable() {
                @Override
                public void run() {
                    String id = data.getStringExtra("id");
                    String feedUrl = CommentsViewActivity.FACEBOOK_GRAPH_NEW_COMMENT.replace(CommentsViewActivity.COMMENT_IDS, id);
                    String jsonFeed = Utils.downloadFile(FacebookPlugin.this, feedUrl + Facebook.getAccessTokenOnThreadPool());
                    Gson gson = new Gson();
                    JsonFeed.Posts.Post.Comments.Comment decoded = null;
                    try {
                        decoded = gson.fromJson(new FileReader(jsonFeed), JsonFeed.Posts.Post.Comments.Comment.class);
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                    if (decoded != null) {
                        decoded.setCommentsCount(0);
                        JsonFeed.Posts.Post post = adapterFeed.getData().getPosts().getData().get(adapterFeed.getCurrentPosition());
                        if (post.getComments() == null) {
                            JsonFeed.Posts.Post.Comments com = new JsonFeed.Posts.Post.Comments();
                            com.setData(new ArrayList<JsonFeed.Posts.Post.Comments.Comment>());
                            post.setComments(com);
                        }
                        post.getComments().getData().add(0, decoded);
                        post.setCommentsCount(1);
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapterFeed.notifyDataSetChanged();
                        }
                    });
                }
            });
        }

    }

    @Override
    protected void backend() {
        Intent intent = getIntent();
        Widget widget = (Widget)intent.getSerializableExtra(EXTRA_WIDGET);

        if(widget == null) {
            handleErrorInit();

            return;
        }

        xml = widget.getPluginXmlData();

        if(TextUtils.isEmpty(xml)) {
            handleErrorInit();

            return;
        }

        resources = getResources();
        try {
            pluralResources = new PluralResources(getResources());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        title = widget.getTitle();
    }

    @Override
    protected void UI() {
        setContentView(R.layout.facebook_plugin);

        setTopBarTitle(TextUtils.isEmpty(title) ? getString(R.string.facebook) : title);
        setTopBarLeftButtonTextAndColor(resources.getString(R.string.home), resources.getColor(android.R.color.black), true, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        setTopBarTitleColor(getResources().getColor(android.R.color.black));

        feed = (PullToRefreshListView)findViewById(R.id.content);
        feed.setRecyclerListener(new AbsListView.RecyclerListener() {
            @Override
            public void onMovedToScrapHeap(View view) {
                Plugin.INSTANCE.cancelTask((Runnable) ViewHolder.get(view, R.id.icon).getTag(R.id.TAG_TASK));
                Plugin.INSTANCE.cancelTask((Runnable) ViewHolder.get(view, R.id.picture).getTag(R.id.TAG_TASK));
                Plugin.INSTANCE.cancelTask((Runnable) ViewHolder.get(view, R.id.picture21).getTag(R.id.TAG_TASK));
                Plugin.INSTANCE.cancelTask((Runnable) ViewHolder.get(view, R.id.picture22).getTag(R.id.TAG_TASK));
                Plugin.INSTANCE.cancelTask((Runnable) ViewHolder.get(view, R.id.picture3).getTag(R.id.TAG_TASK));
                Plugin.INSTANCE.cancelTask((Runnable) ViewHolder.get(view, R.id.picture31).getTag(R.id.TAG_TASK));
                Plugin.INSTANCE.cancelTask((Runnable) ViewHolder.get(view, R.id.picture32).getTag(R.id.TAG_TASK));
                Plugin.INSTANCE.cancelTask((Runnable) ViewHolder.get(view, R.id.picture4).getTag(R.id.TAG_TASK));
                Plugin.INSTANCE.cancelTask((Runnable) ViewHolder.get(view, R.id.picture41).getTag(R.id.TAG_TASK));
                Plugin.INSTANCE.cancelTask((Runnable) ViewHolder.get(view, R.id.picture42).getTag(R.id.TAG_TASK));
                Plugin.INSTANCE.cancelTask((Runnable) ViewHolder.get(view, R.id.picture43).getTag(R.id.TAG_TASK));
            }
        });
        feed.setMode(PullToRefreshBase.Mode.BOTH);
        feed_header = LayoutInflater.from(this).inflate(R.layout.facebook_plugin_feed_header, feed, false);

        feed_header.findViewById(R.id.photo_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FacebookPlugin.this, PhotoActivity.class));
            }
        });
        feed_header.findViewById(R.id.like_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLikeItemClick();
            }
        });
        workaroundLikeButton = (WebView)feed_header.findViewById(R.id.test);
        workaroundLikeButton.getSettings().setJavaScriptEnabled(true);
        workaroundLikeButton.setWebChromeClient(new WebChromeClient());
        workaroundLikeButton.setWebViewClient(new WebViewClient());

        feed_header.findViewById(R.id.video_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FacebookPlugin.this, ActivityVideo.class));
            }
        });

        feed.setHeaderView(feed_header);
        feed.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                if(refreshView.getCurrentMode() == PullToRefreshBase.Mode.PULL_FROM_START)
                    content(false);
                else
                    oldContent();
            }
        });
    }

    private void setLikeItemClick() {
        if(Authorization.isAuthorized(Authorization.AUTHORIZATION_TYPE_FACEBOOK)) {
            MotionEvent down = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 30, 30, 0);
            MotionEvent up = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 30, 30, 0);

            workaroundLikeButton.dispatchTouchEvent(down);
            workaroundLikeButton.dispatchTouchEvent(up);

            if(isPageLiked && Build.VERSION.SDK_INT < 11)
                workaroundLikeButton.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        MotionEvent down = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 30, 30, 0);
                        MotionEvent up = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 30, 30, 0);

                        workaroundLikeButton.dispatchTouchEvent(down);
                        workaroundLikeButton.dispatchTouchEvent(up);
                    }
                }, 500);

            isPageLiked = !isPageLiked;

            feed_header.findViewById(R.id.like_image).setBackgroundResource(isPageLiked ? R.drawable.liked : R.drawable.facebook_plugin_like);
            ((TextView)feed_header.findViewById(R.id.like_text)).setText(resources.getString(isPageLiked ? R.string.like_pressed : R.string.like_unpressed));
            ((TextView)feed_header.findViewById(R.id.like_text)).setTextColor(resources.getColor(isPageLiked ? R.color.header_layout_like_pressed_text_color : R.color.header_layout_text_color));

            try {
                int likes = StaticData.getJsonFeed().getLikes() + (isPageLiked ? 1 : -1);
                StaticData.getJsonFeed().setLikes(likes);
                String count = pluralResources.getQuantityString(R.plurals.numberOfLikes, likes, likes);
                ((TextView) feed_header.findViewById(R.id.likes)).setText(count);
            } catch (Throwable e) {
                e.printStackTrace();
            }
        } else
            Authorization.authorize(this, FacebookPlugin.FACEBOOK_LIKE_AUTH, Authorization.AUTHORIZATION_TYPE_FACEBOOK);
    }

    @Override
    protected void content(final boolean cancelable) {
        progressShow(cancelable);

        Plugin.INSTANCE.addTask(new Runnable() {
            @Override
            public void run() {
                StaticData.setXmlParsedData(ParserXml.parse(xml));

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ParserXml.ParseResult xmlParsedData = StaticData.getXmlParsedData();

                        if (xmlParsedData == null) {
                            handleErrorInit();

                            return;
                        }

                        feed.setBackgroundColor(StaticData.getXmlParsedData().getColorSkin().getColor1());
                        setTopBarBackgroundColor(StaticData.getXmlParsedData().getColorSkin().getColor1());

                        if(Build.VERSION.SDK_INT < 19)
                            workaroundLikeButton.loadDataWithBaseURL("http://",
                                    "<html><body>" +
                                            "<div id=\"fb-root\"></div>" +
                                            "<script>(function(d, s, id) {" +
                                            "  var js, fjs = d.getElementsByTagName(s)[0];" +
                                            "  if (d.getElementById(id)) return;" +
                                            "  js = d.createElement(s); js.id = id;" +
                                            "  js.src = \"//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.3&appId=1411757279101816\";" +
                                            "  fjs.parentNode.insertBefore(js, fjs);" +
                                            "}(document, 'script', 'facebook-jssdk'));</script>" +
                                            "<div class=\"fb-like\" data-href=\"https://www.facebook.com/" + xmlParsedData.getFacebookId() + "\" data-layout=\"button\" data-action=\"like\" data-show-faces=\"false\" data-share=\"false\" />" +
                                            "</body></html>"
                                    , "text/html", "utf-8", "");
                        else
                            workaroundLikeButton.loadUrl("https://www.facebook.com/plugins/like.php?" +
                                    "layout=button&show_faces=false&width=80&height=50&action=like&colorscheme=light&" +
                                    "href=https://www.facebook.com/" + xmlParsedData.getFacebookId());
                    }
                });

                try {
                    final JsonFeed jsonFeed = prepareFeed(
                            Facebook.makeAPIRequestOwnerURL(
                                    getResources().getString(R.string.locale) +
                                            "fields=" +
                                            "name," +
                                            "category," +
                                            "likes," +
                                            "cover," +
                                            "picture," +
                                            "videos.limit(0).summary(1)," +
                                            "posts" +
                                            "{" +
                                            "id," +
                                            "message," +
                                            "source," +
                                            "type," +
                                            "created_time," +
                                            "attachments{media{image{src}},type,subattachments{type,media{image{src}}}}," +
                                            "likes.limit(0).summary(1)," +
                                            "comments.limit(25).summary(1).order(reverse_chronological)" +
                                            "}" +
                                            "&order=reverse_chronological"
                            )
                    );

                    int photosCount = 0;

                    final List<String> next = new ArrayList<String>() {{
                        add(Facebook.makeAPIRequestOwnerURL("/albums?fields=count"));
                    }};

                    do {
                        Albums albums = ParserJson.parseAlbums(Plugin.INSTANCE.getInFuture(new Callable<Plugin.Result<String>>() {
                            @Override
                            public Plugin.Result<String> call() throws Exception {
                                return new Plugin.Result<String>() {
                                    @Override
                                    public String getResult() throws Exception {
                                        return Utils.downloadFileAsString(next.get(0));
                                    }
                                };
                            }
                        }).getResult());

                        for (Album album : albums.getData())
                            photosCount += album.getCount() != null ? album.getCount() : 0;

                        next.set(0, albums.getPaging().getNext());
                    } while (!TextUtils.isEmpty(next.get(0)) && URLUtil.isValidUrl(next.get(0)));

                    jsonFeed.setPhotosCount(photosCount);

                    isPageLiked = Plugin.INSTANCE.getInFuture(new Callable<Plugin.Result<Boolean>>() {
                        @Override
                        public Plugin.Result<Boolean> call() throws Exception {
                            return new Plugin.Result<Boolean>() {
                                @Override
                                public Boolean getResult() throws Exception {
                                    String request = Facebook.makeAPIRequestCurrentUserURL("fields=engagement");

                                    return !TextUtils.isEmpty(request) && Utils.downloadFileAsString(request).toLowerCase().contains("you");
                                }
                            };
                        }
                    }).getResult();

                    try {
                        oldFeedUrl = jsonFeed.getPosts().getPaging().getNext();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }

                    StaticData.setJsonFeed(jsonFeed);
                    adapterFeed = new AdapterFeed(FacebookPlugin.this, jsonFeed, feed_header);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((TextView) feed_header.findViewById(R.id.name)).setText(jsonFeed.getName());
                            ((TextView) feed_header.findViewById(R.id.category)).setText(jsonFeed.getCategory());
                            ((TextView) feed_header.findViewById(R.id.video_count)).setText("(" + jsonFeed.getVideosCount() + ")");
                            ((TextView) feed_header.findViewById(R.id.photo_count)).setText("(" + jsonFeed.getPhotosCount() + ")");

                            feed_header.findViewById(R.id.like_image).setBackgroundResource(isPageLiked ? R.drawable.liked : R.drawable.facebook_plugin_like);
                            ((TextView)feed_header.findViewById(R.id.like_text)).setText(resources.getString(isPageLiked ? R.string.like_pressed : R.string.like_unpressed));
                            ((TextView)feed_header.findViewById(R.id.like_text)).setTextColor(resources.getColor(isPageLiked ? R.color.header_layout_like_pressed_text_color : R.color.header_layout_text_color));

                            try {
                                String count = pluralResources.getQuantityString(R.plurals.numberOfLikes, jsonFeed.getLikes(), jsonFeed.getLikes());
                                ((TextView) feed_header.findViewById(R.id.likes)).setText(count);

                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                            feed.setAdapter(adapterFeed);

                            progressHide();

                            if (!cancelable)
                                feed.onRefreshComplete();
                        }
                    });
                } catch(Exception exception) {
                    exception.printStackTrace();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            handleErrorInit();
                        }
                    });

                    return;
                }
            }
        });
    }

    private void oldContent() {
        progressShow(false);

        Plugin.INSTANCE.addTask(new Runnable() {
            @Override
            public void run() {
                if (TextUtils.isEmpty(oldFeedUrl) || !URLUtil.isValidUrl(oldFeedUrl)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            handleErrorNoOldFeed();
                        }
                    });

                    return;
                }

                try {
                    JsonFeed.Posts posts = preparePostsOnly(oldFeedUrl);

                    try {
                        oldFeedUrl = posts.getPaging().getNext();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }

                    StaticData.getJsonFeed().getPosts().getData().addAll(posts.getData());
                    adapterFeed.getData().getPosts().getData().addAll(posts.getData());
                } catch (Exception exception) {
                    exception.printStackTrace();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            handleErrorRefresh();
                        }
                    });

                    return;
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressHide();

                        feed.onRefreshComplete();
                        adapterFeed.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    private class LinkSpan extends URLSpan {
        public LinkSpan(String url) {
            super(url);
        }

        @Override
        public void onClick(View view) {
            String url = getURL();

            startActivity(Utils.detectYoutube(url) && android.os.Build.VERSION.SDK_INT < 11 ?
                    new Intent(Intent.ACTION_VIEW, Uri.parse(url)) :
                    new Intent(FacebookPlugin.this, WebViewActivity.class).putExtra("url", url));
        }
    }

    private JsonFeed.Posts preparePostsOnly(final String url) throws Exception {
        JsonFeed.Posts posts = ParserJson.parsePostsOnly(Plugin.INSTANCE.getInFuture(new Callable<Plugin.Result<String>>() {
            @Override
            public Plugin.Result<String> call() throws Exception {
                return new Plugin.Result<String>() {
                    @Override
                    public String getResult() throws Exception {
                        return Utils.downloadFile(FacebookPlugin.this, url);
                    }
                };
            }
        }).getResult());

        preparePosts(posts.getData());

        return posts;
    }

    private JsonFeed prepareFeed(final String url) throws Exception {
        JsonFeed jsonFeed = ParserJson.parseFeed(Plugin.INSTANCE.getInFuture(new Callable<Plugin.Result<String>>() {
            @Override
            public Plugin.Result<String> call() throws Exception {
                return new Plugin.Result<String>() {
                    @Override
                    public String getResult() throws Exception {
                        return Utils.downloadFile(FacebookPlugin.this, url);
                    }
                };
            }
        }).getResult());

        preparePosts(jsonFeed.getPosts().getData());

        return jsonFeed;
    }

    private void preparePosts(List<JsonFeed.Posts.Post> posts) {
        SimpleDateFormat parseDateFormat = new SimpleDateFormat(getString(R.string.parse_date_format));
        SimpleDateFormat dateFormat = new SimpleDateFormat(getString(R.string.date_fromat));

        for(JsonFeed.Posts.Post post : posts) {

            Date parsedDate;

            try {
                parsedDate = parseDateFormat.parse(post.getCreatedTime());
            } catch(Exception exception) {
                parsedDate = Calendar.getInstance().getTime();
            }

            post.setFormattedCreatedTime(dateFormat.format(parsedDate));

            String message = post.getMessage();

            final String TAG_OPEN_IFRAME = "<iframe";
            final String TAG_CLOSE_IFRAME = "</iframe>";
            final String SRC = "src=\"";
            final String TAG_BR = "<br/>";
            final String CRLF = "(\r\n|\n)";
            final String SPACE = " ";

            if(!TextUtils.isEmpty(message)) {
                while (message.contains(TAG_OPEN_IFRAME)) {
                    String src = message.substring(message.lastIndexOf(SRC) + SRC.length());

                    for (int i = 0; i < src.length(); i++)
                        if (src.charAt(i) == '\"') {
                            src = src.substring(0, i);

                            break;
                        }

                    message = message.replace(message.substring(message.lastIndexOf(TAG_OPEN_IFRAME), message.lastIndexOf(TAG_CLOSE_IFRAME) + TAG_CLOSE_IFRAME.length()), SPACE + src);
                }

                Spannable spannableMessage;

                try {
                    message = message.replaceAll(CRLF, TAG_BR);
                    spannableMessage = new SpannableString(Html.fromHtml(message));
                    Linkify.addLinks(spannableMessage, Linkify.WEB_URLS);
                    URLSpan[] spans = spannableMessage.getSpans(0, spannableMessage.length(), URLSpan.class);

                    for (URLSpan urlSpan : spans) {
                        spannableMessage.setSpan(new LinkSpan(urlSpan.getURL()), spannableMessage.getSpanStart(urlSpan), spannableMessage.getSpanEnd(urlSpan), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        spannableMessage.removeSpan(urlSpan);
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();

                    spannableMessage = new SpannableString(message);
                }

                post.setSpannableMessage(spannableMessage);
            }

            List<String> attachedImages = new ArrayList<String>();

            if(post.getAttachments() != null)
                for (JsonFeed.Posts.Post.Attachments.Attachment attachment : post.getAttachments().getData()) {
                    if(attachment.getSubattachments() != null)
                        for (JsonFeed.Posts.Post.Attachments.Attachment.Subattachments.Subattachment subattachment : attachment.getSubattachments().getData())
                            attachedImages.add(getUrlFromSafeImage(subattachment.getJsonMedia().getImage().getSrc()));
                    else if(attachment.getJsonMedia() != null)
                        attachedImages.add(getUrlFromSafeImage(attachment.getJsonMedia().getImage().getSrc()));
                }

            post.setAttachedImages(attachedImages);
        }

    }

    private String getUrlFromSafeImage(String url) {
        final String safe_image_php = "safe_image.php?";
        final String url_equals = "url=";
        final String jpg = ".jpg";
        final String _AM = "&";

        if(url.contains(safe_image_php) && url.contains(url_equals)) {
            url = Uri.decode(url.substring(url.lastIndexOf(url_equals) + url_equals.length()));

            if(url.contains(jpg))
                url = url.substring(0, url.lastIndexOf(jpg) + jpg.length());
        }

        return url;
    }

    private void handleErrorInit() {
        Toast.makeText(this, getResources().getString(R.string.error_init), Toast.LENGTH_LONG).show();

        finish();
    }

    private void handleErrorRefresh() {
        progressHide();
        feed.onRefreshComplete();
        Toast.makeText(this, getResources().getString(R.string.error_refresh), Toast.LENGTH_LONG).show();
    }

    private void handleErrorNoOldFeed() {
        progressHide();
        feed.onRefreshComplete();
        Toast.makeText(this, getResources().getString(R.string.error_no_old_feed), Toast.LENGTH_LONG).show();
    }

    public void setLikeObject(String likeObject) {
        this.likeObject = this.likeObject != null ? this.likeObject : likeObject;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = this.shareUrl != null ? this.shareUrl : shareUrl;
    }
}
