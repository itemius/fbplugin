package com.ibuildapp.FacebookPlugin.core;

import android.content.Context;
import android.text.TextUtils;
import com.ibuildapp.FacebookPlugin.data.ParserJson;
import com.ibuildapp.FacebookPlugin.data.ParserXml;
import com.ibuildapp.FacebookPlugin.model.feed.PageInfo;
import com.ibuildapp.FacebookPlugin.model_.JsonFeed;
import com.ibuildapp.FacebookPlugin.model.uploads.Upload;

import java.io.File;
import java.util.List;

public class StaticData {

    private static final String PLUGIN_CACHE_FOLDER = com.appbuilder.sdk.android.Utils.md5("FacebookPlugin");

    private static ParserXml.ParseResult xmlParsedData;
    private static JsonFeed jsonFeed;
    private static String cachePath;
    private static String accessToken;
    private static List<Upload> viewImages;

    public static ParserXml.ParseResult getXmlParsedData() {
        return xmlParsedData;
    }

    public static JsonFeed getJsonFeed() {
        return jsonFeed;
    }

    public static void setJsonFeed(JsonFeed jsonFeed) {
        StaticData.jsonFeed = jsonFeed;
    }

    public static void setXmlParsedData(ParserXml.ParseResult xmlParsedData) {
        StaticData.xmlParsedData = xmlParsedData;
        cachePath = null;
    }

    public static String getCachePath(Context context) {
        if(TextUtils.isEmpty(cachePath))
            cachePath = context.getExternalCacheDir() + File.separator + PLUGIN_CACHE_FOLDER + File.separator;

        return cachePath;
    }

    public static void setAccessToken(String accessToken) {
        StaticData.accessToken = accessToken;
    }

    public static String getAccessToken() {
        return accessToken;
    }

    public static void setViewImages(List<Upload> viewImages) {
        StaticData.viewImages = viewImages;
    }

    public static List<Upload> getViewImages() {
        return viewImages;
    }
}
