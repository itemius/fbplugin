package com.ibuildapp.FacebookPlugin.core;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.widget.GridView;
import com.appbuilder.sdk.android.authorization.FacebookAuthorizationActivity;
import com.google.gson.Gson;
import com.ibuildapp.FacebookPlugin.FacebookPlugin;
import com.ibuildapp.FacebookPlugin.R;
import com.ibuildapp.FacebookPlugin.model.photos.Albums;
import com.ibuildapp.FacebookPlugin.model.replies.Replies;
import com.ibuildapp.FacebookPlugin.model.uploads.Uploads;
import com.ibuildapp.FacebookPlugin.model_.JsonFeed;
import org.apache.http.HttpStatus;

import java.io.*;
import java.net.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Utils {

    private static final String SHARING_URL_BASE_REPLACEMENT = "{user-id}";
    private static final String SHARING_URL_BASE = "https://facebook.com/" + SHARING_URL_BASE_REPLACEMENT + "/posts/";

    public static String downloadFile(Context context, String url) {
       return downloadFile(context, url, com.appbuilder.sdk.android.Utils.md5(url));
    }
    public static String downloadFile(Context context, String url, String md5) {
        final int BYTE_ARRAY_SIZE = 8024;
        final int CONNECTION_TIMEOUT = 30000;
        final int READ_TIMEOUT = 30000;

        try {
            for(int i = 0; i < 3; i++) {
                URL fileUrl = new URL(URLDecoder.decode(url));
                HttpURLConnection connection = (HttpURLConnection)fileUrl.openConnection();
                connection.setConnectTimeout(CONNECTION_TIMEOUT);
                connection.setReadTimeout(READ_TIMEOUT);
                connection.connect();

                int status = connection.getResponseCode();

                if(status >= HttpStatus.SC_BAD_REQUEST) {
                    connection.disconnect();

                    continue;
                }

                BufferedInputStream bufferedInputStream = new BufferedInputStream(connection.getInputStream());
                File file = new File(StaticData.getCachePath(context) + md5);

                if(!file.exists()) {
                    new File(StaticData.getCachePath(context)).mkdirs();
                    file.createNewFile();
                }

                FileOutputStream fileOutputStream = new FileOutputStream(file, false);
                int byteCount;
                byte[] buffer = new byte[BYTE_ARRAY_SIZE];

                while ((byteCount = bufferedInputStream.read(buffer, 0, BYTE_ARRAY_SIZE)) != -1)
                    fileOutputStream.write(buffer, 0, byteCount);

                bufferedInputStream.close();
                fileOutputStream.flush();
                fileOutputStream.close();

                return file.getAbsolutePath();
            }
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

        return null;
    }

    public static String downloadFileAsString(String url) {
        final int CONNECTION_TIMEOUT = 30000;
        final int READ_TIMEOUT = 30000;

        try {
            for(int i = 0; i < 3; i++) {
                URL fileUrl = new URL(URLDecoder.decode(url));
                HttpURLConnection connection = (HttpURLConnection)fileUrl.openConnection();
                connection.setConnectTimeout(CONNECTION_TIMEOUT);
                connection.setReadTimeout(READ_TIMEOUT);
                connection.connect();

                int status = connection.getResponseCode();

                if(status >= HttpStatus.SC_BAD_REQUEST) {
                    connection.disconnect();

                    continue;
                }

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;

                while((line = bufferedReader.readLine()) != null)
                    stringBuilder.append(line);

                bufferedReader.close();

                return stringBuilder.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

        return null;
    }

    public static Bitmap processBitmap(String fileName, Bitmap.Config config, int widthLimit) {
        Bitmap bitmap = null;

        try {File tempFile = new File(fileName);
            BitmapFactory.Options opts = new BitmapFactory.Options();
            BufferedInputStream fileInputStream = new BufferedInputStream(new FileInputStream(tempFile));

            opts.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(fileInputStream, null, opts);
            fileInputStream.close();
            fileInputStream = new BufferedInputStream(new FileInputStream(tempFile));

            //Find the correct scale value. It should be the power of 2.
            int width = opts.outWidth;
            int scale = 1;

            while (true) {
                int halfWidth = width / 2;

                if (halfWidth < widthLimit && (widthLimit - halfWidth) > widthLimit / 4)
                    break;

                width = halfWidth;
                scale *= 2;
            }

            opts = new BitmapFactory.Options();
            opts.inSampleSize = scale;
            opts.inPreferredConfig = config;

            try {
                System.gc();
                bitmap = BitmapFactory.decodeStream(fileInputStream, null, opts);

                if (bitmap != null)
                    return bitmap;
            } catch (Exception ex) {
            } catch (OutOfMemoryError e) {
            }

            fileInputStream.close();
            fileInputStream = new BufferedInputStream(new FileInputStream(tempFile));

            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            try {
                System.gc();
                bitmap = BitmapFactory.decodeStream(fileInputStream, null, opts);

                if (bitmap != null)
                    return bitmap;
            } catch (Exception ex) {
            } catch (OutOfMemoryError ex) {
            }

            fileInputStream.close();
            fileInputStream = new BufferedInputStream(new FileInputStream(tempFile));

            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            try {
                System.gc();
                bitmap = BitmapFactory.decodeStream(fileInputStream, null, opts);
            } catch (Exception ex) {
            } catch (OutOfMemoryError ex) {
            }

            fileInputStream.close();
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return bitmap;
    }

    public static String getLikeUrl(JsonFeed.Posts.Post post) {
        return FacebookPlugin.FACEBOOK_GRAPH_URL + post.getId() + "/likes";
    }

    public static String getCommentUrl(JsonFeed.Posts.Post post) {
        return FacebookPlugin.FACEBOOK_GRAPH_URL + post.getId() + "/comments";
    }

    public static String getPostDirectUrl(String objectId) {
        String[] ids = objectId.split("_");
        return SHARING_URL_BASE.replace(SHARING_URL_BASE_REPLACEMENT, ids[0]) + ids[1];
    }

    public static DateFormat getDateFormat(Context context) {
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    }

    public static boolean complexLike(String objectId) throws FacebookAuthorizationActivity.FacebookNotAuthorizedException,FacebookAuthorizationActivity.FacebookAlreadyLiked {
        return FacebookAuthorizationActivity.like(objectId) && FacebookAuthorizationActivity.like("http://www.ibuildapp.com/fbpostlikes/" + objectId);
    }

    public static boolean complexSourceLike(String objectId, String source) throws FacebookAuthorizationActivity.FacebookNotAuthorizedException,FacebookAuthorizationActivity.FacebookAlreadyLiked {
        return FacebookAuthorizationActivity.like(objectId) && FacebookAuthorizationActivity.like(source);
    }

    public static Albums loadAlbums(Context context, String url)
    {
        String feed = Utils.downloadFile(context, url);

        Gson gson = new Gson();
        Albums decoded = null;
        try {
            decoded = gson.fromJson(new FileReader(feed), Albums.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return decoded;
    }
    public static Uploads loadAUploads(Context context)
    {
        String url = Facebook.makeAPIRequestOwnerURL("/photos?fields=id,picture,source&type=uploaded&limit=18");
        String feed = Utils.downloadFile(context, url);

        Gson gson = new Gson();
        Uploads decoded = null;
        try {
            decoded = gson.fromJson(new FileReader(feed), Uploads.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return decoded;
    }

    public static Albums loadAlbums(Context context)
    {
        String url = Facebook.makeAPIRequestOwnerURL("/albums?"+context.getResources().getString(R.string.locale)+"fields=name,cover_photo,count,created_time");
        String feed = Utils.downloadFile(context, url);

        Gson gson = new Gson();
        Albums decoded = null;
        try {
            decoded = gson.fromJson(new FileReader(feed), Albums.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return decoded;
    }

    public static Uploads loadAUploads(Context context, String url)
    {
        String feed = Utils.downloadFile(context, url);

        Gson gson = new Gson();
        Uploads decoded = null;
        try {
            decoded = gson.fromJson(new FileReader(feed), Uploads.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return decoded;
    }

    public static boolean detectYoutube(String url) {
        Uri uri = Uri.parse(url);

        return "www.youtube.com".equals(uri.getHost()) || "youtube.com".equals(uri.getHost()) || "youtu.be".equals(uri.getHost()) || "m.youtube.com".equals(uri.getHost());
    }

    public static JsonFeed.Posts.Post.Comments loadComments(Context context, String url)
    {
        String feed = Utils.downloadFile(context, url);

        Gson gson = new Gson();
        JsonFeed.Posts.Post.Comments decoded = null;
        try {
            decoded = gson.fromJson(new FileReader(feed), JsonFeed.Posts.Post.Comments.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return decoded;
    }
    public static void prepareGridView(GridView view, Resources res){
        float density = res.getDisplayMetrics().density;
        view.setPadding((int)density*2,(int)density*2,(int)density*2,(int)density*2);
        view.setStretchMode(GridView.STRETCH_COLUMN_WIDTH);
    }

    public static Replies loadReplies(Context context, String url)
    {
        String feed = Utils.downloadFile(context, url);

        Gson gson = new Gson();
        Replies decoded = null;
        try {
            decoded = gson.fromJson(new FileReader(feed), Replies.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return decoded;
    }


    public static void loadAlbumPhotos(String url) {

    }
}
