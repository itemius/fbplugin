package com.ibuildapp.FacebookPlugin.core;

import android.text.TextUtils;
import com.appbuilder.sdk.android.Statics;
import com.appbuilder.sdk.android.authorization.Authorization;
import com.appbuilder.sdk.android.authorization.FacebookAuthorizationActivity;

import java.util.concurrent.Callable;

public enum Facebook {

    public static final String URL_BASE_API = "https://graph.facebook.com/v2.3/";
    public static final String URL_BASE_DIRECT = "https://facebook.com/";
    public static final String _AM = "&";
    public static final String _QM = "?";
    public static final String _RS = "/";
    public static final String QUERY_ACCESS_TOKEN = "access_token=";

    private static String ACCESS_TOKEN;

    public static String makeAPIRequestOwnerURL(String query) {
        return makeAPIRequestObjectURL(StaticData.getXmlParsedData().getFacebookId(), query, getAccessTokenOnThreadPool());
    }

    public static String makeAPIRequestCurrentUserURL(String query) {
        return TextUtils.isEmpty(getCurrentUserAccessToken()) ? "" :
                makeAPIRequestObjectURL(StaticData.getXmlParsedData().getFacebookId(), query, getCurrentUserAccessToken());
    }

    public static String makeAPIRequestObjectURL(String objectId, String query, String accessToken) {
        return URL_BASE_API + objectId +
                (query.startsWith("/") ? query : _QM + query) + _AM +
                QUERY_ACCESS_TOKEN + accessToken;
    }

    public static String getAccessTokenOnThreadPool() {
        if(ACCESS_TOKEN == null)
            try {
                ACCESS_TOKEN = Plugin.INSTANCE.getInFuture(new Callable<Plugin.Result<String>>() {
                    @Override
                    public Plugin.Result<String> call() throws Exception {
                        return new Plugin.Result<String>() {
                            @Override
                            public String getResult() throws Exception {
                                return FacebookAuthorizationActivity.getFbToken(Statics.FACEBOOK_APP_ID, Statics.FACEBOOK_APP_SECRET);
                            }
                        };
                    }
                }).getResult();
            } catch(Exception exception) {
                exception.printStackTrace();
            }

        return ACCESS_TOKEN;
    }

    public static String getCurrentUserAccessToken() {
        return Authorization.isAuthorized(Authorization.AUTHORIZATION_TYPE_FACEBOOK) ? Authorization.getAuthorizedUser().getAccessToken() : "";
    }

}
