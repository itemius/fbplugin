package com.ibuildapp.FacebookPlugin;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.appbuilder.sdk.android.AppBuilderModuleMain;
import com.ibuildapp.FacebookPlugin.R;
import com.ibuildapp.FacebookPlugin.core.StaticData;

public class WebViewActivity extends ActivityParent {

    private String url;

    @Override
    protected void backend() {
        url = getIntent().getStringExtra("url");
    }

    @Override
    protected void UI() {
        setContentView(R.layout.web_view);

        setTopBarBackgroundColor(StaticData.getXmlParsedData().getColorSkin().getColor1());
        setTopBarLeftButtonTextAndColor(getResources().getString(R.string.home), getResources().getColor(android.R.color.black), true, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        setTopBarTitle("");
        setTopBarTitleColor(getResources().getColor(android.R.color.black));

        WebView view = (WebView) findViewById(R.id.webview);
        view.getSettings().setJavaScriptEnabled(true);
        view.setWebChromeClient(new WebChromeClient());
        view.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return !URLUtil.isNetworkUrl(url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progressHide();
            }

        });

        view.loadUrl(url);
    }

    @Override
    protected void content(boolean cancelable) {
        progressShow(true);
    }

}
