package com.ibuildapp.FacebookPlugin.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.ibuildapp.FacebookPlugin.core.StaticData;
import com.ibuildapp.FacebookPlugin.model.feed.Icon;
import com.ibuildapp.FacebookPlugin.model.feed.Feed;
import com.ibuildapp.FacebookPlugin.model.feed.PageInfo;
import com.ibuildapp.FacebookPlugin.model.feed.Picture;
import com.ibuildapp.FacebookPlugin.model.photos.Albums;
import com.ibuildapp.FacebookPlugin.model.video.Videos;
import com.ibuildapp.FacebookPlugin.model_.JsonFeed;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ParserJson {

    public static class ParseResult {

        private final String id;
        private final String name;
        private final Icon icon;
        private final Feed feed;

        private ParseResult(Builder builder) {
            id = builder.id;
            name = builder.name;
            icon = builder.icon;
            feed = builder.feed;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public Icon getIcon() {
            return icon;
        }

        public Feed getFeed() {
            return feed;
        }

        public static class Builder {

            private String id;
            private String name;
            private Icon icon;
            private Feed feed;

            public Builder setId(String id) {
                this.id = id;

                return this;
            }

            public Builder setName(String name) {
                this.name = name;

                return this;
            }

            public Builder setIcon(Icon icon) {
                this.icon = icon;

                return this;
            }

            public Builder setFeed(Feed feed) {
                this.feed = feed;

                return this;
            }

            public ParseResult build() {
                return new ParseResult(this);
            }
        }

    }

    private static final String JSON_ID = "id";
    private static final String JSON_NAME = "name";
    private static final String JSON_PICTURE = "picture";
    private static final String JSON_FEED = "feed";
    private static final String JSON_VIDEOS = "videos";

    private static Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

    public static ParseResult parse(String jsonPath) throws IOException {
        FileReader json = new FileReader(jsonPath);

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        JsonObject jsonObject = gson.fromJson(json, JsonObject.class);

        json.close();

        ParseResult.Builder parseResultBuilder = new ParseResult.Builder();

        JsonElement id = jsonObject.get(JSON_ID);
        JsonElement name = jsonObject.get(JSON_NAME);
        JsonElement picture = jsonObject.get(JSON_PICTURE);
        JsonElement feed = jsonObject.get(JSON_FEED);

        parseResultBuilder
                .setId(id == null ? "" : id.toString())
                .setName(name == null ? "" : name.toString())
                .setIcon(gson.fromJson(picture, Icon.class))
                .setFeed(gson.fromJson(feed, Feed.class));

        return parseResultBuilder.build();
    }

    public static JsonFeed parseFeed(String jsonFilePath) throws IOException {
        Reader json = new FileReader(jsonFilePath);

        JsonFeed jsonFeed = gson.fromJson(json, JsonFeed.class);

        json.close();

        return jsonFeed;
    }

    public static JsonFeed.Posts parsePostsOnly(String jsonFilePath) throws IOException {
        Reader json = new FileReader(jsonFilePath);

        JsonFeed.Posts posts = gson.fromJson(json, JsonFeed.Posts.class);

        json.close();

        return posts;
    }

    public static String parseBigPictureUrl(String jsonPath) throws IOException {
        FileReader json = new FileReader(jsonPath);

        String bigPictureUrl = "";
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        List<Picture.Image> images = gson.fromJson(json, Picture.class).getImages();

        json.close();

        if(images != null && !images.isEmpty()) {
            Collections.sort(images, new Comparator<Picture.Image>() {
                @Override
                public int compare(Picture.Image first, Picture.Image second) {
                    int firstSize = first.getWidth() + first.getHeight();
                    long secondSize = second.getWidth() + second.getHeight();

                    return firstSize > secondSize ? -1 : firstSize < secondSize ? 1 : 0;
                }
            });

            bigPictureUrl = images.get(0).getSource();
        }

        return bigPictureUrl;
    }

    public static PageInfo parsePageInfo(String jsonPath) throws IOException {
        FileReader json = new FileReader(jsonPath);

        PageInfo pageInfo = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().fromJson(json, PageInfo.class);

        json.close();

        return pageInfo;
    }

    public static ParseResult parseOldFeed(String jsonPath) throws IOException {
        FileReader json = new FileReader(jsonPath);

        ParseResult parseResult = new ParseResult.Builder().setFeed(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().fromJson(json, Feed.class)).build();

        json.close();

        return parseResult;
    }

    public static Videos parseVideo(String jsonPath) throws IOException {
        FileReader json = new FileReader(jsonPath);

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        Videos videos = gson.fromJson(gson.fromJson(json, JsonObject.class).get(JSON_VIDEOS), Videos.class);

        json.close();

        return videos;
    }

    public static Videos parseOldVideo(String jsonPath) throws IOException {
        FileReader json = new FileReader(jsonPath);

        Videos videos = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().fromJson(json, Videos.class);

        json.close();

        return videos;
    }

    public static int parseCount(String jsonPath) throws IOException {
        FileReader json = new FileReader(jsonPath);

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        int count = gson.fromJson(gson.fromJson(json, JsonObject.class).getAsJsonObject(StaticData.getXmlParsedData().getFacebookId()).getAsJsonObject("summary").get("total_count"), Integer.class);

        json.close();

        return count;
    }

    public static Albums parseAlbums(String jsonSource) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        return gson.fromJson(jsonSource, Albums.class);
    }

}
