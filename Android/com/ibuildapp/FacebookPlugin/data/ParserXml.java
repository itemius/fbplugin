package com.ibuildapp.FacebookPlugin.data;

import android.sax.Element;
import android.sax.EndTextElementListener;
import android.sax.RootElement;
import android.util.Log;
import android.util.Xml;
import com.ibuildapp.FacebookPlugin.model.ColorSkin;

public class ParserXml {

    public static class ParseResult {

        private final String moduleId;
        private final String appId;
        private final String appName;
        private final String facebookId;
        private final ColorSkin colorSkin;
        
        private ParseResult(Builder builder) {
            moduleId = builder.moduleId;
            appId = builder.appId;
            appName = builder.appName;
            facebookId = builder.facebookId;
            colorSkin = builder.colorSkin;
        }

        public String getModuleId() {
            return moduleId;
        }

        public String getAppId() {
            return appId;
        }

        public String getAppName() {
            return appName;
        }

        public String getFacebookId() {
            return facebookId;
        }

        public ColorSkin getColorSkin() {
            return colorSkin;
        }

        public static class Builder {

            private String moduleId;
            private String appId;
            private String appName;
            private String facebookId;
            private ColorSkin colorSkin = new ColorSkin.Builder().build();

            public Builder setModuleId(String moduleId) {
                this.moduleId = moduleId;

                return this;
            }

            public Builder setAppId(String appId) {
                this.appId = appId;

                return this;
            }

            public Builder setAppName(String appName) {
                this.appName = appName;

                return this;
            }

            public Builder setFacebookId(String facebookId) {
                this.facebookId = facebookId;

                return this;
            }

            public Builder setColorSkin(ColorSkin colorSkin) {
                this.colorSkin = colorSkin;

                return this;
            }

            public ParseResult build() {
                return new ParseResult(this);
            }

        }

    }

    private static final String TAG = ParserXml.class.getCanonicalName();

    private static final String ERROR_XML_PARSING = "Xml parsing error";

    private static final String XML_DATA = "data";
    private static final String XML_MODULE_ID = "module_id";
    private static final String XML_APP_ID = "app_id";
    private static final String XML_APP_NAME = "app_name";
    private static final String XML_FB_ID = "fb_id";
    private static final String XML_COLORSKIN = "colorskin";
    private static final String XML_COLORSKIN_COLOR_1 = "color1";
    private static final String XML_COLORSKIN_COLOR_2 = "color2";
    private static final String XML_COLORSKIN_COLOR_3 = "color3";
    private static final String XML_COLORSKIN_COLOR_4 = "color4";
    private static final String XML_COLORSKIN_COLOR_5 = "color5";
    private static final String XML_COLORSKIN_COLOR_6 = "color6";
    private static final String XML_COLORSKIN_COLOR_7 = "color7";
    private static final String XML_COLORSKIN_COLOR_8 = "color8";
    private static final String XML_COLORSKIN_IS_LIGHT = "isLight";

    public static ParseResult parse(String xml) {
        final ParseResult.Builder parseResultBuilder = new ParseResult.Builder();
        final ColorSkin.Builder colorSkinBuilder = new ColorSkin.Builder();

        RootElement data = new RootElement(XML_DATA);
        Element colorskin = data.getChild(XML_COLORSKIN);

        data.getChild(XML_MODULE_ID).setEndTextElementListener(new EndTextElementListener() {
            @Override
            public void end(String body) {
                parseResultBuilder.setModuleId(body.trim());
            }
        });

        data.getChild(XML_APP_ID).setEndTextElementListener(new EndTextElementListener() {
            @Override
            public void end(String body) {
                parseResultBuilder.setAppId(body.trim());
            }
        });

        data.getChild(XML_APP_NAME).setEndTextElementListener(new EndTextElementListener() {
            @Override
            public void end(String body) {
                parseResultBuilder.setAppName(body.trim());
            }
        });

        data.getChild(XML_FB_ID).setEndTextElementListener(new EndTextElementListener() {
            @Override
            public void end(String body) {
                parseResultBuilder.setFacebookId(body.trim());
            }
        });

        colorskin.getChild(XML_COLORSKIN_COLOR_1).setEndTextElementListener(new EndTextElementListener() {
            @Override
            public void end(String body) {
                colorSkinBuilder.setColor1(body.trim());
            }
        });

        colorskin.getChild(XML_COLORSKIN_COLOR_2).setEndTextElementListener(new EndTextElementListener() {
            @Override
            public void end(String body) {
                colorSkinBuilder.setColor2(body.trim());
            }
        });

        colorskin.getChild(XML_COLORSKIN_COLOR_3).setEndTextElementListener(new EndTextElementListener() {
            @Override
            public void end(String body) {
                colorSkinBuilder.setColor3(body.trim());
            }
        });

        colorskin.getChild(XML_COLORSKIN_COLOR_4).setEndTextElementListener(new EndTextElementListener() {
            @Override
            public void end(String body) {
                colorSkinBuilder.setColor4(body.trim());
            }
        });

        colorskin.getChild(XML_COLORSKIN_COLOR_5).setEndTextElementListener(new EndTextElementListener() {
            @Override
            public void end(String body) {
                colorSkinBuilder.setColor5(body.trim());
            }
        });

        colorskin.getChild(XML_COLORSKIN_COLOR_6).setEndTextElementListener(new EndTextElementListener() {
            @Override
            public void end(String body) {
                colorSkinBuilder.setColor6(body.trim());
            }
        });

        colorskin.getChild(XML_COLORSKIN_COLOR_7).setEndTextElementListener(new EndTextElementListener() {
            @Override
            public void end(String body) {
                colorSkinBuilder.setColor7(body.trim());
            }
        });

        colorskin.getChild(XML_COLORSKIN_COLOR_8).setEndTextElementListener(new EndTextElementListener() {
            @Override
            public void end(String body) {
                colorSkinBuilder.setColor8(body.trim());
            }
        });

        colorskin.getChild(XML_COLORSKIN_IS_LIGHT).setEndTextElementListener(new EndTextElementListener() {
            @Override
            public void end(String body) {
                colorSkinBuilder.setLight(body.trim());
                parseResultBuilder.setColorSkin(colorSkinBuilder.build());
            }
        });

        try {
            Xml.parse(xml, data.getContentHandler());
        } catch(Exception exception) {
            Log.e(TAG, ERROR_XML_PARSING, exception);
        }

        return parseResultBuilder.build();
    }

}
