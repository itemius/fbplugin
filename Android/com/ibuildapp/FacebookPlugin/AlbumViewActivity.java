package com.ibuildapp.FacebookPlugin;

import android.content.Intent;
import android.view.View;
import android.widget.GridView;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;
import com.ibuildapp.FacebookPlugin.adapter.UploadsAdapter;
import com.ibuildapp.FacebookPlugin.core.Facebook;
import com.ibuildapp.FacebookPlugin.core.StaticData;
import com.ibuildapp.FacebookPlugin.core.Utils;
import com.ibuildapp.FacebookPlugin.model.uploads.Uploads;

public class AlbumViewActivity extends ActivityParent {
    private static String ALBUM_ID = "album_id";
    private static String ALBUM_PHOTOS_URL = FacebookPlugin.FACEBOOK_GRAPH_URL + ALBUM_ID+"/photos?"+FacebookPlugin.FACEBOOK_GRAPH_URL_ACCESS_TOKEN+ Facebook.getAccessTokenOnThreadPool();
    private PullToRefreshGridView gridView;
    private UploadsAdapter photoViewAdapter;

    private Uploads uploads;

    /**
     * Load activity backend data. Run in create method. Get data from intent
     */
   @Override
    protected void backend() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                AlbumViewActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressShow(true);
                    }
                });

                Intent intent = getIntent();
                String albumId = intent.getStringExtra("album_id");

                String url = ALBUM_PHOTOS_URL.replace(ALBUM_ID, albumId);
                uploads = Utils.loadAUploads(AlbumViewActivity.this, url);

                photoViewAdapter = new UploadsAdapter(AlbumViewActivity.this, uploads.getData());

                AlbumViewActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        gridView.setAdapter(photoViewAdapter);
                        progressHide();
                    }
                });
            }
        }).start();

    }
    /**
     * Preparing user interface elements. Adding listener to grid view
     */
    @Override
    protected void UI() {
        setContentView(R.layout.photo_album_view);

        int backgroundColor = StaticData.getXmlParsedData().getColorSkin().getColor1();

        Intent intent = getIntent();
        String name = intent.getStringExtra("album_name");

        setTopBarTitle(name);
        setTopBarTitleColor(getResources().getColor(android.R.color.black));

        setTopBarBackgroundColor(backgroundColor);
        setTopBarLeftButtonTextAndColor(getResources().getString(R.string.back), getResources().getColor(android.R.color.black), true, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        gridView = (PullToRefreshGridView)findViewById(R.id.photo_album_view);
        gridView.setBackgroundColor(backgroundColor);
        gridView.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
        gridView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<GridView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<GridView> refreshView) {
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<GridView> refreshView) {
                if (uploads.getPaging()!=null && uploads.getPaging().getNext()!=null)
                    new Thread( new Runnable() {
                        @Override
                        public void run() {
                            try {
                                uploads = Utils.loadAUploads(AlbumViewActivity.this, uploads.getPaging().getNext());
                                photoViewAdapter.addToEnd(uploads);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        photoViewAdapter.notifyDataSetChanged();
                                        gridView.onRefreshComplete();
                                    }
                                });
                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                else
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            gridView.onRefreshComplete();
                        }
                    });
            }
        });
    }
    /**
     * Content inititalization 
     */
    @Override
    protected void content(boolean cancelable) {

    }
}
