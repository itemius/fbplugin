#import <Foundation/Foundation.h>
#import "mFacebookDesign.h"
#import "TBXML.h"

#define kFacebookURL @"https://facebook.com"

/**
 * Class to store module-wide configuration parameters.
 */
@interface mFacebookParameters : NSObject<NSCoding>

/**
 * Convenience method to get the shared instance of the parameters.
 */
+(instancetype)sharedParameters;

/**
 * Creates parameters object and populates it with the contents of the xml element.
 *
 * @param element -- xml element with parameters.
 */
+(instancetype)parametersFromXMLElement:(TBXMLElement)element;

/**
 * Fills self with values from the sourceParameters
 *
 * @param sourceParameters -- parameters for self to be filled with.
 */
-(void)fillWithParameters:(mFacebookParameters *)sourceParameters;

/**
 * Facebook numeric identificator for the feed being presented in the module.
 */
@property (nonatomic, strong) NSString *facebookId;

/**
 * Identificator of the module in the app.
 */
@property (nonatomic, strong) NSString *moduleId;

/**
 * Title of the main controller of the module. Set on the iBuildApp server side.
 *
 * @see mFacebookViewController
 */
@property (nonatomic, strong) NSString *mainPageTitle;

/**
 * Visual parameters, color sceheme of the module.
 *
 * @see mFacebookDesign
 */
@property (nonatomic, strong) mFacebookDesign *design;


/**
 * List of all URLs liked by the user on facebook.
 */
@property (nonatomic, strong) NSMutableSet *likedURLs;

/**
 * List of all Ids liked by the user on facebook.
 */
@property (nonatomic, strong) NSSet *likedIds;

/**
 * List of all pages liked by the user on facebook.
 */
@property (nonatomic, strong) NSSet *likedPages;

/**
 * Tells whether provided URL string is contained in likedURLs.
 *
 * @param urlString - URL as string.
 *
 * @return YES - if contained, NO - otherwise.
 */
-(BOOL)isURLStringLiked:(NSString *)urlString;

/**
 * Tells whether provided Id string is contained in 
 * likedIds and/or likedPages if any.
 *
 * @param facebookId - Item's Facebook Identificator as string.
 *
 * @return YES - if contained, NO - otherwise.
 */
-(BOOL)isFacebookIdLiked:(NSString *)facebookId;

@end
