#import <Foundation/Foundation.h>
#import "mFacebookVideoDescription.h"

@interface mFacebookImageDescription : NSObject

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) NSString *facebookObjectId;


@property (nonatomic, retain) NSString *targetVideoFacebookId;
@property (nonatomic, retain) mFacebookVideoDescription *targetVideoDescription;

@end
