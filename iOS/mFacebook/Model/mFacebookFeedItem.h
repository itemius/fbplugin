#import <Foundation/Foundation.h>
#import "mFacebookCommentedObject.h"

/**
 * An information about a single post in a page's feed.
 *
 * @see mFacebookCommentedObject
 */
@interface mFacebookFeedItem : NSObject<mFacebookCommentedObject>

/**
 * Initializes the feed item with dictionary obtained from facebook.
 */
-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

/**
 * Convenience method, returning an array of feed items from the array of dictionaries.
 *
 * @param feed - NSArray of dictionaries with feed items data, obtained from facebook.
 *
 * @return NSArray of mFacebookFeedItem instances.
 */
+(NSArray *)itemsFromFeed:(NSArray *)feed;

/**
 * Convenience method, returning an array of image descriptions, which target is a video.
 *
 * @return NSArray of mFacebookImageDescription instances, empty if ther is no such images.
 * 
 * @see mFacebookImageDescription
 */
-(NSArray *)attachedImagesTargetedToVideos;

/**
 * Link representing a feed item when sharing it on facebook.
 */
-(NSString *)linkForSharing;

/**
 * Link accumulating likes on a facebook for a feed item.
 */
-(NSString *)linkForLike;

/**
 * Post id on facebook.
 */
@property (nonatomic, strong) NSString *itemFacebookId;

/**
 * Author id on facebook.
 */
@property (nonatomic, strong) NSString *authorFacebookId;

/**
 * @deprecated
 * Facebook id for image inside the feed item.
 *
 * @discussion
 * This field was used prior to multiple-image posiibility
 *
 * @see
 * mFacebookMultipleImageView
 */
@property (nonatomic, strong) NSString *imageFacebookId;

/**
 * Name of the post's author.
 */
@property (nonatomic, strong) NSString *authorName;

/**
 * Post's message text.
 */
@property (nonatomic, strong) NSString *message;

/**
 * Feed item type, e.g. link, video etc.
 */
@property (nonatomic, strong) NSString *type;

/**
 * If a feed item is of video type, 
 * here source is a URL of it's source video.
 */
@property (nonatomic, strong) NSString *source;

/**
 * Time when the feed item was published.
 */
@property (nonatomic, strong) NSDate *creationTime;

/**
 * Post author's avatar URL.
 */
@property (nonatomic, strong) NSURL *authorAvatarURL;

/**
 * Descriptions of attached images.
 *
 * @see mFacebookImageDescription
 */
@property (nonatomic, strong) NSArray *attachedImages;

/**
 * Likes count for feed item.
 */
@property (nonatomic) NSUInteger likesCount;

/**
 * Comments count for feed item.
 */
@property (nonatomic) NSUInteger commentsCount;

/**
 * Flag, tells whether the feed item is liked
 */
@property (nonatomic) BOOL isLiked;

/**
 * Flag, tells whether the message is expanded on the item.
 * By default, large messages appears cut up to the first 5 lines.
 *
 * @see mFacebookFeedTableViewCell
 */
@property (nonatomic) BOOL isMessageExpanded;

@end
