#import "mFacebookParameters.h"

static mFacebookParameters *sharedParameters = nil;

@implementation mFacebookParameters

+(instancetype)sharedParameters
{
  if(!sharedParameters)
  {
    sharedParameters = [[self alloc] init];
  }
  return sharedParameters;
}

-(void)dealloc
{
  self.mainPageTitle = nil;
  self.moduleId = nil;
  self.facebookId = nil;
  
  self.design = nil;
  self.likedURLs = nil;
  self.likedIds = nil;
  
  [super dealloc];
}

+(instancetype)parametersFromXMLElement:(TBXMLElement)element
{
  mFacebookParameters *parameters = [[mFacebookParameters new] autorelease];
  
  NSString *moduleId = @"0";
  TBXMLElement *moduleIdElement = [TBXML childElementNamed:@"module_id" parentElement:&element];
  if ( moduleIdElement )
  {
    moduleId = [TBXML textForElement:moduleIdElement];
  }
  
  NSString *facebookId = @"0";
  TBXMLElement *facebookIdElement = [TBXML childElementNamed:@"fb_id" parentElement:&element];
  if ( facebookIdElement )
  {
    facebookId = [TBXML textForElement:facebookIdElement];
  }
  
  parameters.moduleId = moduleId;
  parameters.facebookId = facebookId;
  
  parameters.design = [mFacebookDesign designFromXMLElement:element];
  
  return parameters;
}

-(void)fillWithParameters:(mFacebookParameters *)sourceParameters
{
  self.moduleId = sourceParameters.moduleId;
  self.facebookId = sourceParameters.facebookId;
  self.mainPageTitle = sourceParameters.mainPageTitle;
  
  self.design = sourceParameters.design;
}

- (id)initWithCoder:(NSCoder *)coder
{
  self = [super init];
  if ( self )
  {
    self.mainPageTitle = [coder decodeObjectForKey:@"mFacebookDesign::mainPageTitle"];
    self.moduleId = [coder decodeObjectForKey:@"mFacebookDesign::module_id"];
    self.facebookId = [coder decodeObjectForKey:@"mFacebookDesign::fb_id"];
    
    self.design = [coder decodeObjectForKey:@"mFacebookDesign::design"];
  }
  return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{  
  [coder encodeObject:self.mainPageTitle forKey:@"mFacebookDesign::mainPageTitle"];
  [coder encodeObject:self.moduleId forKey:@"mFacebookDesign::module_id"];
  [coder encodeObject:self.facebookId forKey:@"mFacebookDesign::fb_id"];
  
  [coder encodeObject:self.design forKey:@"mFacebookDesign::design"];
}

-(BOOL)isURLStringLiked:(NSString *)urlString
{
  /**
   * Filthy facebook bug, when liked link https://facebook.com/123/videos/345
   * comes in the list of liked links as /123/videos/345
   */
  NSRange facebookURLRange = NSMakeRange(0, kFacebookURL.length);
  NSString *trimmedLink = [urlString stringByReplacingCharactersInRange:facebookURLRange
                                                        withString:@""];
  
  for(NSString *likedURL in self.likedURLs)
  {
    if([likedURL isEqualToString:trimmedLink] ||
       [likedURL isEqualToString:urlString])
    {
      return YES;
    }
  }
  
  return NO;
}

-(BOOL)isFacebookIdLiked:(NSString *)facebookId
{
  BOOL isIdLiked = ([self isPageIdLiked:facebookId] ||
                    [self isPlainIdLiked:facebookId]);
  
  return isIdLiked;
}

-(BOOL)isPlainIdLiked:(NSString *)facebookId
{
  for(NSString *likedId in self.likedIds)
  {
    if([likedId isEqualToString:facebookId])
    {
      return YES;
    }
  }
  
  return NO;
}

-(BOOL)isPageIdLiked:(NSString *)facebookId
{
  for(NSDictionary *likedPage in self.likedPages)
  {
    NSString *likedPageId = [likedPage objectForKey:@"id"];
    
    if([likedPageId isEqualToString:facebookId])
    {
      return YES;
    }
  }
  
  return NO;
}

@end
