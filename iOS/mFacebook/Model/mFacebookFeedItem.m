#import "mFacebookFeedItem.h"
#import "mFacebookImageDescription.h"
#import "mFacebookActions.h"
#import "functionLibrary.h"
#import <auth_Share/auth_Share.h>

@interface mFacebookFeedItem()

@property (nonatomic, strong) mFacebookActions *actions;

@end

@implementation mFacebookFeedItem

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
  self = [super init];
  
  if(self)
  {
    self.itemFacebookId = [dictionary objectForKey:@"id"];
    
    NSDictionary *from = [dictionary objectForKey:@"from"];
    
    self.authorFacebookId = [from objectForKey:@"id"];
    self.authorName = [from objectForKey:@"name"];
    
    self.message = [dictionary objectForKey:@"message"];
    self.type = [dictionary objectForKey:@"type"];
    self.source = [dictionary objectForKey:@"link"];
    
    self.imageFacebookId = [dictionary objectForKey:@"object_id"];
    
    self.creationTime = [functionLibrary dateFromInternetDateTimeString:[dictionary objectForKey:@"created_time"]];
    
    NSArray *attachments = [[dictionary objectForKey:@"attachments"] objectForKey:@"data"];
    
    NSMutableArray *images = [NSMutableArray array];
    
    for (NSDictionary *attachment in attachments)
    {
      mFacebookImageDescription *imageDecs = [self imageDescriptionFromMediaTag:attachment];
      if (imageDecs)
        [images addObject:imageDecs];
      
      NSArray *subattachments = [[attachment objectForKey:@"subattachments"] objectForKey:@"data"];
      for (NSDictionary *subattachment in subattachments)
      {
        mFacebookImageDescription *imageDecs = [self imageDescriptionFromMediaTag:subattachment];
        if (imageDecs)
          [images addObject:imageDecs];
      }
    }
  
    self.attachedImages = [[images copy] autorelease];
    
    self.likesCount = [[[[dictionary objectForKey:@"likes"] objectForKey:@"summary"] objectForKey:@"total_count"] integerValue];
    self.commentsCount = [[[[dictionary objectForKey:@"comments"] objectForKey:@"summary"] objectForKey:@"total_count"] integerValue];
  
    NSDictionary *actionsDictionary = [dictionary objectForKey:@"actions"];
    self.actions = [[[mFacebookActions alloc] initWithDictionary:actionsDictionary] autorelease];
  }
  
  return self;
}

-(mFacebookImageDescription *) imageDescriptionFromMediaTag:(NSDictionary *) dict
{
  NSDictionary *imageDict = [[dict objectForKey:@"media"] objectForKey:@"image"];
  
  if (imageDict)
  {
    mFacebookImageDescription *imageDesc = [[[mFacebookImageDescription alloc] initWithDictionary:dict] autorelease];
    return imageDesc;
  }
  
  return nil;
}

+(NSArray *)itemsFromFeed:(NSArray *)feed
{
  NSMutableArray *items = [[[NSMutableArray alloc] init] autorelease];
  
  for(NSDictionary *item in feed){
    mFacebookFeedItem *feedItem = [[[mFacebookFeedItem alloc] initWithDictionary:item] autorelease];
    
    //empty items not showing
    if (feedItem.attachedImages.count || ![feedItem.message isEqualToString:@""])
        [items addObject:feedItem];
  }
  
  return items;
}

-(NSURL *)authorAvatarURL
{
  return [auth_Share facebookPictureURLForId:self.authorFacebookId timestamp:lastRefreshTimestamp];
}

-(void)dealloc
{
  self.itemFacebookId = nil;
  self.authorFacebookId = nil;
  
  self.authorName = nil;
  self.message = nil;
  self.imageFacebookId = nil;
  self.type = nil;
  self.source = nil;
  self.creationTime = nil;
  
  self.attachedImages = nil;
  
  self.actions = nil;
  
  [super dealloc];
}

-(NSArray *)attachedImagesTargetedToVideos
{
  NSMutableArray *videoTargetedImages = [NSMutableArray array];
  
  for(mFacebookImageDescription *image in self.attachedImages)
  {
    if(image.targetVideoFacebookId.length)
    {
      [videoTargetedImages addObject:image];
    }
  }
  
  return videoTargetedImages;
}

-(NSString *)linkForSharing
{
  if(self.actions.shareURLString.length)
  {
    return self.actions.shareURLString;
  }
  
  NSString *facebookId = self.itemFacebookId;
  
  NSArray *idParts = [facebookId componentsSeparatedByString:@"_"];
  
  if(idParts.count != 2)
  {
    NSLog(@"mFacebookFeedItem: error getting id parts, returning empty string for link");
    return @"";
  }
  
  NSString *author = idParts[0];
  NSString *postId = idParts[1];
  
  NSString *link = [NSString stringWithFormat:@"https://www.facebook.com/%@/posts/%@", author, postId];
  
  return link;
}

-(NSString *)linkForLike
{
  NSString *linkForLike = [NSString stringWithFormat:@"http://www.ibuildapp.com/fbpostlikes/%@", self.itemFacebookId];

  return linkForLike;
}

#pragma mark mFacebookCommentedObjectProtocol

- (NSString *)facebookObjectId
{
  return self.itemFacebookId;
}

-(void)incrementCommentsCount
{
  self.commentsCount++;
}

-(void)setNewCommentsCountValue:(NSUInteger)value
{
  self.commentsCount = value;
}

@end
