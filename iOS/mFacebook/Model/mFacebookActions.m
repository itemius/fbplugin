#import "mFacebookActions.h"

@implementation mFacebookActions

-(instancetype)initWithDictionary:(NSDictionary *)actionsDictionary
{
  self = [super init];
  
  if(self)
  {
    for (NSDictionary *action in actionsDictionary)
    {
      NSString *name = [action objectForKey:@"name"];
    
      NSString *actionURLString = [action objectForKey:@"link"];
    
      if([name isEqualToString:@"Like"])
      {
        self.likeURLString = actionURLString;
        continue;
      }
      
      if([name isEqualToString:@"Share"])
      {
        self.shareURLString = actionURLString;
        continue;
      }

      if([name isEqualToString:@"Comment"])
      {
        self.commentURLString = actionURLString;
        continue;
      }
    }
  }
  
  return self;
}

-(void)dealloc
{
  self.likeURLString = nil;
  self.shareURLString = nil;
  self.commentURLString = nil;
  
  [super dealloc];
}

@end
