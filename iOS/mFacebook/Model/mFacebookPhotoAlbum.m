#import "mFacebookPhotoAlbum.h"


@implementation mFacebookPhotoAlbum

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
  self = [super init];
  
  if(self)
  {
    self.albumID = [dictionary objectForKey:@"id"];
    self.name = [dictionary objectForKey:@"name"];
    self.count = [[dictionary objectForKey:@"count"] unsignedIntegerValue];
    self.coverId = [dictionary objectForKey:@"cover_photo"];
  }
  
  return self;
}

-(void)dealloc
{
  self.albumID = nil;
  self.name = nil;
  self.coverId = nil;
  self.coverURL = nil;
  
  [super dealloc];
}

+(NSMutableArray *)itemsFromArray:(NSArray *)array
{
  NSMutableArray *result = [NSMutableArray array];
  
  for (NSDictionary *dict in array)
  {
    mFacebookPhotoAlbum *album = [[[mFacebookPhotoAlbum alloc] initWithDictionary:dict] autorelease];
    if (album.count)
      [result addObject:album];
  }
  
  return result;
}

@end
