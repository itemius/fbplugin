#import "mFacebookVideoDescription.h"

@implementation mFacebookVideoDescription

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
  self = [super init];
  
  if(self)
  {
    self.facebookId = [dictionary objectForKey:@"id"];
    self.pictureURLString = [dictionary objectForKey:@"picture"];
    self.sourceVideoURLString = [dictionary objectForKey:@"source"];
    self.embedHTML = [dictionary objectForKey:@"embed_html"];
    self.descriptionText = [dictionary objectForKey:@"description"];
    
    self.length = [[dictionary objectForKey:@"length"] floatValue];
    
    NSDictionary *comments = [dictionary objectForKey:@"comments"];
    self.commentsCount = [self totalCountFromDictionary:comments];
    
    NSDictionary *likes = [dictionary objectForKey:@"likes"];
    self.likesCount = [self totalCountFromDictionary:likes];
    
    if(dictionary[@"thumbnails"])
    {
      NSDictionary *formatDataDictionary = dictionary[@"thumbnails"][@"data"];
      self.preferredThumbnailURLString = [self preferredThumbnailFromDictionary:formatDataDictionary];
    } else {
      self.preferredThumbnailURLString = self.pictureURLString;
    }
  }
  
  return self;
}

-(NSInteger)totalCountFromDictionary:(NSDictionary *)dictionary
{
  NSDictionary *summary = [dictionary objectForKey:@"summary"];
  NSInteger totalCount = [[summary objectForKey:@"total_count"] integerValue];
  
  return totalCount;
}

-(NSString *)preferredThumbnailFromDictionary:(NSDictionary *)thumbnailsData
{
  NSString *preferredThumbnail = self.pictureURLString;
  
  for(NSDictionary *thumbnail in thumbnailsData)
  {
    bool isPreferred = [[thumbnail objectForKey:@"is_preferred"] boolValue];
    
    if(isPreferred)
    {
      preferredThumbnail = [thumbnail objectForKey:@"uri"];
      
      return preferredThumbnail;
    }
  }
  
  return preferredThumbnail;
}

-(void)dealloc
{
  self.facebookId = nil;
  self.pictureURLString = nil;
  self.sourceVideoURLString = nil;
  self.descriptionText = nil;
  self.embedHTML = nil;
  
  self.title = nil;
  
  [super dealloc];
}

@end
