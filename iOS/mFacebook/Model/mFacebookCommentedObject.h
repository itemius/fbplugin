#import <Foundation/Foundation.h>

@protocol mFacebookCommentedObject <NSObject>

@required

-(NSString *) facebookObjectId;
-(NSUInteger) commentsCount;
-(void) setNewCommentsCountValue:(NSUInteger)value;
-(void) incrementCommentsCount;

@end
