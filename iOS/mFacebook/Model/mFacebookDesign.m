#import "mFacebookDesign.h"
#import "NSString+colorizer.h"

#define kDefaultBackgroundColor [UIColor blackColor]

@implementation mFacebookDesign

+(instancetype)designFromXMLElement:(TBXMLElement)element
{
  TBXMLElement *colorskinElement = [TBXML childElementNamed:@"colorskin" parentElement:&element];
  if (colorskinElement)
  {
    mFacebookDesign *design = [[mFacebookDesign new] autorelease];
    
    NSMutableDictionary *colorSkinDictionary = [NSMutableDictionary new];
    
    TBXMLElement *colorElement = colorskinElement->firstChild;
    while( colorElement )
    {
      NSString *colorElementContent = [TBXML textForElement:colorElement];
      
      if ( [colorElementContent length] )
      {
        [colorSkinDictionary setValue:colorElementContent forKey:[[TBXML elementName:colorElement] lowercaseString]];
      }
      
      colorElement = colorElement->nextSibling;
    }
    
    NSString *color1String = [colorSkinDictionary objectForKey:@"color1"];
    
    design.color1 = [color1String asColor];
    design.color2 = [[colorSkinDictionary objectForKey:@"color2"] asColor];
    design.color3 = [[colorSkinDictionary objectForKey:@"color3"] asColor];
    design.color4 = [[colorSkinDictionary objectForKey:@"color4"] asColor];
    design.color5 = [[colorSkinDictionary objectForKey:@"color5"] asColor];
    
    design.isLight = [[colorSkinDictionary objectForKey:@"isLight"] boolValue];
    
    if([color1String isEqualToString:@"#ffffff"] ||
       [color1String isEqualToString:@"#fff"] ||
       [color1String isEqualToString:@"white"])
    {
      design.isWhiteBackground = YES;
    }
    
    return design;
  }
  
  NSLog(@"mFacebookDesign no colorskin element!");
  return nil;
}

- (id)initWithCoder:(NSCoder *)coder
{
  self = [super init];
  if ( self )
  {
    self.color1 = [coder decodeObjectForKey:@"mFacebookDesign::color1"];
    self.color2 = [coder decodeObjectForKey:@"mFacebookDesign::color2"];
    self.color3 = [coder decodeObjectForKey:@"mFacebookDesign::color3"];
    self.color4 = [coder decodeObjectForKey:@"mFacebookDesign::color4"];
    self.color5 = [coder decodeObjectForKey:@"mFacebookDesign::color5"];
    
    self.isLight = [coder decodeBoolForKey:@"mFacebookDesign::isLight"];
    self.isWhiteBackground = [coder decodeBoolForKey:@"mFacebookDesign::isWhiteBackground"];
  }
  return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
  [coder encodeObject:self.color1 forKey:@"mFacebookDesign::color1"];
  [coder encodeObject:self.color2 forKey:@"mFacebookDesign::color2"];
  [coder encodeObject:self.color3 forKey:@"mFacebookDesign::color3"];
  [coder encodeObject:self.color4 forKey:@"mFacebookDesign::color4"];
  [coder encodeObject:self.color5 forKey:@"mFacebookDesign::color5"];
  
  [coder encodeBool:self.isLight forKey:@"mFacebookDesign::isLight"];
  [coder encodeBool:self.isWhiteBackground forKey:@"mFacebookDesign::isWhiteBackground"];
}

-(void)setColor1:(UIColor *)color1
{
  [color1 retain];
  [_color1 release];
  
  if(!color1){
    _color1 = [kDefaultBackgroundColor retain];
  }
  else
  {
    _color1 = color1;
  }
}

-(void)dealloc
{
  self.color1 = nil;
  self.color2 = nil;
  self.color3 = nil;
  self.color4 = nil;
  self.color5 = nil;
  
  [super dealloc];
}

@end
