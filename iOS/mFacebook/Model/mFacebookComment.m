#import "mFacebookComment.h"
#import "auth_Share.h"

@implementation mFacebookComment

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
  self = [super init];
  
  if(self)
  {
    NSDictionary *from = dictionary[@"from"];
    
    self.authorName = from[@"name"];
    self.facebookId = dictionary[@"id"];
    self.message    = dictionary[@"message"];
    self.avatarURL  = [auth_Share facebookPictureURLForId:from[@"id"]
                                                timestamp:moduleStartTimestamp];
    
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
    
    self.creationTime = [dateFormatter dateFromString:dictionary[@"created_time"]];
    
    self.commentsCount        = [dictionary[@"comment_count"] integerValue];
  }
  
  return self;
}

+(NSMutableArray *)itemsFromArray:(NSArray *)array
{
  NSMutableArray *comments = [NSMutableArray array];
  
  for (NSDictionary *comment in array)
    [comments addObject:[[[mFacebookComment alloc] initWithDictionary:comment] autorelease]];
  
  return comments;
}

-(void)dealloc
{
  self.facebookId   = nil;
  self.authorName   = nil;
  self.message      = nil;
  self.avatarURL    = nil;
  self.creationTime = nil;
  
  [super dealloc];
}

#pragma mark mFacebookCommentedObjectProtocol

-(NSString *)facebookObjectId
{
  return self.facebookId;
}

-(void)incrementCommentsCount
{
  self.commentsCount++;
}

-(void)setNewCommentsCountValue:(NSUInteger)value
{
  self.commentsCount = value;
}
@end
