#import <Foundation/Foundation.h>

/**
 * Stores URLs for facebook actions - like, share, comment
 * for a facebook object.
 */
@interface mFacebookActions : NSObject

/**
 * Initializes the actions with dictionary obtained from facebook.
 */
-(instancetype)initWithDictionary:(NSDictionary *)actionsDictionary;

/**
 * URL string for Like action.
 */
@property (nonatomic, strong) NSString *likeURLString;

/**
 * URL string for Share action.
 *
 * @discussion
 * For some reason, this field is inaccessible through iOS SDK.
 * Though you can see it via Graph API Explorer.
 */
@property (nonatomic, strong) NSString *shareURLString;

/**
 * URL string for Comment action.
 */
@property (nonatomic, strong) NSString *commentURLString;

@end
