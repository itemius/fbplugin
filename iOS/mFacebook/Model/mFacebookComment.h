#import <Foundation/Foundation.h>
#import "mFacebookCommentedObject.h"

@interface mFacebookComment : NSObject<mFacebookCommentedObject>

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

+(NSMutableArray *)itemsFromArray:(NSArray *)array;

@property (nonatomic, strong) NSString    *facebookId;
@property (nonatomic, strong) NSString    *authorName;
@property (nonatomic, strong) NSString    *message;
@property (nonatomic, strong) NSURL       *avatarURL;
@property (nonatomic, strong) NSDate      *creationTime;
@property (nonatomic, assign) NSUInteger  commentsCount;

@end
