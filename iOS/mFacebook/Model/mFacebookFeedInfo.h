#import <Foundation/Foundation.h>

/**
 * An information about the page being currently presented in the module.
 */
@interface mFacebookFeedInfo : NSObject

/**
 * Initializes the feed info with dictionary obtained from facebook.
 */
-(instancetype)initWithDictionary:(NSDictionary *)info;

/**
 * Page id from facebook.
 */
@property (nonatomic, strong) NSString *facebookId;

/**
 * Page URL.
 */
@property (nonatomic, strong) NSString *link;

/**
 * Page name.
 */
@property (nonatomic, strong) NSString *name;

/**
 * Page category (like "Case", "TV Show", "Community Organization", etc).
 */
@property (nonatomic, strong) NSString *category;

/**
 * Likes count for page.
 */
@property (nonatomic) NSUInteger likesCount;

/**
 * Videos count in the page's feed.
 */
@property (nonatomic) NSUInteger videosCount;

/**
 * URL string for splash (background) image (used on header view).
 *
 * @see mFacebookFeedHeaderView
 */
@property (nonatomic, strong) NSURL *splashImageURL;

/**
 * URL string for page avatar image (used on header view).
 *
 * @see mFacebookFeedHeaderView
 */
@property (nonatomic, strong) NSURL *authorAvatarURL;

/**
 * Flag, indicates whether the page is liked by the user.
 */
@property (nonatomic) BOOL isLiked;

@end
