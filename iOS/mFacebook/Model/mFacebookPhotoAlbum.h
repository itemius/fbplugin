#import <Foundation/Foundation.h>

@interface mFacebookPhotoAlbum : NSObject

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@property(nonatomic, strong) NSString *albumID;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, assign) NSUInteger *count;
@property(nonatomic, strong) NSString *coverId;
@property(nonatomic, strong) NSURL *coverURL;

+(NSMutableArray *)itemsFromArray:(NSArray *)array;

@end
