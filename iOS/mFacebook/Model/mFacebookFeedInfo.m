#import "mFacebookFeedInfo.h"
#import <auth_Share/auth_Share.h>

@implementation mFacebookFeedInfo

-(instancetype)initWithDictionary:(NSDictionary *)info
{
  self = [super init];
  
  if(self)
  {
    self.facebookId = [info objectForKey:@"id"];
    self.link = [info objectForKey:@"link"];
    self.name = [info objectForKey:@"name"];
    self.category = [info objectForKey:@"category"];
    self.likesCount = [[info objectForKey:@"likes"] unsignedIntegerValue];
    self.videosCount = [[[[info objectForKey:@"videos"] objectForKey:@"summary"] objectForKey:@"total_count"] unsignedIntegerValue];
    
    [self setSplashImageURLFromDictionary:info];
  }
  
  return self;
}

-(void)setSplashImageURLFromDictionary:(NSDictionary *)info
{
  NSDictionary *cover = [info objectForKey:@"cover"];
  NSString *source = [cover objectForKey:@"source"];
  
  if(source.length){
    self.splashImageURL = [NSURL URLWithString:source];
  } else {
    self.splashImageURL = nil;
  }
}

-(NSURL *)authorAvatarURL
{
  return [auth_Share facebookPictureURLForId:self.facebookId timestamp:lastRefreshTimestamp];
}

-(void)dealloc
{
  self.facebookId = nil;
  self.name = nil;
  self.category = nil;
  self.link = nil;
  
  self.splashImageURL = nil;
  
  [super dealloc];
}

@end
