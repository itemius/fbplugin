#import "mFacebookImageDescription.h"

@implementation mFacebookImageDescription


-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
  self = [super init];
  
  if(self)
  {
    NSDictionary *imageDict = dictionary[@"media"][@"image"];
    
    self.url = [NSURL URLWithString:imageDict[@"src"]];
    self.facebookObjectId = dictionary[@"target"][@"id"];
    
    NSString *type = [dictionary objectForKey:@"type"];
    if([type hasPrefix:@"video"])
    {
      if([type isEqualToString:@"video_inline"] ||
         [type isEqualToString:@"video_autoplay"])
      {
        self.targetVideoFacebookId = [[dictionary objectForKey:@"target"] objectForKey:@"id"];
        
      } else {
        //Surrogate video object for non-inline video.
        //Will be played with mFacebookWebVC
        self.targetVideoDescription = [[[mFacebookVideoDescription alloc] init] autorelease];
        self.targetVideoDescription.title = [dictionary objectForKey:@"title"];
        self.targetVideoDescription.sourceVideoURLString = [dictionary objectForKey:@"url"];
        self.targetVideoDescription.facebookVideoType = mFacebookVideoTypeShared;
      }
    }
  }
  
  return self;
}

-(void)dealloc
{
  self.url = nil;

  self.targetVideoFacebookId = nil;
  self.targetVideoDescription = nil;

  self.facebookObjectId = nil;

  [super dealloc];
}

@end
