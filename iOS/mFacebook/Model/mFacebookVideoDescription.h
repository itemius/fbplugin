#import <Foundation/Foundation.h>

/**
 * Enumeration of possible facebook types of a video.
 * Currently, videos are categorized as "Inline" and "Shared".
 *
 * @see mFacebookVideoDescription
 */
NS_ENUM(NSInteger, mFacebookVideoType)
{
  /**
   * Video is inline, can be played in a local video player
   * @see mFacebookVideoDetailVC
   */
  mFacebookVideoTypeInline = 0,
  
  /**
   * Video is shared, should be opened as a web page in WebVC
   * @see mFacebookWebVC
   */
  mFacebookVideoTypeShared = 1
};

/**
 * Entity which describes facebook video
 */
@interface mFacebookVideoDescription : NSObject

/**
 * Initializes video description with dictionary obtained from facebook.
 */
-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

/**
 * Id of a video on facebook
 */
@property (nonatomic, strong) NSString *facebookId;

/**
 * URL string of a video's still picture
 */
@property (nonatomic, strong) NSString *pictureURLString;

/**
 * URL string of a video's preferred still picture
 *
 * @discussion
 * <code>preferred</code> - is a flag on facebook. When parisng a list of thumbnails,
 * if a picture has this flag, it is considered preferred. 
 * Otherwise pictureURLString is returned.
 */
@property (nonatomic, strong) NSString *preferredThumbnailURLString;

/**
 * URL string of a video itself.
 */
@property (nonatomic, strong) NSString *sourceVideoURLString;

/**
 * NSString with iframe for embedding a video.
 */
@property (nonatomic, strong) NSString *embedHTML;

/**
 * Description for video.
 */
@property (nonatomic, strong) NSString *descriptionText;

/**
 * Title for non-inline videos, e.g. shared from Youtube, Vimeo etc.
 */
@property (nonatomic, strong) NSString *title;

/**
 * Length of a video.
 */
@property (nonatomic) float length;

/**
 * Likes count for a video.
 */
@property (nonatomic) NSInteger likesCount;

/**
 * Comments count for a video.
 */
@property (nonatomic) NSInteger commentsCount;

/**
 * Flag, tells if a video is liked.
 */
@property (nonatomic) BOOL isLiked;

/**
 * Type of a video in facebook terms.
 *
 * @see mFacebookVideoType
 */
@property (nonatomic) enum mFacebookVideoType facebookVideoType;

@end
