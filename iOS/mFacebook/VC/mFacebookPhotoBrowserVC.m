#import "mFacebookPhotoBrowserVC.h"
#import "mFacebookParameters.h"
#import "iphnavbardata.h"
#import "UIColor+HSL.h"
#import "UIColor+RGB.h"

@interface mFacebookPhotoBrowserViewController ()

@property (nonatomic, strong) MWPhoto *curPhoto;
@property (nonatomic, strong) UIBarButtonItem *likeButtonItem;

@end

@implementation mFacebookPhotoBrowserViewController

-(void)dealloc
{
  self.curPhoto = nil;
  self.likeButtonItem = nil;
  
  [auth_Share sharedInstance].delegate = nil;
  [auth_Share sharedInstance].viewController = nil;
  
  [super dealloc];
}

#pragma mark - Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  
  self.navigationController.navigationBar.alpha = 1.0f;
  
  UIToolbar *toolBar = [self toolBar];
  [self placeButtonsOnToolBar:(UIToolbar *)toolBar];
}

-(void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  
  self.navigationController.navigationBar.hidden = NO;

  [auth_Share sharedInstance].viewController = self;
  [auth_Share sharedInstance].delegate = self;
  
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(customizeNavBarAppearanceCompleted:)
                                               name:TIPhoneNavBarDataCustomizeNavBarAppearanceCompleted
                                             object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
 
  [[NSNotificationCenter defaultCenter] removeObserver:self
                                                  name:TIPhoneNavBarDataCustomizeNavBarAppearanceCompleted
                                              object:nil];
}

#pragma mark - Interface
-(void)updateNavigation {
  self.title = NSBundleLocalizedString(@"mFacebook_photo", @"Photo");
}

-(void)placeButtonsOnToolBar:(UIToolbar *)toolBar
{
  UIImage *likeImage = [UIImage imageNamed:resourceFromBundle(@"mFacebook_like_media")];
  UIImage *shareImage = [UIImage imageNamed:resourceFromBundle(@"mFacebook_share_media")];
  
  self.likeButtonItem = [[[UIBarButtonItem alloc] initWithImage:likeImage
                                                    style:UIBarButtonItemStylePlain
                                                   target:self
                                                   action:@selector(likeItem)] autorelease];
  
  UIBarButtonItem *shareButtonItem = [[[UIBarButtonItem alloc] initWithImage:shareImage
                                                     style:UIBarButtonItemStylePlain
                                                    target:self
                                                    action:@selector(shareItem)] autorelease];
  
  UIBarButtonItem *flexibleSpace = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                target:nil
                                                                action:nil] autorelease];
  
  self.likeButtonItem.tintColor = [UIColor whiteColor];
  shareButtonItem.tintColor = [UIColor whiteColor];
  
  NSArray *toolbarItems = @[flexibleSpace, self.likeButtonItem, flexibleSpace, flexibleSpace, shareButtonItem, flexibleSpace];
  
  [toolBar setItems:toolbarItems animated:NO];
}

-(void)customizeNavBarAppearanceCompleted:(NSNotification *)notification
{
  UINavigationBar *navBar = self.navigationController.navigationBar;
  UIColor *backgroundColor = [mFacebookParameters sharedParameters].design.color1;
  
  if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
    if (backgroundColor.isLight)
      [navBar setBarTintColor:[backgroundColor blend:[[UIColor blackColor] colorWithAlphaComponent:0.2f]]];
    else
      [navBar setBarTintColor:[backgroundColor blend:[[UIColor whiteColor] colorWithAlphaComponent:0.4f]]];
    
    [navBar setTintColor:[UIColor blackColor]];
    [navBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor],
                                     NSFontAttributeName : [UIFont systemFontOfSize:18.0f]}];
  }
}


-(void)pageChangedWithPhoto:(id <MWPhoto>)photo
{
  self.curPhoto = (MWPhoto *) photo;
  
  [self indicateIfLiked];
}

-(void)indicateIfLiked
{
  if (self.curPhoto.facebookLiked)
  {
    self.likeButtonItem.enabled = NO;
  }
  else
  {
    self.likeButtonItem.enabled = YES;
    
    NSMutableSet *likedURLs = [mFacebookParameters sharedParameters].likedURLs;
    NSString *link = self.curPhoto.photoURL.absoluteString;
    MWPhoto *likedPhoto = self.curPhoto;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                     BOOL success = NO;
                     
                     for(NSString *likedURL in likedURLs)
                     {
                       if ([likedURL rangeOfString:link].location != NSNotFound)
                       {
                         likedPhoto.facebookLiked = YES;
                         success = YES;
                         break;
                       }
                     }
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                       if(success && self.curPhoto == likedPhoto)
                       {
                         self.likeButtonItem.enabled = NO;
                         [likedURLs addObject:link];
                       }
                     });
                   });
  }
}

#pragma mark - Actions
-(void)likeItem
{
  NSString *urlToLike = self.curPhoto.photoURL.absoluteString;
  [[auth_Share sharedInstance] postLikeForURL:urlToLike
                        withNotificationNamed:nil
                shouldShowLoginRequiredPrompt:NO];
}

-(void)shareItem
{
  MWPhoto *photo = self.curPhoto;
  
  static NSString *URLStringThumbnail = @"http://facebook.com/%@/photos/%@";
  
  NSString *URLStringToShare = [NSString stringWithFormat:URLStringThumbnail,
                                [mFacebookParameters sharedParameters].facebookId,
                                photo.ID];
  
  NSMutableDictionary *shareData = [NSMutableDictionary dictionaryWithObject:URLStringToShare forKey:@"link"];
  
  [[auth_Share sharedInstance] shareContentUsingService:auth_ShareServiceTypeFacebook
                                               fromUser:nil
                                               withData:shareData
                                showLoginRequiredPrompt:NO];
}

#pragma mark - auth_Share delegate
-(void)didFacebookLikeForURL:(NSString *)URL error:(NSError *)error
{
  if(!error)
  {
    NSString *likeUrl = self.curPhoto.photoURL.absoluteString;
    if ([likeUrl isEqualToString:URL])
    {
      [[auth_Share sharedInstance] postLikeForId:self.curPhoto.ID];
    }
  }
}

-(void)didFacebookLikeForId:(NSString *)ID error:(NSError *)error
{
  if(!error)
  {
    NSString *likeID = self.curPhoto.ID;
    if ([likeID isEqualToString:ID])
    {
      self.curPhoto.facebookLiked = YES;
      self.likeButtonItem.enabled = NO;
    }
  }
}

-(void)didAuthorizeOnService:(auth_ShareServiceType)serviceType error:(NSError *)error
{
  if(!error &&
     serviceType == auth_ShareServiceTypeFacebook)
  {
    [[auth_Share sharedInstance] loadFacebookLikedURLs];
  }
}

-(void)didLoadFacebookLikedURLs:(NSMutableSet *)likedItems error:(NSError *)error
{
  if(!error)
  {
    [mFacebookParameters sharedParameters].likedURLs = likedItems;
    [self indicateIfLiked];
  }
}

#pragma mark - Autorotate handlers

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
  return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

-(BOOL)shouldAutorotate
{
  return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
  return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
  return UIInterfaceOrientationPortrait;
}

@end
