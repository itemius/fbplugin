#import "mFacebookPhotoAlbumVC.h"
#import "mFacebookPhotoGridViewCell.h"

#import <UIImageView+WebCache.h>
#import "UIColor+image.h"

@interface mFacebookPhotoAlbumViewController ()

@property (nonatomic, strong) NRGridView *gridView;

@property (nonatomic, strong) mFacebookPhotoAlbum *album;
@property (nonatomic, strong) NSMutableArray *photos;

@property (nonatomic, strong) NSString *albumNextPage;
@property (nonatomic, assign) BOOL photosLoadingInProgress;

@end

@implementation mFacebookPhotoAlbumViewController

-(instancetype) initWithPhotoAlbum:(mFacebookPhotoAlbum *)album
{
  self = [super init];
  
  if (self)
  {
    self.album = album;
  }
  
  return self;
}

-(void)dealloc
{
  self.gridView = nil;
  
  self.album = nil;
  self.photos = nil;
  self.albumNextPage = nil;
  
  [super dealloc];
}

#pragma mark - View lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  
  self.title = self.album.name;
  
  [self placeGridView];
  [self placeLoadingFooter];
  
  [self loadPhotos];
}

#pragma mark - Interface
-(void)placeGridView
{
  CGRect frame = self.view.frame;
  
  self.gridView = [[[NRGridView alloc] initWithFrame:frame] autorelease];
  
  self.gridView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
  
  CGFloat cellSize = self.view.bounds.size.width / 3;
  self.gridView.cellSize = CGSizeMake( cellSize, cellSize );
  self.gridView.delegate = self;
  self.gridView.dataSource = self;
  
  self.view = self.gridView;
}

#pragma mark - Loading

-(void)placeLoadingFooter
{
  CGRect footerRect = (CGRect)
  {
    0.0f,
    0.0f,
    self.gridView.bounds.size.width,
    40.0f,
  };
  
  UIActivityIndicatorView *loadingIndicator = [[UIActivityIndicatorView alloc] initWithFrame:footerRect];
  loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
  
  [loadingIndicator startAnimating];
  
  self.gridView.gridFooterView = loadingIndicator;
  
  [loadingIndicator release];
}

-(void)removeLoadingFooter
{
  self.gridView.gridFooterView = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
}

-(void)loadPhotos
{
  self.photosLoadingInProgress = YES;
  
  NSString *graphPath;
  if (!self.photos || !self.albumNextPage)
  {
    NSString *albumId = self.album.albumID;
    graphPath = [NSString stringWithFormat:@"%@/photos?fields=id,picture,source&limit=20", albumId];
  }
  else
    graphPath = self.albumNextPage;
  
  [aSha postFacebookRequestWithGraphPath:graphPath
                                   completion:^(NSDictionary *data, NSError *error) {
                                     if(!error)
                                     {
                                       self.albumNextPage = [[data objectForKey:@"paging"] objectForKey:@"next"];
                                       
                                       NSArray *photosData = [data objectForKey:@"data"];
                                       
                                       if (!self.photos)
                                         self.photos = [NSMutableArray array];
                                       
                                       for (NSDictionary *photo in photosData)
                                       {
                                         MWPhoto *mwPhoto = [MWPhoto photoWithURL:[NSURL URLWithString:photo[@"source"]]];
                                         mwPhoto.ID = photo[@"id"];
                                         mwPhoto.pictureURL = [NSURL URLWithString:photo[@"picture"]];
                                        
                                         [self.photos addObject:mwPhoto];
                                       }
                                       
                                       [self.gridView reloadData];
                                     }
                                     else
                                     {
                                       NSLog(@"ERROR getting album data from FB!");
                                     }
                                     
                                     self.photosLoadingInProgress = NO;
                                   }];
}

#pragma mark - NRGridView

-(NSInteger)gridView:(NRGridView *)gridView numberOfItemsInSection:(NSInteger)section
{
  return self.photos.count;
}

- (NRGridViewCell *)gridView:(NRGridView *)gridView
      cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *MyCellIdentifier = @"mFacebookPhotosCell";
  
  mFacebookPhotoGridViewCell *cell = (mFacebookPhotoGridViewCell *)[gridView dequeueReusableCellWithIdentifier:MyCellIdentifier];
  
  if (cell == nil)
  {
    cell = [[[mFacebookPhotoGridViewCell alloc] initWithReuseIdentifier:MyCellIdentifier] autorelease];
    cell.backgroundColor = [UIColor clearColor];
    
    cell.cellPadding = CGSizeMake(1.0f, 1.0f);
  }
  
  MWPhoto *photo = (MWPhoto  *)self.photos[indexPath.row];
  UIImage *placeholder = [[UIColor lightGrayColor] asImageWithSize:gridView.cellSize];
  [cell.imageView setImageWithURL:photo.pictureURL placeholderImage:placeholder];
  
  BOOL needLoad = indexPath.row > (self.photos.count - 4);
  BOOL allLoaded = self.photos.count && !self.albumNextPage;
  
  if (!self.photosLoadingInProgress && needLoad && !allLoaded)
    [self loadPhotos];
  
  if (allLoaded) [self removeLoadingFooter];
  
  return cell;
}

-(void)gridView:(NRGridView *)gridView didSelectCellAtIndexPath:(NSIndexPath *)indexPath
{
  [gridView deselectCellAtIndexPath:indexPath
                           animated:NO];
  
  mFacebookPhotoBrowserViewController *browser = [[[mFacebookPhotoBrowserViewController alloc] initWithDelegate:self] autorelease];
  browser.displayActionButton = YES;
  browser.bSavePicture        = YES;
  browser.leftBarButtonCaption = self.album.name;
  [browser setInitialPageIndex:indexPath.row];
  
  [self.navigationController pushViewController:browser animated:YES];
}

#pragma mark MWPhotoBrowserDelegate
-(NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
  return self.photos.count;
}

-(MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index
{
  return self.photos[index];
}


@end
