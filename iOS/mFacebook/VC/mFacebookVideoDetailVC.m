#import "mFacebookVideoDetailVC.h"

static NSString *videoPlayerHTMLTemplate =
@"<!DOCTYPE html>\
<html>\
<body style='margin:0' bgcolor='black'>\
<video src=\"%@\" poster=\"%@\"\
       width=\"%.0f\" height=\"%.0f\"\
       controls\
       webkitallowfullscreen\
       mozallowfullscreen\
       allowfullscreen>\
</video>\
</body>\
</html>";

#define kVideoPlayerWebViewDefaultWidth 320.0f
#define kVideoPlayerWebViewDefaultHeight 180.0f
#define kVideoPlayerWebViewControlsHeight 44.0f
#define kSocialToolbarHeight 44.0f
#define kBackgroundColor [UIColor blackColor]

#define kStatusBarReappearingDelay 0.3f

@interface mFacebookVideoDetailVC()
{
  UIWebView *videoPlayerWebView;
  
  mFacebookSocialToolbar *socialToolbar;
  
  NSString *videoPlayerHTMLString;
}

@end

@implementation mFacebookVideoDetailVC

-(instancetype)initWithVideoDescription:(mFacebookVideoDescription *)videoDescription
{
  self = [super initWithNibName:nil bundle:nil];
  
  if(self)
  {
    _videoDescription = [videoDescription retain];
  }
  
  return self;
}

-(void)dealloc
{
  self.videoDescription = nil;
  
  [videoPlayerHTMLString release];
  videoPlayerHTMLString = nil;
  
  [videoPlayerWebView release];
  videoPlayerWebView = nil;
  
  [socialToolbar release];
  socialToolbar = nil;
  
  [super dealloc];
}

#pragma mark - View lifecycle

-(void)viewDidLoad
{
  [super viewDidLoad];
  
  [self setupInterface];
}

-(void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];

  [self indicateIfLiked];
}

-(void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
}

#pragma mark - Interface
-(void)setupInterface
{
  self.refreshControl = nil;
  self.view.backgroundColor = kBackgroundColor;
  
  self.title = NSBundleLocalizedString(@"mFacebook_video", @"Video");
  
  self.tableView.scrollEnabled = NO;
  
  [self placeVideoPlayer];
  [self placeSocialToolbar];
}

-(void)placeVideoPlayer
{
  [self setupVideoPlayerWebView];
  [self.view addSubview:videoPlayerWebView];
  
  [self prepareVideoPlayerHTMLString];
  [videoPlayerWebView loadHTMLString:videoPlayerHTMLString baseURL:nil];
}

-(void)placeSocialToolbar
{
  CGRect socialToolbarFrame = (CGRect)
  {
    0.0f,
    self.view.bounds.size.height - kSocialToolbarHeight,
    self.view.bounds.size.width,
    kSocialToolbarHeight
  };
  
  socialToolbar = [[mFacebookSocialToolbar alloc] initWithFrame:socialToolbarFrame];
  socialToolbar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
  socialToolbar.facebookSocialToolbarDelegate = self;
  socialToolbar.facebookItemId = self.videoDescription.facebookId;
  
  [self.view addSubview:socialToolbar];
}

-(void)indicateIfLiked
{
  if(self.videoDescription.isLiked)
    {
    [socialToolbar indicateLikeSucceeded];
    return;
    }
  
  NSSet *likedURLs = [mFacebookParameters sharedParameters].likedURLs;
  
  if(!likedURLs.count)
    {
    return;
    }
  
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                 ^{
                   NSString *link = self.videoDescription.sourceVideoURLString;
                   
                   self.videoDescription.isLiked = [[mFacebookParameters sharedParameters] isURLStringLiked:link];
                   
                   dispatch_async(dispatch_get_main_queue(), ^{
                     if(self.videoDescription.isLiked)
                       {
                       [socialToolbar indicateLikeSucceeded];
                       }
                   });
                 });
}

#pragma mark - Playing video

-(void)setupVideoPlayerWebView
{
    CGRect videoPlayerFrame = (CGRect){
      0.0f,
      0.0f,
      self.view.bounds.size.width,
      ceilf(self.view.bounds.size.width * 9 / 16) + kVideoPlayerWebViewControlsHeight
    };
    
    videoPlayerWebView = [[UIWebView alloc] initWithFrame:videoPlayerFrame];
    videoPlayerWebView.backgroundColor = self.view.backgroundColor;
    videoPlayerWebView.delegate = self;
    videoPlayerWebView.scrollView.scrollEnabled = NO;
    videoPlayerWebView.scalesPageToFit = NO;
    videoPlayerWebView.contentMode = UIViewContentModeScaleAspectFit;
    videoPlayerWebView.autoresizesSubviews = YES;
    videoPlayerWebView.allowsInlineMediaPlayback = YES;
    videoPlayerWebView.mediaPlaybackRequiresUserAction = NO;
    videoPlayerWebView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    videoPlayerWebView.hidden = YES;
  
  CGPoint videoPlayerWebViewCenter = self.view.center;
  videoPlayerWebViewCenter.y -= kFacebookNavBarHeight;
  
  videoPlayerWebView.center = videoPlayerWebViewCenter;
}

-(void)setVideoDescription:(mFacebookVideoDescription *)videoDescription
{
  if(_videoDescription != videoDescription)
  {
    [videoDescription retain];
    [_videoDescription release];
    
    _videoDescription = videoDescription;
    
    [self prepareVideoPlayerHTMLString];
  }
}

-(void)prepareVideoPlayerHTMLString
{
  NSString *src = _videoDescription.sourceVideoURLString;
  NSString *poster = _videoDescription.preferredThumbnailURLString;
  
  CGFloat width = kVideoPlayerWebViewDefaultWidth;
  CGFloat height = kVideoPlayerWebViewDefaultHeight;
  
  if(videoPlayerWebView)
  {
    width = videoPlayerWebView.bounds.size.width;
    height = videoPlayerWebView.bounds.size.height - kVideoPlayerWebViewControlsHeight;
  }
  
  videoPlayerHTMLString = [[NSString stringWithFormat:videoPlayerHTMLTemplate, src, poster, width, height] retain];
  
  [videoPlayerWebView loadHTMLString:videoPlayerHTMLString baseURL:nil];
}

#pragma mark - mFacebookSocialToolbarDelegate
-(void)itemLikedFromToolbar:(mFacebookSocialToolbar *)toolbar
{
  NSString *urlToLike = self.videoDescription.sourceVideoURLString;
  
  [aSha postLikeForURL:urlToLike
      withNotificationNamed:nil
   shouldShowLoginRequiredPrompt:NO];
}

-(void)itemSharedFromToolbar:(mFacebookSocialToolbar *)toolbar
{
  NSString *link = self.videoDescription.sourceVideoURLString;
  
  NSMutableDictionary *shareData = [NSMutableDictionary dictionaryWithObject:link forKey:@"link"];
  
  [aSha shareContentUsingService:auth_ShareServiceTypeFacebook
                             fromUser:nil
                             withData:shareData
              showLoginRequiredPrompt:NO];
}

#pragma mark - auth_Share delegate
-(void)didFacebookLikeForURL:(NSString *)Id error:(NSError *)error
{
  if(!error)
  {
    [aSha postLikeForId:self.videoDescription.facebookId];
  }
}

-(void)didFacebookLikeForId:(NSString *)URL error:(NSError *)error
{
  if(!error)
  {
    self.videoDescription.isLiked = YES;
    
    [socialToolbar indicateLikeSucceeded];
  }
}

-(void)didLoadFacebookLikedURLs:(NSMutableSet *)likedItems error:(NSError *)error
{
  [super didLoadFacebookLikedURLs:likedItems
                            error:error];
  
  if(!error)
  {
    [self indicateIfLiked];
  }
}

#pragma mark - UIWebView delegate

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
  videoPlayerWebView.hidden = NO;
}

#pragma mark - Handling iOS 8 fullscreen video behavior
-(void)videoPlayerWillEnterFullscreen:(NSNotification *)notification
{
  [UIView animateWithDuration:0.0f
                   animations:^{
                     [super videoPlayerWillEnterFullscreen:notification];
                   } completion:^(BOOL finished) {
                     videoPlayerWebView.hidden = YES;
                   }];
}

-(void)videoPlayerWillExitFullscreen:(NSNotification *)notification
{
  [UIView animateWithDuration:0.0f
                   animations:^{
                     [super videoPlayerWillExitFullscreen:notification];
                   } completion:^(BOOL finished) {
                     videoPlayerWebView.hidden = NO;
                   }];
}

@end
