#import "mFacebookBaseVC.h"
#import "mFacebookFeedTableViewCell.h"
#import "mFacebookPhotoBrowserVC.h"
#import "mFacebookFeedHeaderView.h"

#import <auth_ShareDelegate.h>
#import "TTTAttributedLabel.h"

/**
 *  Main module class for widget mFacebook. Module entry point.
 */
@interface mFacebookViewController : mFacebookBaseViewController<UITableViewDelegate,
                                                                 UITableViewDataSource,
                                                                 mFacebookFeedTableViewCellDelegate,
                                                                 auth_ShareDelegate,
                                                                 MWPhotoBrowserDelegate,
                                                                 TTTAttributedLabelDelegate>

@end
