#import <UIKit/UIKit.h>
#import "mFacebookParameters.h"
#import "iphnavbardata.h"
#import <auth_Share.h>
#import "reachability.h"

#define kFacebookNavBarColorLight [[UIColor whiteColor] colorWithAlphaComponent:0.5f]
#define kFacebookNavBarColorDark [[UIColor blackColor] colorWithAlphaComponent:0.2f]
#define kFacebookNavBarColor kFacebookNavBarColorDark
#define kFacebookNavBarHeight 44.0f

/**
 * Parent controller for almost all controllers in the module (the exceptions are mFacebookPhotoBrowserVC, mFacebookWebVC).
 * Handles routine tasks like the Internet connection state, appearance, rotation, authorization handling, data refreshing.
 */
@interface mFacebookBaseViewController : UITableViewController<auth_ShareDelegate>
{
  @protected
  /**
   * Instance of the social/sharing library. Same for all the descendants.
   */
  auth_Share *aSha;
}

/**
 *  Parameters inheritable instance.
 */
@property (nonatomic, strong) mFacebookParameters *parameters;

/**
 *  Defines TabBar behavior
 */
@property (nonatomic, assign) BOOL tabBarIsHidden;

/**
 *  Option for show or hide TabBar
 */
@property (nonatomic, assign) BOOL showTabBar;

/**
 * When implementing custom navigation flow, specifies the controller index in
 * navigationController's <code>viewControllers</code> array to pop.
 */
@property (nonatomic) NSInteger controllerIndexToPopTo;

/**
 * Flag, tells whether the Internet is reachable or not.
 */
@property (nonatomic) BOOL isInternetReachable;

/**
 * Notificatiom handler, called when core navigation controller finishes setting it's appearance.
 * Proper place to preform any navbar customizations.
 */
-(void)customizeNavBarAppearanceCompleted:(NSNotification *)notification;

/**
 * When implementing custom navigation flow, specifies the controller index in
 * navigationController's <code>viewControllers</code> array to pop.
 *
 * @return index of self in navigationController's <code>viewControllers</code>, decreased by one.
 */
-(NSInteger)previousViewControllerIndex;


/**
 * Handler for doing any UI adjustments before module window goes fullscreen.
 * (Particularly, used on iOS 8 before video goes fullscreen).
 *
 * @see videoPlayerWillExitFullscreen:
 */
-(void)videoPlayerWillEnterFullscreen:(NSNotification *)notification;

/**
 * Handler for restoring navbar height after module's window exits fullscreen.
 * (Particularly, used to restore navbar's height after quitting fullscreen video on iOS 8).
 *
 * @see videoPlayerWillEnterFullscreen:
 */
-(void)videoPlayerWillExitFullscreen:(NSNotification *)notification;

@end
