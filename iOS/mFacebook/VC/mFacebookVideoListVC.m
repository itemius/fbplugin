#import "mFacebookVideoListVC.h"
#import "mFacebookParameters.h"
#import "mFacebookVideoDetailVC.h"

#import "NSString+colorizer.h"
#import "UIColor+image.h"
#import <UIImageView+WebCache.h>

#define mFacebookOlderVideosLoadingStartDistance 15

typedef NS_ENUM(NSInteger, mFacebookVideosLoadingStrategy)
{
  mFacebookVideosLoadingStrategyOlder,
  mFacebookVideosLoadingStrategyLoadOnStart,
  mFacebookVideosLoadingStrategyNone
};


@interface mFacebookVideoListViewController ()
{
  mFacebookVideosLoadingStrategy currentLoadingStrategy;
}

@property(nonatomic, strong) NRGridView *gridView;
@property(nonatomic, strong) NSMutableArray *videoDescriptions;
@property(nonatomic, strong) NSString *nextPage;

@end

@implementation mFacebookVideoListViewController

-(void)dealloc
{
  self.gridView = nil;
  self.videoDescriptions = nil;
  
  [super dealloc];
}

#pragma mark View lifecycle

-(void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
}

-(void)viewDidLoad {
  [super viewDidLoad];
  
  [self  setupInterface];
  
  currentLoadingStrategy = mFacebookVideosLoadingStrategyLoadOnStart;
  [self loadVideos];
}

#pragma mark Interface

-(void)setupInterface
{
  self.title = NSBundleLocalizedString(@"mFacebook_video", @"Video");
  self.refreshControl = nil;
  
  [self placeGridView];
  [self placeLoadingFooterToView:self.gridView];
}

-(void)placeGridView
{
  self.gridView = [[[NRGridView alloc] initWithFrame:CGRectMake(0,
                                                                0.0f,
                                                                self.view.bounds.size.width,
                                                                self.view.bounds.size.height)] autorelease];
  
  self.gridView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
  
  CGFloat cellSize = self.view.bounds.size.width / 3;
  [self.gridView setCellSize:CGSizeMake( cellSize, cellSize )];
  [self.gridView setDelegate:self];
  [self.gridView setDataSource:self];
  
  self.view = self.gridView;
}

-(void)placeLoadingFooterToView:(UIView *) view
{
  CGRect footerRect = (CGRect)
  {
    0.0f,
    0.0f,
    self.view.bounds.size.width,
    40.0f,
  };
  
  UIActivityIndicatorView *loadingIndicator = [[UIActivityIndicatorView alloc] initWithFrame:footerRect];
  loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
  loadingIndicator.hidesWhenStopped = YES;
  
  [loadingIndicator startAnimating];
  
  self.gridView.gridFooterView = loadingIndicator;
}

-(void)removeLoadingFooterForView:(UIView *) view
{
  self.gridView.gridFooterView = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
}

#pragma mark - Loading

-(void)loadVideos
{
  if(currentLoadingStrategy == mFacebookVideosLoadingStrategyLoadOnStart)
    {
    self.gridView.scrollEnabled = NO;
    }
  
  NSString *customGraphPath = nil;
  NSUInteger limit = 90;
  
  if(currentLoadingStrategy == mFacebookVideosLoadingStrategyOlder){
    if (self.nextPage)
      {
      customGraphPath = self.nextPage;
      limit = 0; //ignore when custom path
      }
  }
  
  auth_ShareFacebookGenericCompletionBlock completion = ^(NSDictionary *data, NSError *error)
  {
  if(!error)
    {
    if (!self.videoDescriptions)
      {
      self.videoDescriptions = [NSMutableArray array];
      }
    
    self.nextPage = [[data objectForKey:@"paging"] objectForKey:@"next"];
    
    NSArray *videoDictionaries = [data objectForKey:@"data"];
    
    for (NSDictionary *videoDictionary in videoDictionaries)
      {
      mFacebookVideoDescription *videoDescription = [[mFacebookVideoDescription alloc] initWithDictionary:videoDictionary];
      [self.videoDescriptions addObject:videoDescription];
      
      [videoDescription release];
      }
    
    [self.gridView reloadData];
    }
  else
    {
    NSLog(@"ERROR getting videos from FB!");
    [self removeLoadingFooterForView:self.gridView];
    }
  
  
  if(currentLoadingStrategy == mFacebookVideosLoadingStrategyLoadOnStart)
    {
    self.gridView.scrollEnabled = YES;
    }
  
  if (self.videoDescriptions.count && !self.nextPage.length)
    {
    [self removeLoadingFooterForView:self.gridView];
    }
  
  currentLoadingStrategy = mFacebookVideosLoadingStrategyNone;
  };
  
  [aSha loadFacebookVideosForId:[mFacebookParameters sharedParameters].facebookId
                customGraphPath:customGraphPath
                          since:0
                          until:0
                          limit:limit
                     completion:completion];
}

#pragma mark - NRGridView delegate

-(NSInteger)gridView:(NRGridView *)gridView numberOfItemsInSection:(NSInteger)section
{
  return self.videoDescriptions.count;
}

- (NRGridViewCell *)gridView:(NRGridView *)gridView
      cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *cellIdentifier = @"Cell";
  
  NRGridViewCell *cell = [gridView dequeueReusableCellWithIdentifier:cellIdentifier];
  
  if (cell == nil)
  {
    cell = [[[NRGridViewCell alloc] initWithReuseIdentifier:cellIdentifier] autorelease];
    cell.backgroundColor = [UIColor clearColor];
    cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
    cell.imageView.clipsToBounds = YES;
    
    cell.cellPadding = CGSizeMake(1.0f, 1.0f);
  }

  mFacebookVideoDescription *videoDescription = self.videoDescriptions[indexPath.row];
  
  NSURL *thumbnailURL = [NSURL URLWithString:videoDescription.pictureURLString];
  
  UIImage *placeholder = [[UIColor lightGrayColor] asImageWithSize:cell.imageView.frame.size];
  [cell.imageView setImageWithURL:thumbnailURL placeholderImage:placeholder];
  
  [self loadOlderVideosIfNeeded:indexPath.row];
  
  return cell;
}

-(void)gridView:(NRGridView *)gridView didSelectCellAtIndexPath:(NSIndexPath *)indexPath
{
  [gridView deselectCellAtIndexPath:indexPath
                           animated:NO];
  
  mFacebookVideoDescription *videoDescription = [self.videoDescriptions objectAtIndex:indexPath.row];
  
  mFacebookVideoDetailVC *videoPlayerVC = [[mFacebookVideoDetailVC alloc] initWithVideoDescription:videoDescription];
  
  [self.navigationController pushViewController:videoPlayerVC animated:YES];
  [videoPlayerVC release];
}

-(void)loadOlderVideosIfNeeded:(NSInteger)currentRow
{
  if(self.nextPage){
    if(currentRow >= self.videoDescriptions.count - mFacebookOlderVideosLoadingStartDistance)
    {
      if(currentLoadingStrategy != mFacebookVideosLoadingStrategyOlder)
      {
        currentLoadingStrategy = mFacebookVideosLoadingStrategyOlder;
        
        [self loadVideos];
      }
    }
  }
}

@end
