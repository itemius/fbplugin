#import "mFacebookBaseVC.h"
#import "mFacebookVideoDescription.h"
#import "mFacebookSocialToolbar.h"


/**
 *  Detail controller for a selected video.
 *  You can play, like and share video here.
 */
@interface mFacebookVideoDetailVC : mFacebookBaseViewController<mFacebookSocialToolbarDelegate,
                                                                UIWebViewDelegate>

/**
 * Initializes the controller with passed videoDescription object.
 *
 * @param videoDescription - entity describing a video to be presented.
 *
 * @see mFacebookVideoDescription
 */
-(instancetype)initWithVideoDescription:(mFacebookVideoDescription *)videoDescription;

/**
 * Description of a video being presented.
 */
@property (nonatomic, retain) mFacebookVideoDescription *videoDescription;

@end
