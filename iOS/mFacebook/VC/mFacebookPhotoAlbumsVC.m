#import "mFacebookPhotoAlbumsVC.h"
#import "mFacebookParameters.h"
#import "mFacebookPhotoAlbum.h"
#import "mFacebookPhotoAlbumTableViewCell.h"
#import "mFacebookPhotoAlbumVC.h"
#import "mFacebookPhotoGridViewCell.h"

#import "NSString+colorizer.h"
#import "UIColor+image.h"
#import <UIImageView+WebCache.h>


#define kTabButtonsHeight 44.0f
#define kTabButtonsCount 3
#define kTabButtonsBackgroundColor [@"#EEEEEE" asColor]
#define kTabButtonsSeparatorColor [[UIColor blackColor] colorWithAlphaComponent:0.15f]
#define kTabButtonsSeparatorWidth 1.0f
#define kTabButtonsSeparatorTopBottomMargin 7.0f
#define kTabButtonsFont [UIFont boldSystemFontOfSize:12.0f]
#define kTabButtonsTextColor [[UIColor blackColor] colorWithAlphaComponent:0.8f]
#define kTabButtonsSelectionTextColor [@"#4F8CFF" asColor]

const NSUInteger ALBUM_TABLE_VIEW_TAG = 66;

typedef NS_ENUM(NSInteger, mFacebookPhotoAlbumSelectedTab)
{
  mFacebookPhotoAlbumSelectedTabUploads,
  mFacebookPhotoAlbumSelectedTabAlbums
};

@interface mFacebookPhotoAlbumsViewController ()

@property(nonatomic, strong) NRGridView *gridView;
@property(nonatomic, strong) NSMutableArray *uploadedPhotos;
@property(nonatomic, strong) NSString *uploadedPhotosNextPage;
@property(nonatomic, strong) UIButton *uploadedPhotosTabButton;
@property(nonatomic, strong) UILabel *uploadedPhotosTabLabel;

@property(nonatomic, strong) UITableView *albumTableView;
@property(nonatomic, strong) NSMutableArray *albums;
@property(nonatomic, strong) NSString *albumsNextPage;
@property(nonatomic, strong) UIButton *albumsTabButton;
@property(nonatomic, strong) UILabel *albumsTabLabel;

@property(nonatomic, assign) mFacebookPhotoAlbumSelectedTab selectedTab;
@property(nonatomic, assign) BOOL uploadedPhotosLoadingInProgress;
@property(nonatomic, assign) BOOL albumsLoadingInProgress;


@end

@implementation mFacebookPhotoAlbumsViewController

-(void)dealloc
{
  self.gridView = nil;
  self.uploadedPhotos = nil;
  self.uploadedPhotosNextPage = nil;
  self.uploadedPhotosTabButton = nil;
  self.uploadedPhotosTabLabel = nil;
  
  self.albumTableView = nil;
  self.albums = nil;
  self.albumsNextPage = nil;
  self.albumsTabButton = nil;
  self.albumsTabLabel = nil;
  
  [super dealloc];
}

#pragma mark - View lifecycle

-(void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
}

-(void)viewDidLoad {
  [super viewDidLoad];

  self.title = NSBundleLocalizedString(@"mFacebook_photo", @"Photo");
  
  self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
  self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
  self.tableView.scrollEnabled = NO;
  
  self.refreshControl = nil;
  
  [self setupInterface];
  
  [self loadUploadedPhotos];
  [self loadAlbums];
}

#pragma mark - Interface
-(void)setupInterface
{
  [self placeUploadTabButton];
  [self placeAlbumsTabButton];
  [self placeGridView];
  [self placeLoadingFooterToView:self.gridView];
  [self placeTableView];
  [self placeLoadingFooterToView:self.albumTableView];
  
  self.selectedTab = mFacebookPhotoAlbumSelectedTabUploads;
}

-(void)placeUploadTabButton
{
  CGFloat tabButtonsWidth = self.view.frame.size.width / 2;
  
  CGRect uploadsTabButtonFrame = (CGRect)
  {
    0,
    0,
    tabButtonsWidth,
    kTabButtonsHeight
  };
  
  self.uploadedPhotosTabButton = [[[UIButton alloc] initWithFrame:uploadsTabButtonFrame] autorelease];
  self.uploadedPhotosTabButton.backgroundColor = kTabButtonsBackgroundColor;
  [self.uploadedPhotosTabButton addTarget:self action:@selector(uploadsTabButtonTapped) forControlEvents:UIControlEventTouchUpInside];
  
  [self.view addSubview:self.uploadedPhotosTabButton];
  
  self.uploadedPhotosTabLabel = [[[UILabel alloc] init] autorelease];
  self.uploadedPhotosTabLabel.font = kTabButtonsFont;
  self.uploadedPhotosTabLabel.backgroundColor = [UIColor clearColor];
  self.uploadedPhotosTabLabel.textColor = kTabButtonsSelectionTextColor;
  self.uploadedPhotosTabLabel.text =  NSBundleLocalizedString(@"mFacebook_uploads", @"Uploads");
  
  CGSize uploadsTabLabelSize = [self.uploadedPhotosTabLabel.text sizeWithFont:self.uploadedPhotosTabLabel.font];
  
  self.uploadedPhotosTabLabel.frame = CGRectMake((tabButtonsWidth - uploadsTabLabelSize.width) / 2,
                                          (kTabButtonsHeight / 2) - (uploadsTabLabelSize.height / 2),
                                          uploadsTabLabelSize.width, uploadsTabLabelSize.height);
  
  [self.uploadedPhotosTabButton addSubview:self.uploadedPhotosTabLabel];
  
  CGRect separatorFrame = (CGRect)
  {
    tabButtonsWidth - 1,
    kTabButtonsSeparatorTopBottomMargin,
    kTabButtonsSeparatorWidth,
    kTabButtonsHeight - (kTabButtonsSeparatorTopBottomMargin * 2)
  };
  
  UIView *separatorView = [[[UIView alloc] initWithFrame:separatorFrame] autorelease];
  separatorView.backgroundColor = kTabButtonsSeparatorColor;
  
  [self.uploadedPhotosTabButton addSubview:separatorView];
}

-(void)placeAlbumsTabButton
{
  CGFloat tabButtonsWidth = self.view.frame.size.width / 2;
  
  CGRect albumsTabButtonFrame = (CGRect)
  {
    tabButtonsWidth,
    0,
    tabButtonsWidth,
    kTabButtonsHeight
  };
  
  self.albumsTabButton = [[[UIButton alloc] initWithFrame:albumsTabButtonFrame] autorelease];
  self.albumsTabButton.backgroundColor = kTabButtonsBackgroundColor;
  [self.albumsTabButton addTarget:self action:@selector(albumsTabButtonTapped) forControlEvents:UIControlEventTouchUpInside];
  
  [self.view addSubview:self.albumsTabButton];
  
  self.albumsTabLabel = [[[UILabel alloc] init] autorelease];
  self.albumsTabLabel.font = kTabButtonsFont;
  self.albumsTabLabel.backgroundColor = [UIColor clearColor];
  self.albumsTabLabel.textColor = kTabButtonsTextColor;
  self.albumsTabLabel.text =  NSBundleLocalizedString(@"mFacebook_albums", @"Albums");
  
  CGSize albumsTabLabelSize = [self.albumsTabLabel.text sizeWithFont:self.albumsTabLabel.font];
  
  self.albumsTabLabel.frame = CGRectMake((tabButtonsWidth - albumsTabLabelSize.width) / 2,
                                          (kTabButtonsHeight / 2) - (albumsTabLabelSize.height / 2),
                                          albumsTabLabelSize.width, albumsTabLabelSize.height);
  
  [self.albumsTabButton addSubview:self.albumsTabLabel];
}

-(void)placeGridView
{
  self.gridView = [[[NRGridView alloc] initWithFrame:CGRectMake(0, kTabButtonsHeight,
                                                                self.view.bounds.size.width,
                                                                self.view.bounds.size.height - kTabButtonsHeight)] autorelease];
  self.gridView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
  
  CGFloat cellSize = self.view.bounds.size.width / 3;
  self.gridView.cellSize = CGSizeMake( cellSize, cellSize );
  self.gridView.delegate = self;
  self.gridView.dataSource = self;
  
  [self.view addSubview:self.gridView];
}

-(void)placeTableView
{
  self.albumTableView = [[[UITableView alloc] initWithFrame:CGRectMake(0, kTabButtonsHeight,
                                                                self.view.bounds.size.width,
                                                                self.view.bounds.size.height - kTabButtonsHeight)] autorelease];
  self.albumTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
  self.albumTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
  self.albumTableView.backgroundColor = [mFacebookParameters sharedParameters].design.color1;
  
  self.albumTableView.delegate = self;
  self.albumTableView.dataSource = self;
  
  self.albumTableView.tag = ALBUM_TABLE_VIEW_TAG;

  
  [self.view addSubview:self.albumTableView];
  
  [self.view bringSubviewToFront:self.gridView];
}

-(void)placeLoadingFooterToView:(UIView *) view
{
  CGRect footerRect = (CGRect)
  {
    0.0f,
    0.0f,
    self.view.bounds.size.width,
    40.0f,
  };
  
  UIActivityIndicatorView *loadingIndicator = [[[UIActivityIndicatorView alloc] initWithFrame:footerRect] autorelease];
  loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
  
  [loadingIndicator startAnimating];
  
  if ([view isKindOfClass:NRGridView.class])
      ((NRGridView *)view).gridFooterView = loadingIndicator;
  else if ([view isKindOfClass:UITableView.class])
      ((UITableView *)view).tableFooterView = loadingIndicator;
}

-(void)removeLoadingFooterForView:(UIView *) view
{
  if ([view isKindOfClass:NRGridView.class])
    ((NRGridView *)view).gridFooterView = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
  else if ([view isKindOfClass:UITableView.class])
    ((UITableView *)view).tableFooterView = nil;
}

#pragma mark - Loading

-(void)loadUploadedPhotos
{
  self.uploadedPhotosLoadingInProgress = YES;
  
  
  NSString *graphPath;
  if (!self.uploadedPhotos || !self.uploadedPhotosNextPage)
    {
    NSString *facebookID = [mFacebookParameters sharedParameters].facebookId;
    graphPath = [NSString stringWithFormat:@"%@/photos?fields=id,picture,source&type=uploaded&limit=90", facebookID];
    }
  else
    graphPath = self.uploadedPhotosNextPage;
  
  
  [aSha postFacebookRequestWithGraphPath:graphPath
                              completion:^(NSDictionary *data, NSError *error) {
                                if(!error)
                                  {
                                  self.uploadedPhotosNextPage = [[data objectForKey:@"paging"] objectForKey:@"next"];
                                  
                                  NSArray *photosData = [data objectForKey:@"data"];
                                  
                                  if (!self.uploadedPhotos)
                                    self.uploadedPhotos = [NSMutableArray array];
                                  
                                  for (NSDictionary *photo in photosData)
                                    {
                                    MWPhoto *mwphoto = [MWPhoto photoWithURL:[NSURL URLWithString:photo[@"source"]]];
                                    mwphoto.ID =  photo[@"id"];
                                    mwphoto.pictureURL = [NSURL URLWithString:photo[@"picture"]];
                                    
                                    
                                    [self.uploadedPhotos addObject:mwphoto];
                                    }
                                  
                                  [self.gridView reloadData];
                                  }
                                else
                                  {
                                  NSLog(@"ERROR getting uploaded photos data from FB!");
                                  }
                                
                                self.uploadedPhotosLoadingInProgress = NO;
                              }];
}

-(void)loadAlbums
{
  self.albumsLoadingInProgress = YES;
  
  NSString *graphPath;
  if (!self.albums || !self.albumsNextPage)
    {
    NSString *facebookID = [mFacebookParameters sharedParameters].facebookId;
    graphPath = [NSString stringWithFormat:@"%@/albums?fields=id,name,cover_photo,count&limit=10", facebookID];
    
    NSString *local = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([local isEqualToString:@"ru"])
      graphPath = [graphPath stringByAppendingString:@"&locale=ru_RU"];
    }
  else
    graphPath = self.albumsNextPage;
  
  [aSha postFacebookRequestWithGraphPath:graphPath
                              completion:^(NSDictionary *data, NSError *error) {
                                if(!error)
                                  {
                                  self.albumsNextPage = [[data objectForKey:@"paging"] objectForKey:@"next"];
                                  
                                  NSArray *albumsData = [data objectForKey:@"data"];
                                  
                                  if (!self.albums)
                                    self.albums = [NSMutableArray array];
                                  
                                  NSMutableArray *albums = [mFacebookPhotoAlbum itemsFromArray:albumsData];
                                  [self.albums addObjectsFromArray:albums];
                                  
                                  [self.albumTableView reloadData];
                                  }
                                else
                                  {
                                  NSLog(@"ERROR getting albums data from FB!");
                                  }
                                
                                self.albumsLoadingInProgress = NO;
                              }];
}


#pragma mark - Actions
-(void)uploadsTabButtonTapped
{
  if (self.selectedTab != mFacebookPhotoAlbumSelectedTabUploads)
    {
    self.uploadedPhotosTabLabel.textColor = kTabButtonsSelectionTextColor;
    self.albumsTabLabel.textColor = kTabButtonsTextColor;
    
    self.selectedTab = mFacebookPhotoAlbumSelectedTabUploads;
    
    [self.view bringSubviewToFront:self.gridView];
    }
}

-(void)albumsTabButtonTapped
{
  if (self.selectedTab != mFacebookPhotoAlbumSelectedTabAlbums)
    {
    self.uploadedPhotosTabLabel.textColor = kTabButtonsTextColor;
    self.albumsTabLabel.textColor = kTabButtonsSelectionTextColor;
    
    self.selectedTab = mFacebookPhotoAlbumSelectedTabAlbums;
    
    [self.view bringSubviewToFront:self.albumTableView];
    }
}

#pragma mark - NRGridView delegate

-(NSInteger)gridView:(NRGridView *)gridView numberOfItemsInSection:(NSInteger)section
{
  return self.uploadedPhotos.count;
}

- (NRGridViewCell *)gridView:(NRGridView *)gridView
      cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *MyCellIdentifier = @"mFacebookUploadedPhotosCell";
  
  mFacebookPhotoGridViewCell *cell = (mFacebookPhotoGridViewCell *)[gridView dequeueReusableCellWithIdentifier:MyCellIdentifier];
  
  if (cell == nil)
  {
    cell = [[[mFacebookPhotoGridViewCell alloc] initWithReuseIdentifier:MyCellIdentifier] autorelease];
    cell.backgroundColor = [UIColor clearColor];
    
    cell.cellPadding = CGSizeMake(1.0f, 1.0f);
  }

  NSURL *url =  ((MWPhoto *)self.uploadedPhotos[indexPath.row]).pictureURL;
  UIImage *placeholder = [[UIColor lightGrayColor] asImageWithSize:gridView.cellSize];
  [cell.imageView setImageWithURL:url placeholderImage:placeholder];
  
  BOOL needLoad = indexPath.row > (self.uploadedPhotos.count - 10);
  BOOL allLoaded = self.uploadedPhotos.count && !self.uploadedPhotosNextPage;
  
  if (!self.uploadedPhotosLoadingInProgress && needLoad && !allLoaded)
    [self loadUploadedPhotos];
    
  if (allLoaded) [self removeLoadingFooterForView:self.gridView];

  return cell;
}

-(void)gridView:(NRGridView *)gridView didSelectCellAtIndexPath:(NSIndexPath *)indexPath
{
  [gridView deselectCellAtIndexPath:indexPath
                           animated:NO];
  
  mFacebookPhotoBrowserViewController *browser = [[[mFacebookPhotoBrowserViewController alloc] initWithDelegate:self] autorelease];
  browser.displayActionButton = YES;
  browser.bSavePicture        = YES;
  [browser setInitialPageIndex:indexPath.row];
  
  [self.navigationController pushViewController:browser animated:YES];
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *cellIdentifier = @"mFacebookAlbumsTableViewCell";
  mFacebookPhotoAlbumTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
  
  if(!cell){
    cell = [[[mFacebookPhotoAlbumTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] autorelease];
  }
  
  cell.album = self.albums[indexPath.row];
  
  BOOL needLoad = indexPath.row > (self.albums.count - 2);
  BOOL allLoaded = self.albums.count && !self.albumsNextPage;
  
  if (!self.albumsLoadingInProgress && needLoad && !allLoaded)
    [self loadAlbums];
  
  if (allLoaded) [self removeLoadingFooterForView:self.albumTableView];
  
  return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  if (tableView.tag == ALBUM_TABLE_VIEW_TAG)
    return self.albums.count;
  else
    return 0;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  mFacebookPhotoAlbum *album = (mFacebookPhotoAlbum *)self.albums[indexPath.row];
  return [mFacebookPhotoAlbumTableViewCell heightForPhotoAlbum:album];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
  
  mFacebookPhotoAlbum *album =  (mFacebookPhotoAlbum *) self.albums[indexPath.row];
  
  mFacebookPhotoAlbumViewController *albumVC = [[mFacebookPhotoAlbumViewController alloc] initWithPhotoAlbum:album];
  
  [self.navigationController pushViewController:albumVC animated:YES];
}

#pragma mark - MWPhotoBrowserDelegate
-(NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
  return self.uploadedPhotos.count;
}

-(MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index
{
  return self.uploadedPhotos[index];
}


@end
