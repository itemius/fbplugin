#import "mFacebookBaseVC.h"
#import "mFacebookPhotoBrowserVC.h"
#import "NRGridView.h"

@interface mFacebookPhotoAlbumsViewController : mFacebookBaseViewController<NRGridViewDataSource,
                                                                            NRGridViewDelegate,
                                                                            MWPhotoBrowserDelegate>

@end
