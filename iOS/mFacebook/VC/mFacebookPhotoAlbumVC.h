#import "mFacebookBaseVC.h"
#import "mFacebookPhotoAlbum.h"
#import "mFacebookPhotoBrowserVC.h"

#import "NRGridView.h"

@interface mFacebookPhotoAlbumViewController : mFacebookBaseViewController<NRGridViewDataSource,
                                                                            NRGridViewDelegate,
                                                                            MWPhotoBrowserDelegate>

-(instancetype) initWithPhotoAlbum:(mFacebookPhotoAlbum *)album;

@end
