#import <UIKit/UIKit.h>
#import "mWebVC.h"

/**
 *  Customized WebVCViewController
 *
 *  @see mWebVCViewController
 */
@interface mFacebookWebViewController : mWebVCViewController

/**
 * Color of the navigation bar. Specified before the controller is pushed.
 */
@property (nonatomic, strong) UIColor *navBarColor;

@end
