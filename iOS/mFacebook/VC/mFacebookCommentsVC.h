#import "mFacebookBaseVC.h"
#import "mFacebookComment.h"

@interface mFacebookCommentsViewController: mFacebookBaseViewController<UITableViewDelegate,
                                                                        UITableViewDataSource,
                                                                        auth_ShareDelegate,
                                                                        UITextViewDelegate>

-(instancetype)initWithCommentedObject:(id<mFacebookCommentedObject>) object;

@end
