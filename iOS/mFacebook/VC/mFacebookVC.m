#import "mFacebookVC.h"
#import "mFacebookFeedTableView.h"
#import "mFacebookCommentsVC.h"
#import "mFacebookWebVC.h"
#import "mFacebookFeedInfo.h"
#import "mFacebookWebVC.h"
#import "mFacebookImageDescription.h"
#import "mFacebookPhotoAlbumsVC.h"
#import "mFacebookVideoListVC.h"
#import "mFacebookVideoDetailVC.h"

#import "NSString+size.h"
#import <objc/runtime.h>
#import "SDImageCache.h"

#define mFacebookOlderPostsLoadingStartDistance 5

typedef NS_ENUM(NSInteger, mFacebookFeedItemsLoadingStrategy)
{
  mFacebookFeedItemsLoadingStrategyNewer,
  mFacebookFeedItemsLoadingStrategyOlder,
  mFacebookFeedItemsLoadingStrategyRefresh,
  mFacebookFeedItemsLoadingStrategyLoadOnStart,
  mFacebookFeedItemsLoadingStrategyNone
};

@interface mFacebookViewController()
{
  BOOL endOfTheFeedReached;
  
  BOOL feedLoadingFailed;
  
  NSDictionary *feedLikes;
  mFacebookFeedItemsLoadingStrategy currentLoadingStrategy;
}

@property (nonatomic, strong) NSMutableArray *feedItems;
@property (nonatomic, strong) NSMutableDictionary *itemsByIds;
@property (nonatomic, strong) NSMutableDictionary *paging;
@property (nonatomic, strong) NSCache *userAvatarsCache;

@property (nonatomic, strong) mFacebookFeedInfo *feedInfo;

@property (nonatomic, strong) UIActivityIndicatorView *loadingIndicator;

@property (nonatomic, strong) mFacebookFeedTableView *feedTableView;
@property (nonatomic, strong) mFacebookFeedHeaderView *feedHeaderView;

@property (nonatomic, strong) NSMutableArray *photosForDisplayInBrowser;

@end

@implementation mFacebookViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  
  if(self){
    self.paging = [[NSMutableDictionary new] autorelease];
    self.feedItems = [[NSMutableArray new] autorelease];
    self.userAvatarsCache = [[[NSCache alloc] init] autorelease];
    self.itemsByIds = [[[NSMutableDictionary alloc] init] autorelease];
    
    aSha = [[auth_Share alloc] init];

    currentLoadingStrategy = mFacebookFeedItemsLoadingStrategyNone;
    
    self.photosForDisplayInBrowser = [NSMutableArray array];
    
    endOfTheFeedReached = NO;
  }
  
  return self;
}

-(void)dealloc
{
  self.feedItems = nil;
  self.paging = nil;
  self.feedTableView = nil;
  self.userAvatarsCache = nil;
  self.feedHeaderView = nil;
  self.feedInfo = nil;
  self.itemsByIds = nil;
  self.photosForDisplayInBrowser = nil;
  
  self.loadingIndicator = nil;
  
  aSha.delegate = nil;
  
  [aSha release];
  aSha = nil;
  
  [[NSNotificationCenter defaultCenter] removeObserver:self
                                                  name:UIApplicationDidBecomeActiveNotification
                                                object:nil];
  
  [super dealloc];
}

+ (void)parseXML:(NSValue *)xmlElement_
      withParams:(NSMutableDictionary *)params_
{
  TBXMLElement element;
  [xmlElement_ getValue:&element];
  
  mFacebookParameters *parameters = [mFacebookParameters parametersFromXMLElement:element];
  parameters.mainPageTitle = [params_ objectForKey:@"title"];
  
  [params_ setObject:parameters forKey:@"mFacebookParameters"];
}

- (void)setParams:(NSMutableDictionary *)params
{
  if ( params != nil )
  {
    mFacebookParameters *parameters = [params objectForKey:@"mFacebookParameters"];
    
    self.parameters = [mFacebookParameters sharedParameters];
    [self.parameters fillWithParameters:parameters];
  }
}

#pragma mark - View lifecycle
-(void)viewDidLoad
{
  [super viewDidLoad];
  self.title = self.parameters.mainPageTitle;
  
  [self placeFeedTableView];
  
  moduleStartTimestamp = [NSDate timeIntervalSinceReferenceDate];
  lastRefreshTimestamp = [NSDate timeIntervalSinceReferenceDate];
  
  currentLoadingStrategy = mFacebookFeedItemsLoadingStrategyLoadOnStart;
  [self loadFeed];
}

-(void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(handleAppDidBecomeActive:)
                                               name:UIApplicationDidBecomeActiveNotification
                                             object:nil];
  
  [self loadFeedInfoEngagement];

  [self.photosForDisplayInBrowser removeAllObjects];
  
  [self.feedTableView reloadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
  
  [[NSNotificationCenter defaultCenter] removeObserver:self
                                                  name:UIApplicationDidBecomeActiveNotification
                                                object:nil];
}

#pragma mark - Feed loading
-(void)loadFeed
{
  feedLoadingFailed = NO;
  
  if(currentLoadingStrategy == mFacebookFeedItemsLoadingStrategyLoadOnStart)
  {
    [self showLoadingIndicator];
  }
  
  [self loadFeedInfo];
  [self loadFeedItems];
  [self loadPhotosCount];
}

-(void)loadFeedInfo
{
  auth_ShareFacebookGenericCompletionBlock infoBlock = ^(NSDictionary *data, NSError *error){
    
    BOOL feedInfoWasLiked = self.feedInfo.isLiked; //prevent blinking
    
    self.feedInfo = [[[mFacebookFeedInfo alloc] initWithDictionary:data] autorelease];
    self.feedInfo.isLiked = feedInfoWasLiked;
    
    [self.feedHeaderView setFeedInfo:self.feedInfo];
    
    self.feedHeaderView.hidden = NO;
    
    [self loadFeedInfoEngagement];
  };
  
  [aSha loadFacebookInfoForId:self.parameters.facebookId
                        completion:infoBlock];
}

-(void)loadFeedInfoEngagement
{
  [aSha loadFacebookEngagementForId:self.feedInfo.facebookId
                              completion:^(NSDictionary *data, NSError *error) {
                                NSString *socialSentence = [data objectForKey:@"social_sentence"];
                                
                                NSUInteger youLocation = [socialSentence rangeOfString:@"You"].location;
                                self.feedInfo.isLiked = youLocation != NSNotFound;
                                
                                [self.feedHeaderView setFeedInfo:self.feedInfo];
                              }];
}

-(void)loadFeedItems
{
  NSString *customGraphPath = [self customGraphPathForCurrentLoadingStrategy];
  
  auth_ShareFacebookFeedLoadingCompletionBlock completion = ^(NSArray *feed, NSDictionary *paging, NSError *error)
  {
    if(!error)
    {
      NSArray *feedItems = [mFacebookFeedItem itemsFromFeed:feed];
      
      [self mergeWithLoadedFeedItems:feedItems];
      [self updatePaging:paging];
      
      if(currentLoadingStrategy == mFacebookFeedItemsLoadingStrategyOlder)
      {
        if(!feedItems.count)
        {
          endOfTheFeedReached = YES;
        }
      }
      
      [self loadLikedFacebookItemsIfNeeded];
      [self loadTargetedVideosIfNeeded:feedItems];
      
      if(currentLoadingStrategy == mFacebookFeedItemsLoadingStrategyLoadOnStart)
      {
        feedLoadingFailed = NO;
      }

      [self.feedTableView reloadData];
    }
    else
    {
      NSLog(@"ERROR getting feed data from FB!");
      
      if(currentLoadingStrategy == mFacebookFeedItemsLoadingStrategyLoadOnStart)
      {
        feedLoadingFailed = YES;
      }
      
      [self notifyIfNoInternet:error];
    }
    
    [self endRefreshingIfNeeded];
    [self removeLoadingFooterIfNeeded];
    [self hideLoadingIndicatorIfNeeded];
    
    currentLoadingStrategy = mFacebookFeedItemsLoadingStrategyNone;
  };
  
  [aSha loadFacebookFeedForId:self.parameters.facebookId
                   customGraphPath:customGraphPath
                        completion:completion];
}

-(void)loadLikedFacebookItemsIfNeeded
{
  if(currentLoadingStrategy == mFacebookFeedItemsLoadingStrategyLoadOnStart)
  {
    [aSha loadFacebookLikedURLs];
  } else {
    [self updatePostsLikedStatus];
  }
}

-(void)loadPhotosCount
{
  [aSha loadFacebookPhotosCountForId:self.parameters.facebookId completion:^(NSUInteger count) {
    [self.feedHeaderView setPhotoCount:count];
  }];
}

-(NSString *)customGraphPathForCurrentLoadingStrategy
{
  NSString *customGraphPath = nil;
  
  if(currentLoadingStrategy == mFacebookFeedItemsLoadingStrategyOlder)
  {
    customGraphPath = [self.paging objectForKey:@"next"];
    
  } else if(currentLoadingStrategy == mFacebookFeedItemsLoadingStrategyRefresh)
  {
    customGraphPath = [self customGraphPathForRefresh];
  }
  
  return customGraphPath;
}

-(NSString *)customGraphPathForRefresh
{
  if(!self.feedItems.count)
  {
    return nil;
  }
  
  //Facebook returns up to 250 posts in a single response,
  //so let's load at least 100 new posts
  //and reload no more than 150 older posts
  NSUInteger itemsCount = self.feedItems.count > 150 ? 150 : self.feedItems.count;
  
  long long sinceInSeconds = [self timestampInSecondsForFeedItem:self.feedItems[(itemsCount-1)]] - 1;
  
  NSString *customGraphPath = [NSString stringWithFormat:@"/%@/posts?fields=id,from,message,object_id,created_time,attachments,"\
                               "type,link,comments.limit(1).summary(true),likes.limit(1).summary(true)"\
                               "&limit=%d&since=%lld",
                               self.parameters.facebookId,
                               250,
                               sinceInSeconds];
  
  return customGraphPath;
}

-(long long)timestampInSecondsForFeedItem:(mFacebookFeedItem *)item
{
  long long sinceInSeconds = (long long)[item.creationTime timeIntervalSince1970];
  
  return sinceInSeconds;
}

-(void)loadTargetedVideosIfNeeded:(NSArray *)sourceFeedItems
{
  NSMutableArray *feedItemsWithVideos = [NSMutableArray array];
  
  for(mFacebookFeedItem *item in sourceFeedItems)
  {
    if([item attachedImagesTargetedToVideos].count)
    {
      [feedItemsWithVideos addObject:item];
    }
  }
  
  if(feedItemsWithVideos.count)
  {
    [self loadTargetedVideosForFeedItemsWithVideos:feedItemsWithVideos];
  }
}

-(void)loadTargetedVideosForFeedItemsWithVideos:(NSArray *)itemsWithVideos
{
  NSMutableSet *targetVideosIds = [NSMutableSet set];
  
  for(mFacebookFeedItem *item in itemsWithVideos)
  {
    NSArray *imagesTargetedToVideos = [item attachedImagesTargetedToVideos];
    
    for(mFacebookImageDescription *imageDecription in imagesTargetedToVideos)
    {
      [targetVideosIds addObject:imageDecription.targetVideoFacebookId];
    }
  }
  
  auth_ShareFacebookGenericCompletionBlock completion = ^(NSDictionary *data, NSError *error){
    if(!error)
    {
      NSArray *videoIds = [[data keyEnumerator] allObjects];
      
      for(NSString *videoId in videoIds)
      {
        NSDictionary *videoDictionary = [data objectForKey:videoId];
        
        mFacebookVideoDescription *videoDescription = [[[mFacebookVideoDescription alloc] initWithDictionary:videoDictionary] autorelease];
        
        for(mFacebookFeedItem *feedItem in itemsWithVideos)
        {
          NSArray *imagesTargetedToVideos = [feedItem attachedImagesTargetedToVideos];
          
          for(mFacebookImageDescription *imageDescription in imagesTargetedToVideos)
          {
            if([imageDescription.targetVideoFacebookId
                isEqualToString:videoDescription.facebookId])
            {
              imageDescription.targetVideoDescription = videoDescription;
              goto videoIteratorNextLoop;
            }
          }
        }
      videoIteratorNextLoop:;
      }
      
      [self.feedTableView reloadData];
    }
  };
  
  [aSha loadFacebookVideosForIds:targetVideosIds
                           completion:completion];
}

-(void)refreshLikesCountForItems:(NSArray *)feedItems
{
  NSSet *ids = [self idsFromItems:feedItems];
  
  [aSha loadFacebookLikesCountForIds:ids];
}

-(void)mergeWithLoadedFeedItems:(NSArray *)feedItems
{
  if(currentLoadingStrategy == mFacebookFeedItemsLoadingStrategyLoadOnStart)
  {
    self.feedItems = [feedItems mutableCopy];
    
  } else if(currentLoadingStrategy == mFacebookFeedItemsLoadingStrategyOlder)
  {
    [self.feedItems addObjectsFromArray:feedItems];
    
  } else if(currentLoadingStrategy == mFacebookFeedItemsLoadingStrategyRefresh)
  {
    [self presetStateForRefreshedItems:feedItems];
    self.feedItems = [feedItems mutableCopy];
  }
}


-(void)presetStateForRefreshedItems:(NSArray *)refreshedFeedItems
{
  for(mFacebookFeedItem *refreshedItem in refreshedFeedItems)
  {
    NSString *link = [refreshedItem linkForLike];
    
    mFacebookFeedItem *existentItem = [self feedItemForURL:link];
    
    if(existentItem)
    {
      refreshedItem.isMessageExpanded = existentItem.isMessageExpanded;
    }
  }
}

-(void)endRefreshingIfNeeded
{
  if(currentLoadingStrategy == mFacebookFeedItemsLoadingStrategyLoadOnStart ||
     currentLoadingStrategy == mFacebookFeedItemsLoadingStrategyRefresh)
  {
    [self.feedTableView layoutIfNeeded];
    [self.refreshControl endRefreshing];
  }
}

-(void)loadOlderFeedItemsIfNeeded:(NSIndexPath *)currentFeedItemIndexPath
{
  if(endOfTheFeedReached)
  {
    return;
  }
  
  if(currentFeedItemIndexPath.row >= self.feedItems.count - mFacebookOlderPostsLoadingStartDistance - 1)
  {
    if(currentLoadingStrategy == mFacebookFeedItemsLoadingStrategyNone)
    {
      currentLoadingStrategy = mFacebookFeedItemsLoadingStrategyOlder;
      [self loadFeedItems];
      [self placeLoadingFooter];
    }
  }
}

-(void)loadCommentsCount:(NSSet *)items
{
  [aSha loadFacebookCommentsCountForIds:items];
}

-(NSSet *)linksFromItems:(NSArray *)feedItems
{
  NSMutableSet *links = [NSMutableSet set];
  
  for(mFacebookFeedItem *item in feedItems)
  {
    NSString *facebookId = [item linkForLike];
    [links addObject:facebookId];
  }
  
  return links;
}

-(NSSet *)idsFromItems:(NSArray *)feedItems
{
  NSMutableSet *ids = [NSMutableSet set];
  
  for(mFacebookFeedItem *item in feedItems)
  {
    NSString *facebookId = [item facebookObjectId];
    [ids addObject:facebookId];
  }
  
  return ids;
}

-(NSString *)objectIdFromFeedItem:(NSDictionary *)feedItem
{
  NSString *objectId = [feedItem objectForKey:@"id"];
  
  return objectId;
}

-(void)updatePaging:(NSDictionary *)newPaging
{
  if(currentLoadingStrategy == mFacebookFeedItemsLoadingStrategyLoadOnStart)
  {
    self.paging = [[newPaging mutableCopy] autorelease];
    
  } else if(currentLoadingStrategy == mFacebookFeedItemsLoadingStrategyOlder)
  {
    NSString *nextPagePath = [newPaging objectForKey:@"next"];
    if(nextPagePath.length)
    {
      [self.paging setObject:nextPagePath forKey:@"next"];
    }
    
    
  } else if(currentLoadingStrategy == mFacebookFeedItemsLoadingStrategyRefresh)
  {
    NSString *previousPagePath = [newPaging objectForKey:@"previous"];
    
    if(previousPagePath.length)
    {
      [self.paging setObject:previousPagePath forKey:@"previous"];
    }
  }
}

-(void)updateLikesCountWithDictionary:(NSDictionary *)likes
{
  for(NSString *feedItemId in likes.keyEnumerator)
    {
    mFacebookFeedItem *feedItem = [self feedItemForId:feedItemId];
    feedItem.likesCount = [[likes objectForKey:feedItemId] unsignedIntegerValue];
    }
}

-(void)updateCommentsCountWithDictionary:(NSDictionary *)commentsCount
{
  for(NSString *feedItemURL in commentsCount.keyEnumerator)
    {
    mFacebookFeedItem *feedItem = [self feedItemForURL:feedItemURL];
    feedItem.commentsCount = [[commentsCount objectForKey:feedItemURL] unsignedIntegerValue];
    }
}

-(void)updatePostsLikedStatus
{
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                 ^{
                   for(mFacebookFeedItem *item in self.feedItems)
                     {
                     item.isLiked = [self.parameters isURLStringLiked:[item linkForLike]];
                     }
                   
                   dispatch_async(dispatch_get_main_queue(), ^{
                     [self.feedTableView reloadData];
                   });
                 });
}

-(mFacebookFeedItem *)feedItemForURL:(NSString *)URL
{
  for(mFacebookFeedItem *feedItem in self.feedItems)
    {
    if([[feedItem linkForLike] isEqualToString:URL]){
      return feedItem;
    }
    }
  return nil;
}

-(mFacebookFeedItem *)feedItemForId:(NSString *)Id
{
  for(mFacebookFeedItem *feedItem in self.feedItems)
    {
    if([[feedItem facebookObjectId] isEqualToString:Id]){
      return feedItem;
    }
  }
  return nil;
}


- (void)pullToRefresh
{
  currentLoadingStrategy = mFacebookFeedItemsLoadingStrategyRefresh;
  lastRefreshTimestamp = [NSDate timeIntervalSinceReferenceDate];
  
  [self loadFeed];
}

#pragma mark - Interface

-(void)placeFeedTableView
{
  CGRect feedTableViewRect = self.view.bounds;
  
  self.feedTableView = [[[mFacebookFeedTableView alloc] initWithFrame:feedTableViewRect] autorelease];
  
  self.feedTableView.delegate = self;
  self.feedTableView.dataSource = self;
  
  self.feedTableView.backgroundColor = self.parameters.design.color1;

  self.tableView = self.feedTableView;
  
  [self placeFeedHeaderView];
}

-(void)placeFeedHeaderView
{
  self.feedHeaderView = [[[mFacebookFeedHeaderView alloc] initWithFeedInfo:nil] autorelease];
  
  [self.feedHeaderView.likeTabButton addTarget:self action:@selector(likePageButtonTapped) forControlEvents:UIControlEventTouchUpInside];
  [self.feedHeaderView.photoTabButton addTarget:self action:@selector(photoAlbumsButtonTapped) forControlEvents:UIControlEventTouchUpInside];
  [self.feedHeaderView.videoTabButton addTarget:self action:@selector(videosButtonTapped) forControlEvents:UIControlEventTouchUpInside];
  
  self.feedHeaderView.hidden = YES;
  
  self.feedTableView.tableHeaderView = self.feedHeaderView;
}

-(void)placeLoadingFooter
{
  CGRect footerRect = (CGRect)
  {
  0.0f,
  0.0f,
  self.feedTableView.bounds.size.width,
  40.0f,
  };
  
  UIActivityIndicatorView *loadingIndicator = [[UIActivityIndicatorView alloc] initWithFrame:footerRect];
  loadingIndicator.color = self.parameters.design.color4;
  
  [loadingIndicator startAnimating];
  
  self.feedTableView.tableFooterView = loadingIndicator;
  
  [loadingIndicator release];
}

-(void)removeLoadingFooter
{
  self.feedTableView.tableFooterView = nil;
}

-(void)showLoadingIndicator
{
  self.feedTableView.scrollEnabled = NO;
  
  self.loadingIndicator = [[[UIActivityIndicatorView alloc]
                            initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge]
                           autorelease];
  
  self.loadingIndicator.color = self.parameters.design.color4;
  
  self.loadingIndicator.hidesWhenStopped = YES;
  
  CGPoint center = self.feedTableView.center;
  center.y -= kFacebookNavBarHeight;
  
  self.loadingIndicator.center = center;
  
  [self.feedTableView addSubview:self.loadingIndicator];
  [self.loadingIndicator startAnimating];
}

-(void)hideLoadingIndicatorIfNeeded
{
  self.feedTableView.scrollEnabled = YES;
  
  [self.loadingIndicator stopAnimating];
}

-(void)removeLoadingFooterIfNeeded
{
  if(currentLoadingStrategy == mFacebookFeedItemsLoadingStrategyOlder)
    {
    [self removeLoadingFooter];
    }
}

#pragma mark - Actions

-(void)likePageButtonTapped
{
  aSha.viewController = self;
  
  [aSha authenticatePersonUsingService:auth_ShareServiceTypeFacebook
                             andCredentials:nil
                             withCompletion:@selector(likePageOnHeaderView)
                                    andData:nil
              shouldShowLoginRequiredPrompt:NO];
}

-(void)likePageOnHeaderView
{
  [self.feedHeaderView likePage];
}

-(void)photoAlbumsButtonTapped
{
  if(!self.feedHeaderView.photoCount)
  {
    return;
  }
  
  mFacebookPhotoAlbumsViewController *photoVC = [[[mFacebookPhotoAlbumsViewController alloc] init] autorelease];
  [self.navigationController pushViewController:photoVC animated:YES];
}

-(void)videosButtonTapped
{
  if(!self.feedInfo.videosCount)
  {
    return;
  }
  
  mFacebookVideoListViewController *videoListVC = [[mFacebookVideoListViewController alloc] init];
  [self.navigationController pushViewController:videoListVC animated:YES];
  [videoListVC release];
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *cellIdentifier = @"mFacebookFeedTableViewCell";
  mFacebookFeedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
  
  if(!cell){
    cell = [[[mFacebookFeedTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] autorelease];
    
    cell.delegate = self;
    cell.messageLabelDelegate = self;
  }
  
  mFacebookFeedItem *feedItem = [self.feedItems objectAtIndex:indexPath.row];
  
  cell.feedItem = feedItem;
  
  [self loadOlderFeedItemsIfNeeded:indexPath];
  
  return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return self.feedItems.count;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  mFacebookFeedItem *item = [self.feedItems objectAtIndex:indexPath.row];
  
  CGFloat height = [mFacebookFeedTableViewCell heightForFeedItem:item];
  
  return height;
}

#pragma mark - auth_ShareDelegate
- (void)didLoadFacebookLikesCount:(NSDictionary *)likes error:(NSError *)error
{
  if(!error){
    [self updateLikesCountWithDictionary:likes];
    [self.feedTableView reloadData];
  }
}

- (void)didLoadFacebookCommentsCount:(NSDictionary *)commentsCount error:(NSError *)error
{
  if(!error){
    [self updateCommentsCountWithDictionary:commentsCount];
    [self.feedTableView reloadData];
  }
}

-(void)didFacebookLikeForURL:(NSString *)URL error:(NSError *)error
{
  if(!error){
    mFacebookFeedItem *feedItem = [self feedItemForURL:URL];
    feedItem.isLiked = YES;
    
    [self refreshLikesCountForItems:@[feedItem]];
  }
}

-(void)didFacebookLikeForId:(NSString *)Id error:(NSError *)error
{
  if(!error)
  {
    mFacebookFeedItem *feedItem = [self feedItemForId:Id];
    feedItem.isLiked = YES;
    
    NSLog(@"mFacebook VC going to like url: %@", [feedItem linkForLike]);
    
    [aSha postLikeForURL:[feedItem linkForLike]
        withNotificationNamed:nil
     shouldShowLoginRequiredPrompt:NO];
  }
}

-(void)didLoadFacebookLikedURLs:(NSMutableSet *)likedURLs error:(NSError *)error
{
  [super didLoadFacebookLikedURLs:likedURLs
                            error:error];
  if(!error)
  {
    [self updatePostsLikedStatus];
  }
}

-(void)didAuthorizeOnService:(auth_ShareServiceType)serviceType error:(NSError *)error
{
  [super didAuthorizeOnService:serviceType
                         error:error];
  if(!error &&
     serviceType == auth_ShareServiceTypeFacebook)
  {
    [self loadFeedInfoEngagement];
  }
}

#pragma mark - mFacebookFeedTableViewCellDelegate

-(void)likeButtonPressedOnCell:(mFacebookFeedTableViewCell *)cell
{
  NSLog(@"mFacebook VC going to like id: %@", [cell.feedItem facebookObjectId]);
  [aSha postLikeForId:[cell.feedItem facebookObjectId]];
}

-(void)commentButtonPressedOnCell:(mFacebookFeedTableViewCell *)cell
{
  mFacebookCommentsViewController *comments = [[[mFacebookCommentsViewController alloc] initWithCommentedObject:cell.feedItem] autorelease];
  [self.navigationController pushViewController:comments animated:YES];
}

-(void)picturePressedOnCell:(mFacebookFeedTableViewCell *)cell imageIndex:(NSUInteger)index
{
  if(cell.feedItem.attachedImages.count)
  {
    [self.photosForDisplayInBrowser removeAllObjects];
    
    for (mFacebookImageDescription *imgDesc in cell.feedItem.attachedImages)
    {
      MWPhoto *photo = [MWPhoto photoWithURL:imgDesc.url];
      photo.pictureURL = imgDesc.url;
      photo.ID = imgDesc.facebookObjectId;
      [self.photosForDisplayInBrowser addObject:photo];
    }
    
    mFacebookPhotoBrowserViewController *browser = [[[mFacebookPhotoBrowserViewController alloc] initWithDelegate:self] autorelease];
    browser.displayActionButton = YES;
    browser.bSavePicture        = YES;
    browser.leftBarButtonCaption = self.parameters.mainPageTitle;
    [browser setInitialPageIndex:index];
    
    [self.navigationController pushViewController:browser animated:YES];
  }
}

-(void)shareButtonPressedOnCell:(mFacebookFeedTableViewCell *)cell
{
  mFacebookFeedItem *item = cell.feedItem;
  
  NSString *link = [item linkForSharing];
  
  NSMutableDictionary *data = [NSMutableDictionary dictionaryWithObject:link forKey:@"link"];
  
  [aSha shareContentUsingService:auth_ShareServiceTypeFacebook
                             fromUser:nil
                             withData:data
              showLoginRequiredPrompt:NO];
}

-(void)messageExpandedOnCell:(mFacebookFeedTableViewCell *)cell
{
  mFacebookFeedItem *item = cell.feedItem;
  
  if(item)
    {
    item.isMessageExpanded = YES;
    NSUInteger i = [self.feedItems indexOfObject:item];
    NSIndexPath *ip = [NSIndexPath indexPathForItem:i inSection:0];
    
    [self.feedTableView reloadRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

-(void)videoWithDescription:(mFacebookVideoDescription *)description pressedOnCell:(mFacebookFeedTableViewCell *)cell
{
  if(description)
    {
    UIViewController *videoPlayer = nil;
    
    enum mFacebookVideoType type = description.facebookVideoType;
    
    if(type == mFacebookVideoTypeInline)
      {
      videoPlayer = [[mFacebookVideoDetailVC alloc] initWithVideoDescription:description];
      
      } else if(type == mFacebookVideoTypeShared)
        {
        mFacebookWebViewController *webVC = [[mFacebookWebViewController alloc] initWithNibName:nil bundle:nil];
        
        webVC.title = description.title;
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
          webVC.navBarColor = self.navigationController.navigationBar.barTintColor;
        }
        
        webVC.showTabBar = self.showTabBar;
        [webVC  hideTBButton];
        
        webVC.URL = description.sourceVideoURLString;
        
        videoPlayer = webVC;
        }
    
    if(videoPlayer){
      [self.navigationController pushViewController:videoPlayer animated:YES];
      [videoPlayer release];
    }
  }
}

#pragma mark - MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
  return self.photosForDisplayInBrowser.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index
{
  return self.photosForDisplayInBrowser[index];
}

#pragma mark - TTTAttributedLabelDelegate
-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
  mFacebookWebViewController *webVC = [[[mFacebookWebViewController alloc] init] autorelease];
  webVC.modalPresentationStyle = UIModalPresentationFullScreen;
  webVC.navBarColor = [UIColor lightGrayColor];
  webVC.showTabBar = self.showTabBar;
  
  webVC.URL = url.absoluteString;
  [webVC  hideTBButton];
  
  [self.navigationController pushViewController:webVC animated:YES];
}

#pragma mark - Reachability

-(void)notifyIfNoInternet:(NSError *)error
{
  NSError *possibleURLError = [error.userInfo objectForKey:@"com.facebook.sdk:ErrorInnerErrorKey"];
  
  if(![possibleURLError isKindOfClass:[NSError class]])
  {
    return;
  }
  
  if([possibleURLError.domain isEqualToString:NSURLErrorDomain] &&
     ((NSInteger)possibleURLError.code) == -1009)
  {
    UIAlertView *msg = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"general_cellularDataTurnedOff",@"Cellular Data is Turned off")
                                                   message:NSLocalizedString(@"general_cellularDataTurnOnMessage",@"Turn on cellular data or use Wi-Fi to access data")
                                                  delegate:nil
                                         cancelButtonTitle:NSLocalizedString(@"general_defaultButtonTitleOK",@"OK")
                                         otherButtonTitles:nil] autorelease];
    [msg show];
  }
}

-(void)reachabilityChanged:(NSNotification *) notification
{
  BOOL internetReachable = [[notification object] currentReachabilityStatus] != NotReachable;
  
  if(internetReachable)
  {
    //user already triggered update by refresh
    if(currentLoadingStrategy == mFacebookFeedItemsLoadingStrategyRefresh)
    {
      return;
    }
    if(feedLoadingFailed){
      currentLoadingStrategy = mFacebookFeedItemsLoadingStrategyLoadOnStart;
      [self loadFeed];
    } else {
      NSIndexPath *lastVisibleCellIndexPath = [[self.feedTableView indexPathsForVisibleRows] lastObject];
      [self loadOlderFeedItemsIfNeeded:lastVisibleCellIndexPath];
    }
  }
}

#pragma mark - App lifecycle
-(void)handleAppDidBecomeActive:(NSNotification *)notification
{
  [self loadFeedInfo];
}

#pragma mark - Handling iOS 8 fullscreen behavior
-(void)videoPlayerWillEnterFullscreen:(NSNotification *)notification
{
  if(feedLoadingFailed)
  {
    return;
  }
  
  [super videoPlayerWillEnterFullscreen:notification];
}

-(void)videoPlayerWillExitFullscreen:(NSNotification *)notification
{
  if(feedLoadingFailed)
  {
    return;
  }
  
  [super videoPlayerWillExitFullscreen:notification];
}

@end
