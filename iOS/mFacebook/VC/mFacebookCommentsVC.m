#import "mFacebookCommentsVC.h"
#import "mFacebookCommentTableViewCell.h"
#import "mFacebookFeedItem.h"

#import "auth_ShareLoginVC.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import "NSString+size.h"
#import <Smartling.i18n/SLLocalization.h>
#import <MBProgressHUD/MBProgressHUD.h>

#define kSeparatorColor [UIColor colorWithWhite:1.0 alpha:0.3]
#define kSeparatorColorForWhiteBackground [UIColor colorWithWhite:0.0 alpha:0.2]

@interface mFacebookCommentsViewController ()

@property (nonatomic, strong) id<mFacebookCommentedObject> commentedObject;

@property (nonatomic, strong) UITableView     *commentsTableView;
@property (nonatomic, strong) NSMutableArray  *comments;
@property (nonatomic, strong) UILabel         *commentsCountLabel;
@property (nonatomic, strong) UIBarButtonItem *showPostCommentFormButton;

@property (nonatomic, strong) UITextView      *postCommentTextView;
@property (nonatomic, strong) UIBarButtonItem *postCommentButton;
@property (nonatomic, strong) UIBarButtonItem *clearCommentTextViewButton;

@property (nonatomic, strong) UIButton        *backButtonCoverButton;

@property (nonatomic, strong) NSString        *nextPagePath;
@property (nonatomic, assign) BOOL             loadingInProgress;

@property (nonatomic, assign) BOOL             postCommentFormShowed;

@end

@implementation mFacebookCommentsViewController

-(instancetype) initWithCommentedObject:(id<mFacebookCommentedObject>)object
{
  self = [super init];
  if (self)
  {
    self.commentedObject = object;
  }
  return self;
}

-(void)dealloc
{
  self.commentedObject              = nil;
  
  self.commentsTableView            = nil;
  self.comments                     = nil;
  self.commentsCountLabel           = nil;
  self.showPostCommentFormButton    = nil;
  
  self.postCommentTextView          = nil;
  self.postCommentButton            = nil;
  self.clearCommentTextViewButton   = nil;
  
  self.backButtonCoverButton        = nil;
  
  self.nextPagePath                 = nil;
  
  [super dealloc];
}

-(void)viewDidLoad
{
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  
  self.title = NSBundleLocalizedString(@"mFacebook_commentsPageTitle", @"Comments");
  
  [self placeCommentsTableView];
  [self placeCommentsTableHeader];
  
  [self placePostCommentTextView];
  
  [self placeLoadingFooter];
  
  self.showPostCommentFormButton = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose
                                                                                  target:self
                                                                                  action:@selector(showPostCommentFormWithAuthentication)] autorelease];
  self.navigationItem.rightBarButtonItem = self.showPostCommentFormButton;
  
  [self loadCommentsWithCompletion:nil];
  
  if (![self.commentedObject commentsCount])
    [self showPostCommentFormWithAuthentication];
}

-(void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  
   self.showPostCommentFormButton.enabled = self.isInternetReachable;
  
  [self updateCommentsCountLabel];
}

-(void)addBackButtonCoverButton
{
  self.backButtonCoverButton = [[[UIButton alloc] init] autorelease];
  self.backButtonCoverButton.frame = CGRectMake(0, 0, 80, CGRectGetHeight(self.navigationController.navigationBar.frame));
  self.backButtonCoverButton.alpha = 1.0f;
  [self.backButtonCoverButton addTarget:self action:@selector(backButtonCoverButtonTapped) forControlEvents:UIControlEventTouchUpInside];
  
  [self.navigationController.navigationBar addSubview:self.backButtonCoverButton];
}

-(void)removeBackButtonCoverButton
{
  [self.backButtonCoverButton removeFromSuperview];
  self.backButtonCoverButton = nil;
}

- (void)placeCommentsTableView
{
  self.commentsTableView = [[[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain] autorelease];
  self.commentsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
  self.commentsTableView.backgroundColor = [mFacebookParameters sharedParameters].design.color1;
  self.commentsTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
  
  self.commentsTableView.delegate = self;
  self.commentsTableView.dataSource = self;
  
  self.tableView = self.commentsTableView;
}

- (void)placeCommentsTableHeader
{
  CGFloat commentsCountFileldHeight = 29.f;
  
  UIView *header = nil;
  
  mFacebookParameters *params = [mFacebookParameters sharedParameters];
  
  if ([self.commentedObject isKindOfClass:[mFacebookComment class]])
  {
    mFacebookComment *comment = (mFacebookComment*)self.commentedObject;
    
    UIImageView *avatarView = [[[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 25, 25)] autorelease];
    avatarView.contentMode = UIViewContentModeScaleAspectFill;
    [avatarView setImageWithURL:comment.avatarURL
               placeholderImage:[UIImage imageNamed:resourceFromBundle(@"mFacebook_small_ava")]];
    avatarView.clipsToBounds = YES;
    
    UILabel *nameLabel = [[[UILabel alloc] init] autorelease];
    nameLabel.text = comment.authorName;
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.textColor = params.design.color4;
    nameLabel.textAlignment = NSTextAlignmentLeft;
    nameLabel.font = [UIFont systemFontOfSize:13];
    
    CGSize nameLabelSize = [nameLabel.text sizeForFont:nameLabel.font
                                                  limitSize:CGSizeMake(self.view.frame.size.width - (CGRectGetMaxX(avatarView.frame) + 10), CGFLOAT_MAX)
                                            nslineBreakMode:nameLabel.lineBreakMode];
    
    nameLabel.frame = CGRectMake(CGRectGetMaxX(avatarView.frame) + 10, avatarView.frame.origin.y, nameLabelSize.width, nameLabelSize.height);
    
    
    UILabel *dateLabel = [[[UILabel alloc] init] autorelease];
    
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    
    dateLabel.text = [dateFormatter stringFromDate:comment.creationTime];
    
    dateLabel.font = [UIFont systemFontOfSize:10.f];
    dateLabel.textAlignment = NSTextAlignmentLeft;
    dateLabel.backgroundColor = [UIColor clearColor];
    dateLabel.textColor = params.design.color4;
    dateLabel.alpha = 0.5f;
    dateLabel.frame = CGRectMake(CGRectGetMaxX(avatarView.frame) + 10, CGRectGetMaxY(nameLabel.frame), 150, 15);
    
    UILabel *messageLabel = [[[UILabel alloc] init] autorelease];
    messageLabel.text = comment.message;
    messageLabel.backgroundColor = [UIColor clearColor];
    messageLabel.textColor = params.design.color4;
    messageLabel.textAlignment = NSTextAlignmentLeft;
    messageLabel.font = [UIFont systemFontOfSize:14];
    messageLabel.numberOfLines = 0;
    
    CGSize messageLabelSize = [messageLabel.text sizeForFont:messageLabel.font
                                                   limitSize:CGSizeMake(self.view.frame.size.width - 10 * 2, CGFLOAT_MAX)
                                             nslineBreakMode:messageLabel.lineBreakMode];
 
    
    messageLabel.frame = CGRectMake(avatarView.frame.origin.x, CGRectGetMaxY(avatarView.frame) + 10, messageLabelSize.width, messageLabelSize.height);

    
    header = [[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, CGRectGetMaxY(messageLabel.frame) + commentsCountFileldHeight + 10)] autorelease];
    
    [header addSubview:avatarView];
    [header addSubview:nameLabel];
    [header addSubview:dateLabel];
    [header addSubview:messageLabel];
    
    UIView *topBorder = [[[UIView alloc] initWithFrame:CGRectMake(0, header.frame.size.height - commentsCountFileldHeight, header.frame.size.width, 1)] autorelease];
    topBorder.backgroundColor = params.design.isWhiteBackground ? kSeparatorColorForWhiteBackground : kSeparatorColor;
    
    [header addSubview:topBorder];
  }
  else
    header = [[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, commentsCountFileldHeight)] autorelease];
  
  self.commentsCountLabel = [[[UILabel alloc] init] autorelease];
  self.commentsCountLabel.backgroundColor = [UIColor clearColor];
  self.commentsCountLabel.frame = CGRectMake(10, header.frame.size.height - commentsCountFileldHeight, self.view.frame.size.width, commentsCountFileldHeight);
  self.commentsCountLabel.textColor = params.design.color4;
  self.commentsCountLabel.textAlignment = NSTextAlignmentLeft;
  self.commentsCountLabel.font = [UIFont systemFontOfSize:14];
  
  [header addSubview:self.commentsCountLabel];
  
  UIView *bottomBorder = [[[UIView alloc] initWithFrame:CGRectMake(0, header.frame.size.height, header.frame.size.width, 1)] autorelease];
  bottomBorder.backgroundColor = params.design.isWhiteBackground ? kSeparatorColorForWhiteBackground : kSeparatorColor;

  
  [header addSubview:bottomBorder];
  
  self.commentsTableView.tableHeaderView = header;
}

-(void)placePostCommentTextView
{
  self.postCommentTextView = [[[UITextView alloc] init] autorelease];
  
  self.postCommentTextView.font = [UIFont systemFontOfSize:17.0f];
  self.postCommentTextView.layer.cornerRadius = 6.0f;
  self.postCommentTextView.layer.borderColor = [UIColor blackColor].CGColor;
  self.postCommentTextView.layer.borderWidth = 1.f;
  self.postCommentTextView.layer.masksToBounds = YES;
  self.postCommentTextView.hidden = YES;
  self.postCommentTextView.delegate = self;
  
  self.postCommentTextView.font = [UIFont systemFontOfSize:13.0f];
  self.postCommentTextView.textColor = [UIColor blackColor];
  
  [self.view addSubview:self.postCommentTextView];
  
  [self placeMiddleToollBar];
}

-(void)placeMiddleToollBar
{
  UIToolbar *middleToolBar = [[[UIToolbar alloc] init] autorelease];
  [middleToolBar sizeToFit];
  middleToolBar.barStyle = UIBarStyleBlack;
  
  self.clearCommentTextViewButton = [[[UIBarButtonItem alloc] init] autorelease];
  self.clearCommentTextViewButton.style = UIBarButtonItemStyleBordered;
  self.clearCommentTextViewButton.target = self;
  self.clearCommentTextViewButton.action = @selector(clearPostCommentTextView);
  self.clearCommentTextViewButton.title = NSBundleLocalizedString(@"mFacebook_postClearButtonTitle", @"Clear");
  
  self.postCommentButton = [[[UIBarButtonItem alloc] init] autorelease];
  self.postCommentButton.style = UIBarButtonItemStyleBordered;
  self.postCommentButton.target = self;
  self.postCommentButton.action = @selector(postComment);
  self.postCommentButton.title = NSBundleLocalizedString(@"mFacebook_postPostButtonTitle", @"Post");
  
  if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"5.0"))
    self.postCommentButton.tintColor = self.clearCommentTextViewButton.tintColor;
  
  NSArray *barItems = [NSArray arrayWithObjects:
                       self.clearCommentTextViewButton,
                       [[[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemFlexibleSpace target:self action:nil] autorelease],
                       self.postCommentButton, nil];
  
  middleToolBar.items = barItems;
  self.postCommentTextView.inputAccessoryView = middleToolBar;
}

-(void)placeLoadingFooter
{
  CGRect footerRect = (CGRect)
  {
    0.0f,
    0.0f,
    self.commentsTableView.bounds.size.width,
    40.0f,
  };
  
  UIActivityIndicatorView *loadingIndicator = [[UIActivityIndicatorView alloc] initWithFrame:footerRect];
  loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
  
  [loadingIndicator startAnimating];
  
  self.commentsTableView.tableFooterView = loadingIndicator;
  
  [loadingIndicator release];
}
-(void)removeLoadingFooter
{
  self.commentsTableView.tableFooterView = nil;
}

-(void)backButtonCoverButtonTapped
{
  if ([self.commentedObject commentsCount] && self.postCommentFormShowed)
    [self hidePostCommentForm];
  else
  {
    [self removeBackButtonCoverButton];
    [self.navigationController popViewControllerAnimated:YES];
  }
}

-(void)updateCommentsCountLabel
{
  NSNumber *number = @([self.commentedObject commentsCount]);
  self.commentsCountLabel.text = [NSString stringWithFormat:SLBundlePluralizedString(@"mFacebook_proflie_%@ comments", number, nil), number];
}

-(void)showPostCommentForm:(BOOL)state
{
  if (self.postCommentFormShowed != state)
  {
    self.backButtonCoverButton.hidden = !state;
    
    self.commentsTableView.scrollEnabled = !state;
    self.postCommentTextView.hidden = !state;
    
    if (state)
    {
      [self clearPostCommentTextView];
    
      CGPoint offset = self.commentsTableView.contentOffset;
    
      self.postCommentTextView.frame = CGRectMake(offset.x + 10, offset.y + 10, 300.0f, 0.0f);
      
      [UIView beginAnimations:nil context:nil];
      [UIView setAnimationDuration:0.3f];
      self.postCommentTextView.frame = CGRectMake(offset.x + 10, offset.y + 10, 300.0f, 130.0f);
      [UIView commitAnimations];
      
      [self.postCommentTextView becomeFirstResponder];
      
      [self addBackButtonCoverButton];
    }
    else
    {
      [self.postCommentTextView resignFirstResponder];
      [self removeBackButtonCoverButton];
    }
    
    self.showPostCommentFormButton.enabled = !state;
    self.postCommentFormShowed = state;
  }
}

-(void)showPostCommentFormWithAuthentication
{
  if ([aSha isAuthenticatedWithFacebook])
    [self showPostCommentForm];
  else
  {
    aSha.viewController = self;
    [aSha authenticatePersonUsingService:auth_ShareServiceTypeFacebook
                               andCredentials:nil
                               withCompletion:@selector(showPostCommentForm)
                                      andData:nil
                shouldShowLoginRequiredPrompt:NO];
  }
}

-(void)showPostCommentForm
{
  [self showPostCommentForm:YES];
}

-(void)hidePostCommentForm
{
  [self showPostCommentForm:NO];
}

-(void)postComment
{
  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
  
  [self hidePostCommentForm];
  
  self.navigationController.navigationBar.userInteractionEnabled = NO;
  self.tableView.userInteractionEnabled = NO;
  self.postCommentButton.enabled = NO;
  
  [aSha postFacebookComment:self.postCommentTextView.text
                  parentObjectId:[self.commentedObject facebookObjectId]
                      completion:^(NSDictionary *data, NSError *error)
   {
     if (!error)
     {
       [self.commentedObject incrementCommentsCount];
       [self updateCommentsCountLabel];

       self.tableView.contentOffset = CGPointZero;
       [self pullToRefresh];
     }

     self.navigationController.navigationBar.userInteractionEnabled = YES;
     self.tableView.userInteractionEnabled = YES;
     self.postCommentButton.enabled = YES;

     [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
   }];
}

-(void)loadCommentsWithCompletion:(void (^)(void)) completion
{
  self.loadingInProgress = YES;
  
  NSString *graphPath;
  if (!self.comments || !self.nextPagePath)
  {
    NSString *graphPattern = @"/%@/comments?fields=comment_count,id,from,created_time,message&limit=20&order=reverse_chronological&summary=1";
    graphPath = [NSString stringWithFormat:graphPattern, [self.commentedObject facebookObjectId]];
  }
  else
    graphPath = self.nextPagePath;
  
  [aSha postFacebookRequestWithGraphPath:graphPath
                                   completion:^(NSDictionary *data, NSError *error) {
                                     if(!error)
                                     {
                                       self.nextPagePath = [[data objectForKey:@"paging"] objectForKey:@"next"];
                                       
                                       NSArray *commentsData = [data objectForKey:@"data"];
                                       
                                       int comments_count = [[[data objectForKey:@"summary"] objectForKey:@"total_count"] integerValue];
                                       [self.commentedObject setNewCommentsCountValue:comments_count];
                                       
                                       if (!self.comments)
                                         self.comments = [NSMutableArray array];
                                       
                                       NSMutableArray *comments = [mFacebookComment itemsFromArray:commentsData];
                                       [self.comments addObjectsFromArray:comments];
                                       
                                       [self updateCommentsCountLabel];
                                       
                                       [self.commentsTableView reloadData];
                                       [self.tableView layoutIfNeeded];
                                     }
                                     else
                                     {
                                       NSLog(@"ERROR getting comments from FB!");
                                     }
                                     if (completion)
                                       completion();
                                     
                                     self.loadingInProgress = NO;
                                     
                                     if (!self.comments || !self.comments.count)
                                       [self removeLoadingFooter];
                                   }];
}

-(void)pullToRefresh
{
  [self placeLoadingFooter];
  
  self.comments = nil;
  
  [self loadCommentsWithCompletion:^{
    [self.refreshControl endRefreshing];
  }];
}

-(void)clearPostCommentTextView
{
  self.postCommentTextView.text = @"";
  [self textViewDidChange:self.postCommentTextView];
}

#pragma mark UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *cellIdentifier = @"mFacebookCommentsTableViewCell";
  mFacebookCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
  
  if(!cell){
    cell = [[[mFacebookCommentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
    cell.avatarView.frame = CGRectMake(10.0f, 10.0f, 30.0f, 30.0f);
  }
  
  mFacebookComment *comment = self.comments[indexPath.row];
  
  [cell.avatarView setImageWithURL:comment.avatarURL
                  placeholderImage:[UIImage imageNamed:resourceFromBundle(@"mFacebook_small_ava")]];

  
  CGFloat textWidth = cell.contentView.frame.size.width - CGRectGetMaxX(cell.avatarView.frame) - 20;
  
  cell.nameLabel.text = comment.authorName;
  CGSize nameLabelSize = [cell.nameLabel.text sizeForFont:cell.nameLabel.font
                                            limitSize:CGSizeMake(textWidth, CGFLOAT_MAX)
                                      nslineBreakMode:cell.textLabel.lineBreakMode];
  cell.nameLabel.frame = CGRectMake(50.0f, 10.0f, nameLabelSize.width, nameLabelSize.height);
  
  
  NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
  
  [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
  [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
  
  cell.dateLabel.text = [dateFormatter stringFromDate:comment.creationTime];
  cell.dateLabel.frame = CGRectMake(50.0f, CGRectGetMaxY(cell.nameLabel.frame), 150, nameLabelSize.height);
  
  cell.messageLabel.text = comment.message;
  CGSize messageLabelSize = [cell.messageLabel.text sizeForFont:cell.messageLabel.font
                                                      limitSize:CGSizeMake(textWidth, CGFLOAT_MAX)
                                                nslineBreakMode:cell.messageLabel.lineBreakMode];
  
  cell.messageLabel.frame = CGRectMake(50.0f, CGRectGetMaxY(cell.dateLabel.frame) + 5, messageLabelSize.width, messageLabelSize.height);
  
  CGSize addCommentSize = {0, 0};
  if (![self.commentedObject isKindOfClass:[mFacebookComment class]])
  {
    if (!comment.commentsCount)
      cell.addCommentLabel.text = NSBundleLocalizedString(@"mFacebook_addCommentMessage", @"Add Comment");
    else
      cell.addCommentLabel.text = [NSString stringWithFormat:@"%@ %lu", NSBundleLocalizedString(@"mFacebook_comments", @"Comments"), (long)comment.commentsCount];
    
    addCommentSize = [cell.addCommentLabel.text sizeForFont:cell.addCommentLabel.font
                                                  limitSize:CGSizeMake(textWidth, CGFLOAT_MAX)
                                            nslineBreakMode:cell.addCommentLabel.lineBreakMode];
    
    cell.addCommentLabel.frame = CGRectMake(50.0f, CGRectGetMaxY(cell.messageLabel.frame) + 5, addCommentSize.width, addCommentSize.height);
  }
  else
  {
    addCommentSize = CGSizeMake(0, 0);
    [cell.addCommentLabel setHidden:YES];
  }
  
  
  cell.separator.frame = CGRectMake(50, CGRectGetMaxY(cell.messageLabel.frame) + cell.addCommentLabel.frame.size.height + 15 - 1,
                                    cell.contentView.frame.size.width - 50, 1);
  
  BOOL needLoad = indexPath.row > (self.comments.count - 10);
  BOOL allLoaded = self.comments.count && !self.nextPagePath;
  
  if (!self.loadingInProgress && needLoad && !allLoaded)
    [self loadCommentsWithCompletion:nil];
  
  if (allLoaded) [self removeLoadingFooter];
  
  return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return self.comments.count;
}

#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  CGFloat result = 0;
  
  CGFloat textWidth = tableView.frame.size.width - 40 - 20;
  
  mFacebookComment *comment = self.comments[indexPath.row];
  
  mFacebookCommentTableViewCell *cell = [[mFacebookCommentTableViewCell alloc] init];
  
  cell.nameLabel.text = comment.authorName;
  CGSize nameLabelSize = [cell.nameLabel.text sizeForFont:cell.nameLabel.font
                                                limitSize:CGSizeMake(textWidth, CGFLOAT_MAX)
                                          nslineBreakMode:cell.textLabel.lineBreakMode];
  cell.messageLabel.text = comment.message;
  CGSize messageLabelSize = [cell.messageLabel.text sizeForFont:cell.messageLabel.font
                                                      limitSize:CGSizeMake(textWidth, CGFLOAT_MAX)
                                                nslineBreakMode:cell.messageLabel.lineBreakMode];
  CGSize addCommentSize = {0, 0};
  if (![self.commentedObject isKindOfClass:[mFacebookComment class]])
  {
    cell.addCommentLabel.text = NSBundleLocalizedString(@"mFacebook_addCommentMessage", @"Add Comment");
    addCommentSize = [cell.addCommentLabel.text sizeForFont:cell.addCommentLabel.font
                                                         limitSize:CGSizeMake(textWidth, CGFLOAT_MAX)
                                                   nslineBreakMode:cell.addCommentLabel.lineBreakMode];
    cell.addCommentLabel.frame = CGRectMake(50.0f, CGRectGetMaxY(cell.messageLabel.frame), addCommentSize.width, addCommentSize.height);
  }
  else
    addCommentSize = CGSizeMake(0, 0);
  
  result = 10 + (nameLabelSize.height * 2) + messageLabelSize.height + addCommentSize.height + 10 + 10;
  
  [cell release];
  
  return result;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  if (![self.commentedObject isKindOfClass:[mFacebookComment class]] && !self.postCommentFormShowed)
  {
    mFacebookCommentsViewController *commentsVC = [[[mFacebookCommentsViewController alloc] initWithCommentedObject:self.comments[indexPath.row]] autorelease];
    [self.navigationController pushViewController:commentsVC animated:YES];
  }
}

#pragma mark UITextViewDelegate

-(void)textViewDidChange:(UITextView *)textView
{
  self.postCommentButton.enabled = self.postCommentTextView.text.length > 0;
}

@end
