#import "mFacebookBaseVC.h"
#import "mFacebookVideoDescription.h"
#import "NRGridViewController.h"

/**
 *  Controller to display the list of available videos on the page.
 */
@interface mFacebookVideoListViewController : mFacebookBaseViewController<NRGridViewDataSource,
                                                                          NRGridViewDelegate>

@end
