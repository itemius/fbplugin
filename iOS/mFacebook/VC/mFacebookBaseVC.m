#import "mFacebookBaseVC.h"
#import "iphnavbardata.h"
#import "functionLibrary.h"
#import "UIColor+RGB.h"
#import "UIColor+HSL.h"

UIColor *navbarTintColorToRestore;
UIColor *navbarBarTintColorToRestore;
NSDictionary *titleTextAttributes;

@implementation mFacebookBaseViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nil bundle:nil];
  
  if(self){
    aSha = [auth_Share sharedInstance];
    
    self.parameters = [mFacebookParameters sharedParameters];
  }
  
  return self;
}

-(void)dealloc
{
  self.parameters = nil;
  
  aSha.viewController = nil;
  aSha.delegate = nil;
  
  [[NSNotificationCenter defaultCenter] removeObserver:self];
  
  [super dealloc];
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
    self.wantsFullScreenLayout = YES;
  }
  
  [self placeRefreshControl];
  
  self.view.backgroundColor = self.parameters.design.color1;
  self.tableView.separatorColor = [UIColor clearColor];
  
}

-(void)placeRefreshControl
{
  UIRefreshControl *refreshControl = [[[UIRefreshControl alloc] init] autorelease];
  [refreshControl setTintColor:self.parameters.design.color4];
  [refreshControl addTarget:self
                     action:@selector(pullToRefresh)
           forControlEvents:UIControlEventValueChanged];
  
  self.refreshControl = refreshControl;
}

-(void)pullToRefresh
{
  //to be implemented in descendants
}

#pragma mark - View lifecycle
- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  
  aSha.viewController = self;
  aSha.delegate = self;
  
  [self isInternetReachable];
  
  // before hiding / displaying tabBar we must remember its previous state
  self.tabBarIsHidden = [[self.tabBarController tabBar] isHidden];
  [[self.tabBarController tabBar] setHidden:!self.showTabBar];
  
  [self.navigationController setNavigationBarHidden:NO];
  
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(reachabilityChanged:)
                                               name:kReachabilityChangedNotification
                                             object:nil];
  
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(customizeNavBarAppearanceCompleted:)
                                               name:TIPhoneNavBarDataCustomizeNavBarAppearanceCompleted
                                             object:nil];
  
  if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
  {
    [self registerVideoPlayerWindowNotifications];
  }
}

-(void)viewWillDisappear:(BOOL)animated
{
  // restore tabBar state
  [[self.tabBarController tabBar] setHidden:self.tabBarIsHidden];
  [self.navigationController setNavigationBarHidden:NO];
  
  [self applyColorsToRestoreIfNeeded];
  
  [[NSNotificationCenter defaultCenter] removeObserver:self
                                                  name:kReachabilityChangedNotification
                                                object:nil];
  
  [[NSNotificationCenter defaultCenter] removeObserver:self
                                                  name:TIPhoneNavBarDataCustomizeNavBarAppearanceCompleted
                                                object:nil];
  
  if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
  {
    [self unregisterVideoPlayerWindowNotifications];
  }
  
  [super viewWillDisappear:animated];
}

#pragma mark - Reachability

-(void)reachabilityChanged:(NSNotification *) notification
{
  NSLog(@"mFacebookBaseVC -- reachability changed");
}

- (BOOL)isInternetReachable
{
  NSString *hostName = [functionLibrary hostNameFromString:[@"http://www.google.com" stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
  
  Reachability *hostReachable = [Reachability reachabilityWithHostName:hostName];
  NetworkStatus hostStatus = [hostReachable currentReachabilityStatus];
  
  return hostStatus != NotReachable;
}

#pragma mark -
-(NSInteger)previousViewControllerIndex
{
  NSArray *viewControllers = self.navigationController.viewControllers;
  NSInteger currentIndex = [viewControllers indexOfObject:self];
  
  return currentIndex - 1;
}

#pragma mark - Appearance customization

-(void)customizeNavBarAppearanceCompleted:(NSNotification *)notification
{
  [self saveColorsToRestoreIfNeeded];
  
  [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
  
  UINavigationBar *navBar = self.navigationController.navigationBar;
  UIColor *backgroundColor = self.parameters.design.color1;
  
  if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
    if (backgroundColor.isLight)
      [navBar setBarTintColor:[backgroundColor blend:[[UIColor blackColor] colorWithAlphaComponent:0.2f]]];
    else
      [navBar setBarTintColor:[backgroundColor blend:[[UIColor whiteColor] colorWithAlphaComponent:0.4f]]];
    
    [navBar setTintColor:[UIColor blackColor]];
    [navBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor],
                                     NSFontAttributeName : [UIFont systemFontOfSize:18.0f]}];
  }
}

-(void)saveColorsToRestoreIfNeeded
{
  if([self.navigationController.viewControllers indexOfObject:self] == 1){
    if(!navbarTintColorToRestore)
    {
      navbarTintColorToRestore = [self.navigationController.navigationBar.tintColor retain];
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") && !navbarBarTintColorToRestore){
      navbarBarTintColorToRestore = [self.navigationController.navigationBar.barTintColor retain];
    }
    
    if(!titleTextAttributes)
    {
      titleTextAttributes = [self.navigationController.navigationBar.titleTextAttributes retain];
    }
  }
}

-(void)applyColorsToRestoreIfNeeded
{
  if(self.navigationController.viewControllers.count == 1){
    self.navigationController.navigationBar.tintColor = navbarTintColorToRestore;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
      self.navigationController.navigationBar.barTintColor = navbarBarTintColorToRestore;
    }
    self.navigationController.navigationBar.titleTextAttributes = titleTextAttributes;
    
    [navbarTintColorToRestore release];
    navbarTintColorToRestore = nil;
    
    [navbarBarTintColorToRestore release];
    navbarBarTintColorToRestore = nil;
    
    [titleTextAttributes release];
    titleTextAttributes = nil;
  }
}

#pragma mark - auth_Share delegate

-(void)didAuthorizeOnService:(auth_ShareServiceType)serviceType error:(NSError *)error
{
  if(!error &&
     serviceType == auth_ShareServiceTypeFacebook)
  {
    [aSha loadFacebookLikedURLs];
  }
}

-(void)didLoadFacebookLikedURLs:(NSMutableSet *)likedURLs error:(NSError *)error
{
  if(!error)
  {
    [mFacebookParameters sharedParameters].likedURLs = likedURLs;
  }
}

#pragma mark - Autorotate handlers

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
  return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

-(BOOL)shouldAutorotate
{
  return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
  return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
  return UIInterfaceOrientationPortrait;
}

#pragma mark - Handling iOS 8 fullscreen video behavior

-(void)registerVideoPlayerWindowNotifications
{
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(videoPlayerWillEnterFullscreen:)
                                               name:UIWindowDidBecomeVisibleNotification
                                             object:nil];
  
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(videoPlayerWillExitFullscreen:)
                                               name:UIWindowDidBecomeHiddenNotification
                                             object:nil];
}

-(void)unregisterVideoPlayerWindowNotifications
{
  [[NSNotificationCenter defaultCenter] removeObserver:self
                                                  name:UIWindowDidBecomeVisibleNotification
                                                object:nil];
  
  [[NSNotificationCenter defaultCenter] removeObserver:self
                                                  name:UIWindowDidBecomeHiddenNotification
                                                object:nil];
}

-(void)videoPlayerWillEnterFullscreen:(NSNotification *)notification
{
  [UIView animateWithDuration:0.0f
                   animations:^{
                     [self.navigationController setNavigationBarHidden:YES animated:NO];
                   }];
}

-(void)videoPlayerWillExitFullscreen:(NSNotification *)notification
{
  [UIView animateWithDuration:0.0f
                   animations:^{
                     
                     [self.navigationController setNavigationBarHidden:NO animated:NO];
                     
                   } completion:^(BOOL finished) {
                     
                     CGRect properNavigationBarFrame = self.navigationController.navigationBar.frame;
                     properNavigationBarFrame.size.height = kFacebookNavBarHeight;
                     self.navigationController.navigationBar.frame = properNavigationBarFrame;
                     
                   }];
}

@end
