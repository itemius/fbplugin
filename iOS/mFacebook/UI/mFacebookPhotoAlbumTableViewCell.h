#import <UIKit/UIKit.h>
#import "mFacebookPhotoAlbum.h"

@interface mFacebookPhotoAlbumTableViewCell : UITableViewCell

@property (nonatomic, strong) mFacebookPhotoAlbum *album;

+(CGFloat)heightForPhotoAlbum:(mFacebookPhotoAlbum *)album;

@end
