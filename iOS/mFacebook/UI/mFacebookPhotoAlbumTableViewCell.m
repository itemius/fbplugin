#import "mFacebookPhotoAlbumTableViewCell.h"
#import "mFacebookParameters.h"

#import <UIImageView+WebCache.h>
#import <auth_Share/auth_Share.h>

const CGFloat IMAGE_VIEW_SIZE = 70;

@implementation mFacebookPhotoAlbumTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self)
  {
    [self setupInterface];
  }
  return self;
}

-(void)dealloc
{
  self.album = nil;
  
  [super dealloc];
}

-(void)setupInterface
{
  self.clipsToBounds = YES;
  
  [self setupContentView];
  
  self.textLabel.textColor = [mFacebookParameters sharedParameters].design.color3;
  self.detailTextLabel.textColor = [[mFacebookParameters sharedParameters].design.color4 colorWithAlphaComponent:0.7f];
}

-(void)setupContentView
{
  self.selectionStyle = UITableViewCellSelectionStyleNone;
  self.contentView.autoresizesSubviews = YES;
  
  self.contentView.clipsToBounds = YES;
  
  self.backgroundColor = [mFacebookParameters sharedParameters].design.color1;
  self.contentView.backgroundColor = [mFacebookParameters sharedParameters].design.color1;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateInterface
{
  self.imageView.image = [UIImage imageNamed:@"photo_placeholder.png"];
  
  if (self.album.coverURL)
  {
    [self.imageView setImageWithURL:self.album.coverURL
                   placeholderImage:[UIImage imageNamed:@"photo_placeholder.png"]];
  }
  else
  {
    mFacebookPhotoAlbum *curAlbum = self.album;
    [[auth_Share sharedInstance] postFacebookRequestWithGraphPath:[NSString stringWithFormat:@"%@", self.album.coverId]
                                                       completion:^(NSDictionary *data, NSError *error) {
                                                         if(!error)
                                                           curAlbum.coverURL = [data objectForKey:@"picture"];
                                                         
                                                         [self.imageView setImageWithURL:self.album.coverURL
                                                                        placeholderImage:[UIImage imageNamed:@"photo_placeholder.png"]];
                                                       }];
  }
  
  self.textLabel.text = self.album.name;
  self.detailTextLabel.text = [NSString stringWithFormat:@"%lu %@", (long)self.album.count, NSBundleLocalizedString(@"mFacebook_photos", @"photos")];
}

-(void)layoutSubviews
{
  [super layoutSubviews];
  
  self.imageView.frame = CGRectMake( 5.0f, 5.0f, IMAGE_VIEW_SIZE, IMAGE_VIEW_SIZE );
  self.imageView.layer.masksToBounds = YES;
  self.imageView.userInteractionEnabled = YES;
  self.imageView.contentMode = UIViewContentModeScaleAspectFill;
  
  self.textLabel.textAlignment = NSTextAlignmentLeft;
  self.textLabel.font = [UIFont systemFontOfSize:20.0f];
  self.textLabel.numberOfLines = 1;
  self.textLabel.backgroundColor = [UIColor clearColor];
  
  CGSize textLabel = [self.textLabel.text sizeWithFont:self.textLabel.font
                                    constrainedToSize:CGSizeMake(230.0f, 30.0f)
                                        lineBreakMode:self.textLabel.lineBreakMode];
  self.textLabel.frame = CGRectMake(15.0f + IMAGE_VIEW_SIZE, 20.0f, textLabel.width, textLabel.height);
  
  
  CGSize detailLabelSize = [self.detailTextLabel.text sizeWithFont:self.detailTextLabel.font
                                                        constrainedToSize:CGSizeMake(230.0f, 50.0f)
                                                  lineBreakMode:self.textLabel.lineBreakMode];
  self.detailTextLabel.textAlignment = NSTextAlignmentLeft;
  self.detailTextLabel.font = [UIFont systemFontOfSize:14.f];
  self.detailTextLabel.frame = CGRectMake(15.0f + IMAGE_VIEW_SIZE, CGRectGetMaxY(self.textLabel.frame), 100, detailLabelSize.height);
  self.detailTextLabel.backgroundColor = [UIColor clearColor];
}

-(void)setAlbum:(mFacebookPhotoAlbum *)album
{
  [album retain];
  [_album release];
  
  _album = album;
  
  [self updateInterface];
}

+(CGFloat)heightForPhotoAlbum:(mFacebookPhotoAlbum *)album
{
  return IMAGE_VIEW_SIZE + 10;
}

@end
