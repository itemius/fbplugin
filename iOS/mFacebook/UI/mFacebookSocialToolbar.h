#import <UIKit/UIKit.h>

@class mFacebookSocialToolbar;

/**
 * Delegate for mFacebookSocialToolbar.
 * Used to respond on actions generated by the toolbar.
 *
 * @see mFacebookSocialToolbar
 */
@protocol mFacebookSocialToolbarDelegate <NSObject>

@required
/**
 * Method to handle the tap on the like button.
 *
 * @param toolbar - source toolbar for this action.
 *
 * @see mFacebookSocialToolbar
 */
-(void)itemLikedFromToolbar:(mFacebookSocialToolbar *)toolbar;

/**
 * Method to handle the tap on the share button.
 *
 * @param toolbar - source toolbar for this action.
 *
 * @see mFacebookSocialToolbar
 */
-(void)itemSharedFromToolbar:(mFacebookSocialToolbar *)toolbar;

@end


/**
 * Custom toolbar with buttons for like and share a facebook item (currently, video)
 * from some kind of detail screen (e.g. detail screen for video).
 */
@interface mFacebookSocialToolbar : UIToolbar

/**
 * Delegate to handle social actions triggered on toolbar.
 * 
 * @see mFacebookSocialToolbarDelegate
 */
@property (nonatomic, assign) id<mFacebookSocialToolbarDelegate> facebookSocialToolbarDelegate;

/**
 * Item id from facebook.
 */
@property (nonatomic, retain) NSString *facebookItemId;

/**
 * Method to disable and gray-out the "Like" button.
 */
-(void)indicateLikeSucceeded;

@end
