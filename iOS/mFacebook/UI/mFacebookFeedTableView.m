#import "mFacebookFeedTableView.h"
#import "mFacebookFeedTableViewCell.h"

#define kFeedTableViewContentInsetBottom 10.0f

@implementation mFacebookFeedTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
  self = [super initWithFrame:frame style:style];
  
  if(self)
  {
    [self setupTableView];
  }
  
  return self;
}

-(void)setupTableView
{
  self.autoresizesSubviews = YES;
  self.autoresizingMask    = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

  [self setShowsHorizontalScrollIndicator:NO];
  [self setShowsVerticalScrollIndicator:YES];
  
  self.separatorColor = [UIColor clearColor];
  
  [self setContentInsetBottom:kFeedTableViewContentInsetBottom];
}

-(void)setContentInsetBottom:(CGFloat)inset
{
  UIEdgeInsets contentInset = self.contentInset;
  contentInset.bottom = inset;//kFeedTableViewContentInsetBottom;
  
  self.contentInset = contentInset;
}

-(void)setTableFooterView:(UIView *)tableFooterView
{
  CGFloat bottomInset = 0.0f;
  
  if(!tableFooterView)
  {
    bottomInset = kFeedTableViewContentInsetBottom;
  }
  
  [self setContentInsetBottom:bottomInset];
  
  [super setTableFooterView:tableFooterView];
}

@end
