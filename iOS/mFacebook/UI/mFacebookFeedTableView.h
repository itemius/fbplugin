#import <UIKit/UIKit.h>

/**
 * Preconfigured table view to display the feed.
 */
@interface mFacebookFeedTableView : UITableView

@end
