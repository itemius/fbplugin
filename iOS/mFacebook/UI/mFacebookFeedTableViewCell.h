#import <UIKit/UIKit.h>
#import "mFacebookFeedItem.h"
#import "mFacebookVideoDescription.h"

#import "TTTAttributedLabel.h"

@class mFacebookFeedTableViewCell;

/**
 * Delegate for mFacebookFeedTableViewCell.
 * Used to respond on actions generated by the cell.
 *
 * @see mFacebookFeedTableViewCell
 */
@protocol mFacebookFeedTableViewCellDelegate<NSObject>

/**
 * Method to handle the tap on the like button.
 *
 * @param cell - source cell for this action.
 *
 * @see mFacebookFeedTableViewCell
 */
-(void)likeButtonPressedOnCell:(mFacebookFeedTableViewCell *)cell;

/**
 * Method to handle a tap on the comment button.
 *
 * @param cell - source cell for this action.
 *
 * @see mFacebookFeedTableViewCell
 */
-(void)commentButtonPressedOnCell:(mFacebookFeedTableViewCell *)cell;

/**
 * Method to hanle a tap on the share button.
 *
 * @param cell - source cell for this action.
 *
 * @see mFacebookFeedTableViewCell
 */
-(void)shareButtonPressedOnCell:(mFacebookFeedTableViewCell *)cell;

/**
 * Method to expand a long message (which was cut) when user taps on it
 *
 * @param cell - source cell for this action.
 *
 * @see mFacebookFeedTableViewCell
 */
-(void)messageExpandedOnCell:(mFacebookFeedTableViewCell *)cell;

-(void)picturePressedOnCell:(mFacebookFeedTableViewCell *)cell
                 imageIndex:(NSUInteger)index;

/**
 * Method to be invoked when user taps a video displayed on the cell (if any).
 *
 * @param description - mFacebookVideoDescription of the video.
 * @param cell - source cell of the action.
 *
 * @see mFacebookVideoDescription
 * @see mFacebookFeedTableViewCell
 */
-(void)videoWithDescription:(mFacebookVideoDescription *)description
              pressedOnCell:(mFacebookFeedTableViewCell *)cell;

@end

/**
 * Custom table view cell to display an item of the feed
 */
@interface mFacebookFeedTableViewCell : UITableViewCell

/**
 * Feed item to be displayed in the cell
 */
@property (nonatomic, strong) mFacebookFeedItem *feedItem;

/**
 * Delegate of the cell.
 *
 * @see mFacebookFeedTableViewCellDelegate
 */
@property (nonatomic, assign) id<mFacebookFeedTableViewCellDelegate> delegate;

/**
 * Delegate of the label with the feed item's message.
 *
 * @see TTTAttributedLabel
 * @see TTTAttributedLabelDelegate
 */
@property (nonatomic, assign) id<TTTAttributedLabelDelegate> messageLabelDelegate;

/**
 * Calculates the height of the cell depending on the content being presented.
 * 
 * @return heightForFeedItem - heigth of the cell
 *
 * @param item - any feed item to measure.
 *
 * @see mFacebookFeedItem
 */
+(CGFloat)heightForFeedItem:(mFacebookFeedItem *)item;

@end
