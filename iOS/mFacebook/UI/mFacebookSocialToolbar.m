#import "mFacebookSocialToolbar.h"

@interface mFacebookSocialToolbar()
{
  UIBarButtonItem *likeButtonItem;
  UIBarButtonItem *shareButtonItem;
  UIBarButtonItem *flexibleSpace;
}

@end

@implementation mFacebookSocialToolbar

-(instancetype)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  
  if(self)
  {
    [self setupToolbar];
  }
  
  return self;
}

-(void)dealloc
{
  self.facebookItemId = nil;
  
  [likeButtonItem release];
  likeButtonItem = nil;
  
  [shareButtonItem release];
  shareButtonItem = nil;
  
  [flexibleSpace release];
  flexibleSpace = nil;
  
  [super dealloc];
}

-(void)setupToolbar
{
  [self setupAppearance];
  [self placeItems];
}

-(void)setupAppearance
{
  self.tintColor = nil;
  
  if ([[UIToolbar class] respondsToSelector:@selector(appearance)]) {
    [self setBackgroundImage:nil forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
    [self setBackgroundImage:nil forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsLandscapePhone];
  }
  self.barStyle = UIBarStyleBlack;
  self.alpha = 0.85f;
  self.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
  
  self.autoresizesSubviews = YES;
}

-(void)placeItems
{
  UIImage *likeImage = [UIImage imageNamed:resourceFromBundle(@"mFacebook_like_media")];
  UIImage *shareImage = [UIImage imageNamed:resourceFromBundle(@"mFacebook_share_media")];
  
  likeButtonItem = [[UIBarButtonItem alloc] initWithImage:likeImage
                                                    style:UIBarButtonItemStylePlain
                                                   target:self
                                                   action:@selector(likeItem:)];
  
  shareButtonItem = [[UIBarButtonItem alloc] initWithImage:shareImage
                                                     style:UIBarButtonItemStylePlain
                                                    target:self
                                                    action:@selector(shareItem:)];
  
  flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                target:nil
                                                                action:nil];
  
  likeButtonItem.tintColor = [UIColor whiteColor];
  shareButtonItem.tintColor = [UIColor whiteColor];
  
  NSArray *toolbarItems = @[flexibleSpace, likeButtonItem, flexibleSpace, flexibleSpace, shareButtonItem, flexibleSpace];
  
  [self setItems:toolbarItems animated:NO];
}

-(void)likeItem:(id)sender
{
  [self.facebookSocialToolbarDelegate itemLikedFromToolbar:self];
}

-(void)shareItem:(id)sender
{
  [self.facebookSocialToolbarDelegate itemSharedFromToolbar:self];
}

-(void)indicateLikeSucceeded
{
  likeButtonItem.enabled = NO;
}

@end
