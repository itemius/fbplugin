#import "mFacebookCommentTableViewCell.h"
#import "mFacebookParameters.h"

#import "UIColor+HSL.h"


#define kSeparatorColor [UIColor colorWithWhite:1.0 alpha:0.3]
#define kSeparatorColorForWhiteBackground [UIColor colorWithWhite:0.0 alpha:0.2]

@implementation mFacebookCommentTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  
  if(self){
    
    mFacebookParameters *params = [mFacebookParameters sharedParameters];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = params.design.color1;
    
    self.avatarView = [[UIImageView new] autorelease];
    self.avatarView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarView.clipsToBounds = YES;
    [self.contentView addSubview:self.avatarView];
    
    self.nameLabel       = [[UILabel new] autorelease];
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    self.nameLabel.font = [UIFont boldSystemFontOfSize:12.0f];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.textColor = params.design.color3;
    [self.contentView addSubview:self.nameLabel];
  
    self.messageLabel       = [[UILabel new] autorelease];
    self.messageLabel.textAlignment = NSTextAlignmentLeft;
    self.messageLabel.font = [UIFont systemFontOfSize:12.0f];
    self.messageLabel.backgroundColor = [UIColor clearColor];
    self.messageLabel.textColor = params.design.color4;
    self.messageLabel.numberOfLines = 0;
    [self.contentView addSubview:self.messageLabel];
    
    self.dateLabel       = [[UILabel new] autorelease];
    self.dateLabel.font = [UIFont systemFontOfSize:12.f];
    self.dateLabel.textAlignment = NSTextAlignmentLeft;
    self.dateLabel.backgroundColor = [UIColor clearColor];
    self.dateLabel.textColor = params.design.color4;
    self.dateLabel.alpha = 0.5f;
    [self.contentView addSubview:self.dateLabel];
   
    self.addCommentLabel = [[UILabel new] autorelease];
    self.addCommentLabel.font = [UIFont systemFontOfSize:12.f];
    self.addCommentLabel.textAlignment = NSTextAlignmentLeft;
    self.addCommentLabel.backgroundColor = [UIColor clearColor];
    self.addCommentLabel.textColor = params.design.color5;
    [self.contentView addSubview:self.addCommentLabel];
    
    self.separator       = [[UIView new] autorelease];
    self.separator.backgroundColor = params.design.isWhiteBackground ? kSeparatorColorForWhiteBackground : kSeparatorColor;
    [self.contentView addSubview:self.separator];
  }
  
  return self;
}

-(void)dealloc
{
  self.nameLabel = nil;
  self.messageLabel = nil;
  self.addCommentLabel = nil;
  self.dateLabel = nil;
  self.separator = nil;
  
  [super dealloc];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
