#import "mFacebookFeedHeaderView.h"

#import "EGORefreshTableHeaderView.h"
#import "UIImageView+WebCache.h"
#import <auth_Share.h>
#import <Smartling.i18n/SLLocalization.h>
#import "NSString+colorizer.h"
#import "UIColor+image.h"

#define kHeaderViewWidth 320.0f
#define kHeaderViewHeight 100.0f

#define kFeedIconViewMarginLeft 10.0f
#define kFeedIconViewMarginBottom 8.0f
#define kFeedIconViewWidth 60.0f
#define kFeedIconViewHeight kFeedIconViewWidth
#define kFeedIconViewBorderWidth 4.0f
#define kFeedIconViewBorderColor [[UIColor whiteColor] CGColor]

#define kNameLabelFrameMarginLeft 10.0f
#define kNameLabelFrameMarginRight 10.0f
#define kNameLabelFontSize 19.0f
#define kNameLabelTextColor [UIColor whiteColor]

#define kCategoryFrameMarginLeft 10.0f
#define kCategoryFrameMarginRight 10.0f
#define kCategoryLabelFontSize 14.0f
#define kCategoryLabelTextColor [UIColor whiteColor]
#define kCategoryLabelMarginTop 1.0f

#define kLikesCountLabelFrameMarginLeft 10.0f
#define kLikesCountLabelFrameMarginRight 10.0f
#define kLikesCountLabelMarginTop 6.0f
#define kLikesCountLabelFontSize 12.0f
#define kLikesCountLabelTextColor [UIColor whiteColor]

#define kMaskViewBackgroundColor [[UIColor blackColor] colorWithAlphaComponent:0.5f]

#define kLabelShadowRadius 2.0f
#define kLabelShadowOpacity 0.9f
#define kLabelShadowColor [[UIColor blackColor] CGColor]
#define kLabelShadowOffset CGSizeMake(0.0f, 0.0f)

#define kTabButtonsHeight 44.0f
#define kTabButtonsCount 3
#define kTabButtonsBackgroundColor [@"#EEEEEE" asColor]
#define kTabButtonsSeparatorColor [[UIColor blackColor] colorWithAlphaComponent:0.15f]
#define kTabButtonsSeparatorWidth 1.0f
#define kTabButtonsSeparatorTopBottomMargin 7.0f
#define kTabButtonsFont [UIFont boldSystemFontOfSize:12.0f]
#define kTabButtonsCountersFont [UIFont systemFontOfSize:11.0f]
#define kTabButtonsTextColor [[UIColor blackColor] colorWithAlphaComponent:0.8f]
#define kTabButtonsCountersColor [[UIColor blackColor] colorWithAlphaComponent:0.5f]
#define kTabButtonsElemetsPadding 4.f

#define kLikeTabBarButtonLikedAlpha 0.3f

@interface mFacebookFeedHeaderView()

@property (nonatomic, strong) UIImageView *feedIconView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *categoryLabel;
@property (nonatomic, strong) UILabel *likesCountLabel;

@property (nonatomic, strong) UIImageView *feedSplashImageView;
@property (nonatomic, strong) UIView *maskView;

@property (nonatomic, assign) CGFloat tabButtonWidth;

@property (nonatomic, strong) UIImageView *likeImageView;
@property (nonatomic, strong) UILabel *likeLabel;

@property (nonatomic, strong) UIImageView *photoImageView;
@property (nonatomic, strong) UILabel *photoLabel;
@property (nonatomic, strong) UILabel *photoCountLabel;

@property (nonatomic, strong) UIImageView *videoImageView;
@property (nonatomic, strong) UILabel *videoLabel;
@property (nonatomic, strong) UILabel *videoCountLabel;

@property (nonatomic, retain) UIControl *nativeLikeButton;

@end

@implementation mFacebookFeedHeaderView

-(instancetype)initWithFeedInfo:(mFacebookFeedInfo *)feedInfo
{
  CGRect headerFrame = (CGRect)
  {
    0.0f,
    0.0f,
    kHeaderViewWidth,
    kHeaderViewHeight + kTabButtonsHeight
  };
  
  self = [super initWithFrame:headerFrame];
  
  if(self){
    _feedInfo = [feedInfo retain];
    
    self.tabButtonWidth = headerFrame.size.width / kTabButtonsCount;
    
    [self setupInterface];
  }
  
  return self;
}

-(instancetype)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  
  if(self){
    [self setupInterface];
  }
  
  return self;
}

-(void)dealloc
{
  self.feedIconView = nil;
  self.nameLabel = nil;
  self.categoryLabel = nil;
  self.likesCountLabel = nil;
  self.feedSplashImageView = nil;
  
  self.maskView = nil;
  
  self.likeTabButton = nil;
  self.photoTabButton = nil;
  self.videoTabButton = nil;
  
  self.likeImageView = nil;
  self.likeLabel = nil;
  
  self.photoImageView = nil;
  self.photoLabel = nil;
  self.photoCountLabel = nil;
  
  self.videoImageView = nil;
  self.videoLabel = nil;
  self.videoCountLabel = nil;
  
  self.nativeLikeButton = nil;
  
  [super dealloc];
}

-(void)setupInterface
{
  self.clipsToBounds = NO;
  
  [self placeFeedSplashImageView];
  [self placeMaskView];
  
  [self placeFeedIconImageView];
  
  //Place labels downside up
  [self placeLikesCountLabel];
  [self placeCategoryLabel];
  [self placeNameLabel];
  
  [self placeLikeTabButton];
  [self placePhotoTabButton];
  [self placeVideoTabButton];
  
  [self updateInterface];
}

-(void)placeFeedSplashImageView
{
  CGRect feedSplashImageFrame = self.bounds;
  
  feedSplashImageFrame.size.height = kHeaderViewHeight;
  
  self.feedSplashImageView = [[[UIImageView alloc] initWithFrame:feedSplashImageFrame] autorelease];
  self.feedSplashImageView.contentMode = UIViewContentModeScaleAspectFill;
  self.feedSplashImageView.clipsToBounds = YES;
  
  [self addSubview:self.feedSplashImageView];
}

-(void)placeMaskView
{
  CGRect maskViewFrame = self.feedSplashImageView.bounds;
  self.maskView = [[[UIView alloc] initWithFrame:maskViewFrame] autorelease];
  self.maskView.backgroundColor = kMaskViewBackgroundColor;
  
  [self.feedSplashImageView addSubview:self.maskView];
}

-(void)placeFeedIconImageView
{
  CGFloat feedIconViewOriginY = kHeaderViewHeight
                                - kFeedIconViewBorderWidth
                                - kFeedIconViewHeight
                                - kFeedIconViewMarginBottom;
  
  CGRect feedIconViewFrame = (CGRect)
  {
    kFeedIconViewMarginLeft,
    feedIconViewOriginY,
    kFeedIconViewWidth,
    kFeedIconViewHeight
  };
  
  self.feedIconView = [[[UIImageView alloc] initWithFrame:feedIconViewFrame] autorelease];
  self.feedIconView.layer.borderColor = kFeedIconViewBorderColor;
  self.feedIconView.layer.borderWidth = kFeedIconViewBorderWidth;
  
  [self insertSubview:self.feedIconView aboveSubview:self.maskView];
}

-(void)placeLikesCountLabel
{
  UIFont *font = [UIFont systemFontOfSize:kLikesCountLabelFontSize];
  CGFloat maxY = CGRectGetMaxY(self.feedIconView.frame);
  CGFloat originX = CGRectGetMaxX(self.feedIconView.frame) + kLikesCountLabelFrameMarginLeft;
  CGFloat width = kHeaderViewWidth - originX - kLikesCountLabelFrameMarginRight;
  
  self.likesCountLabel = [self labelWithFont:font
                                   textColor:kLikesCountLabelTextColor
                                     originX:originX
                                        maxY:maxY
                                       width:width];
  
  [self insertSubview:self.likesCountLabel aboveSubview:self.maskView];
}

-(void)placeCategoryLabel
{
  UIFont *font = [UIFont systemFontOfSize:kCategoryLabelFontSize];
  CGFloat maxY = CGRectGetMinY(self.likesCountLabel.frame) - kLikesCountLabelMarginTop;
  CGFloat originX = self.likesCountLabel.frame.origin.x;
  CGFloat width = self.likesCountLabel.frame.size.width;
  
  self.categoryLabel = [self labelWithFont:font
                                 textColor:kCategoryLabelTextColor
                                   originX:originX
                                      maxY:maxY
                                     width:width];
  
  [self insertSubview:self.categoryLabel aboveSubview:self.maskView];
}

-(void)placeLikeTabButton
{
  CGRect likeTabButtonFrame = (CGRect)
  {
    0,
    self.frame.size.height - kTabButtonsHeight,
    self.tabButtonWidth,
    kTabButtonsHeight
  };

  self.likeTabButton = [[[UIButton alloc] initWithFrame:likeTabButtonFrame] autorelease];
  self.likeTabButton.backgroundColor = kTabButtonsBackgroundColor;
  self.likeTabButton.adjustsImageWhenDisabled = YES;
  
  [self addSubview:self.likeTabButton];
  
  self.likeImageView = [[[UIImageView alloc] init] autorelease];
  self.likeImageView.image = [UIImage imageNamed:resourceFromBundle(@"mFacebook_like_post")];
  self.likeImageView.contentMode = UIViewContentModeCenter;
  
  [self.likeTabButton addSubview:self.likeImageView];
  
  self.likeLabel = [[[UILabel alloc] init] autorelease];
  self.likeLabel.font = kTabButtonsFont;
  self.likeLabel.backgroundColor = [UIColor clearColor];
  self.likeLabel.textColor = kTabButtonsTextColor;
  self.likeLabel.text =  NSBundleLocalizedString(@"mFacebook_do_like", @"Like");
  
  [self.likeTabButton addSubview:self.likeLabel];
}

-(void)placePhotoTabButton
{
  CGRect photoTabButtonFrame = (CGRect)
  {
    self.tabButtonWidth * 1,
    self.frame.size.height - kTabButtonsHeight,
    self.tabButtonWidth,
    kTabButtonsHeight
  };
  
  self.photoTabButton = [[[UIButton alloc] initWithFrame:photoTabButtonFrame] autorelease];
  self.photoTabButton.backgroundColor = kTabButtonsBackgroundColor;
  
  [self addSubview:self.photoTabButton];
  
  CGRect leftSeparatorFrame = (CGRect)
  {
    0,
    kTabButtonsSeparatorTopBottomMargin,
    kTabButtonsSeparatorWidth,
    kTabButtonsHeight - (kTabButtonsSeparatorTopBottomMargin * 2)
  };
  
  UIView *leftSeparatorView = [[[UIView alloc] initWithFrame:leftSeparatorFrame] autorelease];
  leftSeparatorView.backgroundColor = kTabButtonsSeparatorColor;
  
  [self.photoTabButton addSubview:leftSeparatorView];
  
  CGRect rightSeparatorFrame = (CGRect)
  {
    self.photoTabButton.frame.size.width - 1,
    kTabButtonsSeparatorTopBottomMargin,
    kTabButtonsSeparatorWidth,
    kTabButtonsHeight - (kTabButtonsSeparatorTopBottomMargin * 2)
  };
  
  UIView *rightSeparatorView = [[[UIView alloc] initWithFrame:rightSeparatorFrame] autorelease];
  rightSeparatorView.backgroundColor = kTabButtonsSeparatorColor;
  
  [self.photoTabButton addSubview:rightSeparatorView];
  
  self.photoImageView = [[[UIImageView alloc] init] autorelease];
  self.photoImageView.image = [UIImage imageNamed:resourceFromBundle(@"mFacebook_photo")];
  self.photoImageView.contentMode = UIViewContentModeCenter;
  
  [self.photoTabButton addSubview:self.photoImageView];
  
  self.photoLabel = [[[UILabel alloc] init] autorelease];
  self.photoLabel.font = kTabButtonsFont;
  self.photoLabel.backgroundColor = [UIColor clearColor];
  self.photoLabel.textColor = kTabButtonsTextColor;
  self.photoLabel.text =  NSBundleLocalizedString(@"mFacebook_photo", @"Photo");
  
  [self.photoTabButton addSubview:self.photoLabel];
  
  self.photoCountLabel = [[[UILabel alloc] init] autorelease];
  self.photoCountLabel.font = kTabButtonsCountersFont;
  self.photoCountLabel.backgroundColor = [UIColor clearColor];
  self.photoCountLabel.textColor = kTabButtonsCountersColor;
  
  [self.photoTabButton addSubview:self.photoCountLabel];
}
-(void)placeVideoTabButton
{
  CGRect videoTabButtonFrame = (CGRect)
  {
    self.tabButtonWidth * 2,
    self.frame.size.height - kTabButtonsHeight,
    self.tabButtonWidth,
    kTabButtonsHeight
  };
  
  self.videoTabButton = [[[UIButton alloc] initWithFrame:videoTabButtonFrame] autorelease];
  self.videoTabButton.backgroundColor = kTabButtonsBackgroundColor;
  
  [self addSubview:self.videoTabButton];
  
  self.videoImageView = [[[UIImageView alloc] init] autorelease];
  self.videoImageView.image = [UIImage imageNamed:resourceFromBundle(@"mFacebook_video")];
  self.videoImageView.contentMode = UIViewContentModeCenter;
  
  [self.videoTabButton addSubview:self.videoImageView];
  
  self.videoLabel = [[[UILabel alloc] init] autorelease];
  self.videoLabel.font = kTabButtonsFont;
  self.videoLabel.backgroundColor = [UIColor clearColor];
  self.videoLabel.textColor = kTabButtonsTextColor;
  self.videoLabel.text =  NSBundleLocalizedString(@"mFacebook_video", @"Video");
  
  [self.videoTabButton addSubview:self.videoLabel];
  
  self.videoCountLabel = [[[UILabel alloc] init] autorelease];
  self.videoCountLabel.font = kTabButtonsCountersFont;
  self.videoCountLabel.backgroundColor = [UIColor clearColor];
  self.videoCountLabel.textColor = kTabButtonsCountersColor;
  self.videoCountLabel.textAlignment = NSTextAlignmentLeft;
  
  [self.videoTabButton addSubview:self.videoCountLabel];
}

-(void)placeNameLabel
{
  UIFont *font = [UIFont systemFontOfSize:kNameLabelFontSize];
  CGFloat maxY = CGRectGetMinY(self.categoryLabel.frame) - kCategoryLabelMarginTop;
  CGFloat originX = self.categoryLabel.frame.origin.x;
  CGFloat width = self.categoryLabel.frame.size.width;
  
  self.nameLabel = [self labelWithFont:font
                             textColor:kNameLabelTextColor
                               originX:originX
                                  maxY:maxY
                                 width:width];

  [self insertSubview:self.nameLabel aboveSubview:self.maskView];
}

-(UILabel *)labelWithFont:(UIFont *)font
                textColor:(UIColor *)textColor
                  originX:(CGFloat)originX
                     maxY:(CGFloat)maxY
                    width:(CGFloat)width
{
  UILabel *label = [[[UILabel alloc] init] autorelease];
  label.font = font;
  label.numberOfLines = 1;
  label.lineBreakMode = NSLineBreakByTruncatingTail;
  label.textColor = textColor;
  label.backgroundColor = [UIColor clearColor];
  
  //Òp is a stub text to calculate high eneough bounds for label:
  //we have a capital letter with diacritic (Ò) and a character with hanging element (p)
  CGSize labelSize = [@"Òp" sizeWithFont:label.font
                     constrainedToSize:(CGSize){width, CGFLOAT_MAX}
                         lineBreakMode:label.lineBreakMode];
  
  CGFloat originY = maxY - labelSize.height;
  
  CGRect labelFrame = (CGRect)
  {
    originX,
    originY,
    width,
    labelSize.height
  };
  
  label.frame = labelFrame;
  
  label.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
  label.layer.shadowColor = kLabelShadowColor;
  label.layer.shadowOpacity = kLabelShadowOpacity;
  label.layer.shadowRadius = kLabelShadowRadius;
  label.layer.masksToBounds = NO;
  
  label.clipsToBounds = NO;

  return label;
}

-(void)updateInterface
{
  if(self.feedInfo)
  {
    self.nameLabel.text = self.feedInfo.name;
    self.categoryLabel.text = self.feedInfo.category;
    
    if (self.feedInfo.likesCount)
    {
      self.likesCountLabel.hidden = NO;
      NSNumber *likesCount = @(self.feedInfo.likesCount);
      
      NSString *likesCountText = nil;
      
      if([likesCount integerValue] == 1)
      {
        likesCountText = NSBundleLocalizedString(@"mFacebook_exactly_one_like", @"1 Like");
      } else
      {
        likesCountText = [NSString stringWithFormat:SLBundlePluralizedString(@"mFacebook_%@ likes", likesCount, nil), likesCount];
      }
      
      self.likesCountLabel.text = likesCountText;
    }
    else
      self.likesCountLabel.hidden = YES;;
    
    self.videoCountLabel.text = [NSString stringWithFormat:@"(%lu)", (unsigned long) self.feedInfo.videosCount];
    
    [self updateFeedIcon];
    [self updateSplashImageView];
    [self updateLikeTabButton];
    
    [self layoutTabButtons];
  }
}

-(void)updateLikeTabButton
{
  [self refreshNativeLikeButton];
  
  CGFloat alpha = 1.0f;
  
  BOOL liked = self.feedInfo.isLiked;
  
  if(liked)
  {
    alpha = kLikeTabBarButtonLikedAlpha;
  }
  
  self.likeImageView.alpha = alpha;
  self.likeLabel.alpha = alpha;
}

-(void)refreshNativeLikeButton
{
  [self.nativeLikeButton removeFromSuperview];
  
  if(self.feedInfo.facebookId)
  {
    [self.nativeLikeButton removeFromSuperview];
    
    self.nativeLikeButton = [[auth_Share sharedInstance] nativeFacebookLikeControlForId:self.feedInfo.facebookId];
    self.nativeLikeButton.frame = self.likeTabButton.frame;
    
    [self insertSubview:self.nativeLikeButton
           belowSubview:self.likeTabButton];
  }
}

-(void)updateFeedIcon
{
  UIImage *placeholderImage = self.feedIconView.image;
  
  if(!placeholderImage)
  {
    placeholderImage = [[UIColor lightGrayColor] asImageWithSize:self.feedIconView.frame.size];
  }
  
  [self.feedIconView setImageWithURL:self.feedInfo.authorAvatarURL
                    placeholderImage:placeholderImage];
}

-(void)updateSplashImageView
{
  UIImage *placeholderImage = self.feedSplashImageView.image;
  
  [self.feedSplashImageView setImageWithURL:self.feedInfo.splashImageURL
                           placeholderImage:placeholderImage];
}

-(void)setFeedInfo:(mFacebookFeedInfo *)feedInfo
{
  [_feedInfo release];
  _feedInfo = [feedInfo retain];
  
  [self updateInterface];
}

-(void)setPhotoCount:(NSInteger)count
{
  _photoCount = count;
  
  self.photoCountLabel.text = [NSString stringWithFormat:@"(%lu)", (unsigned long) count];
  [self layoutTabButtons];
}

-(void)layoutTabButtons
{
  [self layoutViews:@[ self.likeImageView, self.likeLabel ] inCenterOfView:self.likeTabButton];
  [self layoutViews:@[ self.photoImageView, self.photoLabel, self.photoCountLabel ] inCenterOfView:self.photoTabButton];
  [self layoutViews:@[ self.videoImageView, self.videoLabel, self.videoCountLabel ] inCenterOfView:self.videoTabButton];
}

-(void)layoutViews:(NSArray *)views inCenterOfView:(UIView *)view
{
  CGFloat viewsWidth = 0;
  
  CGFloat viewHeightCenter = view.bounds.size.height / 2;
  
  for (UIView *v in views)
  {
    CGRect viewFrame = CGRectZero;
    if ([v isKindOfClass:[UILabel class]])
    {
      UILabel *label = (UILabel *)v;
      CGSize labelSize = [label.text sizeWithFont:label.font];
      
      viewFrame.size = labelSize;
      viewFrame.origin = CGPointMake(0, viewHeightCenter - viewFrame.size.height / 2);
    }
    else if ([v isKindOfClass:[UIImageView class]])
    {
      UIImageView *imageView = (UIImageView *)v;
      
      viewFrame.size = imageView.image.size;
      viewFrame.origin = CGPointMake(0, viewHeightCenter - viewFrame.size.height / 2);
    }
    v.frame = viewFrame;
    viewsWidth += viewFrame.size.width + kTabButtonsElemetsPadding;
  }
  
  if (viewsWidth)
  {
    CGFloat firstViewX = (view.frame.size.width - viewsWidth + kTabButtonsElemetsPadding) / 2 + kTabButtonsSeparatorWidth;
    
    UIView *firstView = (UIView *) views[0];
    
    CGRect frame = firstView.frame;
    frame.origin = CGPointMake(firstViewX, frame.origin.y);
    firstView.frame = frame;
  }
  
  for (int i = 1; i < views.count; i++)
  {
    UIView *curView = (UIView *)views[i];
    UIView *prevView = (UIView *)views[i - 1];
    
    CGRect frameView = curView.frame;
    
    frameView.origin = CGPointMake(CGRectGetMaxX(prevView.frame) + kTabButtonsElemetsPadding, frameView.origin.y);
    
    curView.frame = frameView;
  }
}

-(UIButton *)findFBButtonInView:(UIView *)view
{
  for(UIView *subview in view.subviews)
  {
    if([NSStringFromClass([subview class]) isEqualToString:@"FBLikeButton"])
    {
      return (UIButton *)subview;
    }
    
    return (UIButton *)[self findFBButtonInView:subview];
  }
  
  return nil;
}

-(void)likePage
{
  UIButton *fbButton = [self findFBButtonInView:self.nativeLikeButton];
  
  [fbButton sendActionsForControlEvents:UIControlEventTouchUpInside];
}

@end
