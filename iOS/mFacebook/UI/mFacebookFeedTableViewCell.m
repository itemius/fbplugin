#import "mFacebookFeedTableViewCell.h"
#import "mFacebookSocialButton.h"
#import "mFacebookParameters.h"
#import "mFacebookSocialLabel.h"
#import "mFacebookImageDescription.h"
#import "mFacebookMultipleImageView.h"

#import "NSString+size.h"
#import "functionLibrary.h"
#import <UIImageView+WebCache.h>
#import <auth_Share.h>
#import "UIColor+image.h"

#import <objc/runtime.h>

#define kContentWidth 300.0f
#define kContentMarginTop 10.0f
#define kContentMarginBottom 0.0f
#define kContentMarginLeft 10.0f
#define kContentCornerRadius 3.0f
#define kContentBorderWidth 1.0f
#define kContentBorderColor [[[UIColor blackColor] colorWithAlphaComponent:0.2f] CGColor]
#define kContentBackgroundColor [UIColor whiteColor]

#define kAuthorAvatarMarginTop 9.0f
#define kAuthorAvatarMarginLeft 10.0f
#define kAuthorAvatarWidth 36.0f
#define kAuthorAvatarHeight kAuthorAvatarWidth
#define kAuthorAvatarCornerRadius 2.0f

#define kAuthorNameLabelMarginTop kAuthorAvatarMarginTop
#define kAuthorNameLabelMarginLeft 10.0f
#define kAuthorNameLabelHeight 20.0f
#define kAuthorNameLabelMaxWidth (kContentWidth - kAuthorAvatarMarginLeft - kAuthorAvatarWidth - kAuthorNameLabelMarginLeft - kAuthorNameLabelMarginLeft)
#define kAuthorNameLabelFontSize 14.0f
#define kAuthorNameLabelTextColor [[UIColor blackColor] colorWithAlphaComponent:0.9f]

#define kMessageLabelMarginLeft kAuthorAvatarMarginLeft
#define kMessageLabelMarginRight 10.0f
#define kMessageLabelMarginBottom 0.0f
#define kMessageLabelMarginTop 14.0f
#define kMessageLabelWidth (kContentWidth - kMessageLabelMarginLeft - kMessageLabelMarginRight)
#define kMessageLabelFontSize 15.0f
#define kMessageLabelTextColor [[UIColor blackColor] colorWithAlphaComponent:0.8f]
#define kMessageLabelNumberOfLinesWhenShrank 5
#define kMessageLabelEllipsis @"More..."

#define kMultipleImageViewMarginLeft kAuthorAvatarMarginLeft
#define kMultipleImageViewMarginRight kMultipleImageViewMarginLeft
#define kMultipleImageViewMarginTop 11.0f

#define kCreationTimeLabelMaxWidth kAuthorNameLabelMaxWidth
#define kCreationTimeLabelHeight 20.0f
#define kCreationTimeLabelMarginTop -0.0f
#define kCreationTimeLabelMarginBottom 10.0f
#define kCreationTimeLabelFontSize 12.0f
#define kCreationTimeLabelTextColor [[UIColor blackColor] colorWithAlphaComponent:0.5f]

#define kSocialButtonsPanelMarginTop 0.0f
#define kSocialButtonsPanelMarginBottom 0.0f
#define kSocialButtonsPanelMarginLeft 0.0f
#define kSocialButtonsPanelMarginRight kSocialButtonsPanelMarginLeft
#define kSocialButtonsPanelWidth kContentWidth
#define kSocialButtonsPanelHeight kSocialButtonHeight
#define kSocialButtonsSpaceBetweenButtons 20.0f

#define kSocialLabelsSeparatorMarginLeft kContentBorderWidth
#define kSocialLabelsSeparatorHeight 0.5f
#define kSocialLabelsSeparatorWidth (kContentWidth - 2 * kContentBorderWidth)
#define kSocialLabelsSeparatorColor [[UIColor blackColor] colorWithAlphaComponent:0.15f]

#define kSocialLabelsPanelMarginTop 0.0f
#define kSocialLabelsPanelMarginLeft kMultipleImageViewMarginLeft
#define kSocialLabelsPanelMarginBottom 0.0f
#define kSocialLabelsPanelHeight kSocialLabelHeight
#define kSocialLabelsPanelWidth (kContentWidth - 2 * kSocialLabelsPanelMarginLeft)
#define kSocialLabelsSpaceInBetween 15.0f
#define kSocialLabelsEmptyMargin 10.0f

#define kSocialButtonsPanelMarginTop 0.0f
#define kSocialButtonsPanelMarginBottom 0.0f
#define kSocialButtonsPanelMarginLeft 0.0f
#define kSocialButtonsPanelMarginRight kSocialButtonsPanelMarginLeft
#define kSocialButtonsPanelWidth kContentWidth
#define kSocialButtonsPanelHeight kSocialButtonHeight
//#define kSocialButtonsSpaceBetweenButtons 20.0f

#define kLikeButtonMarginTop 0.0f
#define kLikeButtonHeight kSocialButtonsPanelHeight
#define kLikeButtonFontSize 14.0f
#define kLikeButtonTextColor [[UIColor blackColor] colorWithAlphaComponent:0.9f]

#define kCommentButtonMarginTop 0.0f
#define kCommentButtonMarginRight 10.0f
#define kCommentButtonHeight kSocialButtonsPanelHeight
#define kCommentButtonFontSize kLikeButtonFontSize
#define kCommentButtonTextColor kLikeButtonTextColor

const NSString* kVideoDescriptionAssociationKey = @"VideoDescription";

@interface mFacebookFeedTableViewCell()
{
  /**
   * Flag for proper reusing a cell.
   * Do not let any stale pictures appear on a rolling out cell.
   */
  BOOL mustUpdateImages;
}


@property (nonatomic, strong) UIImageView *authorAvatarImageView;
@property (nonatomic, strong) UILabel *authorNameLabel;
@property (nonatomic, strong) UILabel *creationTimeLabel;
@property (nonatomic, strong) TTTAttributedLabel *messageLabel;
@property (nonatomic, strong) UIView *expandTapRecognizerView;
@property (nonatomic, strong) mFacebookMultipleImageView *multipleImageView;

@property (nonatomic, strong) UIView *socialLabelsPanel;
@property (nonatomic, strong) mFacebookSocialLabel *likesCountLabel;
@property (nonatomic, strong) mFacebookSocialLabel *commentsCountLabel;

@property (nonatomic, strong) UIView *socialLabelsSeparatorView;

@property (nonatomic, strong) UIView *socialButtonsPanel;
@property (nonatomic, strong) mFacebookSocialButton *likeButton;
@property (nonatomic, strong) mFacebookSocialButton *commentButton;
@property (nonatomic, strong) mFacebookSocialButton *shareButton;

@end

@implementation mFacebookFeedTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self)
  {
    [self setupInterface];
  }
  return self;
}

-(void)dealloc
{
  self.authorAvatarImageView = nil;
  self.authorNameLabel = nil;
  self.messageLabel = nil;
  self.expandTapRecognizerView = nil;
  self.multipleImageView = nil;
  self.creationTimeLabel = nil;
  
  self.socialLabelsPanel = nil;
  self.likesCountLabel = nil;
  self.commentsCountLabel = nil;
  
  self.socialLabelsSeparatorView = nil;
  
  self.socialButtonsPanel = nil;
  self.likeButton = nil;
  self.commentButton = nil;
  self.shareButton = nil;
  
  [super dealloc];
}

-(void)setupInterface
{
  self.clipsToBounds = YES;
  
  [self setupContentView];
  
  [self placeAuthorAvatarImageView];
  [self placeAuthorNameLabel];
  [self placeCreationTimeLabel];
  [self placeMessageLabel];
  [self placeItemImageView];
  
  [self placeSocialLabelsPanel];
  [self placeSocialLabelsSeparatorView];
  [self placeSocialButtonsPanel];
  
  [self makeScrollingEfficient];
}

-(void)setupContentView
{
  self.selectionStyle = UITableViewCellSelectionStyleNone;
  
  self.contentView.layer.cornerRadius = kContentCornerRadius;
  self.contentView.layer.borderColor = kContentBorderColor;
  self.contentView.layer.borderWidth = kContentBorderWidth;
  
  self.contentView.autoresizesSubviews = YES;
  
  self.contentView.clipsToBounds = YES;
  
  self.backgroundColor = [mFacebookParameters sharedParameters].design.color1;
  self.contentView.backgroundColor = kContentBackgroundColor;
}

-(void)placeAuthorAvatarImageView
{
  self.authorAvatarImageView = [[[UIImageView alloc] init] autorelease];
  self.authorAvatarImageView.layer.cornerRadius = kAuthorAvatarCornerRadius;
  self.authorAvatarImageView.clipsToBounds = YES;

  self.authorAvatarImageView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin |
                                                UIViewAutoresizingFlexibleBottomMargin|
  UIViewAutoresizingFlexibleLeftMargin|
  UIViewAutoresizingFlexibleTopMargin;
  
  [self.contentView addSubview:self.authorAvatarImageView];
}

-(void)placeAuthorNameLabel
{
  self.authorNameLabel = [[[UILabel alloc] init] autorelease];
  self.authorNameLabel.font = [UIFont boldSystemFontOfSize:kAuthorNameLabelFontSize];
  self.authorNameLabel.textColor = kAuthorNameLabelTextColor;
  self.authorNameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
  self.authorNameLabel.numberOfLines = 1;
  
  self.authorNameLabel.backgroundColor = kContentBackgroundColor;
  
  [self.contentView addSubview:self.authorNameLabel];
}

-(void)placeMessageLabel
{
  self.messageLabel = [[[TTTAttributedLabel alloc] init] autorelease];
  self.messageLabel.font = [UIFont systemFontOfSize:kMessageLabelFontSize];
  self.messageLabel.textColor = kMessageLabelTextColor;
  
  self.messageLabel.dataDetectorTypes = UIDataDetectorTypeLink;
  
  self.messageLabel.backgroundColor = kContentBackgroundColor;

  [self.contentView addSubview:self.messageLabel];
  
  [self placeExpandTapRecognizerView];
}

-(void)placeExpandTapRecognizerView
{
  UITapGestureRecognizer *expandRecognizer = [[UITapGestureRecognizer alloc]
                                              initWithTarget:self
                                              action:@selector(expandMessage)];
  
  self.expandTapRecognizerView = [[[UIView alloc] initWithFrame:self.messageLabel.frame] autorelease];
  self.expandTapRecognizerView.backgroundColor = self.messageLabel.backgroundColor;
  
  [self.expandTapRecognizerView addGestureRecognizer:expandRecognizer];
  
  [self.contentView insertSubview:self.expandTapRecognizerView belowSubview:self.messageLabel];
  
  [expandRecognizer release];
}


-(void)placeItemImageView
{
  self.multipleImageView = [[[mFacebookMultipleImageView alloc] init] autorelease];

  for (UIImageView *imageView in self.multipleImageView.imageViewArray)
  {
    UITapGestureRecognizer *itemImageViewTapRecognizer = [self makeGestureRecognizerForImageView];
    [imageView addGestureRecognizer:itemImageViewTapRecognizer];
  }
  
  [self.contentView addSubview:self.multipleImageView];
  
  if(!self.feedItem.attachedImages.count)
  {
    self.multipleImageView.hidden = YES;
  }
}

-(UITapGestureRecognizer *)makeGestureRecognizerForImageView
{
  UITapGestureRecognizer *itemImageViewTapRecognizer = [[[UITapGestureRecognizer alloc] init] autorelease];
  [itemImageViewTapRecognizer addTarget:self action:@selector(itemImageViewTapped:)];
  [itemImageViewTapRecognizer setNumberOfTapsRequired:1];
  
  return itemImageViewTapRecognizer;
}

-(void)placeCreationTimeLabel
{
  self.creationTimeLabel = [[[UILabel alloc] init] autorelease];
  self.creationTimeLabel.font = [UIFont systemFontOfSize:kCreationTimeLabelFontSize];
  self.creationTimeLabel.textColor = kCreationTimeLabelTextColor;
  self.creationTimeLabel.lineBreakMode = NSLineBreakByTruncatingTail;
  self.creationTimeLabel.numberOfLines = 1;
  
  self.creationTimeLabel.backgroundColor = kContentBackgroundColor;
  
  [self.contentView insertSubview:self.creationTimeLabel belowSubview:self.authorNameLabel];
}

-(void)placeSocialLabelsPanel
{
  self.socialLabelsPanel = [[[UIView alloc] init] autorelease];
  self.socialLabelsPanel.backgroundColor = kContentBackgroundColor;
  
  [self placeLikesCountLabel];
  [self placeCommentsCountLabel];
  
  [self.contentView addSubview:self.socialLabelsPanel];
}

-(void)placeLikesCountLabel
{
  self.likesCountLabel = [[[mFacebookSocialLabel alloc] initWithType:mFacebookSocialLabelTypeLikesCount] autorelease];
  self.likesCountLabel.backgroundColor = kContentBackgroundColor;
  
  [self.socialLabelsPanel addSubview:self.likesCountLabel];
}

-(void)placeCommentsCountLabel
{
  self.commentsCountLabel = [[[mFacebookSocialLabel alloc] initWithType:mFacebookSocialLabelTypeCommentsCount] autorelease];
  self.commentsCountLabel.backgroundColor = kContentBackgroundColor;
  
  [self.socialLabelsPanel addSubview:self.commentsCountLabel];
}

-(void)placeSocialLabelsSeparatorView
{
  self.socialLabelsSeparatorView = [[[UIView alloc] init] autorelease];
  self.socialLabelsSeparatorView.backgroundColor = kSocialLabelsSeparatorColor;
  
  [self.contentView addSubview:self.socialLabelsSeparatorView];
}

-(void)placeSocialButtonsPanel
{
  self.socialButtonsPanel = [[[UIView alloc] init] autorelease];
  self.socialButtonsPanel.backgroundColor = [UIColor clearColor];
  
  [self placeLikeButton];
  [self placeCommentButton];
  [self placeShareButton];
  
  [self.contentView addSubview:self.socialButtonsPanel];
}

-(void)placeLikeButton
{
  self.likeButton = [[[mFacebookSocialButton alloc] initWithType:mFacebookSocialButtonTypeLike] autorelease];
  self.likeButton.backgroundColor = kContentBackgroundColor;
  
  [self.likeButton addTarget:self
                      action:@selector(like:)
            forControlEvents:UIControlEventTouchUpInside];
  
  [self.socialButtonsPanel addSubview:self.likeButton];
}

-(void)placeCommentButton
{
  self.commentButton = [[[mFacebookSocialButton alloc] initWithType:mFacebookSocialButtonTypeComment] autorelease];
  self.commentButton.backgroundColor = kContentBackgroundColor;
  
  [self.commentButton addTarget:self
                         action:@selector(comment:)
               forControlEvents:UIControlEventTouchUpInside];

  [self.socialButtonsPanel addSubview:self.commentButton];
}

-(void)placeShareButton
{
  self.shareButton = [[[mFacebookSocialButton alloc] initWithType:mFacebookSocialButtonTypeShare] autorelease];
  self.shareButton.backgroundColor = kContentBackgroundColor;
  
  [self.shareButton addTarget:self
                       action:@selector(share:)
             forControlEvents:UIControlEventTouchUpInside];
  
  [self.socialButtonsPanel addSubview:self.shareButton];
}

-(void)makeScrollingEfficient
{
  self.opaque = YES;
  self.layer.shouldRasterize = YES;
  self.layer.rasterizationScale = [[UIScreen mainScreen] scale];
}

-(void)updateInterface
{
  self.authorNameLabel.text = self.feedItem.authorName;
  
  [self updateAuthorAvatarImageView];
  
  [self updateMessageLabel];
  [self updateMultipleImageView];
  
  [self updateCreationTime];
  
  [self updateLikesCountLabel];
  [self updateCommentsCountLabel];
  
  [self updateLikeButton];
  
  mustUpdateImages = NO;
}

-(void)updateMessageLabel
{
  self.messageLabel.text = self.feedItem.message;
  
  [self setNeedsLayout];
}

+(NSUInteger)numberOfLinesForMessage:(NSString *)message expanded:(BOOL)expanded
{
  NSInteger maxNumberOfLines = [[self class] maxNumberOfLinessForMessage:message];
  
  NSUInteger numberOfLines = maxNumberOfLines;
  
  if(!expanded)
  {
    numberOfLines = maxNumberOfLines >
                    kMessageLabelNumberOfLinesWhenShrank ?
                    kMessageLabelNumberOfLinesWhenShrank : maxNumberOfLines;
  }
  
  return numberOfLines;
}

+(NSUInteger)maxNumberOfLinessForMessage:(NSString *)message
{
  CGSize messageSize = [[self class] sizeForMessageLabelWithText:message];
  
  NSUInteger maxNumberOfLines = messageSize.height / [UIFont systemFontOfSize:kMessageLabelFontSize].lineHeight;
  
  return maxNumberOfLines;
}

-(void)updateAuthorAvatarImageView
{
  UIImage *placeholderImage = self.authorAvatarImageView.image;
  
  if(mustUpdateImages || !placeholderImage)
  {
    placeholderImage = [UIImage imageNamed:resourceFromBundle(@"mFacebook_small_ava")];
  }
  
  if(mustUpdateImages)
  {
    [self.authorAvatarImageView setImageWithURL:self.feedItem.authorAvatarURL
                               placeholderImage:placeholderImage];
  }
}

-(void)updateMultipleImageView
{
  if(self.feedItem.attachedImages.count)
  {
    self.multipleImageView.hidden = NO;
    
    self.multipleImageView.imagesCount = self.feedItem.attachedImages.count;
    
    for (int i = 0; i < self.multipleImageView.imagesCount; i++)
    {
      UIImageView *imageView = (UIImageView *)self.multipleImageView.imageViewArray[i];
      
      [[imageView viewWithTag:kBlackShadeViewTag] removeFromSuperview];
      
      mFacebookImageDescription *imageDescription = self.feedItem.attachedImages[i];

      for(UIGestureRecognizer *recognizer in imageView.gestureRecognizers)
      {
        [imageView removeGestureRecognizer:recognizer];
      }
      
      if(imageDescription.targetVideoFacebookId)
      {
        
        [self addPlayVideoCapabilityToImageView:imageView
                                videoDscription:imageDescription.targetVideoDescription];
        
      } else if([self.feedItem.type isEqualToString:@"video"])
      {
        mFacebookVideoDescription *surrogateVideoDescription = [[[mFacebookVideoDescription alloc] init] autorelease];
        surrogateVideoDescription.sourceVideoURLString = self.feedItem.source;
        surrogateVideoDescription.facebookVideoType = mFacebookVideoTypeShared;
        
        UIImageView *imageView = (UIImageView *)self.multipleImageView.imageViewArray[0];
        
        [self addPlayVideoCapabilityToImageView:imageView
                                videoDscription:surrogateVideoDescription];
      } else {
        [imageView addGestureRecognizer:[self makeGestureRecognizerForImageView]];
      }
      
      NSURL *url = imageDescription.url;
      
      UIImage *placeholder = imageView.image;

      if(mustUpdateImages || !placeholder)
      {
        placeholder = [[UIColor lightGrayColor] asImageWithSize:imageView.frame.size];
      }
      
      [imageView setImageWithURL:url
                placeholderImage:placeholder];
      
    }
    
  } else {
    self.multipleImageView.hidden = YES;
  }
  
  [self setNeedsLayout];
}

-(void)addPlayVideoCapabilityToImageView:(UIImageView *)imageView
                         videoDscription:(mFacebookVideoDescription *)videoDescription
{
  UIView *blackShadeView = [[UIView alloc] initWithFrame:CGRectZero];
  blackShadeView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4f];
  blackShadeView.tag = kBlackShadeViewTag;
  [imageView addSubview:blackShadeView];
  
  UIImage *playIconImage = [UIImage imageNamed:resourceFromBundle(@"mFacebook_video_play")];
  
  UIImageView *playIconImageView = [[UIImageView alloc] initWithImage:playIconImage];
  playIconImageView.tag = kPlayIconImageViewTag;
  [blackShadeView addSubview:playIconImageView];
  
  [playIconImageView release];
  [blackShadeView release];
  
  UITapGestureRecognizer *showVideoTapRecognizer = [[UITapGestureRecognizer alloc]
                                                    initWithTarget:self
                                                    action:@selector(showVideo:)];
  
  [imageView addGestureRecognizer:showVideoTapRecognizer];
  [showVideoTapRecognizer release];
  
  objc_setAssociatedObject(imageView, kVideoDescriptionAssociationKey, videoDescription, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)updateLikeButton
{
  self.likeButton.enabled = !self.feedItem.isLiked;
}

-(void)updateCreationTime
{
  NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
  
  [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
  [dateFormatter setTimeStyle:NSDateFormatterShortStyle];

  self.creationTimeLabel.text = [dateFormatter stringFromDate:self.feedItem.creationTime];
}

-(void)updateLikesCountLabel
{
  if (self.feedItem.likesCount)
  {
    self.likesCountLabel.hidden = NO;
    self.likesCountLabel.socialActionsCount = self.feedItem.likesCount;
    [self setNeedsLayout];
  }
  else
    self.likesCountLabel.hidden = YES;
}

-(void)updateCommentsCountLabel
{
  if (self.feedItem.commentsCount)
  {
    self.commentsCountLabel.hidden = NO;
    self.commentsCountLabel.socialActionsCount = self.feedItem.commentsCount;
    [self setNeedsLayout];
  }
  else
    self.commentsCountLabel.hidden = YES;
}

-(void)setFeedItem:(mFacebookFeedItem *)feedItem
{
  mustUpdateImages = ![feedItem.facebookObjectId isEqualToString:_feedItem.facebookObjectId];
  
  [feedItem retain];
  [_feedItem release];
  
  _feedItem = feedItem;
  
  [self updateInterface];
}

+(CGSize)sizeForLikeButtonTitle:(NSString *)title
{
  UIFont *likeButtonTitleFont = [UIFont systemFontOfSize:kLikeButtonFontSize];
  
  CGSize likeButtonTitleMaxSize = (CGSize)
  {
    120.0f,
    kLikeButtonHeight
  };
  
  return [title sizeForFont:likeButtonTitleFont
                  limitSize:likeButtonTitleMaxSize
            nslineBreakMode:NSLineBreakByTruncatingTail];
}

-(void)layoutSubviews
{
  self.contentView.frame = [self boundsForCell];
  
  [self layoutAuthorAvatarImageView];
  [self layoutAuthorNameLabel];
  [self layoutCreationTimeLabel];
  [self layoutMessageLabel];
  [self layoutMultipleImageView];
  
  [self layoutSocialLabelsPanel];
  [self layoutSocialLabelsSeparator];
  [self layoutSocialButtonsPanel];
}

-(CGRect)boundsForCell
{
  CGRect cellBounds = (CGRect)
  {
    kContentMarginLeft,
    kContentMarginTop,
    kContentWidth,
    [[self class] heightForFeedItem:self.feedItem] - kContentMarginBottom - kContentMarginTop
  };
  
  return cellBounds;
}

-(void)layoutAuthorAvatarImageView
{
  CGRect authorAvatarImageViewFrame = (CGRect)
  {
    kAuthorAvatarMarginLeft,
    kAuthorAvatarMarginTop,
    kAuthorAvatarWidth,
    kAuthorAvatarHeight
  };
  
  self.authorAvatarImageView.frame = authorAvatarImageViewFrame;
}

-(void)layoutAuthorNameLabel
{
  CGRect authorNameLabelFrame = (CGRect)
  {
    CGRectGetMaxX(self.authorAvatarImageView.frame) + kAuthorNameLabelMarginLeft,
    kAuthorNameLabelMarginTop,
    kAuthorNameLabelMaxWidth,
    kAuthorNameLabelHeight
  };
  
  self.authorNameLabel.frame = authorNameLabelFrame;
}

-(void)layoutCreationTimeLabel
{
  CGRect creationTimeLabelFrame = (CGRect)
  {
    self.authorNameLabel.frame.origin.x,
    CGRectGetMaxY(self.authorNameLabel.frame),
    kCreationTimeLabelMaxWidth,
    kCreationTimeLabelHeight
  };
  
  self.creationTimeLabel.frame = creationTimeLabelFrame;
}

-(void)layoutMessageLabel
{
  CGFloat messageLabelHeight;
  
  if(self.feedItem.isMessageExpanded)
  {
    messageLabelHeight = [[self class] sizeForMessageLabelWithText:self.feedItem.message].height;
  }
    else
  {
    NSInteger numberOfLines = [[self class] numberOfLinesForMessage:self.feedItem.message
                                                         expanded:self.feedItem.isMessageExpanded];
  
    messageLabelHeight = self.messageLabel.font.lineHeight * numberOfLines;
  }
  
  CGRect messageLabelFrame = (CGRect)
  {
    kMessageLabelMarginLeft,
    CGRectGetMaxY(self.authorAvatarImageView.frame) + kMessageLabelMarginTop,
    kMessageLabelWidth,
    ceilf(messageLabelHeight)
  };
  
  self.messageLabel.frame = messageLabelFrame;
  
  self.messageLabel.numberOfLines = 0;
  self.messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
  
  self.messageLabel.text = self.feedItem.message;
  
  [self layoutExpandTapRecognizerView];
}

-(void)layoutExpandTapRecognizerView
{
  self.expandTapRecognizerView.frame = self.messageLabel.frame;
  
  NSInteger maxNumberOfLines = [[self class] maxNumberOfLinessForMessage:self.messageLabel.text];
  
  if(maxNumberOfLines > kMessageLabelNumberOfLinesWhenShrank)
  {
    self.expandTapRecognizerView.hidden = NO;
  } else {
    self.expandTapRecognizerView.hidden = YES;
  }
  
  if(self.feedItem.isMessageExpanded)
  {
    self.expandTapRecognizerView.hidden = YES;
  }
}

-(void)layoutMultipleImageView
{
  CGFloat width = kContentWidth - kMultipleImageViewMarginLeft - kMultipleImageViewMarginRight;
  
  CGRect multipleImageViewFrame = (CGRect)
  {
    kMultipleImageViewMarginLeft,
    CGRectGetMaxY(self.messageLabel.frame) + kMultipleImageViewMarginTop,
    width,
    [mFacebookMultipleImageView heightForNImages:self.feedItem.attachedImages.count andViewWidth:width]
  };
  
  self.multipleImageView.frame = multipleImageViewFrame;
}

-(void)layoutSocialLabelsPanel
{
  CGFloat originY = kSocialLabelsPanelMarginTop;

  if(!self.multipleImageView.hidden){
    originY = CGRectGetMaxY(self.multipleImageView.frame) + kSocialButtonsPanelMarginTop;
  } else {
    originY += CGRectGetMaxY(self.messageLabel.frame) + kSocialButtonsPanelMarginTop;
  }
  
  CGRect socialLabelsPanelFrame = (CGRect)
  {
    kSocialLabelsPanelMarginLeft,
    originY,
    kSocialLabelsPanelWidth,
    kSocialLabelsPanelHeight
  };
  
  if (!self.feedItem.commentsCount && !self.feedItem.likesCount) {
    socialLabelsPanelFrame.size.height = kSocialLabelsEmptyMargin;
  }
  
  self.socialLabelsPanel.frame = socialLabelsPanelFrame;
  
  [self layoutLikesCountLabel];
  [self layoutCommentsCountLabel];
}

-(void)layoutLikesCountLabel
{
  self.likesCountLabel.frame = [self.likesCountLabel optimalBounds];
}

-(void)layoutCommentsCountLabel
{
  CGRect commentsCountLabelFrame = [self.commentsCountLabel optimalBounds];
  
  if (self.feedItem.likesCount)
    commentsCountLabelFrame.origin.x = CGRectGetMaxX(self.likesCountLabel.frame) + kSocialLabelsSpaceInBetween;
  else
    commentsCountLabelFrame.origin.x = 0;
  
  self.commentsCountLabel.frame = commentsCountLabelFrame;
}

-(void)layoutSocialLabelsSeparator
{
  CGRect separatorFrame = (CGRect)
  {
    kSocialLabelsSeparatorMarginLeft,
    CGRectGetMaxY(self.socialLabelsPanel.frame),
    kSocialLabelsSeparatorWidth,
    kSocialLabelsSeparatorHeight
  };
  
  self.socialLabelsSeparatorView.frame = separatorFrame;
}

-(void)layoutSocialButtonsPanel
{
  CGFloat originY = kSocialButtonsPanelMarginTop +
                    CGRectGetMaxY(self.socialLabelsSeparatorView.frame);
  
  CGRect socialButtonsPanelFrame = (CGRect)
  {
    kSocialButtonsPanelMarginLeft,
    originY,
    kSocialButtonsPanelWidth,
    kSocialButtonsPanelHeight
  };
  
  self.socialButtonsPanel.frame = socialButtonsPanelFrame;
  
  [self layoutSocialButtons];
}

-(void)layoutSocialButtons
{
  CGRect likeButtonFrame = [self.likeButton optimalBounds];
  CGRect commentButtonFrame = [self.commentButton optimalBounds];
  CGRect shareButtonFrame = [self.shareButton optimalBounds];
  
  self.likeButton.frame = likeButtonFrame;
  self.commentButton.frame = commentButtonFrame;
  self.shareButton.frame = shareButtonFrame;
  
  CGFloat buttonsPanelWidth = self.socialButtonsPanel.frame.size.width;
  
  CGFloat commentButtonCenterX = floorf(buttonsPanelWidth / 2);
  
  CGPoint commentButtonCenter = self.commentButton.center;
  commentButtonCenter.x = commentButtonCenterX;
  self.commentButton.center = commentButtonCenter;
  
  
  CGFloat likeButtonCenterX = floorf(buttonsPanelWidth / 6);//floorf(CGRectGetMinX(self.commentButton.frame) / 2);
  
  CGPoint likeButtonCenter = self.likeButton.center;
  likeButtonCenter.x = likeButtonCenterX;
  self.likeButton.center = likeButtonCenter;
  
  
  CGFloat shareButtonCenterX = floorf(5 * buttonsPanelWidth / 6);//floorf(buttonsPanelWidth - (buttonsPanelWidth - CGRectGetMaxX(self.commentButton.frame)) / 2);
  
  CGPoint shareButtonCenter = self.shareButton.center;
  shareButtonCenter.x = shareButtonCenterX;
  self.shareButton.center = shareButtonCenter;
}

+(CGSize)sizeForMessageLabelWithText:(NSString *)message
{
  UIFont *messageFont = [UIFont systemFontOfSize:kMessageLabelFontSize];
  
  CGSize messageLabelMaxSize = (CGSize)
  {
    kMessageLabelWidth,
    CGFLOAT_MAX
  };
  
  return [message sizeForFont:messageFont
                    limitSize:messageLabelMaxSize
              nslineBreakMode:NSLineBreakByTruncatingTail];
}

+(CGFloat)heightForFeedItem:(mFacebookFeedItem *)item
{
  CGFloat height = kContentMarginTop;
  
  height += kAuthorAvatarMarginTop + kAuthorAvatarHeight;
  
  height += kMessageLabelMarginTop;
  
  if(item.isMessageExpanded)
  {
    height += [[self class] sizeForMessageLabelWithText:item.message].height;
  } else {
    height += [UIFont systemFontOfSize:kMessageLabelFontSize].lineHeight *
              [[self class] numberOfLinesForMessage:item.message
                                           expanded:item.isMessageExpanded];
  }
  
  if(item.attachedImages.count)
  {
    CGFloat width = kContentWidth - kMultipleImageViewMarginLeft - kMultipleImageViewMarginRight;
    height += kMultipleImageViewMarginTop + [mFacebookMultipleImageView heightForNImages:item.attachedImages.count andViewWidth:width];
  }
  
  if (item.likesCount || item.commentsCount)
    height += kSocialLabelsPanelMarginTop + kSocialLabelsPanelHeight + kSocialLabelsPanelMarginBottom;
  else
    height += kSocialLabelsEmptyMargin;
  
  height += kSocialLabelsSeparatorHeight;
  
  height += kSocialButtonsPanelMarginTop + kSocialButtonsPanelHeight + kSocialButtonsPanelMarginBottom;
  
  return height;
}

-(void)like:(mFacebookSocialButton *)button
{
  if([self.delegate respondsToSelector:@selector(likeButtonPressedOnCell:)])
  {
    [self.delegate likeButtonPressedOnCell:self];
  }
}

-(void)comment:(mFacebookSocialButton *)button
{
  if([self.delegate respondsToSelector:@selector(commentButtonPressedOnCell:)])
  {
    [self.delegate commentButtonPressedOnCell:self];
  }
}

-(void)share:(mFacebookSocialButton *)button
{
  if([self.delegate respondsToSelector:@selector(shareButtonPressedOnCell:)])
  {
    [self.delegate shareButtonPressedOnCell:self];
  }
}

-(void)itemImageViewTapped:(UITapGestureRecognizer *)sender
{
  [self.delegate picturePressedOnCell:self imageIndex:sender.view.tag];
}

-(void)setMessageLabelDelegate:(id<TTTAttributedLabelDelegate>)messageLabelDelegate
{
  self.messageLabel.delegate = messageLabelDelegate;
}

-(id<TTTAttributedLabelDelegate>)messageLabelDelegate
{
  return self.messageLabel.delegate;
}

-(void)expandMessage
{
  if([self.delegate respondsToSelector:@selector(messageExpandedOnCell:)])
  {
    [self.delegate messageExpandedOnCell:self];
  }
}

-(void)showVideo:(UITapGestureRecognizer *)sender
{
  if([self.delegate respondsToSelector:@selector(videoWithDescription:pressedOnCell:)])
  {
    UIView *thumbnailImageView = sender.view;
    
    mFacebookVideoDescription *videoDecription = objc_getAssociatedObject(thumbnailImageView, kVideoDescriptionAssociationKey);
    [self.delegate videoWithDescription:videoDecription pressedOnCell:self];
  }
}

@end
