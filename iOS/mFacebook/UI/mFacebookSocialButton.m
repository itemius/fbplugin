#import "mFacebookSocialButton.h"
#import "NSString+size.h"

#define kSpaceBetweenImageAndTitle 5.0f
#define kMaxComponentSize (CGSize){90, kSocialButtonHeight}

#define kTitleLabelFontSize 12.0f
#define kTitleLabelNormalTextColor [[UIColor blackColor] colorWithAlphaComponent:0.7f]
#define kTitleLabelDisabledTextColor [[UIColor blackColor] colorWithAlphaComponent:0.7f]

#define kDisabledStateAlpha 0.3f
#define kEnabledStateAlpha 1.0f

@interface mFacebookSocialButton()
{
  UIImage *socialImage;
  NSString *title;
}

@end

@implementation mFacebookSocialButton

-(instancetype)initWithType:(mFacebookSocialButtonType)type
{
  self = [super initWithFrame:(CGRectZero)];
  
  if(self)
  {
    [self loadAppropriateImageForType:type];
    [self loadAppropriateTitleForType:type];
    [self setupButton];
  }
  
  return self;
}

-(void)dealloc
{
  [socialImage release];
  socialImage = nil;
  
  [super dealloc];
}

-(void)setupButton
{
  [self setupTitleLabel];
  [self setupImageView];
}

-(void)loadAppropriateImageForType:(mFacebookSocialButtonType)type
{
  switch (type) {
    case mFacebookSocialButtonTypeLike:
      socialImage = [UIImage imageNamed:resourceFromBundle(@"mFacebook_like_post")];
      break;
      
    case mFacebookSocialButtonTypeComment:
      socialImage = [UIImage imageNamed:resourceFromBundle(@"mFacebook_comment")];
      break;
    
    case mFacebookSocialButtonTypeShare:
      socialImage = [UIImage imageNamed:resourceFromBundle(@"mFacebook_share")];
      break;
      
    default:
      break;
  }
  
  [socialImage retain];
}

-(void)setupImageView
{
  self.adjustsImageWhenDisabled = YES;
  self.imageView.contentMode = UIViewContentModeCenter;
  self.imageView.image = socialImage;
}

-(void)setupTitleLabel
{
  [self setTitleColor:kTitleLabelNormalTextColor forState:UIControlStateNormal];
  [self setTitleColor:kTitleLabelDisabledTextColor forState:UIControlStateDisabled];
  
  self.titleLabel.font = [UIFont boldSystemFontOfSize:kTitleLabelFontSize];
                          
  [self updateTitleLabel];
}

-(void)loadAppropriateTitleForType:(mFacebookSocialButtonType)type
{
  NSString *localizedTitleKey = nil;
  
  switch (type) {
    case mFacebookSocialButtonTypeLike:
      localizedTitleKey = @"mFacebook_do_like";
      break;
      
    case mFacebookSocialButtonTypeComment:
      localizedTitleKey = @"mFacebook_do_comment";
      break;
      
    case mFacebookSocialButtonTypeShare:
      localizedTitleKey = @"mFacebook_do_share";
      break;
      
    default:
      break;
  }
  
  if(localizedTitleKey)
  {
    title = NSBundleLocalizedString(localizedTitleKey, nil);
  } else {
    title = @"";
  }
}

-(void)updateTitleLabel
{
  [self setTitle:title forState:UIControlStateNormal];
  [self setTitle:title forState:UIControlStateDisabled];
  
  if(SYSTEM_VERSION_LESS_THAN(@"8.0"))
  {
    //for some reason on ios 7 setTitle:forState: did not work
    self.titleLabel.text = title;
  }
}

-(void)layoutSubviews
{
  [self layoutImageView];
  [self layoutTitleLabel];
  
  self.bounds = [self optimalBounds];
}

-(void)layoutImageView
{
  self.imageView.frame = [self boundsForImageView];
}

-(void)setEnabled:(BOOL)enabled
{
  [super setEnabled:enabled];
  
  if(enabled)
  {
    self.alpha = kEnabledStateAlpha;
  } else {
    self.alpha = kDisabledStateAlpha;
  }
}

-(void)layoutTitleLabel
{
  self.titleLabel.frame = [self boundsForTitle];
}

-(CGRect)optimalBounds
{
  CGRect bounds = (CGRect)
  {
    0.0f,
    0.0f,
    CGRectGetMaxX([self boundsForTitle]),
    kSocialButtonHeight
  };
  
  return bounds;
}

-(CGRect)boundsForTitle
{
  CGFloat originX = CGRectGetMaxX([self boundsForImageView]) + kSpaceBetweenImageAndTitle;
  
  CGSize titleSize = [[self titleForState:UIControlStateNormal] sizeForFont:self.titleLabel.font
                                                                  limitSize:kMaxComponentSize];
  
  CGRect titleLabelFrame = (CGRect)
  {
    originX,
    0.0f,
    titleSize.width,
    kSocialButtonHeight
  };
  
  return titleLabelFrame;
}

-(CGRect)boundsForImageView
{
  CGRect imageViewFrame = (CGRect)
  {
    0.0f,
    0.0f,
    socialImage.size.width,
    kSocialButtonHeight
  };
  
  return imageViewFrame;
}

-(CGRect)boundsForButton
{
  CGRect bounds = (CGRect)
  {
    0.0f,
    0.0f,
    CGRectGetMaxX(self.titleLabel.frame),
    kSocialButtonHeight
  };
  
  return bounds;
}

@end
