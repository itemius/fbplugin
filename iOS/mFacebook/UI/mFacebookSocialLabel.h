#import <UIKit/UIKit.h>

#define kSocialLabelHeight 38.0f

/**
 * Enumeration of the possible social label types.
 * Each member defines what text will label have.
 *
 * @see mFacebookSocialButton
 */
typedef NS_ENUM(NSInteger, mFacebookSocialLabelType)
{
  /**
   * Like type. Localized "Likes" text.
   */
  mFacebookSocialLabelTypeLikesCount,
  
  /**
   * Comments type. Localized "Comments" text.
   */
  mFacebookSocialLabelTypeCommentsCount
};

/**
 * Custom label to display number of social actions 
 * (currently, likes or comments) for a feed table view cell.
 *
 * @see mFacebookFeedTableViewCells
 */
@interface mFacebookSocialLabel : UILabel

/**
 * Initializes a new social label with provided type.
 *
 * @param type - social label type.
 * @see mFacebookSocialLabelType
 */
-(instancetype)initWithType:(mFacebookSocialLabelType)type NS_DESIGNATED_INITIALIZER;

/**
 * Count of social actions for label, 
 * ignoring their nature, e.g. likes or comments.
 */
@property (nonatomic) NSUInteger socialActionsCount;

/**
 * Minimal bounds to enclose label, needed because the length of it's text 
 * varies depending on localization and counter value.
 *
 * @return CGRect with proposed button's bounds.
 *
 * @see socialActionsCount
 */
-(CGRect)optimalBounds;

@end
