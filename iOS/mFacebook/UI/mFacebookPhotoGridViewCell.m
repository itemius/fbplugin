#import "mFacebookPhotoGridViewCell.h"

@implementation mFacebookPhotoGridViewCell


-(void)layoutSubviews
{
  CGRect cellBounds = [self bounds];
  [[self selectionBackgroundView] setFrame:cellBounds];
  
  CGRect contentViewFrame = CGRectMake( self.cellPadding.width,
                                       self.cellPadding.height,
                                       CGRectGetWidth(cellBounds)  - self.cellPadding.width  * 2.f,
                                       CGRectGetHeight(cellBounds) - self.cellPadding.height * 2.f);
  
  [[self backgroundView] setFrame: contentViewFrame]; //cellBounds];
  [self sendSubviewToBack:[self selectionBackgroundView]];
  [self sendSubviewToBack:[self backgroundView]];
  
  [[self contentView] setFrame:contentViewFrame];
  
  CGRect contentViewBounds = [[self contentView] bounds];
  
  // Layout content...
  CGRect imageViewFrame = CGRectZero;
  if([[self imageView] image] != nil){
    imageViewFrame.size.width = CGRectGetWidth(contentViewBounds);
    imageViewFrame.size.height = CGRectGetHeight(contentViewBounds);
  }
  
  // center imageView
  imageViewFrame.origin.x = floor(CGRectGetWidth(contentViewBounds)/2. - CGRectGetWidth(imageViewFrame)/2.);
  imageViewFrame.origin.y = floor(CGRectGetHeight(contentViewBounds)/2. - CGRectGetHeight(imageViewFrame)/2.);

  self.imageView.frame = imageViewFrame;
  self.imageView.contentMode = UIViewContentModeScaleAspectFill;
}

@end
