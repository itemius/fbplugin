#import <UIKit/UIKit.h>
#import "mFacebookFeedInfo.h"

/**
 * View with the information about the feed
 */
@interface mFacebookFeedHeaderView : UIView

/**
 * Initializes a new header view with the provided feed info
 * @param feedInfo - the infoemation to display
 */
-(instancetype)initWithFeedInfo:(mFacebookFeedInfo *)feedInfo;

/**
 * Method to like a page, called after the authozrization
 * with facebook has been completed
 */
-(void)likePage;

/**
 * Feed info object which is currently displayed.
 */
@property (nonatomic, strong) mFacebookFeedInfo *feedInfo;

/**
 * Button to like a page.
 */
@property (nonatomic, strong) UIButton *likeTabButton;

/**
 * Button to go to the photo gallery page.
 * Also shows the number of the photos in the feed.
 */
@property (nonatomic, strong) UIButton *photoTabButton;

/**
 * Button to go to the video list page.
 * Also shows the number of the videos in the feed.
 */
@property (nonatomic, strong) UIButton *videoTabButton;

/**
 * Number of photos to set. This value is fetched asynchronously,
 */
@property (nonatomic) NSInteger photoCount;

@end
