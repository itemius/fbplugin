#import <UIKit/UIKit.h>

#define kSocialButtonHeight 40.0f

/**
 * Enumeration of the possible social button types (actions).
 * Each member defines what icon and title will button use.
 *
 * @see mFacebookSocialButton
 */
typedef NS_ENUM(NSInteger, mFacebookSocialButtonType)
{
  /**
   * Like type. "Thumbs up" icon, localized "Like" title.
   */
  mFacebookSocialButtonTypeLike,
  
  /**
   * Comment type. "Speech bubble" icon, localized "Comment" title.
   */
  mFacebookSocialButtonTypeComment,
  
  /**
   * Share type. "Curved arrow" icon, localized "Share" title.
   */
  mFacebookSocialButtonTypeShare
};

/**
 * Custom button which performs some social action (currently, like, comment or sharing)
 *
 * @discussion
 * Initially this button contained number of social actions in its title
 * So there are some methods for relayout when this count changed
 */
@interface mFacebookSocialButton : UIButton

/**
 * Initializes a new social button with provided type.
 * 
 * @param type - social button type.
 * @see mFacebookSocialButtonType
 */
-(instancetype)initWithType:(mFacebookSocialButtonType)type NS_DESIGNATED_INITIALIZER;

/**
 * Minimal bounds to enclose button's content,
 * because the length of the localized title varies.
 *
 * @return CGRect with proposed button's bounds.
 */
-(CGRect)optimalBounds;

@end
