#import "mFacebookMultipleImageView.h"
#import "mFacebookImageDescription.h"

const NSUInteger IMAGE_VIEW_COUNT = 4;
const CGFloat IMAGE_PADDING = 3;

@interface mFacebookMultipleImageView()

@property (nonatomic, strong) UILabel *lastImageViewLabel;

@end

@implementation mFacebookMultipleImageView

-(instancetype) init
{
  self = [super init];
  
  if (self){
    [self setupInterface];
  }
  
  return self;
}

-(void)setupInterface
{
  NSMutableArray *imageViews = [NSMutableArray array];
  
  for (int i = 0; i < IMAGE_VIEW_COUNT; i++)
  {
    UIImageView *imageView = [[[UIImageView alloc] init] autorelease];
    
    imageView.userInteractionEnabled = NO;
    imageView.backgroundColor = [UIColor whiteColor];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    imageView.tag = i;
    
    [imageViews addObject:imageView];
    [self addSubview:imageView];
  }
  
  self.imageViewArray = [[imageViews copy] autorelease];
  
  UIImageView *lastImageView = (UIImageView *)[self.imageViewArray lastObject];
  
  self.lastImageViewLabel = [[[UILabel alloc] init] autorelease];
  self.lastImageViewLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4f];
  self.lastImageViewLabel.textColor = [UIColor whiteColor];
  self.lastImageViewLabel.font = [UIFont systemFontOfSize:35];
  self.lastImageViewLabel.contentMode = UIViewContentModeCenter;
  self.lastImageViewLabel.textAlignment = NSTextAlignmentCenter;
  
  [lastImageView addSubview:self.lastImageViewLabel];
}

-(void)dealloc
{
  self.imageViewArray = nil;
  self.lastImageViewLabel = nil;
  
  [super dealloc];
}


-(void)setImagesCount:(NSUInteger)value
{
  _imagesCount = MIN(IMAGE_VIEW_COUNT, value);
  
  if (value > IMAGE_VIEW_COUNT)
  {
    self.lastImageViewLabel.hidden = NO;
    self.lastImageViewLabel.text = [NSString stringWithFormat:@"+%lu", (unsigned long) (value - IMAGE_VIEW_COUNT)];
  }
  else
    self.lastImageViewLabel.hidden = YES;
  
  [self setNeedsLayout];
}

-(void)hideAllImageViews
{
  for (UIImageView *imageView in self.imageViewArray)
  {
    imageView.userInteractionEnabled = NO;
    imageView.hidden = YES;
  }
}

-(void)layoutSubviews
{
  [self hideAllImageViews];
  
  for (int i = 0; i < self.imagesCount; i++)
  {
    UIImageView *imageView = (UIImageView *)self.imageViewArray[i];
    imageView.userInteractionEnabled = YES;
    imageView.hidden = NO;
  }
  
  UIImageView *imageView1 = (UIImageView *) self.imageViewArray[0];
  UIImageView *imageView2 = (UIImageView *) self.imageViewArray[1];
  UIImageView *imageView3 = (UIImageView *) self.imageViewArray[2];
  UIImageView *imageView4 = (UIImageView *) self.imageViewArray[3];

  CGFloat width = self.frame.size.width;
  CGFloat halfWidth = width / 2;
  
  switch (_imagesCount) {
    case 1:
    {
      imageView1.frame = CGRectMake(0, 0, width, width);
      break;
    }
    case 2:
    {
      imageView1.frame = CGRectMake(0, 0, halfWidth - (IMAGE_PADDING / 2), width);
      imageView2.frame = CGRectMake(halfWidth + (IMAGE_PADDING / 2), 0, halfWidth - (IMAGE_PADDING / 2), width);
      break;
    }
    case 3:
    {
      CGFloat bottomImageWidth = (width - IMAGE_PADDING) / 2;
      
      imageView1.frame = CGRectMake(0, 0, width, halfWidth - (IMAGE_PADDING / 2));
      imageView2.frame = CGRectMake(0, halfWidth + (IMAGE_PADDING / 2), bottomImageWidth, bottomImageWidth);
      imageView3.frame = CGRectMake(bottomImageWidth + IMAGE_PADDING, halfWidth + (IMAGE_PADDING / 2), bottomImageWidth, bottomImageWidth);
      break;
    }
    case 4:
    {
      CGFloat bottomImageWidth = (width - (IMAGE_PADDING * 2)) / 3;
      
      CGFloat topImageHeight = width - bottomImageWidth - IMAGE_PADDING;
      imageView1.frame = CGRectMake(0, 0, width, topImageHeight);
      
      imageView2.frame = CGRectMake(0, topImageHeight + IMAGE_PADDING, bottomImageWidth, bottomImageWidth);
      imageView3.frame = CGRectMake(bottomImageWidth + IMAGE_PADDING, topImageHeight + IMAGE_PADDING, bottomImageWidth, bottomImageWidth);
      imageView4.frame = CGRectMake((bottomImageWidth + IMAGE_PADDING) * 2, topImageHeight + IMAGE_PADDING, bottomImageWidth, bottomImageWidth);
      break;
    }
  }
  
  if(_imagesCount)
  {
    for(UIImageView *imageView in self.imageViewArray)
    {
      UIView *blackShadeView = [imageView viewWithTag:kBlackShadeViewTag];
      
      if(blackShadeView)
      {
        blackShadeView.frame = imageView.bounds;
        
        UIView *playIconImageView = [blackShadeView viewWithTag:kPlayIconImageViewTag];
        playIconImageView.center = blackShadeView.center;
      }
    }
  }
  
  if (!self.lastImageViewLabel.hidden)
    self.lastImageViewLabel.frame = imageView4.bounds;
}

+(CGFloat)heightForNImages:(NSUInteger)n andViewWidth:(CGFloat)width
{  
  return n ? width : 0;
}

@end
