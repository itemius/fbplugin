#import <UIKit/UIKit.h>
#import "mFacebookFeedItem.h"

#define kPlayIconImageViewTag 1234321
#define kBlackShadeViewTag 12343212

@interface mFacebookMultipleImageView : UIView

-(instancetype) init;

@property (nonatomic, assign) NSUInteger imagesCount;
@property (nonatomic, strong) NSArray *imageViewArray;

+(CGFloat)heightForNImages:(NSUInteger)n andViewWidth:(CGFloat)width;

@end
