#import <UIKit/UIKit.h>

@interface mFacebookCommentTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel     *nameLabel;
@property (nonatomic, strong) UILabel     *messageLabel;
@property (nonatomic, strong) UILabel     *addCommentLabel;
@property (nonatomic, strong) UILabel     *dateLabel;
@property (nonatomic, strong) UIImageView *avatarView;
@property (nonatomic, strong) UIView      *separator;

@end
