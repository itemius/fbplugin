#import "mFacebookSocialLabel.h"

#import "NSString+size.h"
#import <Smartling.i18n/SLLocalization.h>

#define kSocialLabelTextColor [[UIColor blackColor] colorWithAlphaComponent:0.5f]
#define kSocialLabelFontSize 12.0f
#define kSocialLabelMaxSize (CGSize){145, kSocialLabelHeight}

@interface mFacebookSocialLabel()
{
  mFacebookSocialLabelType labelType;
}

@end

@implementation mFacebookSocialLabel


-(instancetype)initWithType:(mFacebookSocialLabelType)type
{
  self = [super initWithFrame:CGRectZero];
  
  if(self)
  {
    labelType = type;
    
    [self setupLabel];
  }
  
  return self;
}

-(void)setupLabel
{
  self.backgroundColor = [UIColor blueColor];
  
  self.font = [UIFont systemFontOfSize:kSocialLabelFontSize];
  self.textColor = kSocialLabelTextColor;
  self.lineBreakMode = NSLineBreakByTruncatingTail;
  
  [self update];
}

-(void)update
{
  NSString *text = @"";
  NSString *pluralizedKey = nil;

  if(labelType == mFacebookSocialLabelTypeLikesCount)
  {
    pluralizedKey = @"mFacebook_%@ likes";
    
  } else if(labelType == mFacebookSocialLabelTypeCommentsCount)
  {
    pluralizedKey = @"mFacebook_%@ comments";
  }
  
  if(pluralizedKey)
  {
    NSNumber *count = @(self.socialActionsCount);
    
    if (labelType == mFacebookSocialLabelTypeLikesCount)
    {
      if ([count integerValue] == 1)
        text = NSBundleLocalizedString(@"mFacebook_exactly_one_like", @"1 Like");
      else
        text = [NSString stringWithFormat:SLBundlePluralizedString(pluralizedKey, count, nil), count];
    }
    else
      text = [NSString stringWithFormat:SLBundlePluralizedString(pluralizedKey, count, nil), count];
  }
  
  self.text = text;
  
  [self setNeedsLayout];
}

-(void)setSocialActionsCount:(NSUInteger)socialActionsCount
{
  _socialActionsCount = socialActionsCount;
  [self update];
}

-(void)layoutSubviews
{
  [super layoutSubviews];
  
  self.bounds = [self optimalBounds];
}

-(CGRect)optimalBounds
{
  CGSize labelSize = [self.text sizeForFont:self.font
                                  limitSize:kSocialLabelMaxSize
                            nslineBreakMode:self.lineBreakMode];
  
  CGRect bounds = (CGRect)
  {
    0.0f,
    0.0f,
    labelSize.width,
    kSocialLabelHeight
  };
  
  return bounds;
}

@end
